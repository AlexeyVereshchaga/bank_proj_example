package ru.bankproj.android.mobilebank.main.transfer.legal.person.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.product.CardLimit;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.di.scope.ConfigPersistent;
import ru.bankproj.android.mobilebank.main.transfer.legal.person.ITransferLegalPersonContract;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.rest.RequestParams;
import ru.bankproj.android.mobilebank.rest.response.CardLimitResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferResponse;
import ru.bankproj.android.mobilebank.utils.RequestConstants;
import ru.bankproj.android.mobilebank.utils.Utils;

import static ru.bankproj.android.mobilebank.app.Constants.TAG;

/**
 * Created by maxmobiles on 27.12.2017.
 */
@ConfigPersistent
public class TransferLegalPersonPresenter extends BaseEventBusPresenter<ITransferLegalPersonContract.View>
        implements ITransferLegalPersonContract.Presenter, IBaseResponseCallback {

    private static final String SAVE_SELECTED_PRODUCT = "TransferLegalPersonPresenter.SAVE_SELECTED_PRODUCT";

    private static final String AMOUNT = "TransferLegalPersonPresenter.AMOUNT";
    private static final String TRANSFER_RESPONSE = "TransferLegalPersonPresenter.TRANSFER_RESPONSE";
    private static final String TEMPLATE_MODE = "TransferLegalPersonPresenter.TEMPLATE_MODE";

    private RequestManager mRequestManager;

    private Disposable mDisposable;

    private BaseProduct mSelectedProduct;
    private String amount;
    private TransferResponse transferResponse;
    private boolean templateMode;
    private Double productLimit;

    private PublishSubject<Boolean> subject = PublishSubject.create();
    private String recipientName;
    private String inn;
    private String accountNumberRecipient;
    private String kpp;
    private String bic;
    private String passportDeal;
    private String transferTarget;

    @Inject
    public TransferLegalPersonPresenter(RequestManager requestManager) {
        this.mRequestManager = requestManager;
        mDisposable = Disposables.empty();
    }

    @Override
    public void setListObservableField(List<Observable<Boolean>> observables) {
        observables.add(subject);
        Disposable validationDisposable = Observable.combineLatest(observables, objects -> {
            boolean state = true;
            for (Object object : objects) {
                state &= (Boolean) object;
            }
            return state;
        }).subscribe(s -> getMvpView().fieldValidationSuccess(s));
        addDisposable(validationDisposable);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            getMvpView().initView(templateMode);
            setProductData(mSelectedProduct);
        }
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            mSelectedProduct = (BaseProduct) params[0];
            templateMode = (boolean) params[1];
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(SAVE_SELECTED_PRODUCT, mSelectedProduct);
        outState.putString(AMOUNT, amount);
        outState.putParcelable(TRANSFER_RESPONSE, transferResponse);
        outState.putBoolean(TEMPLATE_MODE, templateMode);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        mSelectedProduct = savedInstanceState.getParcelable(SAVE_SELECTED_PRODUCT);
        amount = savedInstanceState.getString(AMOUNT);
        transferResponse = savedInstanceState.getParcelable(TRANSFER_RESPONSE);
        templateMode = savedInstanceState.getBoolean(TEMPLATE_MODE);
    }

    @Override
    public void setSelectedProduct(BaseProduct baseProduct) {
        setProductData(baseProduct);
    }

    private void setProductData(BaseProduct baseProduct) {
        this.mSelectedProduct = baseProduct;
        subject.onNext(mSelectedProduct != null);
        if (mSelectedProduct != null && mSelectedProduct instanceof Card) {
            getCardLimits(mSelectedProduct);
        }
        getMvpView().initProductData(mSelectedProduct);
    }

    private void getCardLimits(BaseProduct sourceProduct) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                productLimit = null;
                mDisposable = mRequestManager.getCardLimits(sessionId, sourceProduct.getId(),
                        new BaseRequestController(mEventBusController, Action.GET_CARD_LIMITS_ACTION, getMvpView().getClassUniqueDeviceId()));
            }
        });
    }

    @Override
    public void onNextClicked(String amount, String recipientName, String inn, String accountNumberRecipient, String kpp, String bic,
                              String passportDeal, String transferTarget) {
        this.amount = amount;
        this.recipientName = recipientName;
        this.inn = inn;
        this.accountNumberRecipient = accountNumberRecipient;
        this.kpp = kpp;
        this.bic = bic;
        this.passportDeal = passportDeal;
        this.transferTarget = transferTarget;

        int validationValue = validateAmount(amount);
        if (validationValue == -1) {
            checkTransferRequest(amount, recipientName, inn, accountNumberRecipient, kpp, bic, passportDeal, transferTarget);
        } else {
            getMvpView().showNotValidAmountMessage(validationValue);
        }
    }

    private boolean validateFields(String accountNumberRecipient, String bic) {
        boolean accNumberValid = Utils.accountNumberValidation(bic, accountNumberRecipient);
        getMvpView().showAccountNumberError(!accNumberValid);
        return accNumberValid;
    }

    private int validateAmount(String amountStr) {
        Float amount = -1f;
        try {
            amount = Float.valueOf(amountStr);
        } catch (NumberFormatException e) {
            Log.e(TAG, "validateAmount: ", e);
        }
        if (productLimit != null && productLimit < amount) {
            return R.string.payment_restriction_exceeded;
        }
        if (amount <= 0) {
            return R.string.amount_error_message;
        }
        return -1;
    }

    private void checkTransferRequest(String amount, String recipientName, String inn, String accountNumberRecipient, String kpp, String bic,
                                      String passportDeal, String transferTarget) {
        if (validateFields(accountNumberRecipient, bic)) {
            undisposable(mDisposable);
            mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
                if (isUnAuthorize) {
                    getMvpView().unAuthorized();
                } else {
                    Integer sourceType = -1;
                    String sourceId = mSelectedProduct.getId();
                    Integer targetType = RequestConstants.ADDITION_PARAMETERS;
                    Map<String, String> additionParams = RequestParams.getAdditionalParamsPerson(recipientName, inn, accountNumberRecipient,
                            kpp, bic, "1", passportDeal, transferTarget, "jur", null);
                    String targetId = null;
                    if (mSelectedProduct instanceof Card) {
                        sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
                    } else if (mSelectedProduct instanceof Account) {
                        sourceType = RequestConstants.SOURCE_CLIENTS_ACCOUNT;
                    }
                    mDisposable = mRequestManager.transferCheck(
                            sessionId,
                            RequestParams.getConfirmationStrategiesParams(ConfirmRequestDescriptor.TRANSFER, null, amount, Constants.RUR, null, sourceType, targetType, -1),
                            Constants.RUR, amount, sourceType, sourceId, targetType, targetId, null, additionParams, mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                            new BaseRequestController(mEventBusController, Action.CHECK_TRANSFER, getMvpView().getClassUniqueDeviceId()));
                    addDisposable(mDisposable);
                }
            });
        }
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case START_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                    case Action.GET_CARD_LIMITS_ACTION:
                        getMvpView().startProgressDialog();
                        break;
                }
                break;
            case SUCCESS_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                        undisposable(mDisposable);
                        Response<TransferResponse> responseResponse = (Response<TransferResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), responseResponse, this);
                        break;
                    case Action.GET_CARD_LIMITS_ACTION:
                        undisposable(mDisposable);
                        Response<CardLimitResponse> baseResponse = (Response<CardLimitResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                        break;
                }
                break;
            case FAIL_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                        undisposable(mDisposable);
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        break;
                    case Action.GET_CARD_LIMITS_ACTION:
                        undisposable(mDisposable);
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        mSelectedProduct = null;
                        setProductData(mSelectedProduct);
                        break;
                }
                break;
        }
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case Action.CHECK_TRANSFER:
                getMvpView().completeProgressDialog();
                transferResponse = (TransferResponse) data.getValue();
                CheckTransferData checkTransferData = new CheckTransferData();
                checkTransferData.setAmount(amount);
                checkTransferData.setSourceProduct(mSelectedProduct);
                checkTransferData.setTransferDescription("Юридическому лицу или ИП");
                checkTransferData.setTransferResponse(transferResponse);
                checkTransferData.setType(CheckTransferData.LEGAL_TRANSFER);
                Map<String, String> additionParams = RequestParams.getAdditionalParamsPerson(recipientName, inn, accountNumberRecipient, kpp, bic, "1",
                        passportDeal, transferTarget, "jur", null);
                checkTransferData.setAdditionParams(additionParams);
                getMvpView().openTransferCheckScreen(checkTransferData, templateMode);
                break;
            case Action.GET_CARD_LIMITS_ACTION:
                getMvpView().completeProgressDialog();
                CardLimitResponse cardLimitResponse = (CardLimitResponse) data.getValue();
                if (cardLimitResponse != null) {
                    ArrayList<CardLimit> cardLimits = cardLimitResponse.getLimitList();
                    productLimit = null;
                    for (CardLimit cardLimit :
                            cardLimits) {
                        double localLimit = cardLimit.getAmount() - cardLimit.getUsedAmount();
                        if (productLimit == null || productLimit > localLimit) {
                            productLimit = localLimit;
                        }
                        if (productLimit <= 0) {
                            mSelectedProduct = null;
                            setProductData(mSelectedProduct);
                            getMvpView().showCardLimitDialog();
                            return;
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                getMvpView().completeProgressDialog();
                getMvpView().unAuthorized();
                break;
            case BAD_REQUEST_ERROR:
            case DEFAULT_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
            default:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
        }
        switch (actionCode) {
            case Action.GET_CARD_LIMITS_ACTION:
                mSelectedProduct = null;
                setProductData(mSelectedProduct);
                break;
        }
    }
}
