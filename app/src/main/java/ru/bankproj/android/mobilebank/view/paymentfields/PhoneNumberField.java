package ru.bankproj.android.mobilebank.view.paymentfields;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.redmadrobot.inputmask.MaskedTextChangedListener;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.ValidFields;
import ru.bankproj.android.mobilebank.dataclasses.providers.PaymentParameter;
import ru.bankproj.android.mobilebank.view.paymentfields.base.BaseField;

/**
 * Created by rrust on 17.12.2017.
 */

public class PhoneNumberField extends BaseField {

    private LinearLayout mRootView;
    private EditText mEtPhoneNumber;
    private TextView tvDescription;
    private PaymentParameter mPaymentParameter;

    private String mPhoneValue;

    public PhoneNumberField(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        return mRootView;
    }

    @Override
    protected void initView() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mRootView = (LinearLayout) inflater.inflate(R.layout.view_type_phone_field, null, false);
        mEtPhoneNumber = mRootView.findViewById(R.id.et_phone_number);
        tvDescription = mRootView.findViewById(R.id.tv_description_field);
    }

    public String getValue() {
        return mPhoneValue;
    }

    @Override
    public void setPaymentParameter(PaymentParameter paymentParameter) {
        mPaymentParameter = paymentParameter;
        initConfigField();
    }

    @Override
    public String getPaymentParameterId() {
        return mPaymentParameter != null ? mPaymentParameter.getId() : "";
    }

    public void setValue(String phoneValue) {
        mPhoneValue = phoneValue;
        mEtPhoneNumber.setText(phoneValue);
    }

    public Observable<Boolean> getEmptyFieldWatcherObservable(@NonNull final EditText editText) {
        final PublishSubject<Boolean> subject = PublishSubject.create();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                subject.onNext(ValidFields.isNotEmptyField(s.toString()) && isValid(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        return subject;
    }

    @Override
    public Observable<Boolean> getObservable() {
        return getEmptyFieldWatcherObservable(mEtPhoneNumber);
    }

    public boolean isValid(String s) {
        return s.length() <= mPaymentParameter.getMax() && s.length() >= mPaymentParameter.getMin();
    }

    @Override
    public void enableView(boolean isEnable) {
        mEtPhoneNumber.setEnabled(isEnable);
    }

    private void initConfigField() {
        if (mPaymentParameter != null) {
            mRootView.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(mPaymentParameter.getName())) {
                mEtPhoneNumber.setHint(mPaymentParameter.getName());
            }
            if (!TextUtils.isEmpty(mPaymentParameter.getDescription())) {
                tvDescription.setText(mPaymentParameter.getDescription());
                tvDescription.setVisibility(View.VISIBLE);
            } else {
                tvDescription.setVisibility(View.GONE);
            }
            if (mPaymentParameter.getMax() > 0) {
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(mPaymentParameter.getMax());
                mEtPhoneNumber.setFilters(fArray);
                mEtPhoneNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
            }
        } else {
            mRootView.setVisibility(View.GONE);
        }
    }
}