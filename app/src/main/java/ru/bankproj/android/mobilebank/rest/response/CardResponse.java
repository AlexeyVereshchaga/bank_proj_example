package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;

/**
 * Created by j7ars on 29.11.2017.
 */

public class CardResponse extends BaseResponse {

    @SerializedName("cards")
    private ArrayList<Card> cardList;

    public ArrayList<Card> getCardList() {
        return cardList;
    }

    public void setCardList(ArrayList<Card> cardList) {
        this.cardList = cardList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.cardList);
    }

    public CardResponse() {
    }

    protected CardResponse(Parcel in) {
        super(in);
        this.cardList = in.createTypedArrayList(Card.CREATOR);
    }

    public static final Creator<CardResponse> CREATOR = new Creator<CardResponse>() {
        @Override
        public CardResponse createFromParcel(Parcel source) {
            return new CardResponse(source);
        }

        @Override
        public CardResponse[] newArray(int size) {
            return new CardResponse[size];
        }
    };
}
