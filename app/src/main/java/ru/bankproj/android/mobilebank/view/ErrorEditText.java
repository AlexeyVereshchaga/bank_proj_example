package ru.bankproj.android.mobilebank.view;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.utils.FieldConverter;

/**
 * Created by j7ars on 01.11.2017.
 */

public class ErrorEditText extends AppCompatEditText {

    private Drawable mDrawable;
    private int mErrorColor;

    public ErrorEditText(Context context) {
        super(context);
        initView();
    }

    public ErrorEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ErrorEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        clearError();
    }

    private void initView() {
        mErrorColor = FieldConverter.getColor(R.color.tomato);
        mDrawable = getBackground();
    }

    public void setErrorColor(int color) {
        mErrorColor = color;
        mDrawable.setColorFilter(mErrorColor, PorterDuff.Mode.SRC_ATOP);
    }

    public void clearError() {
        mDrawable.clearColorFilter();
    }

    public void setError() {
        mDrawable.setColorFilter(mErrorColor, PorterDuff.Mode.SRC_ATOP);
    }

}
