package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.CardDetail;

/**
 * Created by j7ars on 05.12.2017.
 */

public class CardDetailResponse extends BaseResponse {

    @SerializedName("card")
    private CardDetail cardDetail;

    public CardDetail getCardDetail() {
        return cardDetail;
    }

    public void setCardDetail(CardDetail mCardDetail) {
        this.cardDetail = mCardDetail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(this.cardDetail, flags);
    }

    public CardDetailResponse() {
    }

    protected CardDetailResponse(Parcel in) {
        super(in);
        this.cardDetail = in.readParcelable(CardDetail.class.getClassLoader());
    }

    public static final Creator<CardDetailResponse> CREATOR = new Creator<CardDetailResponse>() {
        @Override
        public CardDetailResponse createFromParcel(Parcel source) {
            return new CardDetailResponse(source);
        }

        @Override
        public CardDetailResponse[] newArray(int size) {
            return new CardDetailResponse[size];
        }
    };
}
