package ru.bankproj.android.mobilebank.rest.response;

import java.util.ArrayList;

import retrofit2.Response;
import ru.bankproj.android.mobilebank.base.response.BaseResponse;

/**
 * Created by j7ars on 21.11.2017.
 */

public class ZipResponse {

    private ArrayList<Response> mResponseList;

    public ZipResponse(Response ... response) {
        this.mResponseList = new ArrayList<>();
        if(response != null) {
            for (int i = 0; i < response.length; i ++ ){
                mResponseList.add(response[i]);
            }
        }
    }

    public Response getResponse(){
        if(mResponseList != null) {
            Response resultResponse = mResponseList.get(mResponseList.size() - 1);
            for (Response response : mResponseList) {
                if (!response.isSuccessful()) {
                    resultResponse = response;
                    break;
                } else {
                    if (response.body() instanceof BaseResponse) {
                        if (((BaseResponse) response.body()).getResult() != 0) {
                            resultResponse = response;
                            break;
                        } else {
                            return resultResponse;
                        }
                    }
                }
            }
            return resultResponse;
        } else{
            return null;
        }
    }

}
