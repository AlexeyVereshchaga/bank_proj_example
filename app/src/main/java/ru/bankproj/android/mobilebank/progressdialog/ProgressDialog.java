package ru.bankproj.android.mobilebank.progressdialog;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.fragment.BaseDialogFragment;
import ru.bankproj.android.mobilebank.dataclasses.ErrorData;
import ru.bankproj.android.mobilebank.rest.error.RetrofitException;

/**
 * Created by j7ars on 25.10.2017.
 */

public class ProgressDialog extends BaseDialogFragment {

    public static final String TAG = "ProgressDialog.TAG";

    @BindView(R.id.rl_dialog_progress)
    RelativeLayout rlProgress;
    @BindView(R.id.pb_dialog_load)
    ProgressBar pbLoad;
    @BindView(R.id.ll_dialog_progress_error)
    LinearLayout llError;
    @BindView(R.id.iv_error_logo)
    ImageView ivErrorLogo;
    @BindView(R.id.tv_dialog_progress_error)
    TextView tvError;
    @BindView(R.id.btn_dialog_progress_settings)
    TextView btnDialogProgressSettings;

    private OnProgressDialogVisibleListener mListener;

    private ErrorData mErrorData;
    private RetrofitException mRetrofitException;

    private OnOkButtonClickCallbackListener mOnOkButtonClickCallbackListener;

    public void setOnProgressDialogVisibleListener(OnProgressDialogVisibleListener onProgressDialogVisibleListener) {
        this.mListener = onProgressDialogVisibleListener;
    }

    public void setOnOkButtonClickCallbackListener(OnOkButtonClickCallbackListener mOnOkButtonClickCallbackListener) {
        this.mOnOkButtonClickCallbackListener = mOnOkButtonClickCallbackListener;
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_progress;
    }

    @OnClick({R.id.btn_dialog_progress_settings, R.id.btn_dialog_progress_close})
    void okClick(View view) {
        switch (view.getId()) {
            case R.id.btn_dialog_progress_settings:
                Intent settingsIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                startActivityForResult(settingsIntent, Constants.START_SETTINGS);
                break;
            case R.id.btn_dialog_progress_close:
                if (mOnOkButtonClickCallbackListener != null) {
                    if (mErrorData != null) {
                        mOnOkButtonClickCallbackListener.onOkButtonClick(mErrorData.getErrorCode());
                    }
                }
                break;
        }
        ProgressDialogManager.getInstance().unsubscribe();

    }

    public void startLoading() {
        setCancelable(false);
    }

    public void completeLoading() {
        setCancelable(true);
        rlProgress.setVisibility(View.GONE);
        llError.setVisibility(View.GONE);
        pbLoad.setVisibility(View.GONE);
    }

    public void errorLoading(OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, ErrorData errorData) {
        setCancelable(true);
        this.mOnOkButtonClickCallbackListener = onOkButtonClickCallbackListener;
        this.mErrorData = errorData;
        btnDialogProgressSettings.setVisibility(View.GONE);
        if(mErrorData != null) {
            tvError.setText(mErrorData.getErrorMessage());
        }
        ivErrorLogo.setImageResource(errorData.getErrorDrawable());
        rlProgress.setVisibility(View.VISIBLE);
        llError.setVisibility(View.VISIBLE);
        pbLoad.setVisibility(View.GONE);
    }

    public void errorLoading(OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, RetrofitException retrofitException) {
        setCancelable(true);
        this.mOnOkButtonClickCallbackListener = onOkButtonClickCallbackListener;
        this.mRetrofitException = retrofitException;
        if (retrofitException.getKind() == RetrofitException.ERROR_NETWORK || retrofitException.getKind() == RetrofitException.ERROR_SOCKET_TIMEOUT) {
            btnDialogProgressSettings.setVisibility(View.VISIBLE);
        } else {
            btnDialogProgressSettings.setVisibility(View.GONE);
        }
        if(retrofitException != null) {
            tvError.setText(retrofitException.getMessage());
        }
        ivErrorLogo.setImageResource(retrofitException.getErrorIcon());
        rlProgress.setVisibility(View.VISIBLE);
        llError.setVisibility(View.VISIBLE);
        pbLoad.setVisibility(View.GONE);
    }

    public interface OnProgressDialogVisibleListener {
        void onProgressDialogVisible();
    }

    public interface OnOkButtonClickCallbackListener {
        void onOkButtonClick(int errorCode);
    }


}
