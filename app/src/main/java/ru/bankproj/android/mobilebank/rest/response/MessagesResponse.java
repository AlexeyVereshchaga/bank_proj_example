package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.messages.Message;

/**
 * Created by j7ars on 24.11.2017.
 */

public class MessagesResponse extends BaseResponse {

    @SerializedName("messages")
    private ArrayList<Message> messageList;

    public ArrayList<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(ArrayList<Message> messageList) {
        this.messageList = messageList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.messageList);
    }

    public MessagesResponse() {
    }

    protected MessagesResponse(Parcel in) {
        super(in);
        this.messageList = in.createTypedArrayList(Message.CREATOR);
    }

    public static final Creator<MessagesResponse> CREATOR = new Creator<MessagesResponse>() {
        @Override
        public MessagesResponse createFromParcel(Parcel source) {
            return new MessagesResponse(source);
        }

        @Override
        public MessagesResponse[] newArray(int size) {
            return new MessagesResponse[size];
        }
    };
}
