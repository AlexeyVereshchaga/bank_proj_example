package ru.bankproj.android.mobilebank.main.transfer.individual.person;

import java.util.List;

import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;

/**
 * Created by j7ars on 27.12.2017.
 */

public interface ITransferIndividualPersonContract {

    interface View extends IBaseProgressView {

        void initView(boolean templateMode);

        void initProductData(BaseProduct baseProduct);

        void openTransferCheckScreen(CheckTransferData checkTransferData, boolean templateMode);

        void showAccountNumberError(boolean showError);

        void showCardLimitDialog();

        void fieldValidationSuccess(Boolean success);

        void showNotValidAmountMessage(int validationValue);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void setListObservableField(List<Observable<Boolean>> observables);

        void setSelectedProduct(BaseProduct baseProduct);

        void onNextClicked(String amount, String recipientName, String accountNumberRecipient, String bik, String transferTarget);
    }
}
