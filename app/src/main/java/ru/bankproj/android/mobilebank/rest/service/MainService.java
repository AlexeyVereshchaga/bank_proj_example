package ru.bankproj.android.mobilebank.rest.service;

import java.util.LinkedHashMap;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.QueryMap;
import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.rest.RequestField;
import ru.bankproj.android.mobilebank.rest.Urls;
import ru.bankproj.android.mobilebank.rest.response.ATMResponse;
import ru.bankproj.android.mobilebank.rest.response.AttachedDeviceResponse;
import ru.bankproj.android.mobilebank.rest.response.CapabilitiesResponse;
import ru.bankproj.android.mobilebank.rest.response.ConfirmationStrategiesResponse;
import ru.bankproj.android.mobilebank.rest.response.CreditPaymentScheduleResponse;
import ru.bankproj.android.mobilebank.rest.response.MessagesResponse;
import ru.bankproj.android.mobilebank.rest.response.NewsResponse;
import ru.bankproj.android.mobilebank.rest.response.OffersResponse;
import ru.bankproj.android.mobilebank.rest.response.ProviderCategoriesResponse;
import ru.bankproj.android.mobilebank.rest.response.ProviderPaymentsResponse;
import ru.bankproj.android.mobilebank.rest.response.SubsidiariesResponse;

/**
 * Created by j7ars on 27.10.2017.
 */

public interface MainService {

    @GET(Urls.NEWS)
    Single<Response<NewsResponse>> getNews(@QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.OFFERS)
    Single<Response<OffersResponse>> getOffers(@QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.MESSAGES)
    Single<Response<MessagesResponse>> getMessages(@Header(RequestField.JSESSION) String sessionId, @QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.GET_SERVER_CAPABILITIES)
    Single<Response<CapabilitiesResponse>> getServerCapabilities(@Header(RequestField.JSESSION) String sessionId);

    @GET(Urls.GET_PAYMENT_CATEGORIES)
    Single<Response<ProviderCategoriesResponse>> getProviderCategories(@Header(RequestField.JSESSION) String sessionId);

    @GET(Urls.GET_PAYMENT_PROVIDERS)
    Single<Response<ProviderPaymentsResponse>> getPaymentProviders(@Header(RequestField.JSESSION) String sessionId);

    @GET(Urls.GET_ATM)
    Single<Response<ATMResponse>> getAtm(@QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.GET_ATTACHED_DEVICES)
    Single<Response<AttachedDeviceResponse>> getAttachedDevices(@Header(RequestField.JSESSION) String sessionId);

    @GET(Urls.GET_CONFIRMATION_STRATEGIES)
    Single<Response<ConfirmationStrategiesResponse>> getConfirmationStrategies(@Header(RequestField.JSESSION) String sessionId, @QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.GET_PAYMENT_SCHEDULE)
    Single<Response<CreditPaymentScheduleResponse>> getCreditPaymentSchedule(@Header(RequestField.JSESSION) String sessionId, @QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.SEND_MAIL)
    Single<Response<BaseResponse>> sendMail(@Header(RequestField.JSESSION) String sessionId, @QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.GET_SUBSIDIARIES)
    Single<Response<SubsidiariesResponse>> getSubsidiaries(@Header(RequestField.JSESSION) String sessionId);

}
