package ru.bankproj.android.mobilebank.main.transfer.clientgpb.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventData;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.product.CardLimit;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;

import ru.bankproj.android.mobilebank.main.transfer.clientgpb.ITransferClientGpbContract;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.rest.RequestParams;
import ru.bankproj.android.mobilebank.rest.response.CardLimitResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferResponse;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.RequestConstants;
import ru.bankproj.android.mobilebank.utils.Utils;

import static ru.bankproj.android.mobilebank.app.Constants.TAG;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public class TransferClientGpbPresenter extends BaseEventBusPresenter<ITransferClientGpbContract.View>
        implements ITransferClientGpbContract.Presenter, IBaseResponseCallback {

    private static final String SOURCE_CARD = "TransferClientGpbPresenter.SOURCE_CARD";
    private static final String TRANSFER_DESCRIPTION = "TransferClientGpbPresenter.TRANSFER_DESCRIPTION";

    private String transferDescription;
    private BaseProduct sourceProduct;
    private String amount;
    private RequestManager requestManager;
    private Disposable disposable;
    private String cardNumber;
    private String fioRecipient;
    private boolean templateMode;
    private Double productLimit;

    private PublishSubject<Boolean> subject = PublishSubject.create();

    @Inject
    public TransferClientGpbPresenter(RequestManager requestManager) {
        this.requestManager = requestManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            showSourceProduct();
            boolean gpbClient = !TextUtils.isEmpty(transferDescription)
                    && transferDescription.equalsIgnoreCase(FieldConverter.getString(R.string.transfer_internal_client));
            getMvpView().init(templateMode, gpbClient);
        }
    }

    private void showSourceProduct() {
        if (sourceProduct != null && sourceProduct instanceof Card) {
            getCardLimits(sourceProduct);
        }
        subject.onNext(sourceProduct != null);
        getMvpView().showSourceProduct(sourceProduct);
    }

    private void getCardLimits(BaseProduct sourceProduct) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                productLimit = null;
                disposable = requestManager.getCardLimits(sessionId, sourceProduct.getId(),
                        new BaseRequestController(mEventBusController, Action.GET_CARD_LIMITS_ACTION, getMvpView().getClassUniqueDeviceId()));
            }
        });
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            sourceProduct = (BaseProduct) params[0];
            transferDescription = (String) params[1];
            templateMode = (boolean) params[2];
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(SOURCE_CARD, sourceProduct);
        outState.putString(TRANSFER_DESCRIPTION, transferDescription);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        sourceProduct = savedInstanceState.getParcelable(SOURCE_CARD);
        transferDescription = savedInstanceState.getString(TRANSFER_DESCRIPTION);
    }

    @Override
    public void setListObservableField(List<Observable<Boolean>> mListObservable) {
        mListObservable.add(subject);
        Disposable validationDisposable = Observable.combineLatest(mListObservable, objects -> {
            boolean state = true;
            for (Object object : objects) {
                state &= (Boolean) object;
            }
            return state;
        }).subscribe(s -> getMvpView().fieldValidationSuccess(s));
        addDisposable(validationDisposable);
    }

    @Override
    public void onSourceClicked() {
        FilterProductData data = new FilterProductData();
        data.setFilterMode(FilterProductData.CARDS);
        getMvpView().openSelectCardScreen(data);
    }

    @Override
    public void onNextClicked(String amount) {
        this.amount = amount;
        int validationValue = validateAmount(amount);
        if (validationValue == -1) {
                    checkTransferRequest(amount);
        } else {
            getMvpView().showNotValidAmountMessage(validationValue);
        }
    }

    private int validateAmount(String amountStr) {
        Float amount = -1f;
        try {
            amount = Float.valueOf(amountStr);
        } catch (NumberFormatException e) {
            Log.e(TAG, "validateAmount: ", e);
        }
        if (productLimit != null && productLimit < amount) {
            return R.string.payment_restriction_exceeded;
        }
        if (amount <= 0) {
            return R.string.amount_error_message;
        }
        return -1;
    }

    private BaseProduct assignProductName(BaseProduct product) {
        if (product != null) {
            if (TextUtils.isEmpty(product.getName())) {
                if (product instanceof Card) {
                    switch (((Card) product).getType()) {
                        case Card.TYPE_CREDIT_CARD:
                            product.setName(FieldConverter.getString(R.string.card_name_credit));
                            break;
                        case Card.TYPE_DEBIT_CARD:
                            product.setName(FieldConverter.getString(R.string.card_name_debit));
                            break;
                    }
                } else if (product instanceof Account) {
                    product.setName("Счёт");
                }
            }
        }

        return product;
    }

    @Override
    public void setCardNumber(String number) {
        cardNumber = number;
    }

    @Override
    public void setFioRecipient(String fio) {
        fioRecipient = fio;
    }

    private void checkTransferRequest(String amount) {
        undisposable(disposable);
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                Integer sourceType = -1;
                String sourceId = sourceProduct.getId();
                Integer targetType = RequestConstants.EXTERNAL_CARD;
                String targetId = Utils.getClearNumberCard(cardNumber);
                if (sourceProduct instanceof Card) {
                    sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
                } else if (sourceProduct instanceof Account) {
                    sourceType = RequestConstants.SOURCE_CLIENTS_ACCOUNT;
                }
                disposable = requestManager.transferCheck(
                        sessionId,
                        RequestParams.getConfirmationStrategiesParams(ConfirmRequestDescriptor.TRANSFER, null, amount, Constants.RUR, null, 1, targetType, -1),
                        sourceProduct.getCurrency(), amount, sourceType, sourceId, targetType, targetId, fioRecipient, null, mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                        new BaseRequestController(mEventBusController, Action.CHECK_TRANSFER, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case START_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                    case Action.GET_CARD_LIMITS_ACTION:
                        getMvpView().startProgressDialog();
                        break;
                }

                break;
            case SUCCESS_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                        undisposable(disposable);
                        Response<TransferResponse> responseResponse = (Response<TransferResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), responseResponse, this);
                        break;
                    case Action.GET_CARD_LIMITS_ACTION:
                        undisposable(disposable);
                        Response<CardLimitResponse> baseResponse = (Response<CardLimitResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                        break;
                }
                break;
            case FAIL_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                        undisposable(disposable);
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        break;
                    case Action.GET_CARD_LIMITS_ACTION:
                        undisposable(disposable);
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        sourceProduct = null;
                        showSourceProduct();
                        break;
                }
                break;
            case CUSTOM_EVENT:
                switch (event.getActionCode()) {
                    case Action.SOURCE_PRODUCT_SELECTED_EVENT:
                        getMvpView().moveToTop();
                        sourceProduct = ((EventData<Card>) event).getData();
                        showSourceProduct();
                        break;
                }
                break;
        }
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case Action.CHECK_TRANSFER:
                getMvpView().completeProgressDialog();
                TransferResponse transferResponse = (TransferResponse) data.getValue();
                CheckTransferData checkTransferData = new CheckTransferData();
                checkTransferData.setAmount(amount);
                checkTransferData.setSourceProduct(assignProductName(sourceProduct));
                checkTransferData.setTransferDescription(transferDescription);
                checkTransferData.setTransferResponse(transferResponse);
                checkTransferData.setType(CheckTransferData.CLIENT_GPB);
                checkTransferData.setFioRecipient(fioRecipient);
                checkTransferData.setNumberCard(cardNumber);
                getMvpView().openTransferCheckScreen(checkTransferData, templateMode);
                break;
            case Action.GET_CARD_LIMITS_ACTION:
                getMvpView().completeProgressDialog();
                CardLimitResponse cardLimitResponse = (CardLimitResponse) data.getValue();
                if (cardLimitResponse != null) {
                    ArrayList<CardLimit> cardLimits = cardLimitResponse.getLimitList();
                    productLimit = null;
                    for (CardLimit cardLimit :
                            cardLimits) {
                        double localLimit = cardLimit.getAmount() - cardLimit.getUsedAmount();
                        if (productLimit == null || productLimit > localLimit) {
                            productLimit = localLimit;
                        }
                        if (productLimit <= 0) {
                            sourceProduct = null;
                            showSourceProduct();
                            getMvpView().showCardLimitDialog();
                            return;
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                getMvpView().completeProgressDialog();
                getMvpView().unAuthorized();
                break;
            case BAD_REQUEST_ERROR:
            case DEFAULT_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
            default:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
        }
        switch (actionCode) {
            case Action.GET_CARD_LIMITS_ACTION:
                sourceProduct = null;
                showSourceProduct();
                break;
        }
    }
}