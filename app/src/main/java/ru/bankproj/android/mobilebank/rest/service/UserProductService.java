package ru.bankproj.android.mobilebank.rest.service;

import java.util.LinkedHashMap;
import java.util.Map;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.rest.RequestField;
import ru.bankproj.android.mobilebank.rest.Urls;
import ru.bankproj.android.mobilebank.rest.response.AccountCategoryResponse;
import ru.bankproj.android.mobilebank.rest.response.AccountConditionsResponse;
import ru.bankproj.android.mobilebank.rest.response.AccountDetailResponse;
import ru.bankproj.android.mobilebank.rest.response.AccountOperationResponse;
import ru.bankproj.android.mobilebank.rest.response.AccountResponse;
import ru.bankproj.android.mobilebank.rest.response.CardDetailResponse;
import ru.bankproj.android.mobilebank.rest.response.CardLimitResponse;
import ru.bankproj.android.mobilebank.rest.response.CardOperationResponse;
import ru.bankproj.android.mobilebank.rest.response.CardResponse;
import ru.bankproj.android.mobilebank.rest.response.CardRestrictionsResponse;
import ru.bankproj.android.mobilebank.rest.response.CheckPaymentResponse;
import ru.bankproj.android.mobilebank.rest.response.ConfirmOpenAccountResponse;
import ru.bankproj.android.mobilebank.rest.response.CreditDetailResponse;
import ru.bankproj.android.mobilebank.rest.response.CustomPaymentExecuteResponse;
import ru.bankproj.android.mobilebank.rest.response.CustomPaymentRequestResponse;
import ru.bankproj.android.mobilebank.rest.response.CustomRequestResponse;
import ru.bankproj.android.mobilebank.rest.response.DebitDetailResponse;
import ru.bankproj.android.mobilebank.rest.response.ExecutePaymentResponse;
import ru.bankproj.android.mobilebank.rest.response.OpenAccountResponse;
import ru.bankproj.android.mobilebank.rest.response.TemplateResponse;

/**
 * Created by j7ars on 13.11.2017.
 */

public interface UserProductService {

    @GET(Urls.GET_ACCOUNT_LIST)
    Single<Response<AccountResponse>> getAccountList(@Header(RequestField.JSESSION) String sessionId);

    @GET(Urls.GET_CARD_LIST)
    Single<Response<CardResponse>> getCardList(@Header(RequestField.JSESSION) String sessionId);

    @GET(Urls.GET_CARD_DETAIL)
    Single<Response<CardDetailResponse>> getCardDetail(@Header(RequestField.JSESSION) String sessionId, @Query(RequestField.ID) String cardId);

    @GET(Urls.GET_CARD_HISTORY)
    Single<Response<CardOperationResponse>> getCardOperationHistory(@Header(RequestField.JSESSION) String sessionId, @QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.GET_ACCOUNT_DETAIL)
    Single<Response<CreditDetailResponse>> getCreditDetail(@Header(RequestField.JSESSION) String sessionId, @Query(RequestField.ID) String accountId);

    @GET(Urls.GET_ACCOUNT_DETAIL)
    Single<Response<DebitDetailResponse>> getDebitDetail(@Header(RequestField.JSESSION) String sessionId, @Query(RequestField.ID) String accountId);

    @GET(Urls.GET_ACCOUNT_DETAIL)
    Single<Response<AccountDetailResponse>> getAccountDetail(@Header(RequestField.JSESSION) String sessionId, @Query(RequestField.ID) String accountId);

    @GET(Urls.GET_ACCOUNT_HISTORY)
    Single<Response<AccountOperationResponse>> getAccountOperationHistory(@Header(RequestField.JSESSION) String sessionId, @QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.GET_CARD_CARD_LIMITS)
    Single<Response<CardLimitResponse>> getCardLimits(@Header(RequestField.JSESSION) String sessionId, @Query(RequestField.CARD) String cardId);

    @FormUrlEncoded
    @POST(Urls.EDIT_CARD)
    Single<Response<BaseResponse>> editCard(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.CARD_ACTIVATION_CHANGE)
    Single<Response<BaseResponse>> changeCardActivation(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.ACCOUNT_ACTIVATION_CHANGE)
    Single<Response<BaseResponse>> changeAccountActivation(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.EDIT_ACCOUNT)
    Single<Response<BaseResponse>> editAccount(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @GET(Urls.GET_TEMPLATES)
    Single<Response<TemplateResponse>> getTemplates(@Header(RequestField.JSESSION) String sessionId);

    @GET(Urls.GET_CARD_RESTRICTIONS)
    Single<Response<CardRestrictionsResponse>> getCardRestrictions(@Header(RequestField.JSESSION) String sessionId, @Query(RequestField.CARD) String cardId);

    @FormUrlEncoded
    @POST(Urls.EDIT_CARD_RESTRICTION)
    Single<Response<BaseResponse>> editCardRestrictions(@Header(RequestField.JSESSION) String sessionId, @FieldMap LinkedHashMap<String, String> params);

    @GET(Urls.GET_ACCOUNT_CATEGORY)
    Single<Response<AccountCategoryResponse>> getAccountCategory(@Header(RequestField.JSESSION) String sessionId, @QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.GET_ACCOUNT_CONDITIONS)
    Single<Response<AccountConditionsResponse>> getAccountConditions(@Header(RequestField.JSESSION) String sessionId, @QueryMap LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.OPEN_ACCOUNT)
    Single<Response<OpenAccountResponse>> openAccount(@Header(RequestField.JSESSION) String sessionId, @FieldMap LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.CONFIRM_OPEN_ACCOUNT)
    Single<Response<ConfirmOpenAccountResponse>> confirmOpenAccount(@Header(RequestField.JSESSION) String sessionId, @FieldMap LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.PAYMENT_CHECK)
    Single<Response<CheckPaymentResponse>> checkPayment(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST(Urls.PAYMENT_EXECUTE)
    Single<Response<ExecutePaymentResponse>> executePayment(@Header(RequestField.JSESSION) String sessionId,
                                                            @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST(Urls.CHECK_TEMPLATE)
    Single<Response<CheckPaymentResponse>> checkTemplate(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.EXECUTE_TEMPLATE)
    Single<Response<ExecutePaymentResponse>> executeTemplate(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.EDIT_CARD_LIMIT)
    Single<Response<BaseResponse>> editCardLimit(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    //fine
    @FormUrlEncoded
    @POST(Urls.CUSTOM_REQUEST)
    Single<Response<CustomRequestResponse>> customRequest(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.CUSTOM_PAYMENT_REQUEST)
    Single<Response<CustomPaymentRequestResponse>> customPaymentRequest(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.CUSTOM_PAYMENT_EXECUTE_REQUEST)
    Single<Response<CustomPaymentExecuteResponse>> customPaymentExecuteRequest(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

}
