package ru.bankproj.android.mobilebank.managers;

import android.content.res.TypedArray;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.dataclasses.ErrorData;
import ru.bankproj.android.mobilebank.utils.FieldConverter;

/**
 * Created by j7ars on 30.10.2017.
 */
@Singleton
public class ErrorMessageManager {

    private HashMap<Integer, ErrorData> mErrorMap;

    @Inject
    public ErrorMessageManager(){
        mErrorMap = getErrorMap();
    }

    public ErrorData getErrorMessage(int errorCode){
        if(mErrorMap == null){
            mErrorMap = getErrorMap();
        }
        if(mErrorMap.get(errorCode) == null){
            ErrorData errorData = new ErrorData(errorCode, FieldConverter.getString(R.string.message_error_default), R.drawable.ic_error_msg);
            return errorData;
        } else {
            return mErrorMap.get(errorCode);
        }
    }

    private HashMap<Integer, ErrorData> getErrorMap(){
        int [] errorCodeArray = FieldConverter.getIntArray(R.array.error_code);
        TypedArray errorIcon = FieldConverter.getTypedArray(R.array.error_icon);
        String [] errorMessageArray = FieldConverter.getStringArray(R.array.error_message);
        HashMap<Integer, ErrorData> errorMap = new HashMap<>();
        for(int i = 0; i < errorCodeArray.length ; i ++ ){
            ErrorData errorData = new ErrorData(errorCodeArray[i], errorMessageArray[i], errorIcon.getResourceId(i, -1));
            errorMap.put(errorCodeArray[i], errorData);
        }
        return errorMap;
    }

}
