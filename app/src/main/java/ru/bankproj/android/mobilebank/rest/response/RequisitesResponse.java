package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;

/**
 * Created by Karina on 20.01.2018.
 */

public class RequisitesResponse extends BaseResponse {

    @SerializedName("requisites")
    @Expose
    private String mRequisites;

    public String getRequisites() {
        return mRequisites;
    }

    public void setRequisites(String requisites) {
        this.mRequisites = requisites;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.mRequisites);
    }

    public RequisitesResponse() {
    }

    protected RequisitesResponse(Parcel in) {
        super(in);
        this.mRequisites = in.readString();
    }

    public static final Creator<RequisitesResponse> CREATOR = new Creator<RequisitesResponse>() {
        @Override
        public RequisitesResponse createFromParcel(Parcel source) {
            return new RequisitesResponse(source);
        }

        @Override
        public RequisitesResponse[] newArray(int size) {
            return new RequisitesResponse[size];
        }
    };
}
