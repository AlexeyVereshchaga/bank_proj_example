package ru.bankproj.android.mobilebank.main.transfer.main.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.providers.PaymentParameter;
import ru.bankproj.android.mobilebank.dataclasses.providers.Provider;
import ru.bankproj.android.mobilebank.main.transfer.main.ITransferContract;

import static ru.bankproj.android.mobilebank.main.dialogs.InfoDialog.TAG;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public class TransferPresenter extends BaseEventBusPresenter<ITransferContract.View>
        implements ITransferContract.Presenter, IBaseResponseCallback {

    private static final String SELECTED_PRODUCT = "TransferPresenter.SELECTED_PRODUCT";
    private static final String TEMPLATE_MODE = "TransferPresenter.TEMPLATE_MODE";

    private BaseProduct selectedProduct;
    private boolean templateMode;

    @Inject
    public TransferPresenter() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        }
        List<Card> creditCards = mDataManager.getCardListByType(Card.TYPE_CREDIT_CARD);
        List<Account> creditAccounts = mDataManager.getCreditList();
        if (creditCards == null || creditCards.isEmpty()) {
            getMvpView().disableRepaymentCards();
        }
        if (creditAccounts == null || creditAccounts.isEmpty()) {
            getMvpView().disableRepaymentAccounts();
        }
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            selectedProduct = (BaseProduct) params[0];
            templateMode = (boolean) params[1];
            mDataManager.getAllProviders().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                        Log.i("TAG", "setArguments: " + s);
                        List<String> strings = null;
                        List<PaymentParameter> paymentParameters = null;
                        for (Provider provider :
                                s) {
                            if (provider.getRids() != null) {
                                strings = provider.getRids();
                                Log.d(TAG, "setArguments: " + strings);
                            }
                            if (provider.getPaymentParameterRecords() != null) {
                                paymentParameters = provider.getPaymentParameterRecords();
                                Log.d(TAG, "setArguments: " + paymentParameters);
                            }
                        }
                        if (strings == null) {
                            Log.d(TAG, "setArguments: null Rids");
                        }
                        if (paymentParameters == null) {
                            Log.d(TAG, "setArguments: null paymentParameters");
                        }
                    });
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(SELECTED_PRODUCT, selectedProduct);
        outState.putBoolean(TEMPLATE_MODE, templateMode);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        selectedProduct = savedInstanceState.getParcelable(SELECTED_PRODUCT);
        templateMode = savedInstanceState.getBoolean(SELECTED_PRODUCT);
    }

    @Override
    public void onEvent(Event event) {

    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {

    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {

    }

    @Override
    public void ownAccountClicked() {
        if (templateMode) {
            getMvpView().showTransferOwnAccountScreen(null, templateMode);
        } else {
            getMvpView().showTransferOwnAccountScreen(selectedProduct, templateMode);
        }
    }

    @Override
    public void clientGpbClicked() {
        if (templateMode || !(selectedProduct instanceof Card)) {
            getMvpView().showTransferClientGpbScreen(null, templateMode);
        } else {
            getMvpView().showTransferClientGpbScreen(selectedProduct, templateMode);
        }
    }

    @Override
    public void otherClientClicked() {
        if (templateMode || !(selectedProduct instanceof Card)) {
            getMvpView().showTransferOtherClientScreen(null, templateMode);
        } else {
            getMvpView().showTransferOtherClientScreen(selectedProduct, templateMode);
        }
    }

    @Override
    public void creditCardClicked() {
        if (templateMode || !(selectedProduct instanceof Card)) {
            getMvpView().showRepaymentCreditCardScreen(null, templateMode);
        } else {
            getMvpView().showRepaymentCreditCardScreen(selectedProduct, templateMode);
        }
    }

    @Override
    public void creditAccountClicked() {
        if (templateMode || !(selectedProduct instanceof Card)) {
            getMvpView().showRepaymentCreditAccountScreen(null, templateMode);
        } else {
            getMvpView().showRepaymentCreditAccountScreen(selectedProduct, templateMode);
        }
    }

    @Override
    public void individualClicked() {
        if (templateMode || !(selectedProduct instanceof Card)) {
            getMvpView().showTransferIndividualScreen(null, templateMode);
        } else {
            getMvpView().showTransferIndividualScreen(selectedProduct, templateMode);
        }
    }

    @Override
    public void legalClicked() {
        if (templateMode || !(selectedProduct instanceof Card)) {
            getMvpView().showTransferLegalScreen(null, templateMode);
        } else {
            getMvpView().showTransferLegalScreen(selectedProduct, templateMode);
        }
    }

    @Override
    public void budgetClicked() {
        if (templateMode || !(selectedProduct instanceof Card)) {
            getMvpView().showTransferBudgetScreen(null, templateMode);
        } else {
            getMvpView().showTransferBudgetScreen(selectedProduct, templateMode);
        }
    }
}
