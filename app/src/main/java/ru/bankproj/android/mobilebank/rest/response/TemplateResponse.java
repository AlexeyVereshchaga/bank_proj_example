package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.templates.Template;

/**
 * Created by j7ars on 08.12.2017.
 */

public class TemplateResponse extends BaseResponse {

    @SerializedName("operations")
    private ArrayList<Template> templateList;

    public ArrayList<Template> getTemplateList() {
        return templateList;
    }

    public void setTemplateList(ArrayList<Template> templateList) {
        this.templateList = templateList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.templateList);
    }

    public TemplateResponse() {
    }

    protected TemplateResponse(Parcel in) {
        super(in);
        this.templateList = in.createTypedArrayList(Template.CREATOR);
    }

    public static final Creator<TemplateResponse> CREATOR = new Creator<TemplateResponse>() {
        @Override
        public TemplateResponse createFromParcel(Parcel source) {
            return new TemplateResponse(source);
        }

        @Override
        public TemplateResponse[] newArray(int size) {
            return new TemplateResponse[size];
        }
    };
}
