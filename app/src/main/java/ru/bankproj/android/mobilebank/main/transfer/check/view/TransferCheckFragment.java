package ru.bankproj.android.mobilebank.main.transfer.check.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.providers.PaymentParameter;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.template.create.success.view.CreateTemplateSuccessActivity;
import ru.bankproj.android.mobilebank.main.transfer.check.ITransferCheckContract;
import ru.bankproj.android.mobilebank.main.transfer.success.view.TransferSuccessActivity;
import ru.bankproj.android.mobilebank.rest.response.TransferExecResponse;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.view.EnableButton;
import ru.bankproj.android.mobilebank.view.ShowHidePasswordEditText;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

import static ru.bankproj.android.mobilebank.rest.response.ConfirmationStrategiesResponse.PASSWORD_CONFIRMATION_STRATEGY;

/**
 * Created by Alexey Vereshchaga on 22.12.17.
 */

public class TransferCheckFragment extends BaseMvpFragment implements ITransferCheckContract.View {

    public static final String CHECK_TRANSFER_DATA = "TransferCheckFragment.CHECK_TRANSFER_DATA";
    public static final String TEMPLATE_MODE = "TransferCheckFragment.TEMPLATE_MODE";

    @BindView(R.id.tv_transfer_description)
    TextView tvTransferDescription;
    @BindView(R.id.tv_transfer_recipient_product_name)
    TextView tvRecipientName;
    @BindView(R.id.tv_transfer_recipient_number)
    TextView tvRecipientNumber;
    @BindView(R.id.tv_transfer_debiting_product_name)
    TextView tvDebitingName;
    @BindView(R.id.tv_transfer_debiting_product_number)
    TextView tvDebitingNumber;
    @BindView(R.id.tv_transfer_amount)
    TextView tvAmount;
    @BindView(R.id.tv_transfer_fee_amount)
    TextView tvFeeAmount;
    @BindView(R.id.tv_transfer_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.ll_password_container)
    LinearLayout llPasswordContainer;
    @BindView(R.id.et_password)
    ShowHidePasswordEditText etPassword;
    @BindView(R.id.btn_confirm)
    EnableButton btnConfirm;
    @BindView(R.id.ll_addition_params_container)
    LinearLayout llAdditionParamsContainer;
    @BindView(R.id.tv_target_title)
    TextView tvTargetTitle;

    @Inject
    ITransferCheckContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs() {
        if (getArguments() != null) {
            presenter.setArguments(getArguments().getParcelable(CHECK_TRANSFER_DATA),
                    getArguments().getBoolean(TEMPLATE_MODE));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.onCreate(savedInstanceState);
        Utils.hideKeyboard(mActivity);
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.passwordTextChanged(editable.toString());
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        presenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        presenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_transfer_check;
    }

    @Override
    public void showTransferInfo(CheckTransferData checkTransferData) {
        tvTransferDescription.setText(checkTransferData.getTransferDescription());
        BaseProduct baseProductTarget = checkTransferData.getTargetProduct();
        if (baseProductTarget != null) {
            tvRecipientName.setText(getNameOfProduct(baseProductTarget));
            tvRecipientNumber.setText(checkTransferData.getTargetProduct().getNumber());
        } else {
            tvRecipientName.setVisibility(View.GONE);
            tvRecipientNumber.setVisibility(View.GONE);
            tvTargetTitle.setVisibility(View.GONE);
        }
        tvDebitingName.setText(getNameOfProduct(checkTransferData.getSourceProduct()));
        tvDebitingNumber.setText(checkTransferData.getSourceProduct().getNumber());

        float amountF = Float.parseFloat(checkTransferData.getAmount());
        float fee = checkTransferData.getTransferResponse().getFee();
        String rubSign = getString(R.string.currency_rub);
        String format = "%s %s";
        tvAmount.setText(String.format(format, Utils.formatAmount(amountF), rubSign));
        tvFeeAmount.setText(String.format(format, Utils.formatAmount(fee), rubSign));
        tvTotalAmount.setText(String.format(format, Utils.formatAmount(amountF + fee), rubSign));

        boolean passwordConfirmation = !TextUtils.isEmpty(checkTransferData.getTransferResponse().getConfirmationStrategy())
                && checkTransferData.getTransferResponse().getConfirmationStrategy().equalsIgnoreCase(PASSWORD_CONFIRMATION_STRATEGY);
        llPasswordContainer.setVisibility(passwordConfirmation ? View.VISIBLE : View.GONE);
        btnConfirm.enable(!passwordConfirmation);
        if (checkTransferData.getAdditionParams() != null && !checkTransferData.getAdditionParams().isEmpty()) {
            fillAdditionParams(checkTransferData);
        } else {
            llAdditionParamsContainer.setVisibility(View.GONE);
        }
    }

    private String getNameOfProduct(BaseProduct baseProduct) {
        String name = "";
        if (baseProduct instanceof Card) {
            name = ((Card) baseProduct).getCardName();
        } else if (baseProduct instanceof Account) {
            name = ((Account) baseProduct).getAccountName();
        }
        return name;
    }

    private void fillAdditionParams(CheckTransferData checkTransferData) {
        llAdditionParamsContainer.setVisibility(View.VISIBLE);
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        List<String> paramsNames = Arrays.asList(getResources().getStringArray(R.array.param_names));
        String[] views = getResources().getStringArray(R.array.param_views);
        for (Map.Entry<String, String> entry :
                checkTransferData.getAdditionParams().entrySet()) {
            int position = paramsNames.indexOf(entry.getKey());
            if (position != -1) {
                View view = layoutInflater.inflate(R.layout.row_addition_param_check, null);
                TextView tvTitle = view.findViewById(R.id.tv_param_title);
                TextView tvParamView = view.findViewById(R.id.tv_param_view);
                tvTitle.setText(views[position]);
                String value = entry.getValue();
                try {
                    value = URLDecoder.decode(entry.getValue(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    Log.e(TransferCheckFragment.class.getSimpleName(), "fillAdditionParams: ", e);
                }
                tvParamView.setText(value);
                llAdditionParamsContainer.addView(view);
            }
        }
    }

    @Override
    public void showTemplateTransferInfo(CheckTransferData checkTransferData) {
        tvTransferDescription.setText(checkTransferData.getTemplate().getTypeNameByType());
        BaseProduct baseProductTarget = checkTransferData.getTemplate().getTargetProduct();
        if (baseProductTarget != null) {
            tvRecipientName.setText(getNameOfProduct(baseProductTarget));
            tvRecipientNumber.setText(baseProductTarget.getNumber());
        } else {
            tvRecipientName.setVisibility(View.GONE);
            tvRecipientNumber.setVisibility(View.GONE);
            tvTargetTitle.setVisibility(View.GONE);
        }

        BaseProduct sourceProduct = checkTransferData.getTemplate().getSourceProduct();
        tvDebitingName.setText(getNameOfProduct(sourceProduct));
        tvDebitingNumber.setText(sourceProduct.getNumber());

        float amountF = Float.parseFloat(checkTransferData.getAmount());
        float fee = checkTransferData.getTransferResponse().getFee();
        String rubSign = getString(R.string.currency_rub);
        String format = "%s %s";
        tvAmount.setText(String.format(format, Utils.formatAmount(amountF), rubSign));
        tvFeeAmount.setText(String.format(format, Utils.formatAmount(fee), rubSign));
        tvTotalAmount.setText(String.format(format, Utils.formatAmount(amountF + fee), rubSign));

        boolean passwordConfirmation = !TextUtils.isEmpty(checkTransferData.getTransferResponse().getConfirmationStrategy())
                && checkTransferData.getTransferResponse().getConfirmationStrategy().equalsIgnoreCase(PASSWORD_CONFIRMATION_STRATEGY);
        llPasswordContainer.setVisibility(passwordConfirmation ? View.VISIBLE : View.GONE);
        btnConfirm.enable(!passwordConfirmation);
        if (checkTransferData.getTemplate().getPaymentParameters() != null && !checkTransferData.getTemplate().getPaymentParameters().isEmpty()) {
            fillAdditionParams(checkTransferData.getTemplate().getPaymentParameters());
        } else {
            llAdditionParamsContainer.setVisibility(View.GONE);
        }
    }

    private void fillAdditionParams(ArrayList<PaymentParameter> paymentParameters) {
        llAdditionParamsContainer.setVisibility(View.VISIBLE);
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        List<String> paramsNamesInternal = Arrays.asList(getResources().getStringArray(R.array.param_names_without_prefix));
        String[] paramsNamesExternal = getResources().getStringArray(R.array.param_views);
        for (PaymentParameter paymentParameter :
                paymentParameters) {
            String id = paymentParameter.getId();

            int position = paramsNamesInternal.indexOf(id);
            if (position != -1) {
                View view = layoutInflater.inflate(R.layout.row_addition_param_check, null);
                TextView tvTitle = view.findViewById(R.id.tv_param_title);
                TextView tvParamView = view.findViewById(R.id.tv_param_view);
                tvTitle.setText(paramsNamesExternal[position]);
                tvParamView.setText(paymentParameter.getValue());
                llAdditionParamsContainer.addView(view);
            }
        }
    }

    @Override
    public void enableConfirmButton(boolean b) {
        btnConfirm.enable(b);
    }

    @Override
    public void openSuccessTransferScreen(TransferExecResponse transferExecResponse) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferSuccessActivity.TRANSFER_EXEC_RESPONSE, transferExecResponse);
        getScreenCreator().startActivity(this, mActivity, TransferSuccessActivity.class, bundle, Constants.START_TRANSFER_SUCCESS_ACTIVITY);
    }

    @Override
    public void openCreateTemplateSuccessScreen(String templateName, String id) {
        getScreenCreator().startActivity(this, mActivity, CreateTemplateSuccessActivity.class, getCreateTemplateSuccessBundle(templateName, id));
    }

    private Bundle getCreateTemplateSuccessBundle(String templateName, String id) {
        Bundle bundle = new Bundle();
        bundle.putString(CreateTemplateSuccessActivity.TEMPLATE_NAME, templateName);
        bundle.putString(CreateTemplateSuccessActivity.TEMPLATE_ID, id);
        return bundle;
    }

    @OnClick(R.id.btn_confirm)
    void onConfirmClick() {
        presenter.onConfirmClicked(etPassword.getText());
    }
}
