package ru.bankproj.android.mobilebank.eventbus;

import ru.bankproj.android.mobilebank.base.event.Event;

/**
 * Created by j7ars on 25.10.2017.
 */

public interface IEventObserver {

    void onEvent(Event event);

}
