package ru.bankproj.android.mobilebank.main.transfer.repayment.check.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.app.ValidFields;
import ru.bankproj.android.mobilebank.app.session.ISessionValueCallback;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.dataclasses.authorization.PasswordError;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SourceType;
import ru.bankproj.android.mobilebank.dataclasses.transfer.TargetType;
import ru.bankproj.android.mobilebank.di.scope.ConfigPersistent;
import ru.bankproj.android.mobilebank.main.transfer.repayment.check.IRepaymentCheckContract;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.rest.response.TemplateCreateResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferExecResponse;
import ru.bankproj.android.mobilebank.utils.RequestConstants;

import static ru.bankproj.android.mobilebank.app.Action.CREATE_TEMPLATE;
import static ru.bankproj.android.mobilebank.app.Action.REPAYMENT_EXEC;

/**
 * Created by Alexey Vereshchaga on 22.12.17.
 */
@ConfigPersistent
public class RepaymentCheckPresenter extends BaseEventBusPresenter<IRepaymentCheckContract.View>
        implements IRepaymentCheckContract.Presenter, IBaseResponseCallback {

    public static final String CHECK_TRANSFER_DATA = "RepaymentCheckPresenter.CHECK_TRANSFER_DATA";
    private static final String TEMPLATE_MODE = "RepaymentCheckPresenter.TEMPLATE_MODE";

    private CheckTransferData checkTransferData;
    private boolean templateMode;

    private RequestManager requestManager;
    private Disposable disposable;


    @Inject
    public RepaymentCheckPresenter(RequestManager requestManager) {
        this.requestManager = requestManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            showTransferInfo();
        }
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            checkTransferData = (CheckTransferData) params[0];
            templateMode = (boolean) params[1];
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(CHECK_TRANSFER_DATA, checkTransferData);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        checkTransferData = savedInstanceState.getParcelable(CHECK_TRANSFER_DATA);
    }

    private void showTransferInfo() {
        getMvpView().showRepaymentInfo(checkTransferData);
    }

    private void getTransferExec(String pass) {
        mDataManager.getSessionId(new ISessionValueCallback() {
            @Override
            public void sessionValue(boolean isUnAuthorize, String sessionId) {
                if (isUnAuthorize) {
                    getMvpView().unAuthorized();
                } else {
                    disposable = makeRequestByTransferType(sessionId, pass);
                    addDisposable(disposable);
                }
            }
        });
    }

    private Disposable makeRequestByTransferType(String sessionId, String pass) {
        int sourceType = -1;
        int targetType = -1;
        String sourceId = null;
        String targetId = checkTransferData.getTargetProduct().getId();
        String fullName = null;
        String flags = null;
        Map<String, String> additionalParams = checkTransferData.getAdditionParams();

        if (checkTransferData.getSourceProduct() != null) {
            if (checkTransferData.getSourceProduct() instanceof Card) {
                sourceType = SourceType.CARD.getId();
            } else if (checkTransferData.getSourceProduct() instanceof Account) {
                sourceType = SourceType.ACCOUNT.getId();
            }
            sourceId = checkTransferData.getSourceProduct().getId();
        }

        switch (checkTransferData.getType()) {
            case CheckTransferData.CREDIT_REPAYMENT_CARD:
                targetType = TargetType.CLIENTS_CARD.getId();
                flags = "1";/* ??? different flags with the same description (see business processes doc)*/
                break;
            case CheckTransferData.CREDIT_REPAYMENT_ACCOUNT:
                targetType = TargetType.ACCOUNT_IN_SAME_BANK.getId();
                flags = "2";
                break;
        }

        disposable = requestManager.repaymentExec(
                sessionId, Constants.RUR, checkTransferData.getAmount(), checkTransferData.getTransferResponse().getTransRef(),
                targetId, pass, null, targetType, sourceId, flags,
                sourceType, fullName, additionalParams, null,
                mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                new BaseRequestController(mEventBusController, REPAYMENT_EXEC, getMvpView().getClassUniqueDeviceId()));
        return disposable;
    }

    private void createTemplate(String pass) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                int actionType = RequestConstants.AFTER_REPAYMENT_CHECK;
                int sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
                Integer targetType = null;
                String pid = null;
                Integer operationSubtype = null;
                Integer flags = RequestConstants.TRANSFER_BETWEEN_CLIENTS_PRODUCTS;
                String targetId = null;
                String fullName = null;

                BaseProduct sourceProduct = checkTransferData.getSourceProduct();
                Map<String, String> params = checkTransferData.getAdditionParams();
                String amount = String.valueOf(Float.valueOf(checkTransferData.getAmount()));
                String templateName = checkTransferData.getNewTemplateName();
                String transRef = checkTransferData.getTransferResponse().getTransRef();
                BaseProduct targetProduct = checkTransferData.getTargetProduct();

                switch (checkTransferData.getType()) {
                    case CheckTransferData.CREDIT_REPAYMENT_CARD:
                    case CheckTransferData.CREDIT_REPAYMENT_ACCOUNT:
                        if (sourceProduct instanceof Card) {
                            sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
                        } else if (sourceProduct instanceof Account) {
                            sourceType = RequestConstants.SOURCE_CLIENTS_ACCOUNT;
                        }
                        if (actionType == RequestConstants.AFTER_TRANSFER_CHECK || actionType == RequestConstants.AFTER_REPAYMENT_CHECK) {
                            if (targetProduct instanceof Card) {
                                targetType = RequestConstants.CLIENTS_CARD;
                            } else if (targetProduct instanceof Account) {
                                targetType = RequestConstants.ACCOUNT_IN_SAME_BANK;
                            }
                        }
//                if (actionType == RequestConstants.AFTER_PAYMENT_CHECK) {
//                    pid = provider.getPid();
//                }
//                        if (actionType == RequestConstants.AFTER_TRANSFER_CHECK) {
//                            if (targetProduct instanceof Account && ((Account) targetProduct).getType() == Account.TYPE_DEBIT) {
//                                operationSubtype = RequestConstants.DEPOSIT_REFILL;
//                            } else {
//                                operationSubtype = RequestConstants.NORMAL_OPERATION;
//                            }
//                        }
                        targetId = targetProduct.getId();
                        flags = RequestConstants.TRANSFER_BETWEEN_CLIENTS_PRODUCTS;
                        break;
                }

                disposable = requestManager.createTemplate(sessionId, actionType, sourceProduct.getCurrency(), amount,
                        sourceType, sourceProduct.getId(), targetType, targetId,
                        pid, templateName, fullName, params, null, operationSubtype, flags, transRef, pass,
                        mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                        new BaseRequestController(mEventBusController, CREATE_TEMPLATE, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    @Override
    public void passwordTextChanged(String pass) {
        getMvpView().enableConfirmButton(checkPassword(pass));
    }

    @Override
    public void onConfirmClicked(Editable text) {
        if (templateMode) {
            createTemplate(text.toString());
        } else {
            getTransferExec(text.toString());
        }
    }

    private boolean checkPassword(String pass) {
        PasswordError passwordError = ValidFields.isValidPassword(pass);
        return passwordError.getErrorCode() == PasswordError.DEFAULT_MESSAGE;
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case START_REQUEST:
                if (event.getActionCode() == REPAYMENT_EXEC
                        || event.getActionCode() == CREATE_TEMPLATE) {
                    getMvpView().startProgressDialog();
                }
                break;
            case SUCCESS_REQUEST:
                if (event.getActionCode() == REPAYMENT_EXEC) {
                    undisposable(disposable);
                    Response<TransferExecResponse> baseResponse = (Response<TransferExecResponse>) ((EventSuccessRequest) event).getData();
                    ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                }
                if (event.getActionCode() == CREATE_TEMPLATE) {
                    undisposable(disposable);
                    Response<TemplateCreateResponse> baseResponse = (Response<TemplateCreateResponse>) ((EventSuccessRequest) event).getData();
                    ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                }
                break;
            case FAIL_REQUEST:
                if (event.getActionCode() == REPAYMENT_EXEC
                        || event.getActionCode() == CREATE_TEMPLATE) {
                    undisposable(disposable);
                    getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                }
                break;
        }
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case REPAYMENT_EXEC:
                TransferExecResponse transferExecResponse = (TransferExecResponse) data.getValue();
                getMvpView().openSuccessTransferScreen(transferExecResponse);
                getMvpView().completeProgressDialog();
                break;
            case Action.CREATE_TEMPLATE:
                TemplateCreateResponse templateCreateResponse = (TemplateCreateResponse) data.getValue();
                getMvpView().completeProgressDialog();
                getMvpView().openCreateTemplateSuccessScreen(checkTransferData.getNewTemplateName(), templateCreateResponse.getId());
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                getMvpView().completeProgressDialog();
                getMvpView().unAuthorized();
                break;
            case BAD_REQUEST_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
            case DEFAULT_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
        }
        switch (errorCode) {
            // TODO: 27.12.17 to fill errors
            case ResponseHandler.MAX_CONFIRMATION_ATTEMPT_WAS_EXCEEDED_ERROR:
            case ResponseHandler.PROVIDED_PARAMETERS_VALUES_ERROR:
            case ResponseHandler.INCORRECT_TRANSREF:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode)); //temporary
                break;
        }
    }

}