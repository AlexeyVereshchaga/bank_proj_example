package ru.bankproj.android.mobilebank.gcm.registration;

import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpView;

/**
 * Created by Alexey Vereshchaga on 02.03.18.
 */

public interface IRegistrationServiceContract {
    interface View extends IBaseMvpView {
        void sendBroadcast();
    }

    interface Presenter extends IBaseMvpPresenter<View> {
        void sendRegistrationToServer(String token);
    }
}
