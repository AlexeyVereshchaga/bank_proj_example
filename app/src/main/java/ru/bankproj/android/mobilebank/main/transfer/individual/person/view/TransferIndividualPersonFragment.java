package ru.bankproj.android.mobilebank.main.transfer.individual.person.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.app.ValidFields;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.dialogs.InfoDialog;
import ru.bankproj.android.mobilebank.main.payment.selectcardoraccount.view.SelectCardOrAccountActivity;
import ru.bankproj.android.mobilebank.main.transfer.check.view.TransferCheckActivity;
import ru.bankproj.android.mobilebank.main.transfer.individual.person.ITransferIndividualPersonContract;
import ru.bankproj.android.mobilebank.utils.AmountDigitsInputFilter;
import ru.bankproj.android.mobilebank.utils.CheckUtils;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.view.EnableButton;
import ru.bankproj.android.mobilebank.view.ErrorLabelContainer;
import ru.bankproj.android.mobilebank.view.SourceProductView;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static ru.bankproj.android.mobilebank.utils.UI.crossFade;

/**
 * Created by maxmobiles on 27.12.2017.
 */

public class TransferIndividualPersonFragment extends BaseMvpFragment implements ITransferIndividualPersonContract.View {

    public static final String SOURCE_PRODUCT = "TransferIndividualPersonFragment.SOURCE_PRODUCT";
    public static final String TEMPLATE_MODE = "TransferIndividualPersonFragment.TEMPLATE_MODE";

    @BindView(R.id.ll_source_container)
    LinearLayout llSourceContainer;
    @BindView(R.id.rl_select_product)
    RelativeLayout rlSelectProduct;
    @BindView(R.id.v_source_product)
    SourceProductView sourceProductView;
    @BindView(R.id.el_name)
    ErrorLabelContainer elName;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.el_account_number_receiver)
    ErrorLabelContainer elAccountNumberRecipient;
    @BindView(R.id.et_account_number_receiver)
    EditText etAccountNumberRecipient;
    @BindView(R.id.el_bik)
    ErrorLabelContainer elBik;
    @BindView(R.id.et_bik)
    EditText etBik;
    @BindView(R.id.el_transfer_target)
    ErrorLabelContainer elTransferTarget;
    @BindView(R.id.et_transfer_target)
    EditText etTransferTarget;
    @BindView(R.id.et_limit_assignment)
    EditText etAmount;
    @BindView(R.id.tv_currency_sign)
    TextView tvCurrencySign;
    @BindView(R.id.btn_further)
    EnableButton btnFurther;
    @BindView(R.id.tv_select_card_text)
    TextView tvSelectCardText;
    //Template views
    @BindView(R.id.ll_template_name_container)
    LinearLayout llTemplateNameContainer;
    @BindView(R.id.el_template_name)
    ErrorLabelContainer elTemplateName;
    @BindView(R.id.et_template_name)
    EditText etTemplateName;

    @Inject
    ITransferIndividualPersonContract.Presenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs() {
        if (getArguments() != null) {
            mPresenter.setArguments(getArguments().getParcelable(SOURCE_PRODUCT),
                    getArguments().getBoolean(TEMPLATE_MODE));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.onCreate(savedInstanceState);
        tvSelectCardText.setText(FieldConverter.getString(R.string.debit_select_card_from_title));
        etAmount.setFilters(new InputFilter[]{new AmountDigitsInputFilter()});
    }

    @Override
    public void initView(boolean templateMode) {
        llTemplateNameContainer.setVisibility(templateMode ? VISIBLE : GONE);

        List<Observable<Boolean>> listObservable = new ArrayList<>();
        if (templateMode) {
            listObservable.add(Utils.getRegexObservable(etTemplateName, ValidFields.TEMPLATE_NAME_REGEX, getString(R.string.transfer_invalid_value)));
        }
        listObservable.add(Utils.getRegexObservable(etAccountNumberRecipient, ValidFields.ACCOUNT_NUMBER_REGEX, getString(R.string.transfer_error_account)));
        listObservable.add(Utils.getRegexObservable(etBik, ValidFields.BIC_REGEX, getString(R.string.transfer_error_bic)));
        listObservable.add(Utils.getMultiplePredicatesObservable(etTransferTarget, new ArrayList<CheckUtils.CheckEditTextPredicate>() {
            {
                add(new CheckUtils.RegexPredicate(ValidFields.TRANSFER_TARGET_REGEX, getString(R.string.transfer_invalid_value)));
                add(new CheckUtils.RegexPredicate(ValidFields.SPECIFY_TRANSFER_TARGET_REGEX, getString(R.string.transfer_invalid_value)));
            }
        }));
        listObservable.add(Utils.getRegexObservable(etName, ValidFields.TARGET_NAME_REGEX, getString(R.string.transfer_invalid_value)));
        etName.setOnFocusChangeListener((view, hasFocus) -> {
            if (!hasFocus) {
                etName.setText(etName.getText().toString().trim());
            }
        });

        etAccountNumberRecipient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0 && !(editable.charAt(0) == '3' || editable.charAt(0) == '4')) {
                    editable.replace(0, 1, "");
                }
            }
        });
        mPresenter.setListObservableField(listObservable);
    }

    @Override
    public void initProductData(BaseProduct baseProduct) {
        if (baseProduct != null) {
            if (sourceProductView.getVisibility() == GONE) {
                crossFade(sourceProductView, rlSelectProduct);
            }
            sourceProductView.initProductData(mActivity, baseProduct);
        } else {
            crossFade(rlSelectProduct, sourceProductView);
        }
    }

    @OnClick({R.id.rl_select_product, R.id.v_source_product, R.id.btn_further})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_select_product:
            case R.id.v_source_product:
                FilterProductData data = new FilterProductData();
                data.setFilterMode(FilterProductData.CARDS);
                Bundle bundle = new Bundle();
                bundle.putParcelable(SelectCardOrAccountActivity.FILTER_PRODUCTS, data);
                getScreenCreator().startActivity(this, mActivity, SelectCardOrAccountActivity.class, bundle, Constants.START_SELECT_PRODUCT_ACTIVITY);
                break;
            case R.id.btn_further:
                break;
        }
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_transfer_individual_person;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.START_SELECT_PRODUCT_ACTIVITY:
                if (resultCode == Constants.RESULT_SELECT_PRODUCT_ACTIVITY) {
                    if (data.getExtras() != null) {
                        if (data.getExtras().getParcelable(Constants.SELECTED_CARD_CONTENT) != null) {
                            mPresenter.setSelectedProduct(data.getExtras().getParcelable(Constants.SELECTED_CARD_CONTENT));
                        } else {
                            mPresenter.setSelectedProduct(data.getExtras().getParcelable(Constants.SELECTED_ACCOUNT_CONTENT));
                        }
                    }
                } else if (resultCode == Constants.RESULT_LOGOUT) {
                    unAuthorized();
                }
                break;
            case Constants.START_TRANSFER_CHECK_ACTIVITY:
                if (resultCode == Constants.RESULT_LOGOUT) {
                    unAuthorized();
                }
                break;
        }
    }

    @Override
    public void openTransferCheckScreen(CheckTransferData checkTransferData, boolean templateMode) {
        if (templateMode) {
            checkTransferData.setNewTemplateName(etTemplateName.getText().toString());
        }
        getScreenCreator().startActivity(this, mActivity, TransferCheckActivity.class,
                createTransferCheckBundle(checkTransferData, templateMode), Constants.START_TRANSFER_CHECK_ACTIVITY);
    }

    @Override
    public void showAccountNumberError(boolean showError) {
        if (showError) {
            elAccountNumberRecipient.setError(getString(R.string.transfer_error_bic_and_account));
            elBik.setError(getString(R.string.transfer_error_bic_and_account));
        } else {
            elAccountNumberRecipient.clearError();
            elBik.clearError();
        }
    }

    @Override
    public void showCardLimitDialog() {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(R.string.limit_transfer_card_dialog_text));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @Override
    public void fieldValidationSuccess(Boolean success) {
        btnFurther.enable(success);
    }

    @Override
    public void showNotValidAmountMessage(int validationValue) {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(validationValue));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @NonNull
    private Bundle createTransferCheckBundle(CheckTransferData checkTransferData, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferCheckActivity.CHECK_TRANSFER_DATA, checkTransferData);
        bundle.putBoolean(TransferCheckActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    @OnClick(R.id.btn_further)
    void onNextClick() {
        mPresenter.onNextClicked(etAmount.getText().toString(), etName.getText().toString(),
                etAccountNumberRecipient.getText().toString(), etBik.getText().toString(), etTransferTarget.getText().toString());
    }
}
