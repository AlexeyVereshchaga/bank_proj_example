package ru.bankproj.android.mobilebank.main.transfer.success;

import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;

/**
 * Created by Alexey Vereshchaga on 27.12.17.
 */

public interface ITransferSuccessContract {

    interface Presenter extends IBaseMvpPresenter<View> {
        void completeClicked();

        void sendReceiptClicked();

        void sendMail(String email);

        void saveTemplate(String name);
    }

    interface View extends IBaseProgressView {
        void showSendEmailDialog();

        void showTemplateSavedMessage();

        void showMailSuccessMessage();

        void showCreateTemplate(boolean show);
    }
}
