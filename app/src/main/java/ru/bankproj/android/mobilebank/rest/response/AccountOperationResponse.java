package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.operationhistory.AccountOperationHistory;

/**
 * Created by j7ars on 13.12.2017.
 */

public class AccountOperationResponse extends BaseResponse{

    @SerializedName("history")
    private ArrayList<AccountOperationHistory> historyList;

    public ArrayList<AccountOperationHistory> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(ArrayList<AccountOperationHistory> historyList) {
        this.historyList = historyList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.historyList);
    }

    public AccountOperationResponse() {
    }

    protected AccountOperationResponse(Parcel in) {
        super(in);
        this.historyList = in.createTypedArrayList(AccountOperationHistory.CREATOR);
    }

    public static final Creator<AccountOperationResponse> CREATOR = new Creator<AccountOperationResponse>() {
        @Override
        public AccountOperationResponse createFromParcel(Parcel source) {
            return new AccountOperationResponse(source);
        }

        @Override
        public AccountOperationResponse[] newArray(int size) {
            return new AccountOperationResponse[size];
        }
    };
}
