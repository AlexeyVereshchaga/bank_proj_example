package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.atm.ATM;

/**
 * Created by j7ars on 28.11.2017.
 */

public class ATMResponse extends BaseResponse {

    @SerializedName("points")
    private ArrayList<ATM> atmList;

    public ArrayList<ATM> getAtmList() {
        return atmList;
    }

    public void setAtmList(ArrayList<ATM> atmList) {
        this.atmList = atmList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.atmList);
    }

    public ATMResponse() {
    }

    protected ATMResponse(Parcel in) {
        super(in);
        this.atmList = in.createTypedArrayList(ATM.CREATOR);
    }

    public static final Creator<ATMResponse> CREATOR = new Creator<ATMResponse>() {
        @Override
        public ATMResponse createFromParcel(Parcel source) {
            return new ATMResponse(source);
        }

        @Override
        public ATMResponse[] newArray(int size) {
            return new ATMResponse[size];
        }
    };
}
