package ru.bankproj.android.mobilebank.main.transfer.template.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.product.CardLimit;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.templates.Template;
import ru.bankproj.android.mobilebank.main.transfer.template.ITransferTemplateContract;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.rest.RequestParams;
import ru.bankproj.android.mobilebank.rest.response.CardLimitResponse;
import ru.bankproj.android.mobilebank.rest.response.CheckPaymentResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferResponse;
import ru.bankproj.android.mobilebank.utils.RequestConstants;

import static ru.bankproj.android.mobilebank.app.Constants.TAG;

/**
 * Created by Alexey Vereshchaga on 16.01.18.
 */

public class TransferTemplatePresenter extends BaseEventBusPresenter<ITransferTemplateContract.View>
        implements ITransferTemplateContract.Presenter, IBaseResponseCallback {

    private static final String TEMPLATE = "TransferTemplatePresenter.TEMPLATE";

    private Template template;
    private String amount;
    private RequestManager requestManager;
    private Disposable disposable;
    private CheckPaymentResponse templateCheckResponse;
    private Double productLimit;

    @Inject
    public TransferTemplatePresenter(RequestManager requestManager) {
        this.requestManager = requestManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        }
        mDataManager.getTemplateWithTargetProduct(template);
        getMvpView().showTemplate(template);
        checkSourceCard();
    }

    private void checkSourceCard() {
        if (template.getSourceProduct() == null) {
            getMvpView().showEmptySourceCardDialog();
        } else if (template.getSourceProduct() instanceof Card) {
            switch (((Card) template.getSourceProduct()).getStatus()) {
                case Card.BLOCKED_FRAUDULENT_ACTIONS:
                case Card.CARD_BLOCKED:
                case Card.INVALID_PIN_LIMIT_EXCEEDED:
                case Card.STATUS_UNKNOWN:
                    getMvpView().showBlockedSourceCardDialog();
                    break;
                case Card.CARD_EXPIRED:
                    getMvpView().showExpiredSourceCardDialog();
                    break;
                case Card.TEMPORARY_BLOCKED:
                    getMvpView().showTemporaryBlockedCardDialog();
                    break;
                case Card.VALID_CARD:
                    getCardLimitsRequest(template.getSourceProduct());
                    break;
            }
        }
    }

    private void getCardLimitsRequest(BaseProduct sourceProduct) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                productLimit = null;
                disposable = requestManager.getCardLimits(sessionId, sourceProduct.getId(),
                        new BaseRequestController(mEventBusController, Action.GET_CARD_LIMITS_ACTION, getMvpView().getClassUniqueDeviceId()));
            }
        });
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            template = (Template) params[0];
            amount = String.valueOf(template.getAmount());
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(TEMPLATE, template);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        template = savedInstanceState.getParcelable(TEMPLATE);
    }

    @Override
    public void onNextClicked(String amount) {
        this.amount = amount;
        int validationValue = validateAmount(amount);
        if (validationValue == -1) {
            checkTemplateRequest();
        } else {
            getMvpView().showNotValidAmountMessage(validationValue);
        }
    }

    private int validateAmount(String amountStr) {
        Float amount = -1f;
        try {
            amount = Float.valueOf(amountStr);
        } catch (NumberFormatException e) {
            Log.e(TAG, "validateAmount: ", e);
        }
        if (productLimit != null && productLimit < amount) {
            return R.string.payment_restriction_exceeded;
        }
        if (amount <= 0) {
            return R.string.amount_error_message;
        }
        return -1;
    }

    private TransferResponse createTransferResponse(CheckPaymentResponse checkPaymentResponse) {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setConfirmationStrategy(checkPaymentResponse.getConfirmationStrategy());
        transferResponse.setConfirmReq(checkPaymentResponse.getConfirmReq());
        transferResponse.setCurrency(checkPaymentResponse.getCurrency());
        transferResponse.setDetailList(checkPaymentResponse.getDetails());
        transferResponse.setFee(checkPaymentResponse.getFee());
        transferResponse.setTransRef(checkPaymentResponse.getTransRef());
        return new TransferResponse();
    }

    private void checkTemplateRequest() {
        undisposable(disposable);
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                disposable = requestManager.checkTemplate(
                        sessionId,
                        RequestParams.getConfirmationStrategiesParams(ConfirmRequestDescriptor.FROM_TEMPLATE, template.getUid(), amount,
                                template.getCurrency(), null, -1, -1, -1),
                        template.getUid(), template.getCurrency(), amount, mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                        new BaseRequestController(mEventBusController, Action.CHECK_TEMPLATE, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case START_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TEMPLATE:
                    case Action.GET_CARD_LIMITS_ACTION:
                        getMvpView().startProgressDialog();
                        break;
                }

                break;
            case SUCCESS_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TEMPLATE:
                        undisposable(disposable);
                        Response<CheckPaymentResponse> response = (Response<CheckPaymentResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), response, this);
                        break;
                    case Action.GET_CARD_LIMITS_ACTION:
                        undisposable(disposable);
                        Response<CardLimitResponse> baseResponse = (Response<CardLimitResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                        break;
                }
                break;
            case FAIL_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TEMPLATE:
                    case Action.GET_CARD_LIMITS_ACTION:
                        undisposable(disposable);
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        break;
                }
                break;
        }
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case Action.CHECK_TEMPLATE:
                getMvpView().completeProgressDialog();
                templateCheckResponse = (CheckPaymentResponse) data.getValue();
                getMvpView().openTransferCheckScreen(template, createTransferResponse(templateCheckResponse), amount);
                break;
            case Action.GET_CARD_LIMITS_ACTION:
                getMvpView().completeProgressDialog();
                CardLimitResponse cardLimitResponse = (CardLimitResponse) data.getValue();
                if (cardLimitResponse != null) {
                    ArrayList<CardLimit> cardLimits = cardLimitResponse.getLimitList();
                    for (CardLimit cardLimit :
                            cardLimits) {
                        double localLimit = cardLimit.getAmount() - cardLimit.getUsedAmount();
                        if (productLimit == null || productLimit > localLimit) {
                            productLimit = localLimit;
                        }
                        if (productLimit <= 0) {
                            getMvpView().showTemplateLimitSourceCardDialog();
                            return;
                        }
                        checkTemplateTargetProduct();
                    }
                }
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                getMvpView().completeProgressDialog();
                getMvpView().unAuthorized();
                break;
            case BAD_REQUEST_ERROR:
            case DEFAULT_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
            default:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
        }
    }

    private void checkTemplateTargetProduct() {
        if (template != null) {
            switch (template.getTargetType()) {
                case RequestConstants.EXTERNAL_CARD:
                    break;
                default:
                    if (template.getTargetId() != null) {
                        if (template.getTargetProduct() == null) {
                            getMvpView().showTargetProductBlockedDialog();
                        } else {
                            if (template.getTargetProduct() instanceof Account) {
                                switch (((Account) template.getTargetProduct()).getStatus()) {
                                    case Account.TEMPORARY_BLOCKED:
                                        getMvpView().showTargetAccountTemporaryBlockedDialog();
                                        break;
                                    case Account.ACCOUNT_BLOCKED:
                                        getMvpView().showTargetAccountBlockedDialog();
                                        break;
                                }
                            } else if (template.getTargetProduct() instanceof Card) {
                                switch (((Card) template.getTargetProduct()).getStatus()) {
                                    case Card.BLOCKED_FRAUDULENT_ACTIONS:
                                    case Card.CARD_BLOCKED:
                                    case Card.CARD_EXPIRED:
                                    case Card.INVALID_PIN_LIMIT_EXCEEDED:
                                    case Card.STATUS_UNKNOWN:
                                        getMvpView().showBlockedTargetCardDialog();
                                        break;
                                    case Card.TEMPORARY_BLOCKED:
                                        getMvpView().showTemporaryBlockedTargetCardDialog();
                                        break;
                                    case Card.VALID_CARD:
                                        break;
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }
}
