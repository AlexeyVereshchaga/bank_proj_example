package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;

/**
 * Created by j7ars on 25.12.2017.
 */

public class CustomPaymentRequestResponse extends BaseResponse{

    @SerializedName("flash")
    private String flash;
    @SerializedName("payload")
    private String payload;

    public String getFlash() {
        return flash;
    }

    public void setFlash(String flash) {
        this.flash = flash;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.flash);
        dest.writeString(this.payload);
    }

    public CustomPaymentRequestResponse() {
    }

    protected CustomPaymentRequestResponse(Parcel in) {
        super(in);
        this.flash = in.readString();
        this.payload = in.readString();
    }

    public static final Creator<CustomPaymentRequestResponse> CREATOR = new Creator<CustomPaymentRequestResponse>() {
        @Override
        public CustomPaymentRequestResponse createFromParcel(Parcel source) {
            return new CustomPaymentRequestResponse(source);
        }

        @Override
        public CustomPaymentRequestResponse[] newArray(int size) {
            return new CustomPaymentRequestResponse[size];
        }
    };
}
