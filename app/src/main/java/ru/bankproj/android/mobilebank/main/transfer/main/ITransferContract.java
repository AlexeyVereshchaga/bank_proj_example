package ru.bankproj.android.mobilebank.main.transfer.main;

import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpView;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public interface ITransferContract {

    interface View extends IBaseMvpView {
        void showTransferOwnAccountScreen(BaseProduct baseProduct, boolean templateMode);

        void showTransferClientGpbScreen(BaseProduct baseProduct, boolean templateMode);

        void showTransferOtherClientScreen(BaseProduct baseProduct, boolean templateMode);

        void showRepaymentCreditCardScreen(BaseProduct baseProduct, boolean templateMode);

        void showRepaymentCreditAccountScreen(BaseProduct baseProduct, boolean templateMode);

        void showTransferIndividualScreen(BaseProduct baseProduct, boolean templateMode);

        void showTransferLegalScreen(BaseProduct baseProduct, boolean templateMode);

        void showTransferBudgetScreen(BaseProduct baseProduct, boolean templateMode);

        void disableRepaymentAccounts();

        void disableRepaymentCards();
    }

    interface Presenter extends IBaseMvpPresenter<View> {
        void ownAccountClicked();

        void clientGpbClicked();

        void otherClientClicked();

        void creditCardClicked();

        void creditAccountClicked();

        void individualClicked();

        void legalClicked();

        void budgetClicked();
    }
}
