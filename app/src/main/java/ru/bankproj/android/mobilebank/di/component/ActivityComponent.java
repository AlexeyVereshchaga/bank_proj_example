package ru.bankproj.android.mobilebank.di.component;

import dagger.Subcomponent;
import ru.bankproj.android.mobilebank.di.module.ActivityModule;
import ru.bankproj.android.mobilebank.di.module.PresenterModule;
import ru.bankproj.android.mobilebank.di.scope.PerActivity;
import ru.bankproj.android.mobilebank.main.dataloader.view.DataLoaderActivity;
import ru.bankproj.android.mobilebank.menu.drawermenu.view.DrawerMenuActivity;
import ru.bankproj.android.mobilebank.splash.view.SplashScreenActivity;

/**
 * Created by j7ars on 25.10.2017.
 */
@PerActivity
@Subcomponent(modules = {ActivityModule.class, PresenterModule.class})
public interface ActivityComponent {

    void inject(SplashScreenActivity splashScreenActivity);

    void inject(DrawerMenuActivity drawerMenuActivity);

    void inject(DataLoaderActivity dataLoaderActivity);

}
