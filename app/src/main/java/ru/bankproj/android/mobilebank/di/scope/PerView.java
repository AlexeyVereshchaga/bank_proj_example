package ru.bankproj.android.mobilebank.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by j7ars on 25.10.2017.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerView {
}
