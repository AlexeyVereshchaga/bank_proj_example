package ru.bankproj.android.mobilebank.main.transfer.budget.organization.main;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SelectBudgetOrgData;

/**
 * Created by j7ars on 28.12.2017.
 */

public interface ITransferBudgetOrganizationContract {

    interface View extends IBaseProgressView {

        void initView(boolean templateMode);

        void initProductData(BaseProduct baseProduct);

        void showDate(String date);

        void setDocumentType(String documentType);

        void setTransferType(String transferType);

        void setNalogPeriod(String nalogPeriod);

        void setTransferReason(String transferReason);

        void setPayerStatus(String payerStatus);

        void openTransferCheckScreen(CheckTransferData checkTransferData, boolean templateMode);

        void showAccountNumberError(boolean showError);

        void showCardLimitDialog();

        void fieldValidationSuccess(Boolean success);

        void showNotValidAmountMessage(int validationValue);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void setListObservableField(List<Observable<Boolean>> observables);

        void setSelectedProduct(BaseProduct baseProduct);

        void setSelectedBudgetOrgData(SelectBudgetOrgData selectedBudgetOrgData);

        void setDocumentDate(Date date);

        void onNextClicked(String amount, String customerInn, String receiverName, String receiverInn, String receiverKpp, String receiverAccount,
                           String receiverBic, String transferTarget, String uin, String cbc, String oktm, String documentDate, String documentNumber,
                           String custCode, String taxPeriodParam2, String taxPeriodParam3, String passportDeal);
    }
}
