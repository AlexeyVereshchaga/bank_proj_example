package ru.bankproj.android.mobilebank.main.transfer.own.account.view;

import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.activity.BaseBackContainerActivity;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public class TransferOwnAccountActivity extends BaseBackContainerActivity {

    public static final String TRANSFER_DESCRIPTION = "TransferOwnAccountActivity.TRANSFER_DESCRIPTION";
    public static final String SOURCE_PRODUCT = "TransferOwnAccountActivity.SOURCE_PRODUCT";
    public static final String TARGET_PRODUCT = "TransferOwnAccountActivity.TARGET_PRODUCT";
    public static final String TEMPLATE_MODE = "TransferOwnAccountActivity.TEMPLATE_MODE";

    private String descriptionTransfer;
    private BaseProduct sourceProduct;
    private BaseProduct targetProduct;
    private boolean templateMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.transfer_title));
        getExtras();
        if (savedInstanceState == null) {
            openFragment();
        }
    }

    private void getExtras() {
        if (getIntent().getExtras() != null) {
            sourceProduct = getIntent().getParcelableExtra(SOURCE_PRODUCT);
            descriptionTransfer = getIntent().getStringExtra(TRANSFER_DESCRIPTION);
            targetProduct = getIntent().getParcelableExtra(TARGET_PRODUCT);
            templateMode = getIntent().getBooleanExtra(TEMPLATE_MODE, false);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(SOURCE_PRODUCT, sourceProduct);
        outState.putString(TRANSFER_DESCRIPTION, descriptionTransfer);
        outState.putParcelable(TARGET_PRODUCT, targetProduct);
        outState.putBoolean(TEMPLATE_MODE, templateMode);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        sourceProduct = savedInstanceState.getParcelable(SOURCE_PRODUCT);
        descriptionTransfer = savedInstanceState.getString(TRANSFER_DESCRIPTION);
        targetProduct = savedInstanceState.getParcelable(TARGET_PRODUCT);
        templateMode = savedInstanceState.getBoolean(TEMPLATE_MODE);
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container, getScreenCreator().newInstance(TransferOwnAccountFragment.class, createBundle())).commitAllowingStateLoss();
    }

    private Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferOwnAccountFragment.SOURCE_PRODUCT, sourceProduct);
        bundle.putString(TransferOwnAccountFragment.DESCRIPTION_TRANSFER, descriptionTransfer);
        bundle.putParcelable(TransferOwnAccountFragment.TARGET_PRODUCT, targetProduct);
        bundle.putBoolean(TransferOwnAccountFragment.TEMPLATE_MODE, templateMode);
        return bundle;
    }

}
