package ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.dataclasses.transfer.Subsidiary;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.ISubsidiaryContract;
import ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.adapter.SubsidiaryAdapter;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

/**
 * SubsidiaryFragment.java 20.01.2018
 * Class description
 *
 * @author Alex.
 */

public class SubsidiaryFragment extends BaseMvpFragment implements ISubsidiaryContract.View {

    @BindView(R.id.rv_subsidiaries)
    RecyclerView rvSubsidiaries;

    @Inject
    ISubsidiaryContract.Presenter presenter;

    private SubsidiaryAdapter subsidiaryAdapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initAdapters();
        presenter.onCreate(savedInstanceState);
    }

//    private void getArgs() {
//        if(getArguments() != null){
//            presenter.setArguments(getArguments().getInt(SELECT_TYPE));
//        }
//    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        presenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    private void initAdapters(){
        subsidiaryAdapter = new SubsidiaryAdapter();
        subsidiaryAdapter.setOnItemClickListener(position -> presenter.subsidiariesSelectItem(position));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        rvSubsidiaries.setLayoutManager(layoutManager);
        rvSubsidiaries.setAdapter(subsidiaryAdapter);
    }

    @Override
    public void initData(List<Subsidiary> selectDataList) {
        subsidiaryAdapter.setData(selectDataList);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        presenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_subsidiaries;
    }

}
