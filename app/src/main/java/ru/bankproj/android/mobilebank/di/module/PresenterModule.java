package ru.bankproj.android.mobilebank.di.module;

import dagger.Binds;
import dagger.Module;
import ru.bankproj.android.mobilebank.gcm.registration.IRegistrationServiceContract;
import ru.bankproj.android.mobilebank.gcm.registration.RegistrationServicePresenter;
import ru.bankproj.android.mobilebank.main.atm.atmdetail.IATMDetailContract;
import ru.bankproj.android.mobilebank.main.atm.atmdetail.presenter.ATMDetailPresenter;
import ru.bankproj.android.mobilebank.main.atm.filteratm.IAtmFilterContract;
import ru.bankproj.android.mobilebank.main.atm.filteratm.presenter.AtmFilterPresenter;
import ru.bankproj.android.mobilebank.main.atm.list.IAtmListContract;
import ru.bankproj.android.mobilebank.main.atm.list.presenter.AtmListPresenter;
import ru.bankproj.android.mobilebank.main.atm.main.IAtmContract;
import ru.bankproj.android.mobilebank.main.atm.main.presenter.AtmPresenter;
import ru.bankproj.android.mobilebank.main.atm.map.IAtmMapContract;
import ru.bankproj.android.mobilebank.main.atm.map.presenter.AtmMapPresenter;
import ru.bankproj.android.mobilebank.main.authorization.initpassword.IInitPasswordContract;
import ru.bankproj.android.mobilebank.main.authorization.initpassword.presenter.InitPasswordPresenter;
import ru.bankproj.android.mobilebank.main.authorization.resetpassword.IResetPassword;
import ru.bankproj.android.mobilebank.main.authorization.resetpassword.presenter.ResetPasswordPresenter;
import ru.bankproj.android.mobilebank.main.authorization.signin.ISignInContract;
import ru.bankproj.android.mobilebank.main.authorization.signin.presenter.SignInPresenter;
import ru.bankproj.android.mobilebank.main.authorization.signup.ISignUpContract;
import ru.bankproj.android.mobilebank.main.authorization.signup.presenter.SignUpPresenter;
import ru.bankproj.android.mobilebank.main.authorization.smscode.ISmsCodeContract;
import ru.bankproj.android.mobilebank.main.authorization.smscode.presenter.SmsCodePresenter;
import ru.bankproj.android.mobilebank.main.block.atm.IAtmBlockContract;
import ru.bankproj.android.mobilebank.main.block.atm.presenter.AtmBlockPresenter;
import ru.bankproj.android.mobilebank.main.block.main.IBlockManagerContract;
import ru.bankproj.android.mobilebank.main.block.main.presenter.BlockManagerPresenter;
import ru.bankproj.android.mobilebank.main.block.myproduct.myaccount.IMyAccountBlockContract;
import ru.bankproj.android.mobilebank.main.block.myproduct.myaccount.presenter.MyAccountBlockPresenter;
import ru.bankproj.android.mobilebank.main.block.myproduct.mycard.IMyCardBlockContract;
import ru.bankproj.android.mobilebank.main.block.myproduct.mycard.presenter.MyCardBlockPresenter;
import ru.bankproj.android.mobilebank.main.block.myproduct.mycredit.IMyCreditBlockContract;
import ru.bankproj.android.mobilebank.main.block.myproduct.mycredit.presenter.MyCreditBlockPresenter;
import ru.bankproj.android.mobilebank.main.block.myproduct.mydebit.IMyDebitBlockContract;
import ru.bankproj.android.mobilebank.main.block.myproduct.mydebit.presenter.MyDebitBlockPresenter;
import ru.bankproj.android.mobilebank.main.block.news.INewsBlockContract;
import ru.bankproj.android.mobilebank.main.block.news.presenter.NewsBlockPresenter;
import ru.bankproj.android.mobilebank.main.block.offers.IOffersBlockContract;
import ru.bankproj.android.mobilebank.main.block.offers.presenter.OffersBlockPresenter;
import ru.bankproj.android.mobilebank.main.block.template.ITemplateBlockContract;
import ru.bankproj.android.mobilebank.main.block.template.presenter.TemplateBlockPresenter;
import ru.bankproj.android.mobilebank.main.contacts.IContactContract;
import ru.bankproj.android.mobilebank.main.contacts.presenter.ContactPresenter;
import ru.bankproj.android.mobilebank.main.dataloader.IDataLoaderContract;
import ru.bankproj.android.mobilebank.main.dataloader.presenter.DataLoaderPresenter;
import ru.bankproj.android.mobilebank.main.fine.document.add.IFineAddDocumentContract;
import ru.bankproj.android.mobilebank.main.fine.document.add.presenter.FineAddDocumentPresenter;
import ru.bankproj.android.mobilebank.main.fine.document.main.IFineDocumentContract;
import ru.bankproj.android.mobilebank.main.fine.document.main.presenter.FineDocumentPresenter;
import ru.bankproj.android.mobilebank.main.fine.finedetail.IFineDetailContract;
import ru.bankproj.android.mobilebank.main.fine.finedetail.presenter.FineDetailPresenter;
import ru.bankproj.android.mobilebank.main.fine.payment.IFinePaymentContract;
import ru.bankproj.android.mobilebank.main.fine.payment.presenter.FinePaymentPresenter;
import ru.bankproj.android.mobilebank.main.fine.selectedfine.ISelectedFineContract;
import ru.bankproj.android.mobilebank.main.fine.selectedfine.presenter.SelectedFinePresenter;
import ru.bankproj.android.mobilebank.main.fine.selectfine.ISelectFineContract;
import ru.bankproj.android.mobilebank.main.fine.selectfine.presenter.SelectFinePresenter;
import ru.bankproj.android.mobilebank.main.fine.successpayment.ISuccessFinePaymentContract;
import ru.bankproj.android.mobilebank.main.fine.successpayment.presenter.SuccessFinePaymentPresenter;
import ru.bankproj.android.mobilebank.main.fine.uid.IFineUinContract;
import ru.bankproj.android.mobilebank.main.fine.uid.presenter.FineUinPresenter;
import ru.bankproj.android.mobilebank.main.news.INewsListContract;
import ru.bankproj.android.mobilebank.main.news.presenter.NewsListPresenter;
import ru.bankproj.android.mobilebank.main.notification.detail.INotificationDetailContract;
import ru.bankproj.android.mobilebank.main.notification.detail.presenter.NotificationDetailPresenter;
import ru.bankproj.android.mobilebank.main.notification.list.INotificationListContract;
import ru.bankproj.android.mobilebank.main.notification.list.presenter.NotificationListPresenter;
import ru.bankproj.android.mobilebank.main.offers.IOffersListContract;
import ru.bankproj.android.mobilebank.main.offers.presenter.OffersListPresenter;
import ru.bankproj.android.mobilebank.main.payment.categories.IPaymentCategoriesContract;
import ru.bankproj.android.mobilebank.main.payment.categories.presenter.PaymentCategoriesPresenter;
import ru.bankproj.android.mobilebank.main.payment.payment.IPaymentExecuteContract;
import ru.bankproj.android.mobilebank.main.payment.payment.presenter.PaymentExecutePresenter;
import ru.bankproj.android.mobilebank.main.payment.paymentconfirm.IPaymentConfirmContract;
import ru.bankproj.android.mobilebank.main.payment.paymentconfirm.presenter.PaymentConfirmPresenter;
import ru.bankproj.android.mobilebank.main.payment.paymentschedule.IPaymentScheduleContract;
import ru.bankproj.android.mobilebank.main.payment.paymentschedule.presenter.CreditPaymentSchedulePresenter;
import ru.bankproj.android.mobilebank.main.payment.paymentsuccess.IPaymentSuccessContract;
import ru.bankproj.android.mobilebank.main.payment.paymentsuccess.presenter.PaymentSuccessPresenter;
import ru.bankproj.android.mobilebank.main.payment.providers.IPaymentProvidersContract;
import ru.bankproj.android.mobilebank.main.payment.providers.presenter.PaymentProvidersPresenter;
import ru.bankproj.android.mobilebank.main.payment.selectcardoraccount.ISelectCardOrAccountContract;
import ru.bankproj.android.mobilebank.main.payment.selectcardoraccount.presenter.SelectCardOrAccountPresenter;
import ru.bankproj.android.mobilebank.main.payment.selectfield.ISelectFieldContract;
import ru.bankproj.android.mobilebank.main.payment.selectfield.presenter.SelectFieldPresenter;
import ru.bankproj.android.mobilebank.main.payment.selectregions.IPaymentCategoriesSelectRegionsContract;
import ru.bankproj.android.mobilebank.main.payment.selectregions.presenter.PaymentCategoriesSelectRegionsPresenter;
import ru.bankproj.android.mobilebank.main.product.account.account.about.IAccountAboutContract;
import ru.bankproj.android.mobilebank.main.product.account.account.about.presenter.AccountAboutPresenter;
import ru.bankproj.android.mobilebank.main.product.account.account.accountdetail.IAccountDetailContract;
import ru.bankproj.android.mobilebank.main.product.account.account.accountdetail.presenter.AccountDetailPresenter;
import ru.bankproj.android.mobilebank.main.product.account.accountoperation.detail.IAccountOperationDetailContract;
import ru.bankproj.android.mobilebank.main.product.account.accountoperation.detail.presenter.AccountOperationDetailPresenter;
import ru.bankproj.android.mobilebank.main.product.account.accountoperation.list.IAccountOperationHistoryListContract;
import ru.bankproj.android.mobilebank.main.product.account.accountoperation.list.presenter.AccountOperationHistoryListPresenter;
import ru.bankproj.android.mobilebank.main.product.account.credit.about.ICreditAboutContract;
import ru.bankproj.android.mobilebank.main.product.account.credit.about.presenter.CreditAboutPresenter;
import ru.bankproj.android.mobilebank.main.product.account.credit.creditdetail.ICreditDetailContract;
import ru.bankproj.android.mobilebank.main.product.account.credit.creditdetail.presenter.CreditDetailPresenter;
import ru.bankproj.android.mobilebank.main.product.account.debit.about.IDebitAboutContract;
import ru.bankproj.android.mobilebank.main.product.account.debit.about.presenter.DebitAboutPresenter;
import ru.bankproj.android.mobilebank.main.product.account.debit.debitdetail.IDebitDetailContract;
import ru.bankproj.android.mobilebank.main.product.account.debit.debitdetail.presenter.DebitDetailPresenter;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.agreement.IDebitAgreementContract;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.agreement.presenter.DebitAgreementPresenter;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.conditions.ISelectConditionsContract;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.conditions.presenter.SelectConditionsPresenter;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.confirm.open.debit.IConfirmOpenDebitContract;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.confirm.open.debit.presenter.ConfirmOpenDebitPresenter;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.main.IOpenDebitContract;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.main.presenter.OpenDebitPresenter;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.success.ISuccessOpenDebitContract;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.success.presenter.SuccessOpenDebitPresenter;
import ru.bankproj.android.mobilebank.main.product.card.carddetail.ICardDetailContract;
import ru.bankproj.android.mobilebank.main.product.card.carddetail.presenter.CardDetailPresenter;
import ru.bankproj.android.mobilebank.main.product.card.cardlimitmanager.cardlimit.ICardLimitContract;
import ru.bankproj.android.mobilebank.main.product.card.cardlimitmanager.cardlimit.presenter.CardLimitPresenter;
import ru.bankproj.android.mobilebank.main.product.card.cardoperation.detail.ICardOperationDetailContract;
import ru.bankproj.android.mobilebank.main.product.card.cardoperation.detail.presenter.CardOperationDetailPresenter;
import ru.bankproj.android.mobilebank.main.product.card.cardoperation.list.ICardOperationHistoryListContract;
import ru.bankproj.android.mobilebank.main.product.card.cardoperation.list.presenter.CardOperationHistoryListPresenter;
import ru.bankproj.android.mobilebank.main.product.card.cardrestriction.restrictionlist.ICardRestrictionListContract;
import ru.bankproj.android.mobilebank.main.product.card.cardrestriction.restrictionlist.presenter.CardRestrictionListPresenter;
import ru.bankproj.android.mobilebank.main.product.card.cardrestriction.restrictionsettings.ICardRestrictionSettingsContract;
import ru.bankproj.android.mobilebank.main.product.card.cardrestriction.restrictionsettings.presenter.CardRestrictionSettingsPresenter;
import ru.bankproj.android.mobilebank.main.product.card.cardstatusmanager.ICardStatusManagerContract;
import ru.bankproj.android.mobilebank.main.product.card.cardstatusmanager.presenter.CardStatusManagerPresenter;
import ru.bankproj.android.mobilebank.main.product.operationfilter.IOperationFilterContract;
import ru.bankproj.android.mobilebank.main.product.operationfilter.presenter.OperationFilterPresenter;
import ru.bankproj.android.mobilebank.main.product.requsites.IRequisetesContract;
import ru.bankproj.android.mobilebank.main.product.requsites.presenter.RequisetesPresenter;
import ru.bankproj.android.mobilebank.main.product.selectcard.ISelectCardContract;
import ru.bankproj.android.mobilebank.main.product.selectcard.presenter.SelectCardPresenter;
import ru.bankproj.android.mobilebank.main.settings.attacheddevice.IAttachDeviceSettingContract;
import ru.bankproj.android.mobilebank.main.settings.attacheddevice.presenter.AttachDeviceSettingPresenter;
import ru.bankproj.android.mobilebank.main.settings.changepassword.IChangePasswordContract;
import ru.bankproj.android.mobilebank.main.settings.changepassword.presenter.ChangePasswordPresenter;
import ru.bankproj.android.mobilebank.main.settings.hidden.products.IHiddenProductsContract;
import ru.bankproj.android.mobilebank.main.settings.hidden.products.presenter.HiddenProductsPresenter;
import ru.bankproj.android.mobilebank.main.settings.main.ISettingsContract;
import ru.bankproj.android.mobilebank.main.settings.main.presenter.SettingsPresenter;
import ru.bankproj.android.mobilebank.main.settings.privacy.IPrivacyContract;
import ru.bankproj.android.mobilebank.main.settings.privacy.presenter.PrivacyPresenter;
import ru.bankproj.android.mobilebank.main.settings.push.IPushSettingContract;
import ru.bankproj.android.mobilebank.main.settings.push.presenter.PushSettingsPresenter;
import ru.bankproj.android.mobilebank.main.template.create.success.ICreateTemplateSuccessContract;
import ru.bankproj.android.mobilebank.main.template.create.success.presenter.CreateTemplateSuccessPresenter;
import ru.bankproj.android.mobilebank.main.template.templates.ITemplateListContract;
import ru.bankproj.android.mobilebank.main.template.templates.presenter.TemplateListPresenter;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.main.ITransferBudgetOrganizationContract;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.main.presenter.TransferBudgetOrganizationPresenter;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.ISelectBudgetOrgDataContract;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.presenter.SelectBudgetOrgDataPresenter;
import ru.bankproj.android.mobilebank.main.transfer.check.ITransferCheckContract;
import ru.bankproj.android.mobilebank.main.transfer.check.presenter.TransferCheckPresenter;
import ru.bankproj.android.mobilebank.main.transfer.clientgpb.ITransferClientGpbContract;
import ru.bankproj.android.mobilebank.main.transfer.clientgpb.presenter.TransferClientGpbPresenter;
import ru.bankproj.android.mobilebank.main.transfer.individual.person.ITransferIndividualPersonContract;
import ru.bankproj.android.mobilebank.main.transfer.individual.person.presenter.TransferIndividualPersonPresenter;
import ru.bankproj.android.mobilebank.main.transfer.legal.person.ITransferLegalPersonContract;
import ru.bankproj.android.mobilebank.main.transfer.legal.person.presenter.TransferLegalPersonPresenter;
import ru.bankproj.android.mobilebank.main.transfer.main.ITransferContract;
import ru.bankproj.android.mobilebank.main.transfer.main.presenter.TransferPresenter;
import ru.bankproj.android.mobilebank.main.transfer.own.account.ITransferOwnAccountContract;
import ru.bankproj.android.mobilebank.main.transfer.own.account.presenter.TransferOwnAccountPresenter;
import ru.bankproj.android.mobilebank.main.transfer.repayment.check.IRepaymentCheckContract;
import ru.bankproj.android.mobilebank.main.transfer.repayment.check.presenter.RepaymentCheckPresenter;
import ru.bankproj.android.mobilebank.main.transfer.repayment.main.IRepaymentContract;
import ru.bankproj.android.mobilebank.main.transfer.repayment.main.presenter.RepaymentPresenter;
import ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.ISubsidiaryContract;
import ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.presenter.SubsidiaryPresenter;
import ru.bankproj.android.mobilebank.main.transfer.success.ITransferSuccessContract;
import ru.bankproj.android.mobilebank.main.transfer.success.presenter.TransferSuccessPresenter;
import ru.bankproj.android.mobilebank.main.transfer.template.ITransferTemplateContract;
import ru.bankproj.android.mobilebank.main.transfer.template.presenter.TransferTemplatePresenter;
import ru.bankproj.android.mobilebank.main.webviewdetail.IWebViewDetailContract;
import ru.bankproj.android.mobilebank.main.webviewdetail.presenter.WebViewDetailPresenter;
import ru.bankproj.android.mobilebank.menu.drawermenu.IDrawerMenuContract;
import ru.bankproj.android.mobilebank.menu.drawermenu.presenter.DrawerMenuPresenter;
import ru.bankproj.android.mobilebank.menu.drawerview.IDrawerViewContract;
import ru.bankproj.android.mobilebank.menu.drawerview.presenter.DrawerViewPresenter;
import ru.bankproj.android.mobilebank.session.ISessionControlContract;
import ru.bankproj.android.mobilebank.session.SessionControlPresenter;
import ru.bankproj.android.mobilebank.splash.ISplashScreenContract;
import ru.bankproj.android.mobilebank.splash.presenter.SplashScreenPresenter;

/**
 * Created by j7ars on 25.10.2017.
 */
@Module
public abstract class PresenterModule {

    @Binds
    abstract IDrawerMenuContract.Presenter bindDrawerMenuPresenter(DrawerMenuPresenter drawerMenuPresenter);

    @Binds
    abstract IDrawerViewContract.Presenter bindDrawerViewPresenter(DrawerViewPresenter drawerViewPresenter);

    @Binds
    abstract ISplashScreenContract.Presenter bindSplashScreenPresenter(SplashScreenPresenter splashScreenPresenter);

    @Binds
    abstract ISignInContract.Presenter bindSignInPresenter(SignInPresenter signInPresenter);

    @Binds
    abstract ISignUpContract.Presenter bindSignUpPresenter(SignUpPresenter signUpPresenter);

    @Binds
    abstract ISmsCodeContract.Presenter bindSmsCodePresenter(SmsCodePresenter smsCodePresenter);

    @Binds
    abstract IInitPasswordContract.Presenter bindInitPasswordPresenter(InitPasswordPresenter initPasswordPresenter);

    @Binds
    abstract IResetPassword.Presenter bindResetPasswordPresenter(ResetPasswordPresenter resetPasswordPresenter);

    @Binds
    abstract IOffersListContract.Presenter bindOfferListPresenter(OffersListPresenter offersListPresenter);

    @Binds
    abstract IWebViewDetailContract.Presenter bindOffersDetailPresenter(WebViewDetailPresenter offerDetailPresenter);

    @Binds
    abstract INewsListContract.Presenter bindNewsPresenter(NewsListPresenter newsListPresenter);

    @Binds
    abstract INotificationListContract.Presenter bindNotificationListPresenter(NotificationListPresenter notificationListPresenter);

    @Binds
    abstract INotificationDetailContract.Presenter bindNotificationDetailPresenter(NotificationDetailPresenter notificationDetailPresenter);

    @Binds
    abstract IBlockManagerContract.Presenter bindBlockManagerPresenter(BlockManagerPresenter blockManagerPresenter);

    @Binds
    abstract IDataLoaderContract.Presenter bindProviderLoaderPresenter(DataLoaderPresenter providerLoaderPresenter);

    @Binds
    abstract ICardOperationHistoryListContract.Presenter bindCardOperationHistoryPresenter(CardOperationHistoryListPresenter lastOperationListPresenter);

    @Binds
    abstract IAccountOperationHistoryListContract.Presenter bindAccountOperationHistoryPresenter(AccountOperationHistoryListPresenter accountOperationHistoryListPresenter);

    @Binds
    abstract ICardDetailContract.Presenter bindCardDetailPresenter(CardDetailPresenter cardDetailPresenter);

    @Binds
    abstract IChangePasswordContract.Presenter bindChangePasswordPresenter(ChangePasswordPresenter changePasswordPresenter);

    @Binds
    abstract ISettingsContract.Presenter bindSettingsPresenter(SettingsPresenter settingsPresenter);

    @Binds
    abstract IPushSettingContract.Presenter bindPushSettingsPresenter(PushSettingsPresenter pushSettingsPresenter);

    @Binds
    abstract IPrivacyContract.Presenter bindPrivacySettingsPresenter(PrivacyPresenter privacyPresenter);

    @Binds
    abstract IAttachDeviceSettingContract.Presenter bindAttachDeviceSettingsPresenter(AttachDeviceSettingPresenter attachDeviceSettingPresenter);

    @Binds
    abstract IAtmContract.Presenter bindAtmPresenter(AtmPresenter atmPresenter);

    @Binds
    abstract IAtmListContract.Presenter bindAtmListPresenter(AtmListPresenter atmListPresenter);

    @Binds
    abstract IAtmMapContract.Presenter bindAtmMapPresenter(AtmMapPresenter atmMapPresenter);

    @Binds
    abstract ICreditDetailContract.Presenter bindCreditDetailPresenter(CreditDetailPresenter creditDetailPresenter);

    @Binds
    abstract ICreditAboutContract.Presenter bindCreditAboutPresenter(CreditAboutPresenter creditAboutPresenter);

    @Binds
    abstract IDebitDetailContract.Presenter bindDebitDetailPresenter(DebitDetailPresenter debitDetailPresenter);

    @Binds
    abstract IDebitAboutContract.Presenter bindDebitAboutPresenter(DebitAboutPresenter debitAboutPresenter);

    @Binds
    abstract IOpenDebitContract.Presenter bindOpenDebitPresenter(OpenDebitPresenter openDebitPresenter);

    @Binds
    abstract ISuccessOpenDebitContract.Presenter bindSuccessOpenDebitPresenter(SuccessOpenDebitPresenter successOpenDebitPresenter);

    @Binds
    abstract IDebitAgreementContract.Presenter bindDebitAgreementPresenter(DebitAgreementPresenter debitAgreementPresenter);

    @Binds
    abstract ISelectCardContract.Presenter bindSelectCardPresenter(SelectCardPresenter selectCardPresenter);

    @Binds
    abstract ISelectCardOrAccountContract.Presenter bindSelectCardOrAccountPresenter(SelectCardOrAccountPresenter selectCardOrAccountPresenter);

    @Binds
    abstract ISelectConditionsContract.Presenter bindSelectConditionsPresenter(SelectConditionsPresenter selectConditionsPresenter);

    @Binds
    abstract IConfirmOpenDebitContract.Presenter bindConfirmOpenDebitPresenter(ConfirmOpenDebitPresenter confirmOpenDebitPresenter);

    @Binds
    abstract IAccountDetailContract.Presenter bindAccountDetailPresenter(AccountDetailPresenter accountDetailPresenter);

    @Binds
    abstract IAccountAboutContract.Presenter bindAccountAboutPresenter(AccountAboutPresenter accountAboutPresenter);

    @Binds
    abstract IFineDocumentContract.Presenter bindFineDocumentPresenter(FineDocumentPresenter fineDocumentPresenter);

    @Binds
    abstract IFineUinContract.Presenter bindFineUidPresenter(FineUinPresenter fineUidPresenter);

    @Binds
    abstract IFineAddDocumentContract.Presenter bindFineAddDocumentPresenter(FineAddDocumentPresenter fineAddDocumentPresenter);

    @Binds
    abstract IFineDetailContract.Presenter bindFineDetailPresenter(FineDetailPresenter fineDetailPresenter);

    @Binds
    abstract ISelectedFineContract.Presenter bindSelectedFinePresenter(SelectedFinePresenter selectedFinePresenter);

    @Binds
    abstract ISelectFineContract.Presenter bindSelectFinePresenter(SelectFinePresenter selectFinePresenter);

    @Binds
    abstract IFinePaymentContract.Presenter bindFinePaymentPresenter(FinePaymentPresenter finePaymentPresenter);

    @Binds
    abstract ISuccessFinePaymentContract.Presenter bindSuccesdsFinePaymentPresenter(SuccessFinePaymentPresenter successFinePaymentPresenter);

    @Binds
    abstract IAccountOperationDetailContract.Presenter bindAccountOperationDetailPresenter(AccountOperationDetailPresenter accountOperationDetailPresenter);

    @Binds
    abstract ICardOperationDetailContract.Presenter bindCardOperationDetailPresenter(CardOperationDetailPresenter cardOperationDetailPresenter);

    @Binds
    abstract IOperationFilterContract.Presenter bindOperationFilterPresenter(OperationFilterPresenter operationFilterPresenter);

    @Binds
    abstract IAtmFilterContract.Presenter bindAtmFilterPresenter(AtmFilterPresenter atmFilterPresenter);

    @Binds
    abstract IATMDetailContract.Presenter bindATMDetailPresenter(ATMDetailPresenter atmDetailPresenter);

    @Binds
    abstract ITransferIndividualPersonContract.Presenter bindTransferIndividualPersonPresenter(TransferIndividualPersonPresenter transferIndividualPersonPresenter);

    @Binds
    abstract ITransferLegalPersonContract.Presenter bindTransferLegalPersonPresenter(TransferLegalPersonPresenter transferLegalPersonPresenter);

    @Binds
    abstract ITransferBudgetOrganizationContract.Presenter bindBudgetOrganizationPresenter(TransferBudgetOrganizationPresenter budgetOrganizationPresenter);

    @Binds
    abstract ISelectBudgetOrgDataContract.Presenter bindSelectBudgetOrganizationDataPresenter(SelectBudgetOrgDataPresenter selectBudgetOrgDataPresenter);

    //View
    @Binds
    abstract IOffersBlockContract.Presenter bindOfferBlockPresenter(OffersBlockPresenter offersBlockPresenter);

    @Binds
    abstract INewsBlockContract.Presenter bindNewsBlockPresenter(NewsBlockPresenter newsBlockPresenter);

    @Binds
    abstract IAtmBlockContract.Presenter bindAtmBlockPresenter(AtmBlockPresenter atmBlockPresenter);

    @Binds
    abstract IMyCardBlockContract.Presenter bindMyCardBlockPresenter(MyCardBlockPresenter myCardBlockPresenter);

    @Binds
    abstract IMyAccountBlockContract.Presenter bindMyAccountBlockPresenter(MyAccountBlockPresenter myAccountBlockPresenter);

    @Binds
    abstract IMyCreditBlockContract.Presenter bindMyCreditBlockPresenter(MyCreditBlockPresenter myCreditBlockPresenter);

    @Binds
    abstract IMyDebitBlockContract.Presenter bindMyDebitBlockPresenter(MyDebitBlockPresenter myDebitBlockPresenter);

    @Binds
    abstract ITemplateBlockContract.Presenter bindTemplateBlockPresenter(TemplateBlockPresenter templateBlockPresenter);

    @Binds
    abstract ICardRestrictionListContract.Presenter bindGeoLimitsListPresenter(CardRestrictionListPresenter geoLimitsListPresenter);

    @Binds
    abstract ICardRestrictionSettingsContract.Presenter bindRestrictionSettingsPresenter(CardRestrictionSettingsPresenter restrictionSettingsPresenter);

    @Binds
    abstract ICardStatusManagerContract.Presenter bindStatusManagerPresenter(CardStatusManagerPresenter statusManagerPresenter);

    @Binds
    abstract ICardLimitContract.Presenter bindCardLimitForDayPresenter(CardLimitPresenter cardLimitForDayPresenter);

    @Binds
    abstract IPaymentCategoriesContract.Presenter bindPaymentCategoriesPresenter(PaymentCategoriesPresenter paymentCategoriesPresenter);

    @Binds
    abstract IPaymentProvidersContract.Presenter bindPaymentProvidersPresenter(PaymentProvidersPresenter paymentProvidersPresenter);

    @Binds
    abstract IPaymentCategoriesSelectRegionsContract.Presenter bindPaymentCategoriesSelectregionsPresenter(PaymentCategoriesSelectRegionsPresenter paymentCategoriesSelectRegionsPresenter);

    @Binds
    abstract IPaymentExecuteContract.Presenter bindPaymentExecutePresenter(PaymentExecutePresenter paymentExecutePresenter);

    @Binds
    abstract ITransferContract.Presenter bindTransferPresenter(TransferPresenter transferPresenter);

    @Binds
    abstract ITransferOwnAccountContract.Presenter bindTransferOwnAccountPresenter(TransferOwnAccountPresenter transferOwnAccountPresenter);

    @Binds
    abstract IPaymentScheduleContract.Presenter bindPaymentSchedulePresenter(CreditPaymentSchedulePresenter creditPaymentSchedulePresenter);

    @Binds
    abstract IPaymentConfirmContract.Presenter bindPaymentConfirmPresenter(PaymentConfirmPresenter paymentConfirmPresenter);

    @Binds
    abstract ITransferCheckContract.Presenter bindTransferCheckPresenter(TransferCheckPresenter transferCheckPresenter);

    @Binds
    abstract ITemplateListContract.Presenter bindTemplateListPresenter(TemplateListPresenter templateListPresenter);

    @Binds
    abstract ITransferSuccessContract.Presenter bindTransferSuccessPresenter(TransferSuccessPresenter transferSuccessPresenter);

    @Binds
    abstract ITransferClientGpbContract.Presenter bindTransferClientGpbPresenter(TransferClientGpbPresenter transferClientGpbPresenter);

    @Binds
    abstract IPaymentSuccessContract.Presenter bindPaymentSuccessbPresenter(PaymentSuccessPresenter paymentSuccessPresenter);

    @Binds
    abstract ICreateTemplateSuccessContract.Presenter bindCreateTemplateSuccessPresenter(CreateTemplateSuccessPresenter createTemplateSuccessPresenter);

    @Binds
    abstract IRepaymentContract.Presenter bindRepaymentPresenter(RepaymentPresenter repaymentPresenter);

    @Binds
    abstract IRepaymentCheckContract.Presenter bindRepaymentCheckPresenter(RepaymentCheckPresenter repaymentCheckPresenter);

    @Binds
    abstract ITransferTemplateContract.Presenter bindTransferTemplatePresenter(TransferTemplatePresenter transferTemplatePresenter);

    @Binds
    abstract ISessionControlContract.Presenter bindSessionControlPresenter(SessionControlPresenter sessionControlPresenter);

    @Binds
    abstract IContactContract.Presenter bindContactPresenter(ContactPresenter contactPresenter);

    @Binds
    abstract ISubsidiaryContract.Presenter bindSubsidiaryPresenter(SubsidiaryPresenter subsidiaryPresenter);

    @Binds
    abstract IRequisetesContract.Presenter bindRequisetesPresenter(RequisetesPresenter requisetesPresenter);

    @Binds
    abstract IHiddenProductsContract.Presenter bindHiddenProductsPresenter(HiddenProductsPresenter hiddenProductsPresenter);

    @Binds
    abstract IRegistrationServiceContract.Presenter bindRegistrationServicePresenter(RegistrationServicePresenter registrationServicePresenter);

    @Binds
    abstract ISelectFieldContract.Presenter bindSelectFieldPresenter(SelectFieldPresenter selectFieldPresenter);
}
