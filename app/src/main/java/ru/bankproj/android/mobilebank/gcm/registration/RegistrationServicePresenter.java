package ru.bankproj.android.mobilebank.gcm.registration;

import android.content.Context;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.di.qualifier.ApplicationContext;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.utils.storage.AppStorage;

/**
 * Created by Alexey Vereshchaga on 02.03.18.
 */

public class RegistrationServicePresenter extends BaseEventBusPresenter<IRegistrationServiceContract.View>
        implements IRegistrationServiceContract.Presenter, IBaseResponseCallback {
    private Disposable disposable;


    private RequestManager requestManager;

    @Inject
    @ApplicationContext
    Context mContext;

    @Inject
    public RegistrationServicePresenter(RequestManager requestManager) {
        this.requestManager = requestManager;
        disposable = Disposables.empty();
    }


    @Override
    public void sendRegistrationToServer(String token) {
        subscribeOnPushNotifications(token);
    }

    public void subscribeOnPushNotifications(String token) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                disposable = requestManager.subscribeOnPushNotifications(sessionId, token, mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                        new BaseRequestController(mEventBusController, Action.SUBSCRIBE_PUSH_ACTION, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case SUCCESS_REQUEST:
                switch (event.getActionCode()) {
                    case Action.SUBSCRIBE_PUSH_ACTION:
                        undisposable(disposable);
                        Response<BaseResponse> baseResponse = (Response<BaseResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                        break;
                }
                break;
            case FAIL_REQUEST:
                switch (event.getActionCode()) {
                    case Action.SUBSCRIBE_PUSH_ACTION:
                        undisposable(disposable);
                        savePushSubscribing(false);
                        break;
                }
                break;
        }
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case Action.SUBSCRIBE_PUSH_ACTION:
                BaseResponse baseResponse = (BaseResponse) data.getValue();
                savePushSubscribing(true);
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                break;
            case BAD_REQUEST_ERROR:
                break;
            case DEFAULT_ERROR:
                break;
        }
        switch (actionCode) {
            case Action.SUBSCRIBE_PUSH_ACTION:
                savePushSubscribing(false);
                break;
        }
    }

    private void savePushSubscribing(boolean pushSubscribed) {
        AppStorage.setBooleanValue(Constants.IS_PUSH_SUBSCRIBED, pushSubscribed, mContext);
        getMvpView().sendBroadcast();
    }
}
