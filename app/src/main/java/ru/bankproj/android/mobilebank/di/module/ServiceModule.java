package ru.bankproj.android.mobilebank.di.module;

import android.app.Service;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ru.bankproj.android.mobilebank.di.qualifier.ServiceContext;
import ru.bankproj.android.mobilebank.di.scope.PerService;

/**
 * Created by j7ars on 25.10.2017.
 */
@Module
public class ServiceModule {

    private final Service mService;

    public ServiceModule(Service service){
        this.mService = service;
    }

    @Provides
    @PerService
    Service provideService() {
        return mService;
    }

    @Provides
    @PerService
    @ServiceContext
    Context provideContext(){
        return mService.getApplicationContext();
    }

}
