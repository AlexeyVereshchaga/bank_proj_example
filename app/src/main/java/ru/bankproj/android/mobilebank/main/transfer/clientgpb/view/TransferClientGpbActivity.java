package ru.bankproj.android.mobilebank.main.transfer.clientgpb.view;

import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.activity.BaseBackContainerActivity;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;

/**
 * Created by rrust on 28.12.2017.
 */

public class TransferClientGpbActivity extends BaseBackContainerActivity {

    public static final String TRANSFER_DESCRIPTION = "TransferClientGpbActivity.TRANSFER_DESCRIPTION";
    public static final String SOURCE_CARD = "TransferClientGpbActivity.SOURCE_CARD";
    public static final String TEMPLATE_MODE = "TransferClientGpbActivity.TEMPLATE_MODE";

    private String descriptionTransfer;
    private Card debitingSourceCard;
    private boolean templateMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.transfer_title));
        getExtras();
        if (savedInstanceState == null) {
            openFragment();
        }
    }

    private void getExtras() {
        if (getIntent().getExtras() != null) {
            debitingSourceCard = getIntent().getParcelableExtra(SOURCE_CARD);
            descriptionTransfer = getIntent().getStringExtra(TRANSFER_DESCRIPTION);
            templateMode = getIntent().getBooleanExtra(TEMPLATE_MODE, false);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(SOURCE_CARD, debitingSourceCard);
        outState.putString(TRANSFER_DESCRIPTION, descriptionTransfer);
        outState.putBoolean(TEMPLATE_MODE, templateMode);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        debitingSourceCard = savedInstanceState.getParcelable(SOURCE_CARD);
        descriptionTransfer = savedInstanceState.getString(TRANSFER_DESCRIPTION);
        templateMode = savedInstanceState.getBoolean(TEMPLATE_MODE);
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container, getScreenCreator().newInstance(TransferClientGpbFragment.class, createBundle())).commitAllowingStateLoss();
    }

    private Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferClientGpbFragment.DEBITING_SOURCE_CARD, debitingSourceCard);
        bundle.putString(TransferClientGpbFragment.DESCRIPTION_TRANSFER, descriptionTransfer);
        bundle.putBoolean(TransferClientGpbFragment.TEMPLATE_MODE, templateMode);
        return bundle;
    }
}
