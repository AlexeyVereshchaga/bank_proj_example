package ru.bankproj.android.mobilebank.main.transfer.clientgpb.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redmadrobot.inputmask.MaskedTextChangedListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.block.dialogs.BalanceDialog;
import ru.bankproj.android.mobilebank.main.dialogs.InfoDialog;
import ru.bankproj.android.mobilebank.main.payment.selectcardoraccount.view.SelectCardOrAccountActivity;
import ru.bankproj.android.mobilebank.main.transfer.check.view.TransferCheckActivity;
import ru.bankproj.android.mobilebank.main.transfer.clientgpb.ITransferClientGpbContract;
import ru.bankproj.android.mobilebank.utils.AmountDigitsInputFilter;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.utils.storage.AppStorage;
import ru.bankproj.android.mobilebank.view.EnableButton;
import ru.bankproj.android.mobilebank.view.ErrorLabelContainer;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by rrust on 28.12.2017.
 */

public class TransferClientGpbFragment extends BaseMvpFragment implements ITransferClientGpbContract.View {

    public static final String DEBITING_SOURCE_CARD = "TransferClientGpbFragment.SOURCE_CARD";
    public static final String DESCRIPTION_TRANSFER = "TransferClientGpbFragment.TRANSFER_DESCRIPTION";

    private static final int REQUEST_SCAN = 100;
    public static final String TEMPLATE_MODE = "TransferClientGpbFragment.TEMPLATE_MODE";

    @BindView(R.id.transfer_source)
    View transferSource;
    @BindView(R.id.source_card)
    View sourceCardInclude;
    @BindView(R.id.et_limit_assignment)
    EditText etAmount;
    @BindView(R.id.btn_transfer_next_step)
    EnableButton btnNext;
    private IncludedCardView sourceView;
    @BindViews({R.id.transfer_source, R.id.source_card})
    List<View> debitingViews;
    @BindView(R.id.et_card_number)
    EditText etCardNumber;
    @BindView(R.id.et_fio_transfer)
    EditText etFioTransfer;
    //Template views
    @BindView(R.id.ll_template_name_container)
    LinearLayout llTemplateNameContainer;
    @BindView(R.id.el_template_name)
    ErrorLabelContainer elTemplateName;
    @BindView(R.id.et_template_name)
    EditText etTemplateName;

    @Inject
    ITransferClientGpbContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs() {
        if (getArguments() != null) {
            presenter.setArguments(getArguments().getParcelable(DEBITING_SOURCE_CARD),
                    getArguments().getString(DESCRIPTION_TRANSFER),
                    getArguments().getBoolean(TEMPLATE_MODE));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bindIncluded();
        presenter.onCreate(savedInstanceState);
        etFioTransfer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.setFioRecipient(editable.toString());
            }
        });
        etAmount.setFilters(new InputFilter[]{new AmountDigitsInputFilter()});
        Utils.hideKeyboard(mActivity);
    }

    private void bindIncluded() {
        sourceView = new IncludedCardView();
        ButterKnife.bind(sourceView, sourceCardInclude);
        initIncluded(sourceView);
    }

    private void initIncluded(IncludedCardView includedCardView) {
        SpannableString sb1 = new SpannableString(FieldConverter.getString(R.string.card_balance));
        sb1.setSpan(new UnderlineSpan(), 0, FieldConverter.getString(R.string.card_balance).length(), 0);
        includedCardView.tvCardBalance.setText(sb1);
        if (AppStorage.getBooleanValue(Constants.IS_HIDE_BALANCE, mActivity)) {
            includedCardView.llAmountBalance.setVisibility(VISIBLE);
            includedCardView.llAmountContainer.setVisibility(GONE);
            includedCardView.tvCardBalance.setOnClickListener(view
                    -> crossFade(includedCardView.llAmountContainer, includedCardView.llAmountBalance));
            includedCardView.llAmountContainer.setOnClickListener(view
                    -> crossFade(includedCardView.llAmountBalance, includedCardView.llAmountContainer));
            includedCardView.ivCardBalance.setOnClickListener(view -> showBalanceDialog());
        } else {
            includedCardView.llAmountBalance.setVisibility(GONE);
            includedCardView.llAmountContainer.setVisibility(VISIBLE);
        }
        ((TextView) transferSource.findViewById(R.id.tv_select_card_text)).setText(FieldConverter.getString(R.string.debit_select_card_from_title));
    }

    private void crossFade(LinearLayout showLayout, LinearLayout hideLayout) {
        hideLayout.animate()
                .alpha(0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        hideLayout.setVisibility(View.GONE);
                        showLayout.setAlpha(0f);
                        showLayout.setVisibility(View.VISIBLE);
                        showLayout.animate()
                                .alpha(1f)
                                .setDuration(300)
                                .setListener(null);
                    }
                });
    }

    public void showBalanceDialog() {
        BalanceDialog dialogFragment = BalanceDialog.newInstance();
        dialogFragment.setOnResultListener(new BalanceDialog.OnDialogResultListener() {
            @Override
            public void onResultDialog(int statusCode) {
                if (statusCode == Activity.RESULT_OK) {
                    dialogFragment.dismiss();
                } else {
                    dialogFragment.dismiss();
                }
            }
        });
        dialogFragment.show(getActivity().getSupportFragmentManager(), BalanceDialog.TAG);
    }


    @Override
    public void showCardLimitDialog() {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(R.string.limit_transfer_card_dialog_text));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @Override
    public void fieldValidationSuccess(Boolean success) {
        btnNext.enable(success);
    }

    private Observable<Boolean> getMaskedCardObservable(boolean isGpbClient) {
        final PublishSubject<Boolean> subject = PublishSubject.create();
        String format = isGpbClient ? "[0000] [0000] [0000] [0000]" : "[0000] [0000] [0000] [0000999]";
        MaskedTextChangedListener listener = new MaskedTextChangedListener(
                format,
                true,
                etCardNumber,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean b, String s) {
                        if (b) {
                            if (Utils.checkAlgoritmLuhn(s)) {
                                etCardNumber.setTextColor(Color.BLACK);
                                presenter.setCardNumber(s);
                                subject.onNext(true);
                            } else {
                                etCardNumber.setTextColor(Color.RED);
                                subject.onNext(false);
                            }
                        } else {
                            etCardNumber.setTextColor(Color.RED);
                            subject.onNext(false);
                        }
                    }
                });
        etCardNumber.addTextChangedListener(listener);
        return subject;
    }

    @Override
    public void showNotValidAmountMessage(int validationValue) {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(validationValue));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    private void startCardReader() {
        Intent intent = new Intent(mActivity, CardIOActivity.class)
                .putExtra(CardIOActivity.EXTRA_NO_CAMERA, false)
                .putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, false)
                .putExtra(CardIOActivity.EXTRA_SCAN_EXPIRY, false)
                .putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false)
                .putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false)
                .putExtra(CardIOActivity.EXTRA_RESTRICT_POSTAL_CODE_TO_NUMERIC_ONLY, false)
                .putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, false)
                .putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, true)
                .putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true)
                .putExtra(CardIOActivity.EXTRA_LANGUAGE_OR_LOCALE, Constants.RU_LANGUAGE)
                .putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME, true)
                .putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, FieldConverter.getColor(R.color.colorAccent))
                .putExtra(CardIOActivity.EXTRA_SUPPRESS_CONFIRMATION, true)
                .putExtra(CardIOActivity.EXTRA_SUPPRESS_SCAN, false)
                .putExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, false);

        startActivityForResult(intent, REQUEST_SCAN);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        presenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        presenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_transfer_client_gpb;
    }

    @Override
    public void openSelectCardScreen(FilterProductData data) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SelectCardOrAccountActivity.FILTER_PRODUCTS, data);
        getScreenCreator().startActivity(this, mActivity, SelectCardOrAccountActivity.class, bundle);
    }

    @Override
    public void moveToTop() {
        Utils.moveToTopCurrent(mActivity);
    }

    @Override
    public void showSourceProduct(BaseProduct debitingProduct) {
        boolean showCard = debitingProduct != null;
        ButterKnife.apply(debitingViews, SHOW_PRODUCT, showCard);
        if (showCard) {
            initProduct(sourceView, debitingProduct);// TODO: 23.12.17
        }
    }

    @Override
    public void init(boolean templateMode, boolean gpbClient) {
        List<Observable<Boolean>> listObservable = new ArrayList<>();
        if (templateMode) {
            listObservable.add(Utils.getRegexObservable(etTemplateName, "^.{1,300}$", getString(R.string.transfer_invalid_value)));
        }
        listObservable.add(getMaskedCardObservable(gpbClient));
        presenter.setListObservableField(listObservable);
        llTemplateNameContainer.setVisibility(templateMode ? VISIBLE : GONE);
    }

    @Override
    public void openTransferCheckScreen(CheckTransferData checkTransferData, boolean templateMode) {
        if (templateMode) {
            checkTransferData.setNewTemplateName(etTemplateName.getText().toString());
        }
        getScreenCreator().startActivity(this, mActivity, TransferCheckActivity.class,
                createTransferCheckBundle(checkTransferData, templateMode), Constants.START_TRANSFER_CHECK_ACTIVITY);
    }

    @NonNull
    private Bundle createTransferCheckBundle(CheckTransferData checkTransferData, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferCheckActivity.CHECK_TRANSFER_DATA, checkTransferData);
        bundle.putBoolean(TransferCheckActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    @OnClick(R.id.iv_scan_card_number)
    public void onClickBtnScanCard() {
        startCardReader();
    }

    @OnClick(R.id.fl_source)
    void onSourceProductClick() {
        presenter.onSourceClicked();
    }

    private void initProduct(IncludedCardView includedCardView, BaseProduct debitingProduct) {
        includedCardView.tvCardName.setText(debitingProduct.getName());
        includedCardView.tvCardNumber.setText(Utils.getMaskedPan(debitingProduct.getNumber()));
        includedCardView.tvCardAmount.setText(Utils.formatMoneyValue(debitingProduct.getBalance()));
        includedCardView.tvCardCurrency.setText(FieldConverter.getCurrency(debitingProduct.getCurrency()));
        if (debitingProduct instanceof Card) {
            includedCardView.ivCardIcon.setImageResource(((Card) debitingProduct).getCardBrand());
        }
    }

    static class IncludedCardView {
        @BindView(R.id.tv_card_name)
        TextView tvCardName;
        @BindView(R.id.tv_card_number)
        TextView tvCardNumber;
        @BindView(R.id.tv_card_amount)
        TextView tvCardAmount;
        @BindView(R.id.tv_card_currency)
        TextView tvCardCurrency;
        @BindView(R.id.iv_card_icon)
        ImageView ivCardIcon;
        @BindView(R.id.tv_card_balance)
        TextView tvCardBalance;
        @BindView(R.id.iv_card_balance)
        ImageView ivCardBalance;
        @BindView(R.id.ll_amount_container)
        LinearLayout llAmountContainer;
        @BindView(R.id.ll_amount_balance)
        LinearLayout llAmountBalance;
    }

    static final ButterKnife.Setter<View, Boolean> SHOW_PRODUCT = (view, value, index) -> {
        switch (view.getId()) {
            case R.id.source_card:
                view.setVisibility(value ? View.VISIBLE : View.GONE);
                break;
            case R.id.transfer_source:
                view.setVisibility(value ? View.GONE : View.VISIBLE);
                break;
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SCAN && data != null
                && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
            CreditCard result = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
            if (result != null) {
                etCardNumber.setText(result.cardNumber);
            }
        }
        switch (requestCode) {
            case Constants.START_TRANSFER_CHECK_ACTIVITY:
                if (resultCode == Constants.RESULT_LOGOUT) {
                    unAuthorized();
                }
                break;
        }
    }

    @OnClick(R.id.btn_transfer_next_step)
    void onNextClick() {
        presenter.onNextClicked(etAmount.getText().toString());
    }
}
