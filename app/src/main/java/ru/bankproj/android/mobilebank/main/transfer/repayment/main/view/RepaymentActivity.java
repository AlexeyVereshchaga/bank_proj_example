package ru.bankproj.android.mobilebank.main.transfer.repayment.main.view;

import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.activity.BaseBackContainerActivity;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.transfer.TargetType;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public class RepaymentActivity extends BaseBackContainerActivity {

    public static final String TRANSFER_DESCRIPTION = "RepaymentActivity.TRANSFER_DESCRIPTION";
    public static final String SOURCE_CARD = "TransferOwnAccountActivity.SOURCE_CARD";
    public static final String TARGET_TYPE = "RepaymentActivity.TARGET_TYPE";
    public static final String TARGET_ACCOUNT = "RepaymentActivity.TARGET_ACCOUNT";
    public static final String TEMPLATE_MODE = "TransferClientGpbActivity.TEMPLATE_MODE";

    private String descriptionTransfer;
    private Card sourceCard;
    private int targetType;
    private Account targetAccount;
    private boolean templateMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.transfer_title));
        getExtras();
        if (savedInstanceState == null) {
            openFragment();
        }
    }

    private void getExtras() {
        if (getIntent().getExtras() != null) {
            sourceCard = getIntent().getParcelableExtra(SOURCE_CARD);
            descriptionTransfer = getIntent().getStringExtra(TRANSFER_DESCRIPTION);
            targetType = getIntent().getIntExtra(TARGET_TYPE, TargetType.CLIENTS_CARD.getId());
            targetAccount = getIntent().getParcelableExtra(TARGET_ACCOUNT);
            templateMode = getIntent().getBooleanExtra(TEMPLATE_MODE, false);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(SOURCE_CARD, sourceCard);
        outState.putString(TRANSFER_DESCRIPTION, descriptionTransfer);
        outState.putInt(TARGET_TYPE, targetType);
        outState.putParcelable(TARGET_ACCOUNT, targetAccount);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        sourceCard = savedInstanceState.getParcelable(SOURCE_CARD);
        descriptionTransfer = savedInstanceState.getString(TRANSFER_DESCRIPTION);
        targetType = savedInstanceState.getInt(TARGET_TYPE);
        targetAccount = savedInstanceState.getParcelable(TARGET_ACCOUNT);
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container, getScreenCreator().newInstance(RepaymentFragment.class, createBundle())).commitAllowingStateLoss();
    }

    private Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(RepaymentFragment.SOURCE_CARD, sourceCard);
        bundle.putString(RepaymentFragment.DESCRIPTION_TRANSFER, descriptionTransfer);
        bundle.putInt(RepaymentFragment.TARGET_TYPE, targetType);
        bundle.putParcelable(RepaymentFragment.TARGET_ACCOUNT, targetAccount);
        bundle.putBoolean(RepaymentFragment.TEMPLATE_MODE, templateMode);
        return bundle;
    }

}
