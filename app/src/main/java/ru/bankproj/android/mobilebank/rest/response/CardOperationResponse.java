package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.operationhistory.CardOperationHistory;

/**
 * Created by j7ars on 04.12.2017.
 */

public class CardOperationResponse extends BaseResponse {

    @SerializedName("history")
    private ArrayList<CardOperationHistory> historyList;

    public ArrayList<CardOperationHistory> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(ArrayList<CardOperationHistory> mHistoryList) {
        this.historyList = mHistoryList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.historyList);
    }

    public CardOperationResponse() {
    }

    protected CardOperationResponse(Parcel in) {
        super(in);
        this.historyList = in.createTypedArrayList(CardOperationHistory.CREATOR);
    }

    public static final Creator<CardOperationResponse> CREATOR = new Creator<CardOperationResponse>() {
        @Override
        public CardOperationResponse createFromParcel(Parcel source) {
            return new CardOperationResponse(source);
        }

        @Override
        public CardOperationResponse[] newArray(int size) {
            return new CardOperationResponse[size];
        }
    };
}
