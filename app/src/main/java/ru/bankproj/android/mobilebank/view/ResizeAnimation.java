package ru.bankproj.android.mobilebank.view;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by maxmobiles on 23.11.17.
 */

public class ResizeAnimation extends Animation {

    private View mView;
    private float mToHeight;
    private float mFromHeight;

    public ResizeAnimation(View v, float fromHeight, float toHeight) {
        mToHeight = toHeight;
        mFromHeight = fromHeight;
        mView = v;
        setDuration(1000);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        float height = 0;
        if(mToHeight > mFromHeight) {
           height = (mToHeight - mFromHeight) * interpolatedTime + mFromHeight;
        } else{
            height = (mFromHeight - mToHeight) * interpolatedTime + mToHeight;
        }
        ViewGroup.LayoutParams p = mView.getLayoutParams();
        p.height = (int) height;
        mView.requestLayout();
    }
}
