package ru.bankproj.android.mobilebank.main.transfer.clientgpb;

import java.util.List;

import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;


/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public interface ITransferClientGpbContract {
    interface View extends IBaseProgressView {
        void openSelectCardScreen(FilterProductData data);

        void moveToTop();

        void showSourceProduct(BaseProduct debitingCard);

        void init(boolean templateMode, boolean gpbClient);

        void openTransferCheckScreen(CheckTransferData checkTransferData, boolean templateMode);

        void showCardLimitDialog();

        void fieldValidationSuccess(Boolean success);

        void showNotValidAmountMessage(int validationValue);
    }

    interface Presenter extends IBaseMvpPresenter<View> {
        void setListObservableField(List<Observable<Boolean>> mListObservable);

        void onSourceClicked();

        void onNextClicked(String s);

        void setCardNumber(String number);

        void setFioRecipient(String fio);
    }
}
