package ru.bankproj.android.mobilebank.di.module;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import dagger.Module;
import dagger.Provides;
import ru.bankproj.android.mobilebank.di.qualifier.ViewHolderContext;
import ru.bankproj.android.mobilebank.di.scope.PerViewHolder;

/**
 * Created by j7ars on 25.10.2017.
 */
@Module
public class ViewHolderModule {

    private RecyclerView.ViewHolder mViewHolder;
    private View mItemView;

    public ViewHolderModule(RecyclerView.ViewHolder viewHolder, View itemView){
        this.mViewHolder = viewHolder;
        this.mItemView = itemView;
    }

    @Provides
    @PerViewHolder
    RecyclerView.ViewHolder provideViewHolder(){
        return mViewHolder;
    }

    @Provides
    @PerViewHolder
    View provideItemView(){
        return mItemView;
    }

    @Provides
    @PerViewHolder
    @ViewHolderContext
    Context provideContext(){
        return mItemView.getContext();
    }

}
