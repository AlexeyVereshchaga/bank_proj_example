package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.providers.Category;

/**
 * Created by j7ars on 16.11.2017.
 */

public class ProviderCategoriesResponse extends BaseResponse {

    @SerializedName("categories")
    private List<Category> categories;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.categories);
    }

    public ProviderCategoriesResponse() {
    }

    protected ProviderCategoriesResponse(Parcel in) {
        super(in);
        this.categories = in.createTypedArrayList(Category.CREATOR);
    }

    public static final Creator<ProviderCategoriesResponse> CREATOR = new Creator<ProviderCategoriesResponse>() {
        @Override
        public ProviderCategoriesResponse createFromParcel(Parcel source) {
            return new ProviderCategoriesResponse(source);
        }

        @Override
        public ProviderCategoriesResponse[] newArray(int size) {
            return new ProviderCategoriesResponse[size];
        }
    };
}
