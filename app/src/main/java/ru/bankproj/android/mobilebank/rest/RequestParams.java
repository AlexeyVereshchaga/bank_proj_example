package ru.bankproj.android.mobilebank.rest;

import android.location.Location;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.dataclasses.PhoneData;
import ru.bankproj.android.mobilebank.dataclasses.authorization.PasswordInputType;
import ru.bankproj.android.mobilebank.dataclasses.operationhistory.TransactionState;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.store.StoreOptions;
import ru.bankproj.android.mobilebank.utils.RequestConstants;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.view.paymentfields.base.BaseField;

/**
 * Created by j7ars on 25.10.2017.
 */

public class RequestParams {

    private static final String TRUE_STATE = "true";
    private static final String FALSE_STATE = "false";
    private static final String RU_LANGUAGE = "ru";
    private static final String RU_LOCALE = "ru_RU";

    public static LinkedHashMap<String, String> getLanguageParams() {
        LinkedHashMap<String, String> params = new LinkedHashMap<>(2);
        params.put(RequestField.LANG, RU_LANGUAGE);
        params.put(RequestField.LOCALE, RU_LOCALE);
        return params;
    }

    public static LinkedHashMap<String, String> getJoiningAndResettingParams(String login) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>(3);
        params.putAll(getLanguageParams());
        params.put(RequestField.LOGIN, login);
        return params;
    }

    public static LinkedHashMap<String, String> getConfirmUserSessionParams(String usrCode, String devKey, PhoneData phoneData) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>(4);
        params.put(RequestField.USR_CODE, getUtf8String(usrCode));
        params.put(RequestField.DEV_KEY, getUtf8String(devKey));
        params.put(RequestField.DEV_ID, getUtf8String(phoneData.getDeviceId()));
        params.put(RequestField.DEV_NAME, getUtf8String(phoneData.getDeviceName()));
        return params;
    }

    public static LinkedHashMap<String, String> getSetPasswordParams(String newPassword) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.NEW_PASSWORD, getUtf8String(newPassword));
        return params;
    }

    public static LinkedHashMap<String, String> getLoginParams(String login, String password, PhoneData phoneData, PasswordInputType passwordInputType) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.putAll(getLanguageParams());
        params.put(RequestField.NAME, getUtf8String(login));
        params.put(RequestField.PASSWORD, getUtf8String(password));
        params.put(RequestField.DEV_ID, getUtf8String(phoneData.getDeviceId()));
        params.put(RequestField.PASSWORD_INPUT_MODE, getUtf8String(passwordInputType.getPasswordInputType()));
        return params;
    }

    public static LinkedHashMap<String, String> getChangePasswordParams(String oldPassword, String newPassword) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.OLD_PASSWORD, getUtf8String(oldPassword));
        params.put(RequestField.NEW_PASSWORD, getUtf8String(newPassword));
        return params;
    }

    public static LinkedHashMap<String, String> getAttachDeviceParams(String devKey, String devId) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>(2);
        params.put(RequestField.DEV_KEY, devKey);
        params.put(RequestField.DEV_ID, devId);
        return params;
    }

    public static LinkedHashMap<String, String> getDetachDeviceParams(String devId) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>(1);
        params.put(RequestField.DEV_ID, devId);
        return params;
    }

    public static LinkedHashMap<String, String> getConfirmDeviceParams(String devCode, PhoneData phoneData) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>(3);
        params.put(RequestField.DEV_CODE, devCode);
        params.put(RequestField.DEV_ID, phoneData.getDeviceId());
        params.put(RequestField.DEV_NAME, phoneData.getDeviceName());
        return params;
    }

    public static LinkedHashMap<String, String> getConfirmationStrategiesParams(int actionType, String operationUID, String amount, String currency, String pid, int sourceType, int targetType, int operationSubType) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.ACTION_TYPE, String.valueOf(actionType));
        if (operationUID != null) {
            params.put(RequestField.OPERATION_UID, operationUID);
        }
        params.put(RequestField.AMOUNT, amount);
        params.put(RequestField.CURRENCY, currency);
        if (pid != null) {
            params.put(RequestField.PID, pid);
        }
        if (sourceType != -1) {
            params.put(RequestField.SOURCE_TYPE, String.valueOf(sourceType));
        }
        if (targetType != -1) {
            params.put(RequestField.TARGET_TYPE, String.valueOf(targetType));
        }
        if (operationSubType != -1) {
            params.put(RequestField.OPERATION_SUB_TYPE, String.valueOf(operationSubType));
        }
        return params;
    }

    public static Map<String, String> getTransferCheckParams(String currency, float amount, int sourceType, String sourceId,
                                                             int targetType, String targetId, String fullName, Map<String, String> additionalParameters,
                                                             String confirmationStrategy, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.CURRENCY, currency);
        params.put(RequestField.AMOUNT, String.valueOf(amount));
        params.put(RequestField.SOURCE_TYPE, String.valueOf(sourceType));
        params.put(RequestField.SOURCE_ID, sourceId);
        params.put(RequestField.TARGET_TYPE, String.valueOf(targetType));
        if (targetId != null) {
            params.put(RequestField.TARGET_ID, targetId);
        }
        if (fullName != null) {
            params.put(RequestField.FULL_NAME, getUtf8String(fullName));
        }
        if (additionalParameters != null) {
            params.putAll(additionalParameters);
        }
        if (!TextUtils.isEmpty(confirmationStrategy)) {
            params.put(RequestField.CONFIRMATION_STRATEGY, confirmationStrategy);
        }
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static Map<String, String> getTransferExecParams(String currency, float amount, String transRef, String targetId,
                                                            String confirmVal, String operationSubtype, int targetType, String sourceId,
                                                            String flags, int sourceType, String fullName,
                                                            Map<String, String> additionalParameters, String memo, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.CURRENCY, currency);
        params.put(RequestField.AMOUNT, String.valueOf(amount));
        if (transRef != null) {
            params.put(RequestField.TRANS_REF, transRef);
        }
        if (targetId != null) {
            params.put(RequestField.TARGET_ID, targetId);
        }
        if (!TextUtils.isEmpty(confirmVal)) {
            params.put(RequestField.CONFIRM_VAL, confirmVal);
        }
        if (operationSubtype != null) {
            params.put(RequestField.OPERATION_SUB_TYPE, operationSubtype);
        }
        if (targetType != -1) {
            params.put(RequestField.TARGET_TYPE, String.valueOf(targetType));
        }
        params.put(RequestField.SOURCE_ID, sourceId);
        params.put(RequestField.FLAGS, flags);
        params.put(RequestField.SOURCE_TYPE, String.valueOf(sourceType));
        if (fullName != null) {
            params.put(RequestField.FULL_NAME, getUtf8String(fullName));
        }
        if (additionalParameters != null) {
            params.putAll(additionalParameters);
        }
        if (memo != null) {
            params.put(RequestField.MEMO, memo);
        }
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static LinkedHashMap<String, String> getAtmParams(String search, Location location, int pointType) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.LATITUDE, String.valueOf(location.getLatitude()));
        params.put(RequestField.LONGITUDE, String.valueOf(location.getLongitude()));
        if (search != null) {
            params.put(RequestField.SEARCH, search);
        }
        if (pointType != -1) {
            params.put(RequestField.TYPE, String.valueOf(pointType));
        }
        params.put(RequestField.LANG, RU_LANGUAGE);
        return params;
    }

    public static LinkedHashMap<String, String> getCardOperationHistoryParams(String cardId, int page, String startDate, String endDate, TransactionState transactionState) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.CARD, cardId);
        if (page != -1) {
            params.put(RequestField.PAGE, String.valueOf(page));
        }
        if (startDate != null) {
            params.put(RequestField.START_DATE, startDate);
        }
        if (endDate != null) {
            params.put(RequestField.END_DATE, endDate);
        }
        if (transactionState != null) {
            params.put(RequestField.TRANSACTION_STATE, transactionState.getTransactionState());
        }
        return params;
    }

    public static LinkedHashMap<String, String> getAccountOperationHistoryParams(String accountId, int page, String startDate, String endDate, TransactionState transactionState) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.ACCOUNT, accountId);
        if (page != -1) {
            params.put(RequestField.PAGE, String.valueOf(page));
        }
        if (startDate != null) {
            params.put(RequestField.START_DATE, startDate);
        }
        if (endDate != null) {
            params.put(RequestField.END_DATE, endDate);
        }
        if (transactionState != null) {
            params.put(RequestField.TRANSACTION_STATE, transactionState.getTransactionState());
        }
        return params;
    }

    public static LinkedHashMap<String, String> getEditCardParams(String cardId, String name, int status, long statusActiveDuration, int securityFlags, int active, String secCode) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.CARD, cardId);
        if (name != null) {
            params.put(RequestField.NAME, getUtf8String(name));
        }
        if (status != -1) {
            params.put(RequestField.STATUS, getUtf8String(String.valueOf(status)));
        }
        if (statusActiveDuration != -1) {
            params.put(RequestField.STATUS_ACTIVE_DURATION, getUtf8String(String.valueOf(statusActiveDuration)));
        }
        if (securityFlags != -1) {
            params.put(RequestField.SECURITY_FLAGS, getUtf8String(String.valueOf(securityFlags)));
        }
        if (active != -1) {
            params.put(RequestField.ACTIVE, getUtf8String(String.valueOf(active)));
        }
        if (secCode != null) {
            params.put(RequestField.SEC_CODE, getUtf8String(secCode));
        }
        return params;
    }

    public static LinkedHashMap<String, String> getCardActivationParams(String cardId, int active, LinkedHashMap<String, String> additionParams, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.CARD, cardId);
        if (active != -1) {
            params.put(RequestField.STATUS, getUtf8String(String.valueOf(active)));
        }
        if (additionParams != null) {
            params.putAll(additionParams);
        }
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static LinkedHashMap<String, String> getCardActivationAdditionParams(String cardNum, String cardExpDate) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        if (cardNum != null) {
            params.put(RequestField.PARAM_CARD_NUM, getUtf8String(cardNum));
        }
        if (cardExpDate != null) {
            params.put(RequestField.PARAM_CARD_EXP_DATE, getUtf8String(cardExpDate));
        }
        return params;
    }

    public static LinkedHashMap<String, String> getAccountActivationParams(String accountId, int active, LinkedHashMap<String, String> additionParams, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.ID, accountId);
        if (active != -1) {
            params.put(RequestField.ACTIVE, getUtf8String(String.valueOf(active)));
        }
        if (additionParams != null) {
            params.putAll(additionParams);
        }
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static LinkedHashMap<String, String> getAccountActivationAdditionParams(String userPassword, String accountSecretWord) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        if (userPassword != null) {
            params.put(RequestField.PARAM_USER_PASSWORD, getUtf8String(userPassword));
        }
        if (accountSecretWord != null) {
            params.put(RequestField.PARAM_ACCOUNT_SECRET_WORD, getUtf8String(accountSecretWord));
        }
        return params;
    }

    public static LinkedHashMap<String, String> getChangeCardLimitParams(String cardId, String uid, String amount, String currency, int active, long activeDuration) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.CARD, cardId);
        params.put(RequestField.UID, getUtf8String(uid));
        if (amount != null && !amount.isEmpty()) {
            params.put(RequestField.AMOUNT, getUtf8String(String.valueOf(amount)));
        }
        if (currency != null) {
            params.put(RequestField.CURRENCY, getUtf8String(String.valueOf(currency)));
        }
        if (active != -1) {
            params.put(RequestField.ACTIVE, String.valueOf(active));
        }
        if (activeDuration != -1) {
            params.put(RequestField.CHANGE_ACTIVE_DURATION, String.valueOf(activeDuration));
        }
        return params;

    }

    public static LinkedHashMap<String, String> getEditAccountParams(String accountId, String name, int status, long statusActiveDuration) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.ACCOUNT, accountId);
        if (name != null) {
            params.put(RequestField.NAME, getUtf8String(name));
        }
        if (status != -1) {
            params.put(RequestField.STATUS, getUtf8String(String.valueOf(status)));
        }
        if (statusActiveDuration != -1) {
            params.put(RequestField.STATUS_ACTIVE_DURATION, getUtf8String(String.valueOf(statusActiveDuration)));
        }
        return params;
    }

    public static LinkedHashMap<String, String> getEditRestrictionParams(String cardId, String osTypeUid, String regionUid, int active) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.CARD, cardId);
        params.put(RequestField.OS_TYPE_UID, osTypeUid);
        params.put(RequestField.REGION_UID, regionUid);
        if (active != -1) {
            params.put(RequestField.ACTIVE, String.valueOf(active));
        }
        return params;
    }

    public static LinkedHashMap<String, String> getAccountCategoryParams(int accountType) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.ACCOUNT_TYPE, String.valueOf(accountType));
        return params;
    }

    public static LinkedHashMap<String, String> getAccountConditionsParams(int accountType, String categoryId, String cardId) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.ACCOUNT_TYPE, String.valueOf(accountType));
        params.put(RequestField.CATEGORY_ID, categoryId);
        params.put(RequestField.CARD_ID, cardId);
        return params;
    }

    public static LinkedHashMap<String, String> getOpenAccountParams(int accountType, String categoryId, String cardId, int periodDays, String amount, String currency, String planDate, String confirmStrategy) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.ACCOUNT_TYPE, String.valueOf(accountType));
        params.put(RequestField.CATEGORY_ID, categoryId);
        params.put(RequestField.CARD_ID, cardId);
        params.put(RequestField.PERIOD_DAYS, String.valueOf(periodDays));
        params.put(RequestField.AMOUNT, amount);
        params.put(RequestField.CURRENCY, currency);
        params.put(RequestField.PLAN_DATE, planDate);
        if (confirmStrategy != null) {
            params.put(RequestField.CONFIRMATION_STRATEGY, confirmStrategy);
        }
        return params;
    }

    public static LinkedHashMap<String, String> getConfirmOpenAccountParams(String transRef, String confirmVal) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.TRANS_REF, transRef);
        if (confirmVal != null) {
            params.put(RequestField.CONFIRM_VAL, confirmVal);
        }
        return params;
    }

    public static LinkedHashMap<String, String> getCustomRequestParams(String payload, String flash) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        if (payload != null) {
            params.put(RequestField.PAYLOAD, getUtf8String(payload));
        }
        if (flash != null) {
            params.put(RequestField.FLASH, getUtf8String(flash));
        }
        return params;
    }

    public static LinkedHashMap<String, String> getPaymentFieldsRequestParams(ArrayList<BaseField> fields) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        for (BaseField baseField : fields) {
            if (baseField.isFillValue()) {
                params.put(baseField.getPaymentParameterId(), baseField.getValue());
            }
        }
        return params;
    }

    public static LinkedHashMap<String, String> getPaymentTemplateAdditionRequestParams(ArrayList<BaseField> fields) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        for (BaseField baseField : fields) {
            if (baseField.isFillValue()) {
                params.put("param." + baseField.getPaymentParameterId(), baseField.getValue());
            }
        }
        return params;
    }

    public static LinkedHashMap<String, String> getCustomPaymentRequestParams(String pid, String payload, String flash, BaseProduct baseProduct) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.PID, pid);
        if (flash != null) {
            params.put(RequestField.FLASH, getUtf8String(flash));
        }
        if (baseProduct != null) {
            if (baseProduct instanceof Card) {
                params.put(RequestField.SOURCE_CARD, getUtf8String(baseProduct.getId()));
            } else {
                params.put(RequestField.SOURCE_ACCOUNT, getUtf8String(baseProduct.getId()));
            }
        }
        if (payload != null) {
            params.put(RequestField.PAYLOAD, getUtf8String(payload));
        }
        return params;
    }

    public static LinkedHashMap<String, String> getCustomPaymentExecuteRequestParams(String pid, String payload, String flash, BaseProduct product, double amount, String transRef, String confirmValue) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.PID, pid);
        if (flash != null) {
            params.put(RequestField.FLASH, getUtf8String(flash));
        }
        if (payload != null) {
            params.put(RequestField.PAYLOAD, getUtf8String(payload));
        }
        if (product != null) {
            if (product instanceof Card) {
                params.put(RequestField.SOURCE_CARD, getUtf8String(product.getId()));
            } else {
                params.put(RequestField.SOURCE_ACCOUNT, getUtf8String(product.getId()));
            }
        }
        params.put(RequestField.AMOUNT, getUtf8String(String.valueOf(amount)));
        params.put(RequestField.CURRENCY, getUtf8String(Constants.RUR));
        if (transRef != null) {
            params.put(RequestField.TRANS_REF, getUtf8String(transRef));
        }
        if (confirmValue != null) {
            params.put(RequestField.CONFIRM_VAL, getUtf8String(confirmValue));
        }
        return params;
    }

    public static LinkedHashMap<String, String> getTimeStampParams(String timeStamp) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        if (timeStamp != null && !timeStamp.isEmpty()) {
            params.put(RequestField.TIMESTAMP, timeStamp);
        }
        params.put(RequestField.LANG, RU_LANGUAGE);
        return params;
    }

    public static LinkedHashMap<String, String> getCreditPaymentScheduleParams(String accountId, int page, String startDate, String endDate) {
        LinkedHashMap<String, String> resultParams = new LinkedHashMap<>();
        if (accountId != null) {
            resultParams.put(RequestField.ACCOUNT, accountId);
        }
        if (page != -1) {
            resultParams.put(RequestField.PAGE, String.valueOf(page));
        }
        if (startDate != null) {
            resultParams.put(RequestField.START_DATE, startDate);
        }
        if (endDate != null) {
            resultParams.put(RequestField.END_DATE, endDate);
        }
        return resultParams;
    }

    public static LinkedHashMap<String, String> getCheckPaymentParams(String pid, String currency, float amount, int sourceType, String sourceId, Map<String, String> additionalParameters, String confirmStrategy, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.PID, pid);
        if (currency != null) {
            params.put(RequestField.CURRENCY, String.valueOf(currency));
        }
        params.put(RequestField.AMOUNT, String.valueOf(amount));
        params.put(RequestField.SOURCE_TYPE, String.valueOf(sourceType));
        if (sourceId != null) {
            params.put(RequestField.SOURCE_ID, sourceId);
        }

        if (additionalParameters != null) {
            params.putAll(additionalParameters);
        }
        if (confirmStrategy != null) {
            params.put(RequestField.CONFIRMATION_STRATEGY, confirmStrategy);
        }
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static LinkedHashMap<String, String> getExecutePaymentParams(String pid, String currency, Float amount, int sourceType, String sourceId, String transRef, Map<String, String> additionalParameters, String confirmVal, int flags, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.PID, pid);
        if (currency != null) {
            params.put(RequestField.CURRENCY, currency);
        }
        if (amount != null) {
            params.put(RequestField.AMOUNT, String.valueOf(amount));
        }
        params.put(RequestField.SOURCE_TYPE, String.valueOf(sourceType));
        if (sourceId != null) {
            params.put(RequestField.SOURCE_ID, sourceId);
        }
        if (transRef != null) {
            params.put(RequestField.TRANS_REF, transRef);
        }
        if (confirmVal != null) {
            params.put(RequestField.CONFIRM_VAL, confirmVal);
        }
        if (additionalParameters != null) {
            params.putAll(additionalParameters);
        }
        params.put(RequestField.FLAGS, String.valueOf(flags));
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static LinkedHashMap<String, String> getCheckTemplateParams(String uid, String currency, Float amount, String confirmStrategy, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.UID, uid);
        if (currency != null) {
            params.put(RequestField.CURRENCY, String.valueOf(currency));
        }
        params.put(RequestField.AMOUNT, String.valueOf(amount));
        if (confirmStrategy != null) {
            params.put(RequestField.CONFIRMATION_STRATEGY, confirmStrategy);
        }
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static LinkedHashMap<String, String> getExecuteTemplateParams(String uid, String currency, Double amount, String transRef, String memo, String confirmVal, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.UID, uid);
        if (currency != null) {
            params.put(RequestField.CURRENCY, currency);
        }
        if (amount != null) {
            params.put(RequestField.AMOUNT, String.valueOf(amount));
        }
        if (transRef != null) {
            params.put(RequestField.TRANS_REF, transRef);
        }
        if (confirmVal != null) {
            params.put(RequestField.CONFIRM_VAL, getUtf8String(confirmVal));
        }
        if (memo != null) {
            params.put(RequestField.MEMO, memo);
        }
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static LinkedHashMap<String, String> getSendMailParams(String transRef, String to) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>(2);
        params.put(RequestField.TRANS_REF, transRef);
        params.put(RequestField.TO, to);
        return params;
    }


    public static Map<String, String> getCreateTemplateParams(@RequestConstants.TemplateActionType int actionType, String currency, String amount,
                                                              @RequestConstants.SourceType int sourceType, String sourceId,
                                                              @RequestConstants.TargetType Integer targetType, String targetId,
                                                              String pid, String name, String fullName,
                                                              Map<String, String> additionalParams, String memo, Integer operationSubtype, Integer flags,
                                                              String transRef, String confirmVal, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.ACTION_TYPE, String.valueOf(actionType));
        params.put(RequestField.CURRENCY, currency);
        params.put(RequestField.AMOUNT, amount);
        params.put(RequestField.SOURCE_TYPE, String.valueOf(sourceType));
        params.put(RequestField.SOURCE_ID, sourceId);
        if (targetType != null) {
            params.put(RequestField.TARGET_TYPE, String.valueOf(targetType));
        }
        if (targetId != null) {
            params.put(RequestField.TARGET_ID, targetId);
        }
        if (pid != null) {
            params.put(RequestField.PID, pid);
        }
        if (name != null) {
            params.put(RequestField.NAME, getUtf8String(name));
        }
        if (fullName != null) {
            params.put(RequestField.FULL_NAME, getUtf8String(fullName));
        }
        if (additionalParams != null) {
            params.putAll(additionalParams);
        }
        if (memo != null) {
            params.put(RequestField.MEMO, memo);
        }
        if (operationSubtype != null) {
            params.put(RequestField.OPERATION_SUB_TYPE, String.valueOf(operationSubtype));
        }
        if (flags != null) {
            params.put(RequestField.FLAGS, String.valueOf(flags));
        }
        params.put(RequestField.TRANS_REF, transRef);
        if (!TextUtils.isEmpty(confirmVal)) {
            params.put(RequestField.CONFIRM_VAL, confirmVal);
        }
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static Map<String, String> getSaveTemplateParams(String name, String recentOpId, int overwrite, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.NAME, getUtf8String(name));
        params.put(RequestField.RECENT_OP_ID, recentOpId);
        params.put(RequestField.OVERWRITE, String.valueOf(overwrite));
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static Map<String, String> getRenameTemplateParams(String uid, String name, int overwrite, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.UID, uid);
        params.put(RequestField.NAME, getUtf8String(name));
        params.put(RequestField.OVERWRITE, String.valueOf(overwrite));
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static Map<String, String> getDeleteTemplateParams(String uid, String uids, String type, Integer allOperations, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        if (uid != null) {
            params.put(RequestField.UID, uid);
        }
        if (uids != null) {
            params.put(RequestField.UIDS, uids);
        }
        if (type != null) {
            params.put(RequestField.TYPE, type);
        }
        if (allOperations != null) {
            params.put(RequestField.ALL_OPERATIONS, String.valueOf(allOperations));
        }
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    public static Map<String, String> getSubscribeOnPushNotificationsParams(String token, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.NOTIF_TOKEN, getUtf8String(token));
        params.put(RequestField.NOTIF_TYPE, "1"); //Type of used push-notification service. 1 – Google Cloud Messaging (GCM).
        return makeRequestParamsWithStampSignature(params, privateKey);
    }

    @NonNull
    public static Map<String, String> getAdditionalParamsPerson(String recipientName, String inn, String accountNumberRecipient, String kpp, String bik,
                                                                String paramId, String passportDeal, String transferTarget, String paramType,
                                                                String receiverPersAccount) {
        Map<String, String> additionParams = new LinkedHashMap<>();
        if (!TextUtils.isEmpty(transferTarget)) {
            additionParams.put(RequestField.PARAM_GROUND, transferTarget);
        }
        if (!TextUtils.isEmpty(kpp)) {
            additionParams.put(RequestField.PARAM_RECEIVER_KPP, kpp);
        }
        if (!TextUtils.isEmpty(bik)) {
            additionParams.put(RequestField.PARAM_RECEIVER_BANK_BIC, bik);
        }
        if (!TextUtils.isEmpty(paramType)) {
            additionParams.put(RequestField.PARAM_TYPE, paramType);
        }
        if (!TextUtils.isEmpty(passportDeal)) {
            additionParams.put(RequestField.PARAM_PSNUMBER, passportDeal);
        }
        if (!TextUtils.isEmpty(inn)) {
            additionParams.put(RequestField.PARAM_RECEIVER_INN, inn);
        }
        if (!TextUtils.isEmpty(paramId)) {
            additionParams.put(RequestField.PARAM_ID, paramId);
        }
        if (!TextUtils.isEmpty(recipientName)) {
            additionParams.put(RequestField.PARAM_RECEIVER, getUtf8String(recipientName));
        }
        if (!TextUtils.isEmpty(accountNumberRecipient)) {
            additionParams.put(RequestField.PARAM_RECEIVER_ACCOUNT, accountNumberRecipient);
        }
        if (!TextUtils.isEmpty(receiverPersAccount)) {
            additionParams.put(RequestField.PARAM_RECEIVER_PERS_ACCOUNT, receiverPersAccount);
        }
        return additionParams;
    }

    public static Map<String, String> getAdditionalParamsBudget(String customerInn, String receiver, String receiverInn, String receiverKpp,
                                                                String receiverAccount, String receiverBankBic, String ground, String uin, String taxStatus,
                                                                String taxType, String reason, String cbcCode, String okatoCode, String taxDate,
                                                                String docType, String taxNumber, String custCode,
                                                                String taxPeriodParam1, String taxPeriodParam2, String taxPeriodParam3,
                                                                String psNumber, String paramType, String paramId) {
        Map<String, String> additionParams = new LinkedHashMap<>();
        if (!TextUtils.isEmpty(customerInn)) {
            additionParams.put(RequestField.PARAM_CUSTOMER_INN, customerInn);
        }
        if (!TextUtils.isEmpty(receiver)) {
            additionParams.put(RequestField.PARAM_RECEIVER, getUtf8String(receiver));
        }
        if (!TextUtils.isEmpty(receiverInn)) {
            additionParams.put(RequestField.PARAM_RECEIVER_INN, receiverInn);
        }
        if (!TextUtils.isEmpty(receiverKpp)) {
            additionParams.put(RequestField.PARAM_RECEIVER_KPP, receiverKpp);
        }
        if (!TextUtils.isEmpty(receiverAccount)) {
            additionParams.put(RequestField.PARAM_RECEIVER_ACCOUNT, receiverAccount);
        }
        if (!TextUtils.isEmpty(receiverBankBic)) {
            additionParams.put(RequestField.PARAM_RECEIVER_BANK_BIC, receiverBankBic);
        }
        if (!TextUtils.isEmpty(ground)) {
            additionParams.put(RequestField.PARAM_GROUND, getUtf8String(ground));
        }
        if (!TextUtils.isEmpty(uin)) {
            additionParams.put(RequestField.PARAM_UIN, uin);
        }
        if (!TextUtils.isEmpty(taxStatus)) {
            additionParams.put(RequestField.PARAM_TAX_STATUS, taxStatus);
        }
        if (!TextUtils.isEmpty(taxType)) {
            additionParams.put(RequestField.PARAM_TAX_TYPE, getUtf8String(taxType));
        }
        if (!TextUtils.isEmpty(reason)) {
            additionParams.put(RequestField.PARAM_REASON, getUtf8String(reason));
        }
        if (!TextUtils.isEmpty(cbcCode)) {
            additionParams.put(RequestField.PARAM_CBC_CODE, cbcCode);
        }
        if (!TextUtils.isEmpty(okatoCode)) {
            additionParams.put(RequestField.PARAM_OKATO_CODE, okatoCode);
        }
        if (!TextUtils.isEmpty(taxDate)) {
            additionParams.put(RequestField.PARAM_TAX_DATE, taxDate);
        }
        if (!TextUtils.isEmpty(docType)) {
            additionParams.put(RequestField.PARAM_DOC_TYPE, docType);
        }
        if (!TextUtils.isEmpty(taxNumber)) {
            additionParams.put(RequestField.PARAM_TAX_NUMBER, taxNumber);
        }
        if (!TextUtils.isEmpty(custCode)) {
            additionParams.put(RequestField.PARAM_CUST_CODE, custCode);
        }
        if (!TextUtils.isEmpty(taxPeriodParam1)) {
            additionParams.put(RequestField.PARAM_TAX_PERIOD_PARAM1, getUtf8String(taxPeriodParam1));
        }
        if (!TextUtils.isEmpty(taxPeriodParam2)) {
            additionParams.put(RequestField.PARAM_TAX_PERIOD_PARAM2, taxPeriodParam2);
        }
        if (!TextUtils.isEmpty(taxPeriodParam3)) {
            additionParams.put(RequestField.PARAM_TAX_PERIOD_PARAM3, taxPeriodParam3);
        }
        if (!TextUtils.isEmpty(psNumber)) {
            additionParams.put(RequestField.PARAM_PSNUMBER, psNumber);
        }
        if (!TextUtils.isEmpty(paramType)) {
            additionParams.put(RequestField.PARAM_TYPE, paramType);
        }
        if (!TextUtils.isEmpty(paramId)) {
            additionParams.put(RequestField.PARAM_ID, paramId);
        }
        return additionParams;
    }

    public static Map<String, String> getRepaymentCreditAccountAdditionParams(String code) {
        Map<String, String> additionParams = new LinkedHashMap<>(1);
        additionParams.put(RequestField.PARAM_SUBSIDIARY, code);
        return additionParams;
    }

    private static String getUtf8String(String input) {
        try {
            return URLEncoder.encode(input, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return input;
        }
    }

    public static LinkedHashMap<String, String> makeRequestParamsWithStampSignature(LinkedHashMap<String, String> params, PrivateKey privateKey) {
        LinkedHashMap<String, String> resultParams = new LinkedHashMap<>();
        resultParams.putAll(params);
        String stamp = getStamp(params, privateKey);
        if (stamp != null) {
            resultParams.put(RequestField.STAMP, stamp);
        }
        return resultParams;
    }

    public static String getStamp(HashMap<String, String> params, PrivateKey privateKey) {
        String stamp = getRequestParamsWithStamp(params);
        if (stamp != null) {
            try {
                String hexStamp = Utils.byteArrayToHexString(getSha1WithRSA(stamp, privateKey));
                return hexStamp;
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    private static String getRequestParamsWithStamp(HashMap<String, String> params) {
        StringBuilder builder = new StringBuilder();
        params.put(RequestField.STAMP, "00000000000000000000");
        int i = 0;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.append(entry.getKey() + "=" + entry.getValue());
            if (i != params.size() - 1) {
                builder.append("&");
            }
            i++;
        }
        return builder.toString();
    }

    public static byte[] getSha1WithRSA(String data, PrivateKey privateKey) throws Exception {
        Signature signature = Signature.getInstance(StoreOptions.ALGORITHM_SHA1_WITH_RSA);
        signature.initSign(privateKey);
        signature.update(data.getBytes());
        return signature.sign();
    }

    public static Map<String, String> getDeviceInformationParams(String ostype, String osversion, String appversion, String vendor, String model, String screen, PrivateKey privateKey) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        if (ostype != null) {
            params.put(RequestField.OS_TYPE, ostype);
        }
        if (osversion != null) {
            params.put(RequestField.OS_VERSION, String.valueOf(osversion));
        }
        if (appversion != null) {
            params.put(RequestField.APP_VERSION, appversion);
        }
        if (vendor != null) {
            params.put(RequestField.VENDOR, vendor);
        }
        if (model != null) {
            params.put(RequestField.MODEL, model);
        }
        if (screen != null) {
            params.put(RequestField.SCREEN, screen);
        }
        return makeRequestParamsWithStampSignature(params, privateKey);
    }
}
