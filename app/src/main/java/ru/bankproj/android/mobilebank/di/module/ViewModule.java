package ru.bankproj.android.mobilebank.di.module;

import android.content.Context;
import android.view.View;

import dagger.Module;
import dagger.Provides;
import ru.bankproj.android.mobilebank.di.qualifier.ViewContext;
import ru.bankproj.android.mobilebank.di.scope.PerView;

/**
 * Created by j7ars on 25.10.2017.
 */
@Module
public class ViewModule {

    private final View mView;

    public ViewModule(View view){
        this.mView = view;
    }

    @Provides
    @PerView
    View provideView(){
        return mView;
    }

    @Provides
    @PerView
    @ViewContext
    Context provideContext(){
        return mView.getContext();
    }

}
