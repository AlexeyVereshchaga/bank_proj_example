package ru.bankproj.android.mobilebank.view.paymentfields;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.AttrRes;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.view.BaseSelectionView;

/**
 * Created by arsen on 25.12.17.
 */

public class DateField extends BaseSelectionView implements DatePickerDialog.OnDateSetListener{
    private static final String STATE_DEFAULT_VALUE = "TextField.STATE_DEFAULT_VALUE";

    private static final String DIALOG_TAG = "DateField.DIALOG_TAG";

    private Activity mActivity;

    public DateField(Context context, Activity activity) {
        super(context);
        initView(context, activity);
    }

    public DateField(Context context, AttributeSet attrs, Activity activity) {
        super(context, attrs);
        initView(context, attrs, activity);
    }

    public DateField(Context context, AttributeSet attrs, @AttrRes int defStyleAttr, Activity activity) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, activity);
    }

    private void initView(final Context context, Activity activity ){
        this.mActivity = activity;
        setListeners();
    }

    private void initView(final Context context, final AttributeSet attrs, Activity activity) {
        this.mActivity = activity;
        etValue.setFocusable(false);
        setListeners();
    }

    private void setListeners(){
        etValue.setOnClickListener(valueClickListener);
    }

    private View.OnClickListener valueClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Calendar calendar = Calendar.getInstance();

            DatePickerDialog dialog = DatePickerDialog.newInstance(DateField.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dialog.setOkText(FieldConverter.getString(R.string.ok));
            dialog.setCancelText(FieldConverter.getString(R.string.cancel));
            dialog.setAccentColor(FieldConverter.getColor(R.color.colorAccent));
            dialog.setVersion(DatePickerDialog.Version.VERSION_1);
            dialog.dismissOnPause(true);
            dialog.show(mActivity.getFragmentManager(), DIALOG_TAG);
        }
    };

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
    }

    private String getValue(){
        return etValue.getText().toString();
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            super.onRestoreInstanceState(bundle);

        } else
            super.onRestoreInstanceState(state);
    }

    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        super.dispatchFreezeSelfOnly(container);
    }

    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        super.dispatchThawSelfOnly(container);
    }

}
