package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.operationhistory.OperationStatus;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.providers.OutputData;

/**
 * TransferExecResponse.java 26.12.2017
 *
 * @author Alex.
 */

public class TransferExecResponse extends BaseResponse {
    @SerializedName("transRef")
    private String transRef;
    @SerializedName("authid")
    private String authid;
    @SerializedName("status")
    private Integer status;
    @SerializedName("planDate")
    private String planDate;
    @SerializedName("fee")
    private Double fee;
    @SerializedName("currency")
    private String currency;
    @SerializedName("confirmReq")
    private ConfirmRequestDescriptor confirmReq;
    @SerializedName("recentOpId")
    private String recentOpId;
    @SerializedName("details")
    private ArrayList<OutputData> detailList;
    @SerializedName("operationStatus")
    private OperationStatus operationStatus;

    public TransferExecResponse(ExecutePaymentResponse executePaymentResponse) {
        transRef = executePaymentResponse.getTransRef();
        authid = executePaymentResponse.getAuthid();
        status = executePaymentResponse.getStatus();
        fee = executePaymentResponse.getFee();
        currency = executePaymentResponse.getCurrency();
        confirmReq = executePaymentResponse.getConfirmReq();
        recentOpId = executePaymentResponse.getRecentOpId();
        detailList = executePaymentResponse.getDetails();
        operationStatus = executePaymentResponse.getOperationStatus();
        setResult(executePaymentResponse.getResult());
        setDescription(executePaymentResponse.getDescription());
    }

    public String getTransRef() {
        return transRef;
    }

    public void setTransRef(String transRef) {
        this.transRef = transRef;
    }

    public String getAuthid() {
        return authid;
    }

    public void setAuthid(String authid) {
        this.authid = authid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ConfirmRequestDescriptor getConfirmReq() {
        return confirmReq;
    }

    public void setConfirmReq(ConfirmRequestDescriptor confirmReq) {
        this.confirmReq = confirmReq;
    }

    public String getRecentOpId() {
        return recentOpId;
    }

    public void setRecentOpId(String recentOpId) {
        this.recentOpId = recentOpId;
    }

    public ArrayList<OutputData> getDetailList() {
        return detailList;
    }

    public void setDetailList(ArrayList<OutputData> detailList) {
        this.detailList = detailList;
    }

    public OperationStatus getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(OperationStatus operationStatus) {
        this.operationStatus = operationStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.transRef);
        dest.writeString(this.authid);
        dest.writeValue(this.status);
        dest.writeString(this.planDate);
        dest.writeValue(this.fee);
        dest.writeString(this.currency);
        dest.writeParcelable(this.confirmReq, flags);
        dest.writeString(this.recentOpId);
        dest.writeTypedList(this.detailList);
        dest.writeParcelable(this.operationStatus, flags);
    }

    public TransferExecResponse() {
    }

    protected TransferExecResponse(Parcel in) {
        super(in);
        this.transRef = in.readString();
        this.authid = in.readString();
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.planDate = in.readString();
        this.fee = (Double) in.readValue(Double.class.getClassLoader());
        this.currency = in.readString();
        this.confirmReq = in.readParcelable(ConfirmRequestDescriptor.class.getClassLoader());
        this.recentOpId = in.readString();
        this.detailList = in.createTypedArrayList(OutputData.CREATOR);
        this.operationStatus = in.readParcelable(OperationStatus.class.getClassLoader());
    }

    public static final Creator<TransferExecResponse> CREATOR = new Creator<TransferExecResponse>() {
        @Override
        public TransferExecResponse createFromParcel(Parcel source) {
            return new TransferExecResponse(source);
        }

        @Override
        public TransferExecResponse[] newArray(int size) {
            return new TransferExecResponse[size];
        }
    };
}
