package ru.bankproj.android.mobilebank.view.paymentfields;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.ValidFields;
import ru.bankproj.android.mobilebank.dataclasses.providers.PaymentParameter;
import ru.bankproj.android.mobilebank.view.paymentfields.base.BaseField;

/**
 * Created by rrust on 17.12.2017.
 */

public class EmailField extends BaseField {

    private LinearLayout mRootView;
    private EditText mEtEmail;
    private PaymentParameter mPaymentParameter;
    private String mEmailValue;

    public EmailField(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        return mRootView;
    }

    @Override
    protected void initView() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mRootView = (LinearLayout) inflater.inflate(R.layout.view_type_email_field, null, false);
        mEtEmail = mRootView.findViewById(R.id.et_email);
    //    initMasckedPhone();
    }

    @Override
    public void setValue(String value) {

    }

    public String getValue(){
        return mEtEmail.getText().toString();
    }

    @Override
    public void setPaymentParameter(PaymentParameter paymentParametr) {
        mPaymentParameter = paymentParametr;
    }

    @Override
    public String getPaymentParameterId() {
        return mPaymentParameter != null ? mPaymentParameter.getId() : "";
    }

    @Override
    public void enableView(boolean isEnable) {
        mEtEmail.setEnabled(isEnable);
    }

    @Override
    public Observable<Boolean> getObservable() {
        return getEmptyFieldWatcherObservable(mEtEmail);
    }

    public Observable<Boolean> getEmptyFieldWatcherObservable(@NonNull final EditText editText) {
        final PublishSubject<Boolean> subject = PublishSubject.create();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                System.out.println(s);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                subject.onNext(ValidFields.isNotEmptyField(s.toString()) && isValid(s.toString()) && ValidFields.isValidEmail(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return subject;
    }

    public boolean isValid(String s){
        return s.length() <= mPaymentParameter.getMax() && s.length() >= mPaymentParameter.getMin();
    }

//    public void initMasckedPhone(){
//        MaskedTextChangedListener listener = new MaskedTextChangedListener("[_----------------------------------------------------------------------------------------------------]@[_-------------------------------------------------------------------------------------------------].[__---]",
//                false, mEtEmail, null, new MaskedTextChangedListener.ValueListener() {
//            @Override
//            public void onTextChanged(boolean b, String s) {
//                if(b && ValidFields.isValidEmail(s)){
//                    mEmailValue = s;
//                }
//            }
//        });
//        mEtEmail.addTextChangedListener(listener);
//    }

}
