package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.providers.OutputData;

/**
 * Created by maxmobiles on 17.12.2017.
 */

public class ConfirmOpenAccountResponse extends BaseResponse {

    @SerializedName("details")
    private ArrayList<OutputData> detailList;

    public ArrayList<OutputData> getDetailList() {
        return detailList;
    }

    public void setDetailList(ArrayList<OutputData> detailList) {
        this.detailList = detailList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.detailList);
    }

    public ConfirmOpenAccountResponse() {
    }

    protected ConfirmOpenAccountResponse(Parcel in) {
        super(in);
        this.detailList = in.createTypedArrayList(OutputData.CREATOR);
    }

    public static final Creator<ConfirmOpenAccountResponse> CREATOR = new Creator<ConfirmOpenAccountResponse>() {
        @Override
        public ConfirmOpenAccountResponse createFromParcel(Parcel source) {
            return new ConfirmOpenAccountResponse(source);
        }

        @Override
        public ConfirmOpenAccountResponse[] newArray(int size) {
            return new ConfirmOpenAccountResponse[size];
        }
    };
}
