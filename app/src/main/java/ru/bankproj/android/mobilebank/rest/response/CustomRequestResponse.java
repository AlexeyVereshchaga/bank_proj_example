package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;

/**
 * Created by j7ars on 20.12.2017.
 */

public class CustomRequestResponse extends BaseResponse {

    @SerializedName("flash")
    private String flash;
    @SerializedName("payload")
    private String payload;
    @SerializedName("autonotifyStatus")
    private boolean isAutoNotifyStatus;

    public String getFlash() {
        return flash;
    }

    public void setFlash(String flash) {
        this.flash = flash;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public boolean isAutoNotifyStatus() {
        return isAutoNotifyStatus;
    }

    public void setAutoNotifyStatus(boolean autoNotifyStatus) {
        isAutoNotifyStatus = autoNotifyStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.flash);
        dest.writeString(this.payload);
        dest.writeByte(this.isAutoNotifyStatus ? (byte) 1 : (byte) 0);
    }

    public CustomRequestResponse() {
    }

    protected CustomRequestResponse(Parcel in) {
        super(in);
        this.flash = in.readString();
        this.payload = in.readString();
        this.isAutoNotifyStatus = in.readByte() != 0;
    }

    public static final Creator<CustomRequestResponse> CREATOR = new Creator<CustomRequestResponse>() {
        @Override
        public CustomRequestResponse createFromParcel(Parcel source) {
            return new CustomRequestResponse(source);
        }

        @Override
        public CustomRequestResponse[] newArray(int size) {
            return new CustomRequestResponse[size];
        }
    };
}
