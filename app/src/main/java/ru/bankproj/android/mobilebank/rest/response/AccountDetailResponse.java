package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.AccountDetail;

/**
 * Created by j7ars on 19.12.2017.
 */

public class AccountDetailResponse extends BaseResponse{

    @SerializedName("account")
    private AccountDetail accountDetail;

    public AccountDetail getAccountDetail() {
        return accountDetail;
    }

    public void setAccountDetail(AccountDetail accountDetail) {
        this.accountDetail = accountDetail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(this.accountDetail, flags);
    }

    public AccountDetailResponse() {
    }

    protected AccountDetailResponse(Parcel in) {
        super(in);
        this.accountDetail = in.readParcelable(AccountDetail.class.getClassLoader());
    }

    public static final Creator<AccountDetailResponse> CREATOR = new Creator<AccountDetailResponse>() {
        @Override
        public AccountDetailResponse createFromParcel(Parcel source) {
            return new AccountDetailResponse(source);
        }

        @Override
        public AccountDetailResponse[] newArray(int size) {
            return new AccountDetailResponse[size];
        }
    };
}
