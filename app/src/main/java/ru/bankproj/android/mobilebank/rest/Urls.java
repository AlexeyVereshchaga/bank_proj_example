package ru.bankproj.android.mobilebank.rest;

import ru.bankproj.android.mobilebank.BuildConfig;

/**
 * Created by j7ars on 25.10.2017.
 */

public class Urls {

    public static final String BASE_BPC_API = BuildConfig.BANK_SERVER_MODE ? "https://ffapi.dbo.bankproj.ru:54293/api/service/" : "https://testapi.dbo.bankproj.ru:54293/api/service/"; // 

    public static final String INITIATE_JOINING = "userJoin/init";
    public static final String CONFIRM_USER_SESSION = "userJoin/confirm";
    public static final String INITIALIZE_PASSWORD = "userJoin/setPassword";
    public static final String INITIATE_RESETTING_PASSWORD = "passReset/init";
    public static final String CONFIRM_RESETTING_USER_SESSION = "passReset/confirm";
    public static final String INITIALIZE_RESETTING_PASSWORD = "passReset/setPassword";
    public static final String LOGIN = "login";
    public static final String ATTACH_DEVICE = "deviceAttach";
    public static final String DETACH_DEVICE = "deviceDetach";
    public static final String CONFIRM_DEVICE = "deviceConfirm";
    public static final String CHANGE_PASSWORD = "passwordChange";
    public static final String LOGOUT = "logout";
    public static final String CHECK_SESSION = "sessionCheck";
    public static final String NEWS = "news";
    public static final String OFFERS = "offers";
    public static final String MESSAGES = "messages";
    public static final String GET_ATM = "pointsByCoord";
    public static final String GET_ATTACHED_DEVICES = "devices";
    public static final String GET_CARD_RESTRICTIONS = "cardAuthRestrictions";
    public static final String EDIT_CARD_RESTRICTION = "cardAuthRestrictionChange";

    public static final String GET_ACCOUNT_LIST = "accounts";
    public static final String GET_CARD_LIST = "cards";
    public static final String GET_CARD_DETAIL = "card";
    public static final String GET_CARD_HISTORY = "cardHistory";
    public static final String GET_ACCOUNT_DETAIL = "account";
    public static final String GET_ACCOUNT_HISTORY = "accountHistory";
    public static final String GET_CARD_CARD_LIMITS = "cardLimits";
    public static final String EDIT_CARD = "cardChange";
    public static final String EDIT_ACCOUNT = "accountChange";
    public static final String EDIT_CARD_LIMIT = "cardLimitsChange";
    public static final String CARD_REQUISITES = "card/requisites";
    public static final String CARD_ACTIVATION_CHANGE = "card/activation/change";

    public static final String GET_ACCOUNT_CATEGORY = "accountCategories";
    public static final String GET_ACCOUNT_CONDITIONS = "accountConditions";
    public static final String OPEN_ACCOUNT = "openAccount";
    public static final String CONFIRM_OPEN_ACCOUNT = "confirmAccount";
    public static final String GET_CONFIRMATION_STRATEGIES = "getConfirmationStrategies";
    public static final String ACCOUNT_ACTIVATION_CHANGE = "account/activation/change";

    public static final String GET_TEMPLATES = "templates";
    public static final String CREATE_TEMPLATE = "templates/create";
    public static final String SAVE_TEMPLATE = "templates/save";
    public static final String RENAME_TEMPLATE = "templates/change";
    public static final String DELETE_TEMPLATE = "templates/delete";
    public static final String CHECK_TEMPLATE = "templates/check";
    public static final String EXECUTE_TEMPLATE = "templates/exec";
    public static final String DEVICE_INFORMATION = "deviceInfo";

    public static final String GET_SERVER_CAPABILITIES = "serverCapabilities";
    public static final String GET_PAYMENT_CATEGORIES = "paymentCategories";
    public static final String GET_PAYMENT_PROVIDERS = "paymentProviders?allCategories=&scenario=dynamic&scenario=static&scenario=custom&allProviders=&allRegions="; //&allRegions
    public static final String PAYMENT_CHECK = "paymentCheck";
    public static final String PAYMENT_EXECUTE = "paymentExec";

    public static final String CUSTOM_REQUEST = "custom";
    public static final String CUSTOM_PAYMENT_REQUEST = "payments/custom";
    public static final String CUSTOM_PAYMENT_EXECUTE_REQUEST = "payments/custom/execute";
    public static final String SEND_MAIL = "send/mail";

    public static final String GET_PAYMENT_SCHEDULE = "account/credit/schedule";

    public static final String URL_GET_IMAGE = BASE_BPC_API + "getImage?resid=%s";
    public static final String URL_GET_IMAGE_CARD = BASE_BPC_API + "getImage?extResId=%s";

    public static final String GET_TRANSFER_CHECK = "transferCheck";
    public static final String GET_TRANSFER_EXEC = "transferExec";
    public static final String REPAYMENT_CHECK = "repaymentCheck";
    public static final String REPAYMENT_EXEC = "repaymentExec";

    public static final String GET_CONTACTS_INFORMATION = "contacts";

    public static final String GET_SUBSIDIARIES = "subsidiaries";
    public static final String SUBSCRIBE_ON_PUSH_NOTIFICATIONS = "pushNotificationsSubscribe";
}