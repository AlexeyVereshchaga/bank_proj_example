package ru.bankproj.android.mobilebank.rest.service;

import java.util.LinkedHashMap;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.rest.RequestField;
import ru.bankproj.android.mobilebank.rest.Urls;
import ru.bankproj.android.mobilebank.rest.response.SessionResponse;

/**
 * Created by j7ars on 27.10.2017.
 */

public interface AuthorizationService {

    @FormUrlEncoded
    @POST(Urls.INITIATE_JOINING)
    Single<Response<SessionResponse>> initiateJoining(@FieldMap LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.CONFIRM_USER_SESSION)
    Single<Response<BaseResponse>> confirmUserSession(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.INITIALIZE_PASSWORD)
    Single<Response<BaseResponse>> initializePassword(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.INITIATE_RESETTING_PASSWORD)
    Single<Response<SessionResponse>> initiateResettingPassword(@FieldMap LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.CONFIRM_RESETTING_USER_SESSION)
    Single<Response<BaseResponse>> confirmResettingUserSession(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.INITIALIZE_RESETTING_PASSWORD)
    Single<Response<BaseResponse>> initializeResettingPassword(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.LOGIN)
    Single<Response<SessionResponse>> login(@FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.ATTACH_DEVICE)
    Single<Response<BaseResponse>> attachDevice(@Header(RequestField.JSESSION) String sessionId, @FieldMap LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.CONFIRM_DEVICE)
    Single<Response<BaseResponse>> confirmDevice(@Header(RequestField.JSESSION) String sessionId, @FieldMap LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.DETACH_DEVICE)
    Single<Response<BaseResponse>> detachDevice(@Header(RequestField.JSESSION) String sessionId, @FieldMap LinkedHashMap<String, String> params);

    @FormUrlEncoded
    @POST(Urls.CHANGE_PASSWORD)
    Single<Response<BaseResponse>> changePassword(@Header(RequestField.JSESSION) String sessionId, @FieldMap(encoded = true) LinkedHashMap<String, String> params);

    @GET(Urls.LOGOUT)
    Single<Response<BaseResponse>> logout(@Header(RequestField.JSESSION) String sessionId);

    @GET(Urls.CHECK_SESSION)
    Single<Response<BaseResponse>> checkSession(@Header(RequestField.JSESSION) String sessionId);

}
