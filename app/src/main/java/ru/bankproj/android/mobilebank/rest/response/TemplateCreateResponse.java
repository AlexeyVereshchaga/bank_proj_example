package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;

/**
 * Created by Alexey Vereshchaga on 26.01.18.
 */

public class TemplateCreateResponse extends BaseResponse {
    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.id);
    }

    public TemplateCreateResponse() {
    }

    protected TemplateCreateResponse(Parcel in) {
        super(in);
        this.id = in.readString();
    }

    public static final Creator<TemplateCreateResponse> CREATOR = new Creator<TemplateCreateResponse>() {
        @Override
        public TemplateCreateResponse createFromParcel(Parcel source) {
            return new TemplateCreateResponse(source);
        }

        @Override
        public TemplateCreateResponse[] newArray(int size) {
            return new TemplateCreateResponse[size];
        }
    };
}