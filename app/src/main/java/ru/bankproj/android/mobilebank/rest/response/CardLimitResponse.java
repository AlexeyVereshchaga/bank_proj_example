package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.CardLimit;

/**
 * Created by j7ars on 06.12.2017.
 */

public class CardLimitResponse extends BaseResponse {

    @SerializedName("limits")
    private ArrayList<CardLimit> limitList;

    public ArrayList<CardLimit> getLimitList() {
        return limitList;
    }

    public void setLimitList(ArrayList<CardLimit> limitList) {
        this.limitList = limitList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.limitList);
    }

    public CardLimitResponse() {
    }

    protected CardLimitResponse(Parcel in) {
        super(in);
        this.limitList = in.createTypedArrayList(CardLimit.CREATOR);
    }

    public static final Creator<CardLimitResponse> CREATOR = new Creator<CardLimitResponse>() {
        @Override
        public CardLimitResponse createFromParcel(Parcel source) {
            return new CardLimitResponse(source);
        }

        @Override
        public CardLimitResponse[] newArray(int size) {
            return new CardLimitResponse[size];
        }
    };
}
