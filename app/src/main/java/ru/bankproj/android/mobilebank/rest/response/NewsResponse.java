package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.news.News;

/**
 * Created by j7ars on 27.10.2017.
 */

public class NewsResponse extends BaseResponse {

    @SerializedName("news")
    private ArrayList<News> news;

    public ArrayList<News> getNews() {
        return news;
    }

    public void setNews(ArrayList<News> mNews) {
        this.news = mNews;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.news);
    }

    public NewsResponse() {
    }

    protected NewsResponse(Parcel in) {
        super(in);
        this.news = in.createTypedArrayList(News.CREATOR);
    }

    public static final Creator<NewsResponse> CREATOR = new Creator<NewsResponse>() {
        @Override
        public NewsResponse createFromParcel(Parcel source) {
            return new NewsResponse(source);
        }

        @Override
        public NewsResponse[] newArray(int size) {
            return new NewsResponse[size];
        }
    };
}
