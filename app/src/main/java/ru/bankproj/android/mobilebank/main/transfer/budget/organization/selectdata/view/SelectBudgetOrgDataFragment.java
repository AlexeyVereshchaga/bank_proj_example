package ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SelectBudgetOrgData;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.ISelectBudgetOrgDataContract;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.adapter.SelectBudgetOrgDataAdapter;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

/**
 * Created by j7ars on 28.12.2017.
 */

public class SelectBudgetOrgDataFragment extends BaseMvpFragment implements ISelectBudgetOrgDataContract.View, SelectBudgetOrgDataAdapter.OnItemClickListener {

    public static final String SELECT_TYPE = "SelectBudgetOrgDataFragment.SELECT_TYPE";

    @BindView(R.id.rv_budget_org_data)
    RecyclerView rvBudgetOrgData;

    @Inject
    ISelectBudgetOrgDataContract.Presenter mPresenter;

    private SelectBudgetOrgDataAdapter mSelectBudgetOrgDataAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initAdapters();
        mPresenter.onCreate(savedInstanceState);
    }

    private void getArgs() {
        if(getArguments() != null){
            mPresenter.setArguments(getArguments().getInt(SELECT_TYPE));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mPresenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    private void initAdapters(){
        mSelectBudgetOrgDataAdapter = new SelectBudgetOrgDataAdapter(mActivity);
        mSelectBudgetOrgDataAdapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        rvBudgetOrgData.setLayoutManager(layoutManager);
        rvBudgetOrgData.setAdapter(mSelectBudgetOrgDataAdapter);
    }

    @Override
    public void initData(ArrayList<SelectBudgetOrgData> selectDataList) {
        mSelectBudgetOrgDataAdapter.setData(selectDataList);
    }

    @Override
    public void onItemClick(int position) {
        mPresenter.budgetOrgDataSelectItem(position);
    }

    @Override
    public void setResult(SelectBudgetOrgData selectBudgetOrgData) {
        Intent intent = new Intent();
        intent.putExtra(Constants.SELECTED_BUDGET_ORG_DATA, selectBudgetOrgData);
        mActivity.setResult(Constants.RESULT_SELECT_BUDGET_DATA_ACTIVITY, intent);
        mActivity.finish();
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_select_budget_org_data;
    }

}
