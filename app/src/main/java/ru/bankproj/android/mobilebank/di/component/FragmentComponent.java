package ru.bankproj.android.mobilebank.di.component;

import dagger.Subcomponent;
import ru.bankproj.android.mobilebank.di.module.FragmentModule;
import ru.bankproj.android.mobilebank.di.module.PresenterModule;
import ru.bankproj.android.mobilebank.di.scope.PerFragment;
import ru.bankproj.android.mobilebank.main.atm.atmdetail.view.ATMDetailFragment;
import ru.bankproj.android.mobilebank.main.atm.filteratm.view.AtmFilterFragment;
import ru.bankproj.android.mobilebank.main.atm.list.view.AtmListFragment;
import ru.bankproj.android.mobilebank.main.atm.main.view.AtmFragment;
import ru.bankproj.android.mobilebank.main.atm.map.view.AtmMapFragment;
import ru.bankproj.android.mobilebank.main.authorization.initpassword.view.InitPasswordFragment;
import ru.bankproj.android.mobilebank.main.authorization.resetpassword.view.ResetPasswordFragment;
import ru.bankproj.android.mobilebank.main.authorization.signin.view.SignInFragment;
import ru.bankproj.android.mobilebank.main.authorization.signup.view.SignUpFragment;
import ru.bankproj.android.mobilebank.main.authorization.smscode.view.SmsCodeFragment;
import ru.bankproj.android.mobilebank.main.block.main.view.BlockManagerFragment;
import ru.bankproj.android.mobilebank.main.contacts.view.ContactFragment;
import ru.bankproj.android.mobilebank.main.fine.document.add.view.FineAddDocumentFragment;
import ru.bankproj.android.mobilebank.main.fine.document.main.view.FineDocumentFragment;
import ru.bankproj.android.mobilebank.main.fine.finedetail.view.FineDetailFragment;
import ru.bankproj.android.mobilebank.main.fine.payment.view.FinePaymentFragment;
import ru.bankproj.android.mobilebank.main.fine.selectedfine.view.SelectedFineFragment;
import ru.bankproj.android.mobilebank.main.fine.selectfine.view.SelectFineFragment;
import ru.bankproj.android.mobilebank.main.fine.successpayment.view.SuccessFinePaymentFragment;
import ru.bankproj.android.mobilebank.main.fine.uid.view.FineUinFragment;
import ru.bankproj.android.mobilebank.main.news.view.NewsListFragment;
import ru.bankproj.android.mobilebank.main.notification.detail.view.NotificationDetailFragment;
import ru.bankproj.android.mobilebank.main.notification.list.view.NotificationListFragment;
import ru.bankproj.android.mobilebank.main.offers.view.OffersListFragment;
import ru.bankproj.android.mobilebank.main.payment.categories.view.PaymentCategoriesFragment;
import ru.bankproj.android.mobilebank.main.payment.payment.view.PaymentExecuteFragment;
import ru.bankproj.android.mobilebank.main.payment.paymentconfirm.view.PaymentConfirmFragment;
import ru.bankproj.android.mobilebank.main.payment.paymentschedule.view.CreditPaymentScheduleFragment;
import ru.bankproj.android.mobilebank.main.payment.paymentsuccess.view.PaymentSuccessFragment;
import ru.bankproj.android.mobilebank.main.payment.providers.view.PaymentProvidersFragment;
import ru.bankproj.android.mobilebank.main.payment.selectcardoraccount.view.SelectCardOrAccountFragment;
import ru.bankproj.android.mobilebank.main.payment.selectfield.view.SelectFieldFragment;
import ru.bankproj.android.mobilebank.main.payment.selectregions.view.PaymentCategoriesSelectRegionsFragment;
import ru.bankproj.android.mobilebank.main.product.account.account.about.view.AccountAboutFragment;
import ru.bankproj.android.mobilebank.main.product.account.account.accountdetail.view.AccountDetailFragment;
import ru.bankproj.android.mobilebank.main.product.account.accountoperation.detail.view.AccountOperationDetailFragment;
import ru.bankproj.android.mobilebank.main.product.account.accountoperation.list.view.AccountOperationHistoryListFragment;
import ru.bankproj.android.mobilebank.main.product.account.credit.about.view.CreditAboutFragment;
import ru.bankproj.android.mobilebank.main.product.account.credit.creditdetail.view.CreditDetailFragment;
import ru.bankproj.android.mobilebank.main.product.account.debit.about.view.DebitAboutFragment;
import ru.bankproj.android.mobilebank.main.product.account.debit.debitdetail.view.DebitDetailFragment;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.agreement.view.DebitAgreementFragment;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.conditions.view.SelectConditionsFragment;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.confirm.open.debit.view.ConfirmOpenDebitFragment;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.main.view.OpenDebitFragment;
import ru.bankproj.android.mobilebank.main.product.account.debit.opendebit.success.view.SuccessOpenDebitFragment;
import ru.bankproj.android.mobilebank.main.product.card.carddetail.view.CardDetailFragment;
import ru.bankproj.android.mobilebank.main.product.card.cardlimitmanager.cardlimit.view.CardLimitFragment;
import ru.bankproj.android.mobilebank.main.product.card.cardlimitmanager.cardlimitmanager.view.CardLimitsManagerFragment;
import ru.bankproj.android.mobilebank.main.product.card.cardoperation.detail.view.CardOperationDetailFragment;
import ru.bankproj.android.mobilebank.main.product.card.cardoperation.list.view.CardOperationHistoryListFragment;
import ru.bankproj.android.mobilebank.main.product.card.cardrestriction.restrictionlist.view.CardRestrictionListFragment;
import ru.bankproj.android.mobilebank.main.product.card.cardrestriction.restrictionsettings.view.CardRestrictionSettingsFragment;
import ru.bankproj.android.mobilebank.main.product.card.cardstatusmanager.view.CardStatusManagerFragment;
import ru.bankproj.android.mobilebank.main.product.operationfilter.view.OperationFilterFragment;
import ru.bankproj.android.mobilebank.main.product.requsites.view.RequisetesFragment;
import ru.bankproj.android.mobilebank.main.product.selectcard.view.SelectCardFragment;
import ru.bankproj.android.mobilebank.main.settings.attacheddevice.view.AttachDeviceSettingFragment;
import ru.bankproj.android.mobilebank.main.settings.changepassword.view.ChangePasswordFragment;
import ru.bankproj.android.mobilebank.main.settings.hidden.products.view.HiddenProductsFragment;
import ru.bankproj.android.mobilebank.main.settings.main.view.SettingsFragment;
import ru.bankproj.android.mobilebank.main.settings.privacy.view.PrivacyFragment;
import ru.bankproj.android.mobilebank.main.settings.push.view.PushSettingFragment;
import ru.bankproj.android.mobilebank.main.template.create.success.view.CreateTemplateSuccessFragment;
import ru.bankproj.android.mobilebank.main.template.templates.view.TemplateListFragment;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.main.view.TransferBudgetOrganizationFragment;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.view.SelectBudgetOrgDataFragment;
import ru.bankproj.android.mobilebank.main.transfer.check.view.TransferCheckFragment;
import ru.bankproj.android.mobilebank.main.transfer.clientgpb.view.TransferClientGpbFragment;
import ru.bankproj.android.mobilebank.main.transfer.individual.person.view.TransferIndividualPersonFragment;
import ru.bankproj.android.mobilebank.main.transfer.legal.person.view.TransferLegalPersonFragment;
import ru.bankproj.android.mobilebank.main.transfer.main.view.TransferFragment;
import ru.bankproj.android.mobilebank.main.transfer.own.account.view.TransferOwnAccountFragment;
import ru.bankproj.android.mobilebank.main.transfer.repayment.check.view.RepaymentCheckFragment;
import ru.bankproj.android.mobilebank.main.transfer.repayment.main.view.RepaymentFragment;
import ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.view.SubsidiaryFragment;
import ru.bankproj.android.mobilebank.main.transfer.success.view.TransferSuccessFragment;
import ru.bankproj.android.mobilebank.main.transfer.template.view.TransferTemplateFragment;
import ru.bankproj.android.mobilebank.main.webviewdetail.view.WebViewDetailFragment;
import ru.bankproj.android.mobilebank.menu.drawerview.view.DrawerViewFragment;

/**
 * Created by j7ars on 25.10.2017.
 */
@PerFragment
@Subcomponent(modules = {FragmentModule.class, PresenterModule.class})
public interface FragmentComponent {

    void inject(DrawerViewFragment drawerViewFragment);

    void inject(SignInFragment signInFragment);

    void inject(SignUpFragment signUpFragment);

    void inject(SmsCodeFragment smsCodeFragment);

    void inject(InitPasswordFragment initPasswordFragment);

    void inject(ResetPasswordFragment resetPasswordFragment);

    void inject(OffersListFragment offersListFragment);

    void inject(WebViewDetailFragment offerDetailFragment);

    void inject(NewsListFragment newsListFragment);

    void inject(NotificationListFragment notificationListFragment);

    void inject(NotificationDetailFragment notificationDetailFragment);

    void inject(BlockManagerFragment blockManagerFragment);

    void inject(CardOperationHistoryListFragment lastOperationListFragment);

    void inject(CardDetailFragment cardDetailFragment);

    void inject(ChangePasswordFragment changePasswordFragment);

    void inject(CardRestrictionListFragment geoLimitsListFragment);

    void inject(CardRestrictionSettingsFragment restrictionSettingsFragment);

    void inject(CardStatusManagerFragment statusManagerFragment);

    void inject(CardLimitsManagerFragment cardLimitsManagerFragment);

    void inject(CardLimitFragment cardLimitForDayFragment);

    void inject(SettingsFragment settingsFragment);

    void inject(PushSettingFragment pushSettingFragment);

    void inject(PrivacyFragment privacyFragment);

    void inject(AttachDeviceSettingFragment attachDeviceFragment);

    void inject(AtmFragment atmFragment);

    void inject(AtmListFragment atmListFragment);

    void inject(AtmMapFragment atmMapFragment);

    void inject(PaymentCategoriesFragment paymentCategoriesFragment);

    void inject(PaymentProvidersFragment paymentProvidersFragment);

    void inject(PaymentCategoriesSelectRegionsFragment paymentCategoriesSelectRegionsFragment);

    void inject(PaymentExecuteFragment paymentExecuteFragment);

    void inject(CreditPaymentScheduleFragment creditPaymentScheduleFragment);

    void inject(AccountOperationHistoryListFragment accountOperationHistoryListFragment);

    void inject(CreditDetailFragment creditDetailFragment);

    void inject(CreditAboutFragment creditAboutFragment);

    void inject(DebitDetailFragment debitDetailFragment);

    void inject(DebitAboutFragment debitAboutFragment);

    void inject(OpenDebitFragment openDebitFragment);

    void inject(SuccessOpenDebitFragment successOpenDebitFragment);

    void inject(DebitAgreementFragment debitAgreementFragment);

    void inject(SelectCardFragment selectCardFragment);

    void inject(SelectCardOrAccountFragment selectCardOrAccountFragment);

    void inject(SelectConditionsFragment selectConditionsFragment);

    void inject(ConfirmOpenDebitFragment confirmOpenDebitFragment);

    void inject(AccountDetailFragment accountDetailFragment);

    void inject(AccountAboutFragment accountAboutFragment);

    void inject(FineDocumentFragment fineDocumentFragment);

    void inject(FineUinFragment fineUidFragment);

    void inject(FineAddDocumentFragment fineAddDocumentFragment);

    void inject(TransferFragment transferFragment);

    void inject(TransferOwnAccountFragment transferOwnAccountFragment);

    void inject(FineDetailFragment fineDetailFragment);

    void inject(SelectedFineFragment selectedFineFragment);

    void inject(SelectFineFragment selectFineFragment);

    void inject(FinePaymentFragment finePaymentFragment);

    void inject(SuccessFinePaymentFragment successFinePaymentFragment);

    void inject(PaymentConfirmFragment paymentConfirmFragment);

    void inject(TransferCheckFragment transferCheckFragment);

    void inject(TemplateListFragment templateListFragment);

    void inject(AccountOperationDetailFragment accountOperationDetailFragment);

    void inject(CardOperationDetailFragment cardOperationDetailFragment);

    void inject(OperationFilterFragment operationFilterFragment);

    void inject(ATMDetailFragment atmDetailFragment);

    void inject(AtmFilterFragment atmFilterFragment);

    void inject(TransferSuccessFragment transferSuccessFragment);

    void inject(TransferIndividualPersonFragment transferIndividualPersonFragment);

    void inject(TransferLegalPersonFragment transferLegalPersonFragment);

    void inject(TransferBudgetOrganizationFragment budgetOrganizationFragment);

    void inject(SelectBudgetOrgDataFragment selectBudgetOrgDataFragment);

    void inject(TransferClientGpbFragment transferClientGpbFragment);

    void inject(PaymentSuccessFragment paymentSuccessFragment);

    void inject(CreateTemplateSuccessFragment createTemplateSuccessFragment);

    void inject(RepaymentFragment repaymentFragment);

    void inject(RepaymentCheckFragment repaymentCheckFragment);

    void inject(TransferTemplateFragment transferTemplateFragment);

    void inject(ContactFragment contactFragment);

    void inject(SubsidiaryFragment subsidiaryFragment);

    void inject(RequisetesFragment requisetesFragment);

    void inject(HiddenProductsFragment hiddenProductsFragment);

    void inject(SelectFieldFragment selectFieldFragment);
}
