package ru.bankproj.android.mobilebank.di.module;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import dagger.Module;
import dagger.Provides;
import ru.bankproj.android.mobilebank.di.qualifier.FragmentContext;
import ru.bankproj.android.mobilebank.di.scope.PerFragment;

/**
 * Created by j7ars on 25.10.2017.
 */
@Module
public class FragmentModule {

    private Fragment mFragment;

    public FragmentModule(Fragment fragment) {
        mFragment = fragment;
    }

    @Provides
    @PerFragment
    Fragment providesFragment() {
        return mFragment;
    }

    @Provides
    @PerFragment
    Activity provideActivity() {
        return mFragment.getActivity();
    }

    @Provides
    @PerFragment
    @FragmentContext
    Context providesContext() {
        return mFragment.getActivity();
    }

}
