package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;

/**
 * Created by Karina on 18.01.2018.
 */

public class ContactsResponse extends BaseResponse {

    @SerializedName("caption")
    @Expose
    private String mCaption;
    @SerializedName("url")
    @Expose
    private String mUrl;

    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String caption) {
        this.mCaption = caption;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.mCaption);
        dest.writeString(this.mUrl);
    }

    public ContactsResponse() {
    }

    protected ContactsResponse(Parcel in) {
        super(in);
        this.mCaption = in.readString();
        this.mUrl = in.readString();
    }

    public static final Creator<ContactsResponse> CREATOR = new Creator<ContactsResponse>() {
        @Override
        public ContactsResponse createFromParcel(Parcel source) {
            return new ContactsResponse(source);
        }

        @Override
        public ContactsResponse[] newArray(int size) {
            return new ContactsResponse[size];
        }
    };
}
