package ru.bankproj.android.mobilebank.managers;

import java.security.PrivateKey;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.bankproj.android.mobilebank.base.request.IRequestCallback;
import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.rest.RequestParams;
import ru.bankproj.android.mobilebank.rest.error.ErrorConsumer;
import ru.bankproj.android.mobilebank.rest.error.RetrofitException;
import ru.bankproj.android.mobilebank.rest.response.ATMResponse;
import ru.bankproj.android.mobilebank.rest.response.AccountCategoryResponse;
import ru.bankproj.android.mobilebank.rest.response.AccountConditionsResponse;
import ru.bankproj.android.mobilebank.rest.response.AccountDetailResponse;
import ru.bankproj.android.mobilebank.rest.response.AccountOperationResponse;
import ru.bankproj.android.mobilebank.rest.response.AccountResponse;
import ru.bankproj.android.mobilebank.rest.response.AttachedDeviceResponse;
import ru.bankproj.android.mobilebank.rest.response.CapabilitiesResponse;
import ru.bankproj.android.mobilebank.rest.response.CardDetailResponse;
import ru.bankproj.android.mobilebank.rest.response.CardLimitResponse;
import ru.bankproj.android.mobilebank.rest.response.CardOperationResponse;
import ru.bankproj.android.mobilebank.rest.response.CardResponse;
import ru.bankproj.android.mobilebank.rest.response.CardRestrictionsResponse;
import ru.bankproj.android.mobilebank.rest.response.CheckPaymentResponse;
import ru.bankproj.android.mobilebank.rest.response.ConfirmOpenAccountResponse;
import ru.bankproj.android.mobilebank.rest.response.ConfirmationStrategiesResponse;
import ru.bankproj.android.mobilebank.rest.response.ContactsResponse;
import ru.bankproj.android.mobilebank.rest.response.CreditDetailResponse;
import ru.bankproj.android.mobilebank.rest.response.CreditPaymentScheduleResponse;
import ru.bankproj.android.mobilebank.rest.response.CustomPaymentExecuteResponse;
import ru.bankproj.android.mobilebank.rest.response.CustomPaymentRequestResponse;
import ru.bankproj.android.mobilebank.rest.response.CustomRequestResponse;
import ru.bankproj.android.mobilebank.rest.response.DebitDetailResponse;
import ru.bankproj.android.mobilebank.rest.response.ExecutePaymentResponse;
import ru.bankproj.android.mobilebank.rest.response.MessagesResponse;
import ru.bankproj.android.mobilebank.rest.response.NewsResponse;
import ru.bankproj.android.mobilebank.rest.response.OffersResponse;
import ru.bankproj.android.mobilebank.rest.response.OpenAccountResponse;
import ru.bankproj.android.mobilebank.rest.response.ProviderCategoriesResponse;
import ru.bankproj.android.mobilebank.rest.response.ProviderPaymentsResponse;
import ru.bankproj.android.mobilebank.rest.response.RequisitesResponse;
import ru.bankproj.android.mobilebank.rest.response.SessionResponse;
import ru.bankproj.android.mobilebank.rest.response.SubsidiariesResponse;
import ru.bankproj.android.mobilebank.rest.response.TemplateCreateResponse;
import ru.bankproj.android.mobilebank.rest.response.TemplateResponse;
import ru.bankproj.android.mobilebank.rest.response.TemplateSaveResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferExecResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferResponse;
import ru.bankproj.android.mobilebank.rest.response.ZipResponse;
import ru.bankproj.android.mobilebank.rest.service.AdditionalService;
import ru.bankproj.android.mobilebank.rest.service.AuthorizationService;
import ru.bankproj.android.mobilebank.rest.service.FunctionalService;
import ru.bankproj.android.mobilebank.rest.service.MainService;
import ru.bankproj.android.mobilebank.rest.service.UserProductService;
import ru.bankproj.android.mobilebank.utils.RequestConstants;

import static ru.bankproj.android.mobilebank.rest.response.ConfirmationStrategiesResponse.PASSWORD_CONFIRMATION_STRATEGY;

/**
 * Created by j7ars on 25.10.2017.
 */
@Singleton
public class RequestManager {

    private Retrofit mMainRetrofit;
    private DataSourceManager mDataSourceManager;
    private DatabaseManager mDatabaseManager;

    @Inject
    public RequestManager(@Named("main") Retrofit mMainRetrofit, DataSourceManager mDataSourceManager, DatabaseManager mDatabaseManager) {
        this.mMainRetrofit = mMainRetrofit;
        this.mDataSourceManager = mDataSourceManager;
        this.mDatabaseManager = mDatabaseManager;
    }

    private <S> S createService(Retrofit retrofit, Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

    public <T extends Response> Disposable makeSingleRequest(Single<T> single, IRequestCallback requestCallback) {
        return single.doOnSubscribe(d -> requestCallback.onStartRequest())
                .doOnSuccess(s -> requestCallback.onSuccessResponse(s))
                .doOnError(ErrorConsumer.consume(new ErrorConsumer.OnErrorHandler() {
                    @Override
                    public void onHandleError(RetrofitException t, String message) {
                        requestCallback.onErrorResponse(t, message);
                    }
                }))
                .doFinally(() -> requestCallback.onFinishRequest())
                .subscribe();
    }

    public <T extends Response> Disposable makeObservableRequest(Observable<T> observable, IRequestCallback requestCallback) {
        return observable.doOnSubscribe(d -> requestCallback.onStartRequest())
                .doOnNext(s -> requestCallback.onSuccessResponse(s))
                .doOnError(ErrorConsumer.consume(new ErrorConsumer.OnErrorHandler() {
                    @Override
                    public void onHandleError(RetrofitException t, String message) {
                        requestCallback.onErrorResponse(t, message);
                    }
                }))
                .doFinally(() -> requestCallback.onFinishRequest())
                .subscribe();
    }

    public <T extends Response> Disposable makeFlowableRequest(Flowable<T> flowable, IRequestCallback requestCallback) {
        return flowable.doOnSubscribe(d -> requestCallback.onStartRequest())
                .doOnNext(s -> requestCallback.onSuccessResponse(s))
                .doOnError(ErrorConsumer.consume(new ErrorConsumer.OnErrorHandler() {
                    @Override
                    public void onHandleError(RetrofitException t, String message) {
                        requestCallback.onErrorResponse(t, message);
                    }
                }))
                .doFinally(() -> requestCallback.onFinishRequest())
                .subscribe();
    }

    public <T extends Response> Disposable makeMaybeRequest(Maybe<T> maybe, IRequestCallback requestCallback) {
        return maybe.doOnSubscribe(d -> requestCallback.onStartRequest())
                .doOnComplete(() -> requestCallback.onSuccessResponse(null))
                .doOnError(ErrorConsumer.consume(new ErrorConsumer.OnErrorHandler() {
                    @Override
                    public void onHandleError(RetrofitException t, String message) {
                        requestCallback.onErrorResponse(t, message);
                    }
                }))
                .doFinally(() -> requestCallback.onFinishRequest())
                .subscribe();
    }

    public Disposable makeCompletableRequest(Completable completable, IRequestCallback requestCallback) {
        return completable.doOnSubscribe(d -> requestCallback.onStartRequest())
                .doOnComplete(() -> requestCallback.onSuccessResponse(null))
                .doOnError(ErrorConsumer.consume(new ErrorConsumer.OnErrorHandler() {
                    @Override
                    public void onHandleError(RetrofitException t, String message) {
                        requestCallback.onErrorResponse(t, message);
                    }
                }))
                .doFinally(() -> requestCallback.onFinishRequest())
                .subscribe();
    }

    public <T extends Response> Single<T> makeSingleIOMainSubscriber(Single<T> single) {
        return single.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T extends Response> Observable<T> makeObservableIOMainSubscriber(Observable<T> observable) {
        return observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T extends Response> Flowable<T> makeFlowableIOMainSubscriber(Flowable<T> flowable) {
        return flowable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T extends Response> Maybe<T> makeMaybeIOMainSubscriber(Maybe<T> maybe) {
        return maybe.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Completable makeCompletableIOMainSubscriber(Completable completable) {
        return completable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    //requests
    public Single<Response<SessionResponse>> initiateJoining(LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).initiateJoining(params);
    }

    public Disposable initiateJoining(LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(initiateJoining(params)), requestCallback);
    }

    public Single<Response<BaseResponse>> confirmUserSession(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).confirmUserSession(sessionId, params);
    }

    public Disposable confirmUserSession(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(confirmUserSession(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> initializePassword(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).initializePassword(sessionId, params);
    }

    public Disposable initializePassword(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(initializePassword(sessionId, params)), requestCallback);
    }

    public Single<Response<SessionResponse>> initiateResettingPassword(LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).initiateResettingPassword(params);
    }

    public Disposable initiateResettingPassword(LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(initiateResettingPassword(params)), requestCallback);
    }

    public Single<Response<BaseResponse>> confirmResettingUserSession(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).confirmResettingUserSession(sessionId, params);
    }

    public Disposable confirmResettingUserSession(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(confirmResettingUserSession(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> initializeResettingPassword(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).initializeResettingPassword(sessionId, params);
    }

    public Disposable initializeResettingPassword(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(initializeResettingPassword(sessionId, params)), requestCallback);
    }

    public Single<Response<SessionResponse>> login(LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).login(params);
    }

    public Disposable login(LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(login(params)), requestCallback);
    }

    public Single<Response<BaseResponse>> attachDevice(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).attachDevice(sessionId, params);
    }

    public Disposable attachDevice(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(attachDevice(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> confirmDevice(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).confirmDevice(sessionId, params);
    }

    public Disposable confirmDevice(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(confirmDevice(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> detachDevice(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).detachDevice(sessionId, params);
    }

    public Disposable detachDevice(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(detachDevice(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> changePassword(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, AuthorizationService.class).changePassword(sessionId, params);
    }

    public Disposable changePassword(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(changePassword(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> logout(String sessionId) {
        return createService(mMainRetrofit, AuthorizationService.class).logout(sessionId);
    }

    public Disposable logout(String sessionId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(logout(sessionId)), requestCallback);
    }

    public Single<Response<BaseResponse>> checkSession(String sessionId) {
        return createService(mMainRetrofit, AuthorizationService.class).checkSession(sessionId);
    }

    public Disposable checkSession(String sessionId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(checkSession(sessionId)), requestCallback);
    }


    public Single<Response<ConfirmationStrategiesResponse>> getConfirmationStrategies(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, MainService.class).getConfirmationStrategies(sessionId, params);
    }

    public Disposable getConfirmationStrategies(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getConfirmationStrategies(sessionId, params)), requestCallback);
    }

    private Single<Response<TransferResponse>> transferCheck(String sessionId, Map<String, String> params, String confirmationStrategy) {
        return createService(mMainRetrofit, FunctionalService.class).transferCheck(sessionId, params)
                .map(transferResponseResponse -> {
                    TransferResponse transferResponse = transferResponseResponse.body();
                    if (transferResponse != null) {
                        transferResponse.setConfirmationStrategy(confirmationStrategy);
                    }
                    return transferResponseResponse;
                });
    }

    public Disposable transferCheck(String sessionId, LinkedHashMap<String, String> confirmationStrategiesParams,
                                    String currency, String amount, @RequestConstants.SourceType int sourceType, String sourceId,
                                    @RequestConstants.TargetType int targetType, String targetId, String fullName, Map<String, String> additionalParameters,
                                    PrivateKey privateKey,
                                    IRequestCallback iRequestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getConfirmationStrategies(sessionId, confirmationStrategiesParams)
                .flatMap(response -> {
                    String confirmationStrategy = null;
                    ConfirmationStrategiesResponse strategiesResponse = response.body();
                    if (strategiesResponse != null && strategiesResponse.getConfirmationStrategies() != null) {
                        for (String strategy :
                                strategiesResponse.getConfirmationStrategies()) {
                            if (strategy.equalsIgnoreCase(PASSWORD_CONFIRMATION_STRATEGY)) {
                                confirmationStrategy = strategy;
                            }
                        }
                    }
                    Map<String, String> params = RequestParams.getTransferCheckParams(currency, Float.parseFloat(amount),
                            sourceType, sourceId, targetType, targetId, fullName, additionalParameters, confirmationStrategy, privateKey);
                    return transferCheck(sessionId, params, confirmationStrategy);
                })), iRequestCallback);
    }

    private Single<Response<TransferResponse>> repaymentCheck(String sessionId, Map<String, String> params, String confirmationStrategy) {
        return createService(mMainRetrofit, FunctionalService.class).repaymentCheck(sessionId, params)
                .map(transferResponseResponse -> {
                    TransferResponse transferResponse = transferResponseResponse.body();
                    if (transferResponse != null) {
                        transferResponse.setConfirmationStrategy(confirmationStrategy);
                    }
                    return transferResponseResponse;
                });
    }

    public Disposable repaymentCheck(String sessionId, LinkedHashMap<String, String> confirmationStrategiesParams,
                                     String currency, String amount, Integer sourceType, String sourceId,
                                     Integer targetType, String targetId, String fullName, Map<String, String> additionalParameters,
                                     PrivateKey privateKey,
                                     IRequestCallback iRequestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getConfirmationStrategies(sessionId, confirmationStrategiesParams)
                .flatMap(response -> {
                    String confirmationStrategy = null;
                    ConfirmationStrategiesResponse strategiesResponse = response.body();
                    if (strategiesResponse != null && strategiesResponse.getConfirmationStrategies() != null) {
                        for (String strategy :
                                strategiesResponse.getConfirmationStrategies()) {
                            if (strategy.equalsIgnoreCase(PASSWORD_CONFIRMATION_STRATEGY)) {
                                confirmationStrategy = strategy;
                            }
                        }
                    }
                    Map<String, String> params = RequestParams.getTransferCheckParams(
                            currency, Float.parseFloat(amount), sourceType, sourceId, targetType, targetId, fullName, additionalParameters, confirmationStrategy, privateKey);
                    return repaymentCheck(sessionId, params, confirmationStrategy);
                })), iRequestCallback);
    }

    private Single<Response<TransferExecResponse>> transferExec(String sessionId, Map<String, String> params) {
        return createService(mMainRetrofit, FunctionalService.class).transferExec(sessionId, params);
    }

    public Disposable transferExec(String sessionId, String currency, String amount,
                                   String transRef, String targetId, String confirmVal/*password*/, String operationSubtype,
                                   int targetType, String sourceId, String flags, int sourceType,
                                   String fullName, Map<String, String> additionalParameters, String memo,
                                   PrivateKey privateKey, IRequestCallback iRequestCallback) {
        Map<String, String> params = RequestParams.getTransferExecParams(currency, Float.parseFloat(amount), transRef, targetId, confirmVal, operationSubtype,
                targetType, sourceId, flags, sourceType, fullName, additionalParameters, memo, privateKey);
        return makeSingleRequest(makeSingleIOMainSubscriber(transferExec(sessionId, params)), iRequestCallback);
    }

    private Single<Response<TransferExecResponse>> repaymentExec(String sessionId, Map<String, String> params) {
        return createService(mMainRetrofit, FunctionalService.class).repaymentExec(sessionId, params);
    }

    public Disposable repaymentExec(String sessionId, String currency, String amount,
                                    String transRef, String targetId, String confirmVal/*password*/, String operationSubtype,
                                    int targetType, String sourceId, String flags, int sourceType,
                                    String fullName, Map<String, String> additionalParameters, String memo,
                                    PrivateKey privateKey, IRequestCallback iRequestCallback) {
        Map<String, String> params = RequestParams.getTransferExecParams(currency, Float.parseFloat(amount), transRef, targetId, confirmVal, operationSubtype,
                targetType, sourceId, flags, sourceType, fullName, additionalParameters, memo, privateKey);
        return makeSingleRequest(makeSingleIOMainSubscriber(repaymentExec(sessionId, params)), iRequestCallback);
    }

    public Single<Response<AttachedDeviceResponse>> getAttachedDevices(String sessionId) {
        return createService(mMainRetrofit, MainService.class).getAttachedDevices(sessionId);
    }

    public Disposable getAttachedDevices(String sessionId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getAttachedDevices(sessionId)), requestCallback);
    }

    public Single<Response<NewsResponse>> getNews(LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, MainService.class).getNews(params);
    }

    public Disposable getNews(LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getNews(params)
                .map(s -> mDataSourceManager.setNewsList(s))), requestCallback);
    }

    public Single<Response<OffersResponse>> getOffers(LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, MainService.class).getOffers(params);
    }

    public Disposable getOffers(LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getOffers(params)
                .map(s -> mDataSourceManager.setOfferList(s))), requestCallback);
    }

    public Single<Response<ATMResponse>> getAtm(LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, MainService.class).getAtm(params);
    }

    public Disposable getAtm(LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getAtm(params)
                .map(s -> mDataSourceManager.setAtmList(s))), requestCallback);
    }

    public Single<Response<MessagesResponse>> getMessages(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, MainService.class).getMessages(sessionId, params);
    }

    public Disposable getMessages(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getMessages(sessionId, params)), requestCallback);
    }

    public Single<Response<AccountResponse>> getAccountsList(String sessionId) {
        return createService(mMainRetrofit, UserProductService.class).getAccountList(sessionId);
    }

    public Disposable getAccountsList(String sessionId, int accountType, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getAccountsList(sessionId))
                .map(s -> mDataSourceManager.setAccountList(accountType, s)), requestCallback);
    }

    public Single<Response<CardResponse>> getCardsList(String sessionId) {
        return createService(mMainRetrofit, UserProductService.class).getCardList(sessionId);
    }

    public Disposable getCardsList(String sessionId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getCardsList(sessionId))
                .map(s -> mDataSourceManager.setCardList(s)), requestCallback);
    }

    public Single<Response<CardDetailResponse>> getCardDetail(String sessionId, String cardId) {
        return createService(mMainRetrofit, UserProductService.class).getCardDetail(sessionId, cardId);
    }

    public Disposable getCardDetail(String sessionId, String cardId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getCardDetail(sessionId, cardId)), requestCallback);
    }

    public Single<Response<CardOperationResponse>> getCardsOperationHistory(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).getCardOperationHistory(sessionId, params);
    }

    public Disposable getCardsOperationHistory(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getCardsOperationHistory(sessionId, params)), requestCallback);
    }

    public Single<Response<CreditDetailResponse>> getCreditDetail(String sessionId, String accountId) {
        return createService(mMainRetrofit, UserProductService.class).getCreditDetail(sessionId, accountId);
    }

    public Disposable getCreditDetail(String sessionId, String accountId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getCreditDetail(sessionId, accountId)), requestCallback);
    }

    public Single<Response<DebitDetailResponse>> getDebitDetail(String sessionId, String accountId) {
        return createService(mMainRetrofit, UserProductService.class).getDebitDetail(sessionId, accountId);
    }

    public Disposable getDebitDetail(String sessionId, String accountId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getDebitDetail(sessionId, accountId)), requestCallback);
    }

    public Single<Response<AccountDetailResponse>> getAccountDetail(String sessionId, String accountId) {
        return createService(mMainRetrofit, UserProductService.class).getAccountDetail(sessionId, accountId);
    }

    public Disposable getAccountDetail(String sessionId, String accountId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getAccountDetail(sessionId, accountId)), requestCallback);
    }

    public Single<Response<AccountOperationResponse>> getAccountOperationHistory(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).getAccountOperationHistory(sessionId, params);
    }

    public Disposable getAccountOperationHistory(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getAccountOperationHistory(sessionId, params)), requestCallback);
    }

    public Single<Response<CardLimitResponse>> getCardLimits(String sessionId, String cardId) {
        return createService(mMainRetrofit, UserProductService.class).getCardLimits(sessionId, cardId);
    }

    public Disposable getCardLimits(String sessionId, String cardId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getCardLimits(sessionId, cardId)), requestCallback);
    }

    public Single<Response<CardRestrictionsResponse>> getCardRestrictions(String sessionId, String cardId) {
        return createService(mMainRetrofit, UserProductService.class).getCardRestrictions(sessionId, cardId);
    }

    public Disposable getCardRestrictions(String sessionId, String cardId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getCardRestrictions(sessionId, cardId)), requestCallback);
    }

    public Single<Response<BaseResponse>> editCardRestrictions(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).editCardRestrictions(sessionId, params);
    }

    public Disposable editCardRestrictions(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(editCardRestrictions(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> editCard(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).editCard(sessionId, params);
    }

    public Disposable editCard(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(editCard(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> changeCardActivation(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).changeCardActivation(sessionId, params);
    }

    public Disposable changeCardActivation(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(changeCardActivation(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> changeAccountActivation(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).changeAccountActivation(sessionId, params);
    }

    public Disposable changeAccountActivation(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(changeAccountActivation(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> editCardLimit(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).editCardLimit(sessionId, params);
    }

    public Disposable editCardLimit(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(editCardLimit(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> editAccount(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).editAccount(sessionId, params);
    }

    public Disposable editAccount(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(editAccount(sessionId, params)), requestCallback);
    }

    public Single<Response<AccountCategoryResponse>> getAccountCategory(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).getAccountCategory(sessionId, params);
    }

    public Disposable getAccountCategory(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getAccountCategory(sessionId, params)), requestCallback);
    }

    public Single<Response<AccountConditionsResponse>> getAccountCondition(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).getAccountConditions(sessionId, params);
    }

    public Disposable getAccountCondition(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getAccountCondition(sessionId, params)), requestCallback);
    }

    public Single<Response<OpenAccountResponse>> openAccount(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).openAccount(sessionId, params);
    }

    public Disposable openAccount(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(openAccount(sessionId, params)), requestCallback);
    }

    public Single<Response<ConfirmOpenAccountResponse>> confirmOpenAccount(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).confirmOpenAccount(sessionId, params);
    }

    public Disposable confirmOpenAccount(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(confirmOpenAccount(sessionId, params)), requestCallback);
    }

    private Single<Response<CheckPaymentResponse>> checkPayment(String sessionId, Map<String, String> params, String confirmationStrategy) {
        return createService(mMainRetrofit, UserProductService.class).checkPayment(sessionId, params)
                .map(paymentResponseResponse -> {
                    CheckPaymentResponse paymentResponse = paymentResponseResponse.body();
                    if (paymentResponse != null) {
                        paymentResponse.setConfirmationStrategy(confirmationStrategy);
                    }
                    return paymentResponseResponse;
                });
    }

    public Disposable checkPayment(String sessionId, LinkedHashMap<String, String> confirmationStrategiesParams, String pid,
                                   String currency, String amount, Integer sourceType, String sourceId, Map<String, String> additionalParameters,
                                   PrivateKey privateKey,
                                   IRequestCallback iRequestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getConfirmationStrategies(sessionId, confirmationStrategiesParams).delay(1000, TimeUnit.MILLISECONDS)
                .flatMap(response -> {
                    String confirmationStrategy = null;
                    ConfirmationStrategiesResponse strategiesResponse = response.body();
                    if (strategiesResponse != null && strategiesResponse.getConfirmationStrategies() != null) {
                        for (String strategy :
                                strategiesResponse.getConfirmationStrategies()) {
                            if (strategy.equalsIgnoreCase(PASSWORD_CONFIRMATION_STRATEGY)) {
                                confirmationStrategy = strategy;
                            }
                        }
                    }
                    Map<String, String> params = RequestParams.getCheckPaymentParams(pid,
                            currency, Float.parseFloat(amount), sourceType, sourceId, additionalParameters, confirmationStrategy, privateKey);
                    return checkPayment(sessionId, params, confirmationStrategy);
                })), iRequestCallback);
    }

    public Single<Response<ExecutePaymentResponse>> executePayment(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).executePayment(sessionId, params);
    }

    public Disposable executePayment(String sessionId, String pid, String currency, String amount, int sourceType, String sourceId,
                                     String transRef, Map<String, String> additionalParameters, String confirmVal/*password*/, int flags,
                                     PrivateKey privateKey, IRequestCallback iRequestCallback) {
        LinkedHashMap<String, String> params = RequestParams.getExecutePaymentParams(pid, currency, Float.parseFloat(amount), sourceType, sourceId, transRef, additionalParameters,
                confirmVal, flags, privateKey);
        return makeSingleRequest(makeSingleIOMainSubscriber(executePayment(sessionId, params)), iRequestCallback);
    }

    public Single<Response<CheckPaymentResponse>> checkTemplate(String sessionId, LinkedHashMap<String, String> params, String confirmationStrategy) {
        return createService(mMainRetrofit, UserProductService.class).checkTemplate(sessionId, params)
                .map(checkPaymentResponse -> {
                    CheckPaymentResponse templateResponse = checkPaymentResponse.body();
                    if (templateResponse != null) {
                        templateResponse.setConfirmationStrategy(confirmationStrategy);
                    }
                    return checkPaymentResponse;
                });
    }

    public Disposable checkTemplate(String sessionId, LinkedHashMap<String, String> confirmationStrategiesParams, String uid, String currency, String amount, PrivateKey privateKey, IRequestCallback iRequestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getConfirmationStrategies(sessionId, confirmationStrategiesParams).delay(1000, TimeUnit.MILLISECONDS)
                .flatMap(response -> {
                    String confirmationStrategy = null;
                    ConfirmationStrategiesResponse strategiesResponse = response.body();
                    if (strategiesResponse != null && strategiesResponse.getConfirmationStrategies() != null) {
                        for (String strategy :
                                strategiesResponse.getConfirmationStrategies()) {
                            if (strategy.equalsIgnoreCase(PASSWORD_CONFIRMATION_STRATEGY)) {
                                confirmationStrategy = strategy;
                            }
                        }
                    }
                    LinkedHashMap<String, String> params = RequestParams.getCheckTemplateParams(uid,
                            currency, Float.parseFloat(amount), confirmationStrategy, privateKey);
                    return checkTemplate(sessionId, params, confirmationStrategy);
                })), iRequestCallback);
    }

    public Single<Response<ExecutePaymentResponse>> executeTemplate(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).executeTemplate(sessionId, params);
    }

    public Disposable executeTemplate(String sessionId, LinkedHashMap<String, String> params, IRequestCallback iRequestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(executeTemplate(sessionId, params)), iRequestCallback);
    }

    public Single<Response<TemplateResponse>> getTemplates(String sessionId) {
        return createService(mMainRetrofit, UserProductService.class).getTemplates(sessionId);
    }

    public Disposable getTemplates(String sessionId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getTemplates(sessionId)).
                map(s -> mDataSourceManager.setTemplateList(s)), requestCallback);
    }

    public Single<Response<CustomRequestResponse>> customRequest(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).customRequest(sessionId, params);
    }

    public Disposable customRequest(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(customRequest(sessionId, params)), requestCallback);
    }

    public Single<Response<CustomPaymentRequestResponse>> customPaymentRequest(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).customPaymentRequest(sessionId, params);
    }

    public Disposable customPaymentRequest(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(customPaymentRequest(sessionId, params)), requestCallback);
    }

    public Single<Response<CustomPaymentExecuteResponse>> customPaymentExecuteRequest(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, UserProductService.class).customPaymentExecuteRequest(sessionId, params);
    }

    public Disposable customPaymentExecuteRequest(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(customPaymentExecuteRequest(sessionId, params)), requestCallback);
    }

    public Single<Response<CreditPaymentScheduleResponse>> getCreditPaymentSchedule(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, MainService.class).getCreditPaymentSchedule(sessionId, params);
    }

    public Disposable getCreditPaymentSchedule(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getCreditPaymentSchedule(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> sendMail(String sessionId, LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, MainService.class).sendMail(sessionId, params);
    }

    public Disposable sendMail(String sessionId, LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(sendMail(sessionId, params)), requestCallback);
    }

    public Disposable getBankProduct(LinkedHashMap<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(Single.zip(
                makeSingleIOMainSubscriber(getNews(params).map(n -> mDataSourceManager.setNewsList(n))),
                makeSingleIOMainSubscriber(getOffers(params).map(o -> mDataSourceManager.setOfferList(o))),
                (news, offer) -> new ZipResponse(news, offer).getResponse()), requestCallback);
    }

    public Single<Response<ProviderCategoriesResponse>> getProviderCategories(String sessionId) {
        return createService(mMainRetrofit, MainService.class).getProviderCategories(sessionId);
    }

    public Single<Response<ProviderPaymentsResponse>> getPaymentProviders(String sessionId) {
        return createService(mMainRetrofit, MainService.class).getPaymentProviders(sessionId);
    }

    public Single<Response<CapabilitiesResponse>> getServerCapabilities(String sessionId) {
        return createService(mMainRetrofit, MainService.class).getServerCapabilities(sessionId);
    }

    // TODO: 23.01.18 uncomment providers 
    public Disposable getAppData(String sessionId, IRequestCallback requestCallback) {
        return makeSingleRequest(Single.zip(
                getProviderCategories(sessionId).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                        .map(pc -> {
                            Completable.fromAction(() -> saveProviderCategoriesToDB(pc)).subscribeOn(Schedulers.newThread()).subscribe();
                            return pc;
                        }),
                getPaymentProviders(sessionId).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                        .map(pp -> {
                            Completable.fromAction(() -> savePaymentProvidersToDB(pp)).subscribeOn(Schedulers.newThread()).subscribe();
                            return pp;
                        }),
                getServerCapabilities(sessionId).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                        .map(sc -> {
                            Completable.fromAction(() -> saveServerCapabilitiesToDB(sc)).subscribeOn(Schedulers.newThread()).subscribe();
                            return sc;
                        }),
                getAccountsList(sessionId).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                        .map(al -> mDataSourceManager.setAccountData(al)),
                getAccountsList(sessionId).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                        .map(al -> mDataSourceManager.setAccountData(al)),
                getTemplates(sessionId).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                        .map(al -> mDataSourceManager.setTemplateList(al)),
                (category, payment, capabilities, cards, accounts, templates)
                        -> new ZipResponse(category, payment, capabilities, cards, accounts, templates).getResponse()),
                requestCallback);
    }

    private Response<ProviderCategoriesResponse> saveProviderCategoriesToDB(Response<ProviderCategoriesResponse> providerCategoriesResponse) {
        if (providerCategoriesResponse.isSuccessful()) {
            ProviderCategoriesResponse response = providerCategoriesResponse.body();
            if (response.getResult() == ResponseHandler.SUCCESS_CODE) {
                mDatabaseManager.deleteAllCategories();
//                for (Category category : response.getCategories()) {
//                    if (SortListUtils.isNotEmptyList(category.getRids())) {
//                        for (String rid : category.getRids()) {
//                            category.setCategoryRid(rid);
//                            mDatabaseManager.insertOrReplaceCategory(category);
//                        }
//                    } else {
//                        mDatabaseManager.insertOrReplaceCategory(category);
//                    }
//                }
                mDatabaseManager.insertOrReplaceCategories(response.getCategories());
            }
        }
        return providerCategoriesResponse;
    }

    private Response<ProviderPaymentsResponse> savePaymentProvidersToDB(Response<ProviderPaymentsResponse> paymentProvidersResponse) {
        if (paymentProvidersResponse.isSuccessful()) {
            ProviderPaymentsResponse response = paymentProvidersResponse.body();
            if (response.getResult() == ResponseHandler.SUCCESS_CODE) {
                mDatabaseManager.deleteAllProviders();
//                mDatabaseManager.deleteAllPaymentParameterRecords();
//                for (Provider provider : response.getProviders()) {
//                    savePaymentParametersForProvider(provider.getPaymentParameterRecords(), provider.getPid());
//                    if (SortListUtils.isNotEmptyList(provider.getRids())) {
//                        for (String rid : provider.getRids()) {
//                            provider.setProviderRid(rid);
//                            mDatabaseManager.insertOrReplaceProvider(provider);
//                        }
//                    } else {
//                        mDatabaseManager.insertOrReplaceProvider(provider);
//                    }
//                }
                mDatabaseManager.insertOrReplaceProviders(response.getProviders());
            }
        }
        return paymentProvidersResponse;
    }

//    private void savePaymentParametersForProvider(List<PaymentParameter> paymentParameterRecordList, String pId) {
//        if (paymentParameterRecordList != null && paymentParameterRecordList.size() != 0) {
//            for (PaymentParameter paymentParameterRecord : paymentParameterRecordList) {
//                paymentParameterRecord.setPid(pId);
//                mDatabaseManager.insertOrReplacePaymentParameterRecord(paymentParameterRecord);
//            }
//        }
//    }

    private Response<CapabilitiesResponse> saveServerCapabilitiesToDB(Response<CapabilitiesResponse> capabilitiesResponse) {
        if (capabilitiesResponse.isSuccessful()) {
            CapabilitiesResponse response = capabilitiesResponse.body();
            if (response.getResult() == ResponseHandler.SUCCESS_CODE) {
                mDatabaseManager.deleteAllSupportedRegions();
                mDatabaseManager.insertAllSupportedRegions(response.getSupportedRegions());
                mDatabaseManager.deleteAllSupportedCurrencies();
                mDatabaseManager.insertAllSupportedCurrencies(response.getSupportedCurrencies());
                mDatabaseManager.deleteSupportedFunctions();
                mDatabaseManager.insertOrReplaceSupportedFunctions(response.getSupportedFunctions());
            }
        }
        return capabilitiesResponse;
    }

    private Single<Response<TemplateCreateResponse>> createTemplate(String sessionId, Map<String, String> params) {
        return createService(mMainRetrofit, FunctionalService.class).createTemplate(sessionId, params);
    }

    public Disposable createTemplate(String sessionId, @RequestConstants.TemplateActionType int actionType, String currency, String amount,
                                     @RequestConstants.SourceType int sourceType, String sourceId,
                                     @RequestConstants.TargetType Integer targetType, String targetId,
                                     String pid, String name, String fullName, Map<String, String> additionalParams, String memo,
                                     @RequestConstants.OperationSubtype Integer operationSubtype,
                                     Integer flags, //@RequestConstants.TransferFlags @RequestConstants.PaymentFlags
                                     String transRef, String confirmVal,
                                     PrivateKey privateKey, IRequestCallback requestCallback) {
        Map<String, String> params = RequestParams.getCreateTemplateParams(actionType, currency, amount, sourceType, sourceId, targetType, targetId,
                pid, name, fullName, additionalParams, memo, operationSubtype, flags, transRef, confirmVal, privateKey);
        return makeSingleRequest(makeSingleIOMainSubscriber(createTemplate(sessionId, params)), requestCallback);
    }

    private Single<Response<TemplateSaveResponse>> saveTemplate(String sessionId, Map<String, String> params) {
        return createService(mMainRetrofit, FunctionalService.class).saveTemplate(sessionId, params);
    }

    public Disposable saveTemplate(String sessionId, String name, String recentOpId, int overwrite,
                                   PrivateKey privateKey, IRequestCallback requestCallback) {
        Map<String, String> params = RequestParams.getSaveTemplateParams(name, recentOpId, overwrite, privateKey);
        return makeSingleRequest(makeSingleIOMainSubscriber(saveTemplate(sessionId, params)), requestCallback);
    }

    private Single<Response<BaseResponse>> renameTemplate(String sessionId, Map<String, String> params) {
        return createService(mMainRetrofit, FunctionalService.class).renameTemplate(sessionId, params);
    }

    public Disposable renameTemplate(String sessionId, String uid, String name, int overwrite,
                                     PrivateKey privateKey, IRequestCallback requestCallback) {
        Map<String, String> params = RequestParams.getRenameTemplateParams(uid, name, overwrite, privateKey);
        return makeSingleRequest(makeSingleIOMainSubscriber(renameTemplate(sessionId, params)), requestCallback);
    }

    private Single<Response<BaseResponse>> deleteTemplate(String sessionId, Map<String, String> params) {
        return createService(mMainRetrofit, FunctionalService.class).deleteTemplate(sessionId, params);
    }

    public Disposable deleteTemplate(String sessionId, String uid,
                                     PrivateKey privateKey, IRequestCallback requestCallback) {
        Map<String, String> params = RequestParams.getDeleteTemplateParams(uid, null, null, null, privateKey);
        return makeSingleRequest(makeSingleIOMainSubscriber(deleteTemplate(sessionId, params)), requestCallback);
    }

    public Single<Response<ContactsResponse>> getContactsInformation(LinkedHashMap<String, String> params) {
        return createService(mMainRetrofit, FunctionalService.class).getContactsInformation(params);
    }

    public Disposable getContactsInformation(IRequestCallback requestCallback) {
        LinkedHashMap<String, String> params = RequestParams.getLanguageParams();
        return makeSingleRequest(makeSingleIOMainSubscriber(getContactsInformation(params)), requestCallback);
    }

    public Single<Response<RequisitesResponse>> getCardRequisites(String sessionId, String cardId) {
        return createService(mMainRetrofit, FunctionalService.class).getCardRequisites(sessionId, cardId);
    }

    public Disposable getCardRequisites(String sessionId, String cardId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getCardRequisites(sessionId, cardId)), requestCallback);
    }

    public Single<Response<SubsidiariesResponse>> getSubsidiaries(String sessionId) {
        return createService(mMainRetrofit, MainService.class).getSubsidiaries(sessionId);
    }

    public Disposable getSubsidiaries(String sessionId, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getSubsidiaries(sessionId)), requestCallback);
    }

    private Single<Response<BaseResponse>> postDeviceInformation(String sessionId, Map<String, String> params) {
        return createService(mMainRetrofit, FunctionalService.class).postDeviceInformation(sessionId, params);
    }

    public Disposable postDeviceInformation(String sessionId, Map<String, String> params, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(postDeviceInformation(sessionId, params)), requestCallback);
    }

    public Single<Response<BaseResponse>> subscribeOnPushNotifications(String sessionId, Map<String, String> params) {
        return createService(mMainRetrofit, AdditionalService.class).subscribeOnPushNotifications(sessionId, params);
    }

    public Disposable subscribeOnPushNotifications(String sessionId, String token, PrivateKey privateKey, IRequestCallback requestCallback) {
        Map<String, String> params = RequestParams.getSubscribeOnPushNotificationsParams(token, privateKey);
        return makeSingleRequest(subscribeOnPushNotifications(sessionId, params), requestCallback);
    }

}
