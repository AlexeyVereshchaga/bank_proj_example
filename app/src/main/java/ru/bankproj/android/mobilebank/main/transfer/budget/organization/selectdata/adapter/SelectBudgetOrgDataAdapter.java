package ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SelectBudgetOrgData;

/**
 * Created by j7ars on 28.12.2017.
 */

public class SelectBudgetOrgDataAdapter extends RecyclerView.Adapter<SelectBudgetOrgDataAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<SelectBudgetOrgData> mSelectBudgetOrgDataList;

    private OnItemClickListener mOnItemClickListener;

    public SelectBudgetOrgDataAdapter(Context context){
        this.mContext = context;
        mSelectBudgetOrgDataList = new ArrayList<>();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public void setData(ArrayList<SelectBudgetOrgData> selectBudgetOrgDataList) {
        this.mSelectBudgetOrgDataList = selectBudgetOrgDataList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_select_budget_org_data_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SelectBudgetOrgData selectBudgetOrgData = mSelectBudgetOrgDataList.get(position);
        if(selectBudgetOrgData != null){
            if(position == 0) {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.llSelectBudgetOrgContainer.getLayoutParams();
                params.setMargins((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, mContext.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, mContext.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, mContext.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, mContext.getResources().getDisplayMetrics()));
                holder.llSelectBudgetOrgContainer.setLayoutParams(params);
            } else if(position == mSelectBudgetOrgDataList.size() - 1){
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.llSelectBudgetOrgContainer.getLayoutParams();
                params.setMargins((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, mContext.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, mContext.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, mContext.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, mContext.getResources().getDisplayMetrics()));
                holder.llSelectBudgetOrgContainer.setLayoutParams(params);
            } else{
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.llSelectBudgetOrgContainer.getLayoutParams();
                params.setMargins((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, mContext.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, mContext.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, mContext.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, mContext.getResources().getDisplayMetrics()));
                holder.llSelectBudgetOrgContainer.setLayoutParams(params);
            }
            holder.tvBudgetOrgData.setText(selectBudgetOrgData.getValue());
        }
    }

    @Override
    public int getItemCount() {
        return mSelectBudgetOrgDataList == null ? 0 : mSelectBudgetOrgDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private LinearLayout llSelectBudgetOrgContainer;
        private TextView tvBudgetOrgData;

        public ViewHolder(View view) {
            super(view);
            llSelectBudgetOrgContainer = (LinearLayout) itemView.findViewById(R.id.ll_select_budget_org_container) ;
            tvBudgetOrgData = (TextView) itemView.findViewById(R.id.tv_budget_org_data);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(mOnItemClickListener != null){
                mOnItemClickListener.onItemClick(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

}