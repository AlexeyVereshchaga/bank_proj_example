package ru.bankproj.android.mobilebank.main.transfer.repayment.dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.fragment.BaseDialogFragment;
import ru.bankproj.android.mobilebank.utils.FieldConverter;

/**
 * Created by karinka on 31.01.18.
 */

public class RepaymentMessageDialog extends BaseDialogFragment {

    public static final String TAG = "BalanceDialog.TAG";
    public static final String MESSAGE = "RepaymentMessageDialog.MESSAGE";

    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.sub_text)
    TextView mSubText;
    @BindView(R.id.btn_dialog_ok)
    TextView mBtnDialonClose;

    private OnDialogResultListener mListener;
    String mMessage;

    public static RepaymentMessageDialog newInstance(String message) {
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, message);
        RepaymentMessageDialog balanceDialog = new RepaymentMessageDialog();
        balanceDialog.setArguments(bundle);
        return balanceDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData();
    }

    private void setData() {
        mTitle.setVisibility(View.GONE);
        mSubText.setText(mMessage);
        mBtnDialonClose.setText(FieldConverter.getString(R.string.close));
    }

    private void getArgs() {
        if (getArguments() != null) {
            mMessage = getArguments().getString(MESSAGE);
        }
    }

    public void setOnResultListener(OnDialogResultListener listener) {
        mListener = listener;
    }

    @OnClick(R.id.btn_dialog_ok)
    public void onClick() {
        if (mListener != null) {
            mListener.onResultDialog(Activity.RESULT_OK);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_balance;
    }

    public interface OnDialogResultListener {
        void onResultDialog(int statusCode);
    }
}
