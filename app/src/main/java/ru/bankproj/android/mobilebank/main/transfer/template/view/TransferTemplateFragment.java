package ru.bankproj.android.mobilebank.main.transfer.template.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.providers.PaymentParameter;
import ru.bankproj.android.mobilebank.dataclasses.templates.Template;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.block.dialogs.BalanceDialog;
import ru.bankproj.android.mobilebank.main.dialogs.InfoActionDialog;
import ru.bankproj.android.mobilebank.main.dialogs.InfoDialog;
import ru.bankproj.android.mobilebank.main.transfer.check.view.TransferCheckActivity;
import ru.bankproj.android.mobilebank.main.transfer.template.ITransferTemplateContract;
import ru.bankproj.android.mobilebank.rest.response.TransferResponse;
import ru.bankproj.android.mobilebank.utils.AmountDigitsInputFilter;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.RequestConstants;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.utils.storage.AppStorage;
import ru.bankproj.android.mobilebank.view.EnableButton;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Alexey Vereshchaga on 16.01.18.
 */

public class TransferTemplateFragment extends BaseMvpFragment implements ITransferTemplateContract.View {

    public static final String TEMPLATE = "TransferTemplateFragment.TEMPLATE";

    @BindView(R.id.source_product)
    View sourceProductInclude;
    @BindView(R.id.target_product)
    View targetProductInclude;
    @BindView(R.id.ll_target)
    LinearLayout llTarget;
    @BindView(R.id.ll_addition_params_container)
    LinearLayout llAdditionParamsContainer;

    @BindView(R.id.et_limit_assignment)
    EditText etAmount;
    @BindView(R.id.btn_transfer_next_step)
    EnableButton btnNext;

    private IncludedCardView sourceView;
    private IncludedCardView targetView;

    static class IncludedCardView {
        @BindView(R.id.tv_card_name)
        TextView tvCardName;
        @BindView(R.id.tv_card_number)
        TextView tvCardNumber;
        @BindView(R.id.tv_card_amount)
        TextView tvCardAmount;
        @BindView(R.id.tv_card_currency)
        TextView tvCardCurrency;
        @BindView(R.id.iv_card_icon)
        ImageView ivCardIcon;
        @BindView(R.id.tv_card_balance)
        TextView tvCardBalance;
        @BindView(R.id.iv_card_balance)
        ImageView ivCardBalance;
        @BindView(R.id.ll_amount_container)
        LinearLayout llAmountContainer;
        @BindView(R.id.ll_amount_balance)
        LinearLayout llAmountBalance;
    }

    @Inject
    ITransferTemplateContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs() {
        if (getArguments() != null) {
            presenter.setArguments(getArguments().getParcelable(TEMPLATE));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bindIncluded();
        presenter.onCreate(savedInstanceState);
        initView();
        Utils.hideKeyboard(mActivity);
    }

    private void initView() {
        etAmount.setFilters(new InputFilter[]{new AmountDigitsInputFilter()});
        btnNext.enable(true);
    }

    private void bindIncluded() {
        sourceView = new IncludedCardView();
        targetView = new IncludedCardView();
        ButterKnife.bind(sourceView, sourceProductInclude);
        ButterKnife.bind(targetView, targetProductInclude);
        initIncluded(sourceView);
        initIncluded(targetView);
    }

    private void initIncluded(IncludedCardView includedCardView) {
        SpannableString sb1 = new SpannableString(getString(R.string.card_balance));
        sb1.setSpan(new UnderlineSpan(), 0, getString(R.string.card_balance).length(), 0);
        includedCardView.tvCardBalance.setText(sb1);
        if (AppStorage.getBooleanValue(Constants.IS_HIDE_BALANCE, mActivity)) {
            includedCardView.llAmountBalance.setVisibility(VISIBLE);
            includedCardView.llAmountContainer.setVisibility(GONE);
            includedCardView.tvCardBalance.setOnClickListener(view
                    -> crossFade(includedCardView.llAmountContainer, includedCardView.llAmountBalance));
            includedCardView.llAmountContainer.setOnClickListener(view
                    -> crossFade(includedCardView.llAmountBalance, includedCardView.llAmountContainer));
            includedCardView.ivCardBalance.setOnClickListener(view -> showBalanceDialog());
        } else {
            includedCardView.llAmountBalance.setVisibility(GONE);
            includedCardView.llAmountContainer.setVisibility(VISIBLE);
        }
    }

    private void crossFade(LinearLayout showLayout, LinearLayout hideLayout) {
        hideLayout.animate()
                .alpha(0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        hideLayout.setVisibility(GONE);
                        showLayout.setAlpha(0f);
                        showLayout.setVisibility(VISIBLE);
                        showLayout.animate()
                                .alpha(1f)
                                .setDuration(300)
                                .setListener(null);
                    }
                });
    }

    public void showBalanceDialog() {
        BalanceDialog dialogFragment = BalanceDialog.newInstance();
        dialogFragment.setOnResultListener(statusCode -> dialogFragment.dismiss());
        dialogFragment.show(getActivity().getSupportFragmentManager(), BalanceDialog.TAG);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        presenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        presenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_transfer_template;
    }

    @Override
    public void showTemplate(Template template) {
        if (template.getSourceProduct() != null) {
            initProduct(sourceView, template.getSourceProduct());
        }

        if (template.getTargetProduct() != null) {
            llTarget.setVisibility(VISIBLE);
            initProduct(targetView, template.getTargetProduct());
        } else {
            llTarget.setVisibility(GONE);
        }
        etAmount.setText(String.format("%s", template.getAmount()));

        if (template.getPaymentParameters() != null && !template.getPaymentParameters().isEmpty()) {
            fillAdditionParams(template.getPaymentParameters());
        } else {
            llAdditionParamsContainer.setVisibility(View.GONE);
        }
        if (template.getTargetType().equals(RequestConstants.EXTERNAL_CARD)) {
            if (template.getTargetId() != null) {
                ArrayList list = new ArrayList();
                list.add(new PaymentParameter(getResources().getStringArray(R.array.param_names_without_prefix)[22], template.getTargetId())); //<string-array name="param_names_without_prefix">
                if (template.getFullName() != null) {
                    list.add(new PaymentParameter(getResources().getStringArray(R.array.param_names_without_prefix)[23], template.getFullName())); //<string-array name="param_names_without_prefix">
                }
                fillAdditionParams(list);
            }
        }
    }

    private void fillAdditionParams(ArrayList<PaymentParameter> paymentParameters) {
        llAdditionParamsContainer.setVisibility(View.VISIBLE);
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        List<String> paramsNamesInternal = Arrays.asList(getResources().getStringArray(R.array.param_names_without_prefix));
        String[] paramsNamesExternal = getResources().getStringArray(R.array.param_views);
        for (PaymentParameter paymentParameter :
                paymentParameters) {
            String id = paymentParameter.getId();

            int position = paramsNamesInternal.indexOf(id);
            if (position != -1) {
                View view = layoutInflater.inflate(R.layout.row_addition_param_template, null);
                TextView tvTitle = view.findViewById(R.id.tv_param_title);
                TextView tvParamView = view.findViewById(R.id.tv_param_view);
                tvTitle.setText(paramsNamesExternal[position]);
                tvParamView.setText(paymentParameter.getValue());
                llAdditionParamsContainer.addView(view);
            }
        }
    }

    @Override
    public void openTransferCheckScreen(Template template, TransferResponse transferResponse, String amount) {
        getScreenCreator().startActivity(this, mActivity, TransferCheckActivity.class,
                createTransferCheckBundle(template, transferResponse, amount), Constants.START_TRANSFER_CHECK_ACTIVITY);
    }

    private void showInfoActionDialog(@StringRes int stringRes) {
        InfoActionDialog infoActionDialog = InfoActionDialog.newInstance(getString(stringRes));
        infoActionDialog.setOnOkClickListener(() -> getActivity().finish());
        infoActionDialog.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @Override
    public void showEmptySourceCardDialog() {
        showInfoActionDialog(R.string.limit_empty_card_dialog_text);
    }

    @Override
    public void showTemplateLimitSourceCardDialog() {
        showInfoActionDialog(R.string.limit_template_source_card_dialog_text);
    }

    @Override
    public void showBlockedSourceCardDialog() {
        showInfoActionDialog(R.string.limit_template_blocked_source_card_dialog_text);
    }

    @Override
    public void showExpiredSourceCardDialog() {
        showInfoActionDialog(R.string.limit_template_expired_source_card_dialog_text);
    }

    @Override
    public void showTemporaryBlockedCardDialog() {
        showInfoActionDialog(R.string.limit_template_temporary_blocked_source_card_dialog_text);
    }

    @Override
    public void showTargetProductBlockedDialog() {
        showInfoActionDialog(R.string.limit_template_blocked_target_product_dialog_text);
    }

    @Override
    public void showTargetAccountTemporaryBlockedDialog() {
        showInfoActionDialog(R.string.limit_template_temporary_blocked_target_account_dialog_text);
    }

    @Override
    public void showTargetAccountBlockedDialog() {
        showInfoActionDialog(R.string.limit_template_blocked_target_product_dialog_text);
    }

    @Override
    public void showBlockedTargetCardDialog() {
        showInfoActionDialog(R.string.limit_template_blocked_target_product_dialog_text);
    }

    @Override
    public void showTemporaryBlockedTargetCardDialog() {
        showInfoActionDialog(R.string.limit_template_temporary_blocked_target_card_dialog_text);
    }

    @Override
    public void showNotValidAmountMessage(int validationValue) {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(validationValue));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @NonNull
    private Bundle createTransferCheckBundle(Template template, TransferResponse transferResponse, String amount) {
        Bundle bundle = new Bundle();
        CheckTransferData checkTransferData = new CheckTransferData();
        checkTransferData.setTransferResponse(transferResponse);
        checkTransferData.setAmount(amount);
        checkTransferData.setType(CheckTransferData.TEMPLATE_TRANSFER);
        checkTransferData.setTemplate(template);
        bundle.putParcelable(TransferCheckActivity.CHECK_TRANSFER_DATA, checkTransferData);
        return bundle;
    }

    private void initProduct(IncludedCardView includedCardView, BaseProduct baseProduct) {
        String name = null;
        if (baseProduct instanceof Card) {
            name = ((Card) baseProduct).getCardName();
        } else if (baseProduct instanceof Account) {
            name = ((Account) baseProduct).getAccountName();
        }
        includedCardView.tvCardName.setText(name);
        includedCardView.tvCardNumber.setText(Utils.getMaskedPan(baseProduct.getNumber()));
        includedCardView.tvCardAmount.setText(Utils.formatMoneyValue(baseProduct.getBalance()));
        includedCardView.tvCardCurrency.setText(FieldConverter.getCurrency(baseProduct.getCurrency()));
        if (baseProduct instanceof Card) {
            includedCardView.ivCardIcon.setImageResource(((Card) baseProduct).getCardBrand());
        } else {
            includedCardView.ivCardIcon.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.btn_transfer_next_step)
    void onNextClick() {
        presenter.onNextClicked(etAmount.getText().toString());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.START_TRANSFER_CHECK_ACTIVITY:
                if (resultCode == Constants.RESULT_LOGOUT) {
                    unAuthorized();
                }
                break;
        }
    }
}
