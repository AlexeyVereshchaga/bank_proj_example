package ru.bankproj.android.mobilebank.view.progress;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.dataclasses.ErrorData;
import ru.bankproj.android.mobilebank.rest.error.RetrofitException;

/**
 * Created by j7ars on 20.11.2017.
 */

public class BlockProgressView extends RelativeLayout {

    private TextView mTvProgressError;
    private TextView mTvProgressErrorAction;

    private OnRetryListener mOnRetryListener;

    private ErrorData mErrorData;
    private RetrofitException mRetrofitException;

    public void setOnRetryListener(OnRetryListener onRetryListener){
        this.mOnRetryListener = onRetryListener;
    }

    public BlockProgressView(Context context) {
        super(context);
        init(context);
    }

    public BlockProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BlockProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.include_block_progress_layout, this);
    }

    public void errorLoading(ErrorData errorData) {
        this.setVisibility(VISIBLE);
        this.mErrorData = errorData;
        if(mErrorData != null) {
            mTvProgressError.setText(mErrorData.getErrorMessage());
        }
    }

    public void errorLoading(RetrofitException retrofitException) {
        this.setVisibility(VISIBLE);
        this.mRetrofitException = retrofitException;
        if(mRetrofitException != null) {
            mTvProgressError.setText(mRetrofitException.getMessage());
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mTvProgressError = (TextView) this.findViewById(R.id.tv_progress_error);
        mTvProgressErrorAction = (TextView) this.findViewById(R.id.btn_progress_error_retry);

        mTvProgressErrorAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnRetryListener != null){
                    mOnRetryListener.onRetry();
                }
            }
        });
    }

    public interface OnRetryListener{
        void onRetry();
    }
}
