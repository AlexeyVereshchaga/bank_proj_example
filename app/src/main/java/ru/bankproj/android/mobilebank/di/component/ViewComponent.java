package ru.bankproj.android.mobilebank.di.component;

import dagger.Subcomponent;
import ru.bankproj.android.mobilebank.di.module.PresenterModule;
import ru.bankproj.android.mobilebank.di.module.ViewModule;
import ru.bankproj.android.mobilebank.di.scope.PerView;
import ru.bankproj.android.mobilebank.main.block.atm.view.AtmBlockView;
import ru.bankproj.android.mobilebank.main.block.auth.AuthBlockView;
import ru.bankproj.android.mobilebank.main.block.myproduct.myaccount.view.MyAccountBlockView;
import ru.bankproj.android.mobilebank.main.block.myproduct.mycard.view.MyCardBlockView;
import ru.bankproj.android.mobilebank.main.block.myproduct.mycredit.view.MyCreditBlockView;
import ru.bankproj.android.mobilebank.main.block.myproduct.mydebit.view.MyDebitBlockView;
import ru.bankproj.android.mobilebank.main.block.news.view.NewsBlockView;
import ru.bankproj.android.mobilebank.main.block.offers.view.OffersBlockView;
import ru.bankproj.android.mobilebank.main.block.template.view.TemplateBlockView;
import ru.bankproj.android.mobilebank.main.product.card.carddetail.block.CardOperationView;

/**
 * Created by j7ars on 25.10.2017.
 */
@PerView
@Subcomponent(modules = {ViewModule.class, PresenterModule.class})
public interface ViewComponent {

    void inject(OffersBlockView offersBlockView);

    void inject(NewsBlockView newsBlockView);

    void inject(AuthBlockView authBlockView);

    void inject(AtmBlockView atmBlockView);

    void inject(MyCardBlockView myCardBlockView);

    void inject(MyAccountBlockView myAccountBlockView);

    void inject(MyCreditBlockView myCreditBlockView);

    void inject(MyDebitBlockView myDebitBlockView);

    void inject(CardOperationView cardHistoryView);

    void inject(TemplateBlockView templateBlockView);

}
