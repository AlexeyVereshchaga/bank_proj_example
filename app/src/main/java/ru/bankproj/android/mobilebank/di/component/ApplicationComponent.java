package ru.bankproj.android.mobilebank.di.component;

import javax.inject.Singleton;

import dagger.Component;
import ru.bankproj.android.mobilebank.di.module.ApplicationModule;
import ru.bankproj.android.mobilebank.di.module.NetModule;

/**
 * Created by j7ars on 25.10.2017.
 */
@Singleton
@Component(modules = {ApplicationModule.class, NetModule.class})
public interface ApplicationComponent {

    ConfigPersistentComponent configPersistentComponent();

}
