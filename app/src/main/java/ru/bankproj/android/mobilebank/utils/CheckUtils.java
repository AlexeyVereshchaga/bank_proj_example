package ru.bankproj.android.mobilebank.utils;

import android.text.TextUtils;
import android.widget.EditText;

import java.util.Objects;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.view.ErrorLabelContainer;

/**
 * Created by Alexey Vereshchaga on 23.01.18.
 */

public class CheckUtils {

    @FunctionalInterface
    public interface Predicate<T> {
        boolean test(T var1);

        default Predicate<T> and(Predicate<? super T> other) {
            Objects.requireNonNull(other);
            return (t) -> test(t) && other.test(t);
        }

        default Predicate<T> negate() {
            return (t) -> !test(t);
        }

        default Predicate<T> or(Predicate<? super T> other) {
            Objects.requireNonNull(other);
            return (t) -> test(t) || other.test(t);
        }
    }

    public static abstract class CheckEditTextPredicate implements Predicate<String> {
        protected ErrorLabelContainer elContainer;
        protected EditText editText;
        protected String errorMessage;

        public CheckEditTextPredicate() {
        }

        public CheckEditTextPredicate(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public void setEditText(EditText editText) {
            this.editText = editText;
            this.elContainer = (ErrorLabelContainer) editText.getParent();
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public ErrorLabelContainer getElContainer() {
            return elContainer;
        }

        public void setElContainer(ErrorLabelContainer elContainer) {
            this.elContainer = elContainer;
        }
    }

    public static class RegexPredicate extends CheckEditTextPredicate {
        private String regex;

        public RegexPredicate(EditText editText, ErrorLabelContainer errorLabelContainer, String regex, String errorMessage) {
            this.elContainer = errorLabelContainer;
            this.editText = editText;
            this.regex = regex;
            this.errorMessage = errorMessage;
        }

        public RegexPredicate(EditText editText, String regex, String errorMessage) {
            this.editText = editText;
            this.regex = regex;
            this.errorMessage = errorMessage;
        }

        public RegexPredicate(String regex, String errorMessage) {
            this.regex = regex;
            this.errorMessage = errorMessage;
        }

        @Override
        public boolean test(String stringToTest) {
            boolean matches = stringToTest.matches(regex);
            if (!matches) {
                elContainer.setError(errorMessage);
            } else {
                elContainer.clearError();
            }
            return matches;
        }
    }

    public static class RegexPredicateWithoutError extends CheckEditTextPredicate {
        private String regex;

        public RegexPredicateWithoutError(String regex) {
            this.regex = regex;
        }

        @Override
        public boolean test(String stringToTest) {
            return stringToTest.matches(regex);
        }
    }


    public static class EmptyStringPredicate extends CheckEditTextPredicate {
        public EmptyStringPredicate(EditText editText, ErrorLabelContainer errorLabelContainer) {
            this.elContainer = errorLabelContainer;
            this.editText = editText;
        }

        @Override
        public boolean test(String stringToTest) {
            boolean empty = TextUtils.isEmpty(stringToTest);
            if (empty) {
                elContainer.setError(FieldConverter.getString(R.string.empty_field_message));
            } else {
                elContainer.clearError();
            }
            elContainer.setTag(!empty);
            return !empty;
        }
    }

    public static class IndividualPersonInnPredicate extends CheckEditTextPredicate {

        @Override
        public boolean test(String var1) {
            return testInn12Numbers(editText, elContainer);
        }

    }

    private static boolean testInn12Numbers(EditText editText, ErrorLabelContainer elContainer) {
        int[] coefficients1 = {7, 2, 4, 10, 3, 5, 9, 4, 6, 8};
        int[] coefficients2 = {3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8};

        String numberToCheck = editText.getText().toString();
        char[] numberChars = numberToCheck.toCharArray();
        int[] numbers = new int[12];

        int[] numbersComposition1 = new int[10];
        int sum1 = 0;
        int controlNumber1;

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Character.getNumericValue(numberChars[i]);
        }

        for (int i = 0; i < coefficients1.length; i++) {
            numbersComposition1[i] = numbers[i] * coefficients1[i];
        }

        for (int number :
                numbersComposition1) {
            sum1 += number;
        }
        controlNumber1 = sum1 % 11;
        if (controlNumber1 > 9) {
            controlNumber1 = controlNumber1 % 10;
        }

        int[] numbersComposition2 = new int[11];
        int sum2 = 0;
        int controlNumber2;


        for (int i = 0; i < coefficients2.length; i++) {
            numbersComposition2[i] = numbers[i] * coefficients2[i];
        }

        for (int number :
                numbersComposition2) {
            sum2 += number;
        }
        controlNumber2 = sum2 % 11;
        if (controlNumber2 > 9) {
            controlNumber2 = controlNumber2 % 10;
        }

        boolean valid = controlNumber1 == numbers[10] && controlNumber2 == numbers[11];
        elContainer.setTag(valid);
        if (!valid) {
            elContainer.setError(FieldConverter.getString(R.string.transfer_invalid_value));
        } else {
            elContainer.clearError();
        }
        return valid;
    }


    public static class LegalPersonInnPredicate extends CheckEditTextPredicate {

        @Override
        public boolean test(String var1) {
            return testInn10Numbers(editText, elContainer);
        }
    }

    public static class InnPredicate extends CheckEditTextPredicate {
        @Override
        public boolean test(String var1) {
            switch (var1.length()) {
                case 10:
                    return testInn10Numbers(editText, elContainer);
                case 12:
                    return testInn12Numbers(editText, elContainer);
                default:
                    return false;
            }
        }
    }

    private static boolean testInn10Numbers(EditText editText, ErrorLabelContainer elContainer) {
        int[] coefficients1 = {2, 4, 10, 3, 5, 9, 4, 6, 8};
        String numberToCheck = editText.getText().toString();
        char[] numberChars = numberToCheck.toCharArray();
        int[] numbers = new int[10];

        // TODO: 25.01.18 extract method
        int[] numbersComposition = new int[9];
        int sum = 0;
        int controlNumber;

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Character.getNumericValue(numberChars[i]);
        }

        for (int i = 0; i < coefficients1.length; i++) {
            numbersComposition[i] = numbers[i] * coefficients1[i];
        }

        for (int number :
                numbersComposition) {
            sum += number;
        }
        controlNumber = sum % 11;
        if (controlNumber > 9) {
            controlNumber = controlNumber % 10;
        }

        boolean valid = controlNumber == numbers[9];
        elContainer.setTag(valid);
        if (!valid) {
            elContainer.setError(FieldConverter.getString(R.string.transfer_invalid_value));
        } else {
            elContainer.clearError();
        }
        return valid;
    }


    public static class UinPredicate extends CheckEditTextPredicate {
        int[] coefficients1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5};
        int[] coefficients2 = {3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7};

        @Override
        public boolean test(String var1) {
            String numberToCheck = editText.getText().toString();
            char[] numberChars = numberToCheck.toCharArray();
            int uinLength = numberChars.length;
            int[] numbers = new int[uinLength];

            int[] numbersComposition = new int[uinLength];
            int sum = 0;
            int controlNumber;

            for (int i = 0; i < uinLength; i++) {
                numbers[i] = Character.getNumericValue(numberChars[i]);
            }

            for (int i = 0; i < uinLength - 1; i++) {
                numbersComposition[i] = numbers[i] * coefficients1[i];
            }

            for (int number :
                    numbersComposition) {
                sum += number;
            }
            controlNumber = sum % 11;
            // TODO: 30.01.2018 refactoring
            if (controlNumber == 10) {
                for (int i = 0; i < uinLength - 1; i++) {
                    numbersComposition[i] = numbers[i] * coefficients2[i];
                }
                sum = 0;
                for (int number :
                        numbersComposition) {
                    sum += number;
                }

                controlNumber = sum % 11;
                if (controlNumber == 10) {
                    controlNumber = 0;
                }
            }

            boolean valid = controlNumber == numbers[uinLength - 1];
            elContainer.setTag(valid);
            if (!valid) {
                elContainer.setError(FieldConverter.getString(R.string.transfer_invalid_value));
            } else {
                elContainer.clearError();
            }
            return valid;
        }
    }
}
