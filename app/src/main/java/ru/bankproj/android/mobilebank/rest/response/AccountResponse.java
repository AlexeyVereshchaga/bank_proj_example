package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;

/**
 * Created by j7ars on 29.11.2017.
 */

public class AccountResponse extends BaseResponse {

    @SerializedName("accounts")
    private ArrayList<Account> accountList;

    public ArrayList<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(ArrayList<Account> accountList) {
        this.accountList = accountList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.accountList);
    }

    public AccountResponse() {
    }

    protected AccountResponse(Parcel in) {
        super(in);
        this.accountList = in.createTypedArrayList(Account.CREATOR);
    }

    public static final Creator<AccountResponse> CREATOR = new Creator<AccountResponse>() {
        @Override
        public AccountResponse createFromParcel(Parcel source) {
            return new AccountResponse(source);
        }

        @Override
        public AccountResponse[] newArray(int size) {
            return new AccountResponse[size];
        }
    };
}
