package ru.bankproj.android.mobilebank.progressdialog.custom;

import android.app.Activity;
import android.support.v4.app.Fragment;

import ru.bankproj.android.mobilebank.dataclasses.ErrorData;
import ru.bankproj.android.mobilebank.progressdialog.ProgressDialog;
import ru.bankproj.android.mobilebank.progressdialog.ProgressEvent;
import ru.bankproj.android.mobilebank.progressdialog.custom.observer.ProgressObservable;
import ru.bankproj.android.mobilebank.rest.error.RetrofitException;

/**
 * Created by j7ars on 09.11.2017.
 */

public class ProgressActivityManager {

    private static ProgressActivityManager ourInstance = new ProgressActivityManager();

    private ProgressActivityManager() {
    }

    public static ProgressActivityManager getInstance() {
        return ourInstance;
    }

    public void startLoading(Activity activity) {
        ProgressDialogActivity.startActivity(activity);
    }

    public void startLoading(Fragment fragment) {
        ProgressDialogActivity.startActivity(fragment, fragment.getActivity());
    }

    public void errorLoading(ProgressDialog.OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, ErrorData errorData) {
        ProgressObservable.newInstance().sendEvent(new ProgressEvent(ProgressEvent.ERROR_PROGRESS, errorData, onOkButtonClickCallbackListener));
    }

    public void errorLoading(ProgressDialog.OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, RetrofitException retrofitException) {
        ProgressObservable.newInstance().sendEvent(new ProgressEvent(ProgressEvent.ERROR_PROGRESS, retrofitException, onOkButtonClickCallbackListener));
    }

    public void completeLoading() {
        ProgressObservable.newInstance().sendEvent(new ProgressEvent(ProgressEvent.COMPLETE_PROGRESS));
    }

}
