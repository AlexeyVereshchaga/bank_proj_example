package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.CreditAccountDetail;

/**
 * Created by j7ars on 13.12.2017.
 */

public class CreditDetailResponse extends BaseResponse {

    @SerializedName("account")
    private CreditAccountDetail creditAccountDetail;

    public CreditAccountDetail getCreditAccountDetail() {
        return creditAccountDetail;
    }

    public void setCreditAccountDetail(CreditAccountDetail creditAccountDetail) {
        this.creditAccountDetail = creditAccountDetail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(this.creditAccountDetail, flags);
    }

    public CreditDetailResponse() {
    }

    protected CreditDetailResponse(Parcel in) {
        super(in);
        this.creditAccountDetail = in.readParcelable(CreditAccountDetail.class.getClassLoader());
    }

    public static final Creator<CreditDetailResponse> CREATOR = new Creator<CreditDetailResponse>() {
        @Override
        public CreditDetailResponse createFromParcel(Parcel source) {
            return new CreditDetailResponse(source);
        }

        @Override
        public CreditDetailResponse[] newArray(int size) {
            return new CreditDetailResponse[size];
        }
    };
}
