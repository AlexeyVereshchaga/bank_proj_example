package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.providers.Provider;

/**
 * Created by j7ars on 17.11.2017.
 */

public class ProviderPaymentsResponse extends BaseResponse {

    @SerializedName("providers")
    private List<Provider> providers;

    public List<Provider> getProviders() {
        return providers;
    }

    public void setProviders(List<Provider> providers) {
        this.providers = providers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.providers);
    }

    public ProviderPaymentsResponse() {
    }

    protected ProviderPaymentsResponse(Parcel in) {
        super(in);
        this.providers = in.createTypedArrayList(Provider.CREATOR);
    }

    public static final Creator<ProviderPaymentsResponse> CREATOR = new Creator<ProviderPaymentsResponse>() {
        @Override
        public ProviderPaymentsResponse createFromParcel(Parcel source) {
            return new ProviderPaymentsResponse(source);
        }

        @Override
        public ProviderPaymentsResponse[] newArray(int size) {
            return new ProviderPaymentsResponse[size];
        }
    };
}
