package ru.bankproj.android.mobilebank.main.transfer.individual.person.view;

import android.os.Bundle;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.activity.BaseBackContainerActivity;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.utils.FieldConverter;

/**
 * Created by maxmobiles on 27.12.2017.
 */

public class TransferIndividualPersonActivity extends BaseBackContainerActivity {
    public static final String TRANSFER_DESCRIPTION = "TransferClientGpbActivity.TRANSFER_DESCRIPTION";
    public static final String SOURCE_CARD = "TransferClientGpbActivity.SOURCE_CARD";
    public static final String TEMPLATE_MODE = "TransferClientGpbActivity.TEMPLATE_MODE";

    private String descriptionTransfer;
    private Card sourceCard;
    private boolean templateMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(FieldConverter.getString(R.string.transfer_title));
        getExtras();
        if (savedInstanceState == null) {
            openFragment();
        }
    }

    private void getExtras() {
        if (getIntent().getExtras() != null) {
            sourceCard = getIntent().getParcelableExtra(SOURCE_CARD);
            descriptionTransfer = getIntent().getStringExtra(TRANSFER_DESCRIPTION);
            templateMode = getIntent().getBooleanExtra(TEMPLATE_MODE, false);
        }
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, getScreenCreator()
                .newInstance(TransferIndividualPersonFragment.class, createBundle())).commitAllowingStateLoss();
    }

    private Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferIndividualPersonFragment.SOURCE_PRODUCT, sourceCard);
        bundle.putBoolean(TransferIndividualPersonFragment.TEMPLATE_MODE, templateMode);
        return bundle;
    }
}
