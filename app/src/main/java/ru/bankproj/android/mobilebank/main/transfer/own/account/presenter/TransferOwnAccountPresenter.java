package ru.bankproj.android.mobilebank.main.transfer.own.account.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventData;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.product.CardLimit;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;
import ru.bankproj.android.mobilebank.main.transfer.own.account.ITransferOwnAccountContract;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.rest.RequestParams;
import ru.bankproj.android.mobilebank.rest.response.CardLimitResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferResponse;
import ru.bankproj.android.mobilebank.utils.RequestConstants;

import static ru.bankproj.android.mobilebank.app.Constants.TAG;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public class TransferOwnAccountPresenter extends BaseEventBusPresenter<ITransferOwnAccountContract.View>
        implements ITransferOwnAccountContract.Presenter, IBaseResponseCallback {

    private static final String SOURCE_PRODUCT = "TransferOwnAccountPresenter.SOURCE_PRODUCT";
    private static final String TRANSFER_DESCRIPTION = "TransferOwnAccountPresenter.TRANSFER_DESCRIPTION";
    private static final String TARGET_PRODUCT = "TransferOwnAccountPresenter.TARGET_PRODUCT";

    private String transferDescription;
    private BaseProduct sourceProduct;
    private BaseProduct targetProduct;
    private String amount;
    private RequestManager requestManager;
    private Disposable disposable;
    private boolean isTarget;
    private boolean templateMode;
    private PublishSubject<Boolean> subject = PublishSubject.create();
    private Double productLimit;

    private boolean dialogNeeded;

    @Inject
    public TransferOwnAccountPresenter(RequestManager requestManager) {
        this.requestManager = requestManager;
        disposable = Disposables.empty();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            getMvpView().initTemplateMode(templateMode);
            showSourceProduct();
            showTargetProduct();
        }
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            sourceProduct = (BaseProduct) params[0];
            transferDescription = (String) params[1];
            targetProduct = (BaseProduct) params[2];
            templateMode = (boolean) params[3];
            isTarget = targetProduct != null;
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(SOURCE_PRODUCT, sourceProduct);
        outState.putString(TRANSFER_DESCRIPTION, transferDescription);
        outState.putParcelable(TARGET_PRODUCT, targetProduct);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        sourceProduct = savedInstanceState.getParcelable(SOURCE_PRODUCT);
        transferDescription = savedInstanceState.getString(TRANSFER_DESCRIPTION);
        targetProduct = savedInstanceState.getParcelable(TARGET_PRODUCT);
    }

    @Override
    public void setListObservableField(List<Observable<Boolean>> mListObservable) {
        mListObservable.add(subject);
        Disposable validationDisposable = Observable.combineLatest(mListObservable, objects -> {
            boolean state = true;
            for (Object object : objects) {
                state &= (Boolean) object;
            }
            return state;
        }).subscribe(s -> getMvpView().fieldValidationSuccess(s));
        addDisposable(validationDisposable);
    }

    @Override
    public void onSourceClicked() {
        FilterProductData data = new FilterProductData();
        data.setFilterMode(FilterProductData.DYNAMIC);
        data.setBaseProduct(targetProduct);
        data.setPurposeType(FilterProductData.SOURCE);
        getMvpView().openSelectSourceCardScreen(data);
    }

    @Override
    public void onTargetClicked() {
        FilterProductData data = new FilterProductData();
        data.setFilterMode(FilterProductData.DYNAMIC);
        data.setBaseProduct(sourceProduct);
        data.setPurposeType(FilterProductData.TARGET);
        getMvpView().openSelectTargetCardScreen(data);
    }

    @Override
    public void onNextClicked(String amount) {
        this.amount = amount;
        int validationValue = validateAmount(amount);
        if (validationValue == -1) {
            checkTransferRequest(amount);
        } else {
            getMvpView().showNotValidAmountMessage(validationValue);
        }
    }

    private int validateAmount(String amountStr) {
        Float amount = -1f;
        try {
            amount = Float.valueOf(amountStr);
        } catch (NumberFormatException e) {
            Log.e(TAG, "validateAmount: ", e);
        }
        if (productLimit != null && productLimit < amount) {
            return R.string.payment_restriction_exceeded;
        }
        if (amount <= 0) {
            return R.string.amount_error_message;
        }
        return -1;
    }

    @Override
    public void clearTargetProduct() {
        if (!isTarget) {
            targetProduct = null;
            showTargetProduct();
        }
    }

    @Override
    public void onResume() {
        if (dialogNeeded) {
            getMvpView().showDialogBySourceTargetType(sourceProduct, targetProduct);
            dialogNeeded = false;
        }
    }

    private void showSourceProduct() {
        if (sourceProduct != null && sourceProduct instanceof Card) {
            getCardLimits(sourceProduct);
        }
        checkProducts();
        getMvpView().showSourceProduct(sourceProduct);
    }

    private void checkProducts() {
        subject.onNext(sourceProduct != null && targetProduct != null);
    }

    private void getCardLimits(BaseProduct sourceProduct) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                productLimit = null;
                disposable = requestManager.getCardLimits(sessionId, sourceProduct.getId(),
                        new BaseRequestController(mEventBusController, Action.GET_CARD_LIMITS_ACTION, getMvpView().getClassUniqueDeviceId()));
            }
        });
    }

    private void checkTransferRequest(String amount) {
        undisposable(disposable);
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                checkTransfer(sessionId, amount);
            }
        });
    }

    private void checkTransfer(String sessionId, String amount) {
        undisposable(disposable);
        Integer sourceType = -1;
        String sourceId = sourceProduct.getId();
        Integer targetType = -1;
        String targetId = targetProduct.getId();
        String currency = getCurrencyBySourceTargetProduct();
        if (sourceProduct instanceof Card) {
            sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
        } else if (sourceProduct instanceof Account) {
            sourceType = RequestConstants.SOURCE_CLIENTS_ACCOUNT;
        }
        if (targetProduct instanceof Card) {
            targetType = RequestConstants.CLIENTS_CARD;
        } else if (targetProduct instanceof Account) {
            targetType = RequestConstants.CLIENTS_ACCOUNT;
        }
        disposable = requestManager.transferCheck(
                sessionId,
                RequestParams.getConfirmationStrategiesParams(ConfirmRequestDescriptor.TRANSFER, null, amount, currency, null, sourceType, targetType, -1),
                currency, amount, sourceType, sourceId, targetType, targetId, null, null, mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                new BaseRequestController(mEventBusController, Action.CHECK_TRANSFER, getMvpView().getClassUniqueDeviceId()));
        addDisposable(disposable);
    }

    // for the future
    private String getCurrencyBySourceTargetProduct() {
        String cur = Constants.RUR;
        if (sourceProduct instanceof Card && targetProduct instanceof Card) {
            cur = Constants.RUR;
        } else if (sourceProduct instanceof Card && targetProduct instanceof Account) {
            cur = Constants.RUR;
        } else if (sourceProduct instanceof Account && targetProduct instanceof Card) {
            cur = Constants.RUR;
        } else if (sourceProduct instanceof Card && targetProduct instanceof Account) {
            cur = Constants.RUR;
        }

        return cur;
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case START_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                    case Action.GET_CARD_LIMITS_ACTION:
                        getMvpView().startProgressDialog();
                        break;
                }
                break;
            case SUCCESS_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                        undisposable(disposable);
                        Response<TransferResponse> responseResponse = (Response<TransferResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), responseResponse, this);
                        break;
                    case Action.GET_CARD_LIMITS_ACTION:
                        undisposable(disposable);
                        Response<CardLimitResponse> baseResponse = (Response<CardLimitResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                        break;
                }
                break;
            case FAIL_REQUEST:
                getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                switch (event.getActionCode()) {
                    case Action.GET_CARD_LIMITS_ACTION:
                        sourceProduct = null;
                        showSourceProduct();
                        break;
                }
                break;
            case CUSTOM_EVENT:
                switch (event.getActionCode()) {
                    case Action.SOURCE_PRODUCT_SELECTED_EVENT:
                        getMvpView().moveToTop();
                        sourceProduct = ((EventData<BaseProduct>) event).getData();
                        showSourceProduct();
                        break;
                    case Action.TARGET_PRODUCT_SELECTED_EVENT:
                        getMvpView().moveToTop();
                        targetProduct = ((EventData<BaseProduct>) event).getData();
                        showTargetProduct();
                        dialogNeeded = true;
                        break;
                }
                break;
        }
    }

    private void showTargetProduct() {
        getMvpView().showTargetProduct(targetProduct);
        checkProducts();
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case Action.CHECK_TRANSFER:
                getMvpView().completeProgressDialog();
                TransferResponse transferResponse = (TransferResponse) data.getValue();
                CheckTransferData checkTransferData = new CheckTransferData();
                checkTransferData.setAmount(amount);
                checkTransferData.setSourceProduct(sourceProduct);
                checkTransferData.setTargetProduct(targetProduct);
                checkTransferData.setTransferDescription(transferDescription);
                checkTransferData.setTransferResponse(transferResponse);
                checkTransferData.setType(CheckTransferData.OWN_TRANSFER);
                getMvpView().openTransferCheckScreen(checkTransferData, templateMode);
                break;
            case Action.GET_CARD_LIMITS_ACTION:
                getMvpView().completeProgressDialog();
                CardLimitResponse cardLimitResponse = (CardLimitResponse) data.getValue();
                if (cardLimitResponse != null) {
                    ArrayList<CardLimit> cardLimits = cardLimitResponse.getLimitList();
                    for (CardLimit cardLimit :
                            cardLimits) {
                        double localLimit = cardLimit.getAmount() - cardLimit.getUsedAmount();
                        if (productLimit == null || productLimit > localLimit) {
                            productLimit = localLimit;
                        }
                        if (productLimit <= 0) {
                            sourceProduct = null;
                            showSourceProduct();
                            getMvpView().showCardLimitDialog();
                            return;
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                getMvpView().completeProgressDialog();
                getMvpView().unAuthorized();
                break;
            case BAD_REQUEST_ERROR:
            case DEFAULT_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
            default:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
        }
        switch (actionCode) {
            case Action.GET_CARD_LIMITS_ACTION:
                sourceProduct = null;
                showSourceProduct();
                break;
        }
    }
}