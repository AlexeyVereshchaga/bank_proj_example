package ru.bankproj.android.mobilebank.main.transfer.repayment.main.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventData;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.product.CardLimit;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.product.CreditAccountDetail;
import ru.bankproj.android.mobilebank.dataclasses.providers.OutputData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.Subsidiary;
import ru.bankproj.android.mobilebank.main.transfer.repayment.main.IRepaymentContract;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.rest.RequestParams;
import ru.bankproj.android.mobilebank.rest.response.CardLimitResponse;
import ru.bankproj.android.mobilebank.rest.response.CreditDetailResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferResponse;
import ru.bankproj.android.mobilebank.utils.RequestConstants;
import ru.bankproj.android.mobilebank.utils.Utils;

import static ru.bankproj.android.mobilebank.app.Constants.TAG;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public class RepaymentPresenter extends BaseEventBusPresenter<IRepaymentContract.View>
        implements IRepaymentContract.Presenter, IBaseResponseCallback {

    private static final String SOURCE_CARD = "RepaymentPresenter.SOURCE_CARD";
    private static final String TRANSFER_DESCRIPTION = "RepaymentPresenter.TRANSFER_DESCRIPTION";
    private static final String TARGET_TYPE = "RepaymentPresenter.TARGET_TYPE";
    private static final String TARGET_PRODUCT = "RepaymentPresenter.TARGET_PRODUCT";
    private static final String AMOUNT = "RepaymentPresenter.AMOUNT";
    private static final String TRANSFER_RESPONSE = "RepaymentPresenter.TRANSFER_RESPONSE";
    private static final String SUBSIDIARY = "RepaymentPresenter.SUBSIDIARY";
    private static final String CREDIT_DETAILS = "RepaymentPresenter.CREDIT_DETAILS";
    private static final String TEMPLATE_MODE = "RepaymentPresenter.TEMPLATE_MODE";

    private String transferDescription;
    private BaseProduct sourceProduct;
    @RequestConstants.TargetType
    private int targetType;
    private BaseProduct targetProduct;
    private String amount;
    private TransferResponse transferResponse;
    private Subsidiary subsidiary;
    private CreditDetailResponse creditDetailResponse;
    private boolean isTarget;
    private String accountNumber;
    private boolean createTemplateMode;
    private Double productLimit;
    private PublishSubject<Boolean> subject = PublishSubject.create();

    private RequestManager requestManager;
    private Disposable disposable;
    private List<Observable<Boolean>> listObservable;
    private Observable<Boolean> accountNumberObservable;
    private Observable<Boolean> subsidiaryObservable;

    @Inject
    public RepaymentPresenter(RequestManager requestManager) {
        this.requestManager = requestManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            getMvpView().initView(createTemplateMode, targetType, targetProduct);
            showSourceProduct();
            showTargetProduct();
        }
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            sourceProduct = (BaseProduct) params[0];
            transferDescription = (String) params[1];
            targetType = (int) params[2];
            targetProduct = (BaseProduct) params[3];
            isTarget = targetProduct != null;
            if (isTarget) {
                checkAccountNumber();
            }
            createTemplateMode = (boolean) params[4];
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(SOURCE_CARD, sourceProduct);
        outState.putString(TRANSFER_DESCRIPTION, transferDescription);
        outState.putInt(TARGET_TYPE, targetType);
        outState.putParcelable(TARGET_PRODUCT, targetProduct);
        outState.putString(AMOUNT, amount);
        outState.putParcelable(TRANSFER_RESPONSE, transferResponse);
        outState.putParcelable(SUBSIDIARY, subsidiary);
        outState.putParcelable(CREDIT_DETAILS, creditDetailResponse);
        outState.putBoolean(TEMPLATE_MODE, createTemplateMode);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        sourceProduct = savedInstanceState.getParcelable(SOURCE_CARD);
        transferDescription = savedInstanceState.getString(TRANSFER_DESCRIPTION);
        targetType = savedInstanceState.getInt(TARGET_TYPE);
        targetProduct = savedInstanceState.getParcelable(TARGET_PRODUCT);
        amount = savedInstanceState.getString(AMOUNT);
        transferResponse = savedInstanceState.getParcelable(TRANSFER_RESPONSE);
        subsidiary = savedInstanceState.getParcelable(SUBSIDIARY);
        creditDetailResponse = savedInstanceState.getParcelable(CREDIT_DETAILS);
        createTemplateMode = savedInstanceState.getBoolean(TEMPLATE_MODE);
    }

    @Override
    public void setListObservableField(List<Observable<Boolean>> listObservable) {
        this.listObservable = listObservable;
        listObservable.add(subject);
        Disposable validationDisposable = Observable.combineLatest(listObservable, objects -> {
            boolean state = true;
            for (Object object : objects) {
                state &= (Boolean) object;
            }
            return state;
        }).subscribe(s -> getMvpView().fieldValidationSuccess(s));
        addDisposable(validationDisposable);
    }

    @Override
    public void addAccountNumberObservable(Observable<Boolean> accountNumberObservable) {
        this.accountNumberObservable = accountNumberObservable;
        addObservableField(accountNumberObservable);
    }

    public void addObservableField(Observable<Boolean> observable) {
        listObservable.add(observable);
    }

    @Override
    public void removeAccountNumberObservable() {
        listObservable.remove(accountNumberObservable);
    }

    @Override
    public void addSubsidiaryObservable(Observable<Boolean> subsidiaryObservable) {
        this.subsidiaryObservable = subsidiaryObservable;
        addObservableField(subsidiaryObservable);
    }

    @Override
    public void removeSubsidiaryObservable() {
        listObservable.remove(subsidiaryObservable);
    }


    @Override
    public void onSourceClicked() {
        BaseProduct targetProduct = null;
        if (isTarget && this.targetProduct != null) {
            targetProduct = this.targetProduct;
        }
        FilterProductData data = new FilterProductData();
        data.setFilterMode(FilterProductData.CARDS);
        data.setBaseProduct(targetProduct);
        data.setPurposeType(FilterProductData.SOURCE);
        getMvpView().openSelectSourceCardScreen(data);
    }

    @Override
    public void onTargetClicked() {
        if (isTarget || sourceProduct != null) {
            BaseProduct sourceProduct = null;
            if (!isTarget) {
                sourceProduct = this.sourceProduct;
            }
            FilterProductData data = new FilterProductData();
            switch (targetType) {
                case RequestConstants.CLIENTS_CARD:
                    data.setFilterMode(FilterProductData.CREDIT_CARDS);
                    break;
                case RequestConstants.ACCOUNT_IN_SAME_BANK:
                    data.setFilterMode(FilterProductData.CREDIT_ACCOUNTS);
                    break;
            }
            data.setBaseProduct(sourceProduct);
            data.setPurposeType(FilterProductData.TARGET);
            getMvpView().openSelectTargetCardScreen(data);
        }
    }

    @Override
    public void onNextClicked(String amount) {
        this.amount = amount;
        int validationValue = validateAmount(amount);
        if (validationValue == -1) {
            checkTransferRequest();
        } else {
            getMvpView().showNotValidAmountMessage(validationValue);
        }
    }

    private int validateAmount(String amountStr) {
        Float amount = -1f;
        try {
            amount = Float.valueOf(amountStr);
        } catch (NumberFormatException e) {
            Log.e(TAG, "validateAmount: ", e);
        }
        if (productLimit != null && productLimit < amount) {
            return R.string.payment_restriction_exceeded;
        }
        if (amount <= 0) {
            return R.string.amount_error_message;
        }
        return -1;
    }

    @Override
    public void onSubsidiaryClicked() {
        getMvpView().openSubsidiariesScreen();
    }

    private void showSourceProduct() {
        if (!createTemplateMode && sourceProduct != null && sourceProduct instanceof Card) {
            getCardLimits(sourceProduct);
        }
        checkProducts();
        getMvpView().showSourceProduct(targetType, sourceProduct);
    }

    private void checkProducts() {
        subject.onNext(sourceProduct != null && targetProduct != null);
    }

    private void getCardLimits(BaseProduct sourceProduct) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                productLimit = null;
                disposable = requestManager.getCardLimits(sessionId, sourceProduct.getId(),
                        new BaseRequestController(mEventBusController, Action.GET_CARD_LIMITS_ACTION, getMvpView().getClassUniqueDeviceId()));
            }
        });
    }

    private void checkTransferRequest() {
        undisposable(disposable);
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                Integer sourceType = -1;
                String sourceId = sourceProduct.getId();
                String targetId = targetProduct.getId();
                String currency = Constants.RUR; // TODO: 01.02.18 requirement for all repayments
                Map<String, String> additionParameters = null;
                if (sourceProduct instanceof Card) {
                    sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
                } else if (sourceProduct instanceof Account) {
                    sourceType = RequestConstants.SOURCE_CLIENTS_ACCOUNT;
                }
                if (targetType == RequestConstants.ACCOUNT_IN_SAME_BANK) {
                    if (subsidiary != null) {
                        additionParameters = RequestParams.getRepaymentCreditAccountAdditionParams(subsidiary.getCode());
                    }
                    if (creditDetailResponse.getCreditAccountDetail() != null
                            && creditDetailResponse.getCreditAccountDetail().getAccountNumber() != null
                            && !creditDetailResponse.getCreditAccountDetail().getAccountNumber().isEmpty()) {
                        targetId = creditDetailResponse.getCreditAccountDetail().getAccountNumber().get(0);
                    } else {
                        targetId = accountNumber;
                    }
                }
                targetProduct.setId(targetId);
                disposable = requestManager.repaymentCheck(
                        sessionId,
                        RequestParams.getConfirmationStrategiesParams(ConfirmRequestDescriptor.CREDIT_REPAYMENT, null, amount, currency, null, sourceType, targetType, -1),
                        currency, amount, sourceType, sourceId, targetType, targetId, null, additionParameters, mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                        new BaseRequestController(mEventBusController, Action.CHECK_REPAYMENT, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    private void getCreditAccountDetails() {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                disposable = requestManager.getCreditDetail(sessionId, targetProduct.getId(),
                        new BaseRequestController(mEventBusController, Action.GET_CREDIT_DETAIL, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case START_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_REPAYMENT:
                    case Action.GET_CREDIT_DETAIL:
                    case Action.GET_CARD_LIMITS_ACTION:
                        getMvpView().startProgressDialog();
                        break;
                }

                break;
            case SUCCESS_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_REPAYMENT:
                        undisposable(disposable);
                        Response<TransferResponse> response = (Response<TransferResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), response, this);
                        break;
                    case Action.GET_CREDIT_DETAIL:
                        undisposable(disposable);
                        Response<CreditDetailResponse> response1 = (Response<CreditDetailResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), response1, this);
                        break;
                    case Action.GET_CARD_LIMITS_ACTION:
                        undisposable(disposable);
                        Response<CardLimitResponse> baseResponse = (Response<CardLimitResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                        break;
                }
                break;
            case FAIL_REQUEST:
                undisposable(disposable);
                getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                switch (event.getActionCode()) {
                    case Action.CHECK_REPAYMENT:
                    case Action.GET_CREDIT_DETAIL:
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        break;
                    case Action.GET_CARD_LIMITS_ACTION:
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        sourceProduct = null;
                        showSourceProduct();
                        break;
                }
                break;
            case CUSTOM_EVENT:
                switch (event.getActionCode()) {
                    case Action.SOURCE_PRODUCT_SELECTED_EVENT:
                        getMvpView().moveToTop();
                        sourceProduct = ((EventData<BaseProduct>) event).getData();
                        showSourceProduct();
                        break;
                    case Action.TARGET_PRODUCT_SELECTED_EVENT:
                        getMvpView().moveToTop();
                        targetProduct = ((EventData<BaseProduct>) event).getData();
                        checkAccountNumber();
                        break;
                    case Action.SUBSIDIARY_SELECTED_EVENT:
                        getMvpView().moveToTop();
                        subsidiary = ((EventData<Subsidiary>) event).getData();
                        getMvpView().showSubsidiary(subsidiary);
                        break;
                }
                break;
        }
    }

    private void checkAccountNumber() {
        if (targetType == RequestConstants.CLIENTS_CARD) {
            showTargetProduct();
        } else if (targetType == RequestConstants.ACCOUNT_IN_SAME_BANK) {
            getCreditAccountDetails();
        }
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case Action.CHECK_REPAYMENT:
                getMvpView().completeProgressDialog();
                transferResponse = (TransferResponse) data.getValue();
                CheckTransferData checkTransferData = new CheckTransferData();
                checkTransferData.setAmount(amount);
                checkTransferData.setSourceProduct(sourceProduct);
                checkTransferData.setTargetProduct(targetProduct);
                checkTransferData.setTransferDescription(transferDescription);
                checkTransferData.setTransferResponse(transferResponse);
                if (subsidiary != null) {
                    checkTransferData.setAdditionParams(RequestParams.getRepaymentCreditAccountAdditionParams(subsidiary.getCode()));
                }
                switch (targetType) {
                    case RequestConstants.CLIENTS_CARD:
                        checkTransferData.setType(CheckTransferData.CREDIT_REPAYMENT_CARD);
                        break;
                    case RequestConstants.ACCOUNT_IN_SAME_BANK:
                        checkTransferData.setType(CheckTransferData.CREDIT_REPAYMENT_ACCOUNT);
                        break;
                }
                getMvpView().openRepaymentCheckScreen(checkTransferData, createTemplateMode);
                break;
            case Action.GET_CREDIT_DETAIL:
                getMvpView().completeProgressDialog();
                creditDetailResponse = (CreditDetailResponse) data.getValue();

                CreditAccountDetail creditAccountDetail = creditDetailResponse.getCreditAccountDetail();
                if (creditAccountDetail != null) {
                    if ((creditAccountDetail.getDetailList() != null
                            && creditAccountDetail.getDetailList().size() > 0)) {
                        for (OutputData outputData : creditAccountDetail.getDetailList()) {
                            fillCreditAccountObject(creditAccountDetail, outputData);
                        }
                    }
                    Calendar calendar = Calendar.getInstance();
                    if (Utils.getMilleFromDate(Utils.formatSelectedDate(creditAccountDetail.getEndDate())) < Utils.getMilleFromDate(calendar.getTime())) {//если срок действия кредитного договора закончился
                        getMvpView().completeProgressDialog();
                        getMvpView().showErrorDialog(creditAccountDetail);
                        targetProduct = null;
                    } else {
                        showTargetProduct();
                        getMvpView().completeProgressDialog();
                        if (creditAccountDetail.getAccountNumber() != null
                                && !creditAccountDetail.getAccountNumber().isEmpty()) {
                            getMvpView().showAccountNumberField(false);
                        } else {
                            getMvpView().showAccountNumberField(true);
                        }
                    }
                }
                break;
            case Action.GET_CARD_LIMITS_ACTION:
                getMvpView().completeProgressDialog();
                CardLimitResponse cardLimitResponse = (CardLimitResponse) data.getValue();
                if (cardLimitResponse != null) {
                    ArrayList<CardLimit> cardLimits = cardLimitResponse.getLimitList();
                    productLimit = null;
                    for (CardLimit cardLimit :
                            cardLimits) {
                        double localLimit = cardLimit.getAmount() - cardLimit.getUsedAmount();
                        if (productLimit == null || productLimit > localLimit) {
                            productLimit = localLimit;
                        }
                        if (productLimit <= 0) {
                            sourceProduct = null;
                            showSourceProduct();
                            getMvpView().showCardLimitDialog();
                            return;
                        }
                    }
                }
                break;
        }
    }

    private void showTargetProduct() {
        checkProducts();
        getMvpView().showTargetProduct(targetProduct);
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                getMvpView().completeProgressDialog();
                getMvpView().unAuthorized();
                break;
            case BAD_REQUEST_ERROR:
            case DEFAULT_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
            default:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
        }
        switch (actionCode) {
            case Action.GET_CREDIT_DETAIL:
                targetProduct = null;
                showTargetProduct();
                break;
            case Action.GET_CARD_LIMITS_ACTION:
                sourceProduct = null;
                showSourceProduct();
                break;
        }
    }

    private void fillCreditAccountObject(CreditAccountDetail creditAccountDetail, OutputData outputData) {
        switch (outputData.getId()) {
            case CreditAccountDetail.NAME:
                creditAccountDetail.setDetailName(outputData.getStringValue());
                break;
            case CreditAccountDetail.RATE:
                creditAccountDetail.setRate(outputData.getStringValue());
                break;
            case CreditAccountDetail.TOTAL_SUM:
                creditAccountDetail.setTotalSum(outputData.getMoneyEntry());
                break;
            case CreditAccountDetail.PAY_DATE:
                creditAccountDetail.setPayDate(outputData.getStringValue());
                break;
            case CreditAccountDetail.END_DATE:
                creditAccountDetail.setEndDate(outputData.getDateValue());
                break;
            case CreditAccountDetail.NEXT_PAY_SUM:
                creditAccountDetail.setNextPaySum(outputData.getMoneyEntry());
                break;
            case CreditAccountDetail.CONTRACT_NUMBER:
                creditAccountDetail.setContractNumber(outputData.getStringValue());
                break;
            case CreditAccountDetail.OPEN_DATE:
                creditAccountDetail.setOpenDate(outputData.getDateValue());
                break;
            case CreditAccountDetail.CONTRACT_ID:
                creditAccountDetail.setContractId(outputData.getStringValue());
                break;
            case CreditAccountDetail.KIND:
                creditAccountDetail.setKind(outputData.getStringValue());
                break;
            case CreditAccountDetail.PENALTY:
                creditAccountDetail.setPenalty(outputData.getStringValue());
                break;
            case CreditAccountDetail.OVERDUE_DEBT_PENALTY:
                creditAccountDetail.setOverdueDebtPenalty(outputData.getStringValue());
                break;
            case CreditAccountDetail.DETAILS:
                if (outputData.getContent() != null && outputData.getContent().size() != 0) {
                    for (OutputData contentOutputData : outputData.getContent()) {
                        fillCreditAccountObject(creditAccountDetail, contentOutputData);
                    }
                }
                break;
            case CreditAccountDetail.DOLG:
                if (outputData.getContent() != null && outputData.getContent().size() != 0) {
                    for (OutputData dolgOutputData : outputData.getContent()) {
                        fillCreditAccountObject(creditAccountDetail, dolgOutputData);
                    }
                }
                break;
            case CreditAccountDetail.ACCOUNTS:
                if (outputData.getContent() != null && outputData.getContent().size() != 0) {
                    ArrayList<String> accountsList = new ArrayList<>();
                    for (OutputData accountsOutputData : outputData.getContent()) {
                        accountsList.add(accountsOutputData.getStringValue());
                    }
                    creditAccountDetail.setAccountNumber(accountsList);
                }
                break;
            case CreditAccountDetail.PANS:
                if (outputData.getContent() != null && outputData.getContent().size() != 0) {
                    ArrayList<String> accountsList = new ArrayList<>();
                    for (OutputData accountsOutputData : outputData.getContent()) {
                        accountsList.add(accountsOutputData.getStringValue());
                    }
                    creditAccountDetail.setCardNumber(accountsList);
                }
                break;
        }
    }
}