package ru.bankproj.android.mobilebank.di.component;

import dagger.Subcomponent;
import ru.bankproj.android.mobilebank.di.module.PresenterModule;
import ru.bankproj.android.mobilebank.di.module.ServiceModule;
import ru.bankproj.android.mobilebank.di.scope.PerService;
import ru.bankproj.android.mobilebank.gcm.registration.RegistrationIntentService;
import ru.bankproj.android.mobilebank.session.SessionControlService;

/**
 * Created by j7ars on 25.10.2017.
 */
@PerService
@Subcomponent(modules = {ServiceModule.class, PresenterModule.class})
public interface ServiceComponent {

    void inject(SessionControlService sessionControlService);

    void inject(RegistrationIntentService registrationIntentService);

}
