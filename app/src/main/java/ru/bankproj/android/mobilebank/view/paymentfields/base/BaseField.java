package ru.bankproj.android.mobilebank.view.paymentfields.base;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import java.io.Serializable;

import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.dataclasses.providers.PaymentParameter;

/**
 * Created by rrust on 17.12.2017.
 */

public abstract class BaseField implements Serializable{

    protected Context mContext;

    public BaseField(Context context) {
        this.mContext = context;
        initView();
    }

    public abstract View getView();

    protected abstract void initView();

    public abstract void setValue(String value);

    public abstract String getValue();

    public abstract void setPaymentParameter(PaymentParameter paymentParameter);

    public abstract String getPaymentParameterId();

    public abstract void enableView(boolean isEnable);

    public  boolean isFillValue(){
        return !TextUtils.isEmpty(getValue());
    }

    public abstract Observable<Boolean> getObservable();

}
