package ru.bankproj.android.mobilebank.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import ru.bankproj.android.mobilebank.di.qualifier.ActivityContext;
import ru.bankproj.android.mobilebank.di.scope.PerActivity;

/**
 * Created by j7ars on 25.10.2017.
 */
@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        mActivity = activity;
    }

    @Provides
    @PerActivity
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    @PerActivity
    @ActivityContext
    Context providesContext() {
        return mActivity;
    }

}
