package ru.bankproj.android.mobilebank.main.transfer.check.view;

import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.activity.BaseBackContainerActivity;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;

/**
 * Created by Alexey Vereshchaga on 22.12.17.
 */

public class TransferCheckActivity extends BaseBackContainerActivity {

    public static final String CHECK_TRANSFER_DATA = "TransferCheckActivity.CHECK_TRANSFER_DATA";
    public static final String TEMPLATE_MODE = "TransferCheckActivity.TEMPLATE_MODE";

    private CheckTransferData checkTransferData;
    private boolean templateMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.transfer_title));
        getExtras();
        if (savedInstanceState == null) {
            openFragment();
        } else{
            onRestoreInstanceState(savedInstanceState);
        }
    }

    private void getExtras() {
        if (getIntent().getExtras() != null) {
            checkTransferData = getIntent().getParcelableExtra(CHECK_TRANSFER_DATA);
            templateMode = getIntent().getBooleanExtra(TEMPLATE_MODE, false);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(CHECK_TRANSFER_DATA, checkTransferData);
        outState.putBoolean(TEMPLATE_MODE,templateMode);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        checkTransferData = savedInstanceState.getParcelable(CHECK_TRANSFER_DATA);
        templateMode = savedInstanceState.getBoolean(TEMPLATE_MODE);
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container, getScreenCreator().newInstance(TransferCheckFragment.class, createBundle())).commitAllowingStateLoss();
    }

    private Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferCheckFragment.CHECK_TRANSFER_DATA, checkTransferData);
        bundle.putBoolean(TransferCheckFragment.TEMPLATE_MODE, templateMode);
        return bundle;
    }
}
