package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;
import android.os.Parcelable;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;

/**
 * Created by j7ars on 27.10.2017.
 */

public class SessionResponse extends BaseResponse {

    private String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String mSessionId) {
        this.sessionId = mSessionId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sessionId);
    }

    public SessionResponse() {
    }

    protected SessionResponse(Parcel in) {
        this.sessionId = in.readString();
    }

    public static final Parcelable.Creator<SessionResponse> CREATOR = new Parcelable.Creator<SessionResponse>() {
        @Override
        public SessionResponse createFromParcel(Parcel source) {
            return new SessionResponse(source);
        }

        @Override
        public SessionResponse[] newArray(int size) {
            return new SessionResponse[size];
        }
    };
}
