package ru.bankproj.android.mobilebank.main.transfer.success.view;

import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.activity.BaseBackContainerActivity;
import ru.bankproj.android.mobilebank.rest.response.TransferExecResponse;

/**
 * Created by Alexey Vereshchaga on 27.12.17.
 */

public class TransferSuccessActivity extends BaseBackContainerActivity {

    public static final String TRANSFER_EXEC_RESPONSE = "TransferSuccessActivity.TRANSFER_EXEC_RESPONSE";
//    public static final String TRANSFER_PARAMS = "TransferSuccessActivity.TRANSFER_PARAMS";

    private TransferExecResponse transferExecResponse;
    private String transferParamsStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.transfer_title));
        getExtras();
        if (savedInstanceState == null) {
            openFragment();
        }
    }

    private void getExtras() {
        if (getIntent().getExtras() != null) {
            transferExecResponse = getIntent().getParcelableExtra(TRANSFER_EXEC_RESPONSE);
//            transferParamsStr = getIntent().getStringExtra(TRANSFER_PARAMS);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(TRANSFER_EXEC_RESPONSE, transferExecResponse);
//        outState.putString(TRANSFER_PARAMS, transferParamsStr);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        transferExecResponse = savedInstanceState.getParcelable(TRANSFER_EXEC_RESPONSE);
//        transferParamsStr = savedInstanceState.getString(TRANSFER_PARAMS);
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container, getScreenCreator().newInstance(TransferSuccessFragment.class, createBundle())).commitAllowingStateLoss();
    }

    private Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferSuccessFragment.TRANSFER_EXEC_RESPONSE, transferExecResponse);
//        bundle.putString(TransferSuccessFragment.TRANSFER_PARAMS, transferParamsStr);
        return bundle;
    }
}
