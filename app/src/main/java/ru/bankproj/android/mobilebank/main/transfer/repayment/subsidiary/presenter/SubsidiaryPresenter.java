package ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventData;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.dataclasses.transfer.Subsidiary;
import ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.ISubsidiaryContract;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.rest.response.SubsidiariesResponse;

/**
 * SubsidiaryPresenter.java 20.01.2018
 * Class description
 *
 * @author Alex.
 */

public class SubsidiaryPresenter extends BaseEventBusPresenter<ISubsidiaryContract.View>
        implements ISubsidiaryContract.Presenter, IBaseResponseCallback {

    private static final String SAVE_SUBSIDIARIES = "SubsidiaryPresenter.SAVE_SUBSIDIARIES";
    private RequestManager requestManager;
    private ArrayList<Subsidiary> subsidiaries;
    private Disposable disposable;

    @Inject
    public SubsidiaryPresenter(RequestManager requestManager) {
        subsidiaries = new ArrayList<>();
        this.requestManager = requestManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            getSubsidiaries();
        }
        initView();
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(SAVE_SUBSIDIARIES, subsidiaries);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        subsidiaries = savedInstanceState.getParcelableArrayList(SAVE_SUBSIDIARIES);
    }

    private void initView() {
        getMvpView().initData(subsidiaries);
    }

    @Override
    public void subsidiariesSelectItem(int position) {
        Subsidiary subsidiary = subsidiaries.get(position);
        mEventBusController.notifyEvent(new EventData<>(Action.SUBSIDIARY_SELECTED_EVENT, subsidiary));
    }

    private void getSubsidiaries() {
        undisposable(disposable);
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                disposable = requestManager.getSubsidiaries(sessionId, new BaseRequestController(mEventBusController, Action.GET_SUBSIDIARIES, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case START_REQUEST:
                switch (event.getActionCode()) {
                    case Action.GET_SUBSIDIARIES:
                        getMvpView().startProgressDialog();
                        break;
                }

                break;
            case SUCCESS_REQUEST:
                switch (event.getActionCode()) {
                    case Action.GET_SUBSIDIARIES:
                        undisposable(disposable);
                        Response<SubsidiariesResponse> responseResponse = (Response<SubsidiariesResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), responseResponse, this);
                        break;
                }
                break;
            case FAIL_REQUEST:
                switch (event.getActionCode()) {
                    case Action.GET_SUBSIDIARIES:
                        undisposable(disposable);
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        break;
                }
                break;
        }
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case Action.GET_SUBSIDIARIES:
                SubsidiariesResponse subsidiariesResponse = (SubsidiariesResponse) data.getValue();
                if (subsidiariesResponse.getSubsidiaries() != null) {
                    subsidiaries = (ArrayList<Subsidiary>) subsidiariesResponse.getSubsidiaries();
                    getMvpView().initData(subsidiaries);
                    getMvpView().completeProgressDialog();
                }
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                getMvpView().completeProgressDialog();
                getMvpView().unAuthorized();
                break;
            case BAD_REQUEST_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
            case DEFAULT_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
        }
    }
}

