package ru.bankproj.android.mobilebank.managers;

import android.location.Location;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Response;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.dataclasses.atm.ATM;
import ru.bankproj.android.mobilebank.dataclasses.atm.AtmFilterData;
import ru.bankproj.android.mobilebank.dataclasses.news.News;
import ru.bankproj.android.mobilebank.dataclasses.offers.Offer;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.templates.Template;
import ru.bankproj.android.mobilebank.rest.response.ATMResponse;
import ru.bankproj.android.mobilebank.rest.response.AccountResponse;
import ru.bankproj.android.mobilebank.rest.response.CardResponse;
import ru.bankproj.android.mobilebank.rest.response.NewsResponse;
import ru.bankproj.android.mobilebank.rest.response.OffersResponse;
import ru.bankproj.android.mobilebank.rest.response.TemplateResponse;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.SortListUtils;

/**
 * Created by j7ars on 22.11.2017.
 */
@Singleton
public class DataSourceManager {

    private String mLogin;
    private Location mCurrentLocation;
    private ArrayList<News> mNewsList;
    private ArrayList<Offer> mOfferList;
    private ArrayList<ATM> mAtmList;
    private ArrayList<Card> mCardList;
    private ArrayList<Account> mAccountList; // счета
    private ArrayList<Account> mDebitList; // вклады
    private ArrayList<Account> mCreditList; // кредиты
    private ArrayList<Template> mTemplateList;

    @Inject
    public DataSourceManager() {
        mNewsList = new ArrayList<>();
        mOfferList = new ArrayList<>();
        mAtmList = new ArrayList<>();
        mCardList = new ArrayList<>();
        mAccountList = new ArrayList<>();
        mDebitList = new ArrayList<>();
        mCreditList = new ArrayList<>();
        mTemplateList = new ArrayList<>();
    }

    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String login) {
        this.mLogin = login;
    }

    public Location getCurrentLocation() {
        return mCurrentLocation;
    }

    public void setCurrentLocation(Location mCurrentLocation) {
        this.mCurrentLocation = mCurrentLocation;
    }

    public ArrayList<News> getNewsList() {
        return mNewsList;
    }


    public Response<NewsResponse> setNewsList(Response<NewsResponse> response) {
        if (response.isSuccessful()) {
            if (response.body().getResult() == ResponseHandler.SUCCESS_CODE) {
                this.mNewsList = response.body().getNews();
            } else {
                this.mNewsList = null;
            }
        } else {
            this.mNewsList = null;
        }
        return response;
    }

    public ArrayList<Offer> getOfferList() {
        return mOfferList;
    }

    public Response<OffersResponse> setOfferList(Response<OffersResponse> response) {
        if (response.isSuccessful()) {
            if (response.body().getResult() == ResponseHandler.SUCCESS_CODE) {
                this.mOfferList = response.body().getOffers();
            } else {
                this.mOfferList = null;
            }
        } else {
            this.mOfferList = null;
        }
        return response;
    }

    public ArrayList<ATM> getAtmList() {
        return mAtmList;
    }

    public Response<ATMResponse> setAtmList(Response<ATMResponse> response) {
        if (response.isSuccessful()) {
            if (response.body().getResult() == ResponseHandler.SUCCESS_CODE) {
                this.mAtmList = handlingATM(response.body().getAtmList());
            } else {
                this.mAtmList = null;
            }
        } else {
            this.mAtmList = null;
        }
        return response;
    }

    public ArrayList<ATM> handlingATM(ArrayList<ATM> atmList) {
        ArrayList<ATM> atmListResult = new ArrayList<>();
        ArrayList<ATM> lessOneKmList = new ArrayList<>();
        ArrayList<ATM> lessFiveKmList = new ArrayList<>();
        ArrayList<ATM> moreFiveKmList = new ArrayList<>();
        if (atmList != null && atmList.size() != 0) {
            for (ATM atm : atmList) {
                if (atm.getDistance() <= Constants.ONE_KM && atm.getUid() != null) {
                    lessOneKmList.add(atm);
                } else {
                    if (atm.getDistance() <= Constants.FIVE_KM && atm.getUid() != null) {
                        lessFiveKmList.add(atm);
                    } else {
                        if (atm.getUid() != null)
                            moreFiveKmList.add(atm);
                    }
                }
            }
            addAtmHeader(lessOneKmList, atmListResult, FieldConverter.getString(R.string.atm_list_distance_less1km));
            atmListResult.addAll(lessOneKmList);
            addAtmHeader(lessFiveKmList, atmListResult, FieldConverter.getString(R.string.atm_list_distance_less5km));
            atmListResult.addAll(lessFiveKmList);
            addAtmHeader(moreFiveKmList, atmListResult, FieldConverter.getString(R.string.atm_list_distance_more5km));
            atmListResult.addAll(moreFiveKmList);
        }
        return atmListResult;
    }

    private void addAtmHeader(ArrayList<ATM> atmList, ArrayList<ATM> atmListResult, String title) {
        if (atmList != null && atmList.size() != 0) {
            ATM atmHeader = new ATM(ATM.TYPE_HEADER, title);
            atmListResult.add(atmHeader);
        }
    }

    public ArrayList<Card> getCardList() {
        return mCardList;
    }

    public ArrayList<Card> getCardListByStatus(int status) {
        ArrayList<Card> filterList = new ArrayList<>();
        if (SortListUtils.isNotEmptyList(mCardList)) {
            for (Card card : mCardList) {
                if (card.getStatus() == status) {
                    filterList.add(card);
                }
            }
        }

        return filterList;
    }

    public ArrayList<Card> getCardListByType(int type) {
        ArrayList<Card> filterList = new ArrayList<>();
        if (SortListUtils.isNotEmptyList(mCardList)) {
            for (Card card : mCardList) {
                if (card.getType() == type) {
                    filterList.add(card);
                }
            }
        }

        return filterList;
    }

    public void updateCardList(int position, Card card) {
        mCardList.set(position, card);
    }

    public void resetCardList(ArrayList<Card> cardList) {
        mCardList = cardList;
    }

    public Card getCardById(String cardId) {
        if (mCardList != null && mCardList.size() != 0) {
            for (Card card : mCardList) {
                if (card.getId().equalsIgnoreCase(cardId)) {
                    return card;
                }
            }
            return null;
        } else {
            return null;
        }
    }

    public Response<CardResponse> setCardList(Response<CardResponse> response) {
        if (response.isSuccessful()) {
            if (response.body().getResult() == ResponseHandler.SUCCESS_CODE) {
                ArrayList<Card> cardList = response.body().getCardList();
                Collections.sort(cardList, new Comparator<Card>() {
                    @Override
                    public int compare(Card card, Card t1) {
                        return card.getStatus() - t1.getStatus();
                    }
                });
                this.mCardList = cardList;
            } else {
                this.mCardList = null;
            }
        } else {
            this.mCardList = null;
        }
        return response;
    }

    public ArrayList<Template> getTemplateList() {
        return mTemplateList;
    }

    public Response<TemplateResponse> setTemplateList(Response<TemplateResponse> response) {
        if (response.isSuccessful()) {
            if (response.body().getResult() == ResponseHandler.SUCCESS_CODE) {
                this.mTemplateList = response.body().getTemplateList();//SortListUtils.sortTemplatesByDate(response.body().getTemplateList()); timestamp не всегда есть
            } else {
                this.mTemplateList = null;
            }
        } else {
            this.mTemplateList = null;
        }
        return response;
    }

    public void resetTemplateList(ArrayList<Template> templateList) {
        mTemplateList = templateList;
    }

    public void updateTemplateList(int position, Template template) {
        mTemplateList.set(position, template);
    }

    public void removeTemplateByUid(String uid) {
        if (mTemplateList != null && mTemplateList.size() != 0) {
            for (Iterator<Template> iterator = mTemplateList.iterator(); iterator.hasNext(); ) {
                Template template = iterator.next();
                if (template.getUid().equalsIgnoreCase(uid)) {
                    iterator.remove();
                }
            }
        }
    }

    public ArrayList<Account> getAccountList() {
        return mAccountList;
    }

    public ArrayList<Account> getAccountListByStatus(int status) {
        ArrayList<Account> filterList = new ArrayList<>();
        if (SortListUtils.isNotEmptyList(mAccountList)) {
            for (Account account : mAccountList) {
                if (account.getStatus() == status) {
                    filterList.add(account);
                }
            }
        }
        return filterList;
    }

    //trash
    public ArrayList<Account> getDebitAccountListByStatus(int status) {
        ArrayList<Account> filterList = new ArrayList<>();
        if (SortListUtils.isNotEmptyList(mDebitList)) {
            for (Account account : mDebitList) {
                if (account.getStatus() == status) {
                    filterList.add(account);
                }
            }
        }
        return filterList;
    }

    public ArrayList<Account> getCreditAccountListByStatus(int status) {
        ArrayList<Account> filterList = new ArrayList<>();
        if (SortListUtils.isNotEmptyList(mCreditList)) {
            for (Account account : mCreditList) {
                if (account.getStatus() == status) {
                    filterList.add(account);
                }
            }
        }
        return filterList;
    }

    public ArrayList<Account> getAccountListByType(int type) {
        ArrayList<Account> filterList = new ArrayList<>();
        if (SortListUtils.isNotEmptyList(mAccountList)) {
            for (Account account : mAccountList) {
                if (account.getType() == type) {
                    filterList.add(account);
                }
            }
        }
        return filterList;
    }

    public void updateAccountList(int position, Account account) {
        mAccountList.set(position, account);
    }

    public void resetAccountList(ArrayList<Account> accountList) {
        mAccountList = accountList;
    }

    public Account getAccountById(String accountId) {
        if (mAccountList != null && mAccountList.size() != 0) {
            Account accountCard = null;
            for (Account account : mAccountList) {
                if (account.getId().equalsIgnoreCase(accountId)) {
                    return account;
                }
            }
            return null;
        } else {
            return null;
        }
    }

    public Response<AccountResponse> setAccountData(Response<AccountResponse> response) {
        if (response.isSuccessful()) {
            if (response.body().getResult() == ResponseHandler.SUCCESS_CODE) {
                ArrayList<Account> accountList = response.body().getAccountList();
                if (accountList != null && accountList.size() != 0) {
                    if (mCreditList == null) {
                        mCreditList = new ArrayList<>();
                    }
                    if (mDebitList == null) {
                        mDebitList = new ArrayList<>();
                    }
                    if (mAccountList == null) {
                        mAccountList = new ArrayList<>();
                    }
                    mCreditList.clear();
                    mDebitList.clear();
                    mAccountList.clear();
                    for (Account account : accountList) {
                        switch (account.getType()) {
                            case Account.TYPE_CHECKING:
                                mAccountList.add(account);
                                break;
                            case Account.TYPE_DEBIT:
                                if (account.getDepositKind() == 1 || account.getDepositKind() == 3) {
                                    mAccountList.add(account);
                                } else {
                                    mDebitList.add(account);
                                }
                                break;
                            case Account.TYPE_CREDIT:
                                mCreditList.add(account);
                                break;
                            default:
                                mAccountList.add(account);
                                break;
                        }
                    }
                } else {
                    mCreditList = null;
                    mDebitList = null;
                    mAccountList = null;
                }
            } else {
                mCreditList = null;
                mDebitList = null;
                mAccountList = null;
            }

        }
        return response;
    }

    public Response<AccountResponse> setAccountList(int accountType, Response<AccountResponse> response) {
        if (response.isSuccessful()) {
            if (response.body().getResult() == ResponseHandler.SUCCESS_CODE) {
                if (accountType == Account.TYPE_CREDIT) {
                    this.mCreditList = filterAccountList(response.body().getAccountList(), accountType);
                } else if (accountType == Account.TYPE_DEBIT) {
                    this.mDebitList = filterAccountList(response.body().getAccountList(), accountType);
                } else {
                    this.mAccountList = filterAccountList(response.body().getAccountList(), accountType);
                }
            } else {
                if (accountType == Account.TYPE_CREDIT) {
                    this.mCreditList = null;
                } else if (accountType == Account.TYPE_DEBIT) {
                    this.mDebitList = null;
                } else {
                    this.mAccountList = null;
                }
            }
        } else {
            if (accountType == Account.TYPE_CREDIT) {
                this.mCreditList = null;
            } else if (accountType == Account.TYPE_DEBIT) {
                this.mDebitList = null;
            } else {
                this.mAccountList = null;
            }
        }
        return response;
    }

    public ArrayList<Account> getDebitList() {
        return mDebitList;
    }

    public void updateDebitList(int position, Account account) {
        mDebitList.set(position, account);
    }

    public void resetDebitList(ArrayList<Account> accountList) {
        mDebitList = accountList;
    }

    public ArrayList<Account> getCreditList() {
        return mCreditList;
    }

    public void updateCreditList(int position, Account account) {
        mCreditList.set(position, account);
    }

    public void resetCreditList(ArrayList<Account> accountList) {
        mCreditList = accountList;
    }

    private ArrayList<Account> filterAccountList(ArrayList<Account> accountList, int accountType) {
        ArrayList<Account> resultAccountList = new ArrayList<>();
        if (accountList != null && accountList.size() != 0) {
            for (Account account : accountList) {
                switch (accountType) {
                    case Account.TYPE_CHECKING:
                        if (account.getType() == Account.TYPE_CHECKING
                                || (account.getType() == Account.TYPE_DEBIT
                                && (account.getDepositKind() == 1 || account.getDepositKind() == 3))) {
                            resultAccountList.add(account);
                        }
                        break;
                    case Account.TYPE_DEBIT:
                        if (account.getType() == Account.TYPE_DEBIT
                                && account.getDepositKind() != 1
                                && account.getDepositKind() != 3) {
                            resultAccountList.add(account);
                        }
                        break;
                    case Account.TYPE_CREDIT:
                        if (account.getType() == Account.TYPE_CREDIT) {
                            resultAccountList.add(account);
                        }
                        break;
                }
            }
        }
        return resultAccountList;
    }

    public void logout() {
        mLogin = null;
        mCurrentLocation = null;
        mNewsList = null;
        mOfferList = null;
        mAtmList = null;
        mCardList = null;
        mAccountList = null;
        mDebitList = null;
        mCreditList = null;
        mTemplateList = null;
    }

    public AtmFilterData generateFilterData() {
        return new AtmFilterData(AtmFilterData.ATM_DISTANCE_ALL, AtmFilterData.ATM_TYPE_ALL, AtmFilterData.ATM_STATUS_ALL);
    }
}
