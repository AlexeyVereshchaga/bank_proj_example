package ru.bankproj.android.mobilebank.eventbus;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Response;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventFinishRequest;
import ru.bankproj.android.mobilebank.base.event.EventStartRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.rest.error.RetrofitException;

/**
 * Created by j7ars on 25.10.2017.
 */
@Singleton
public class EventBusController implements IEventSubject {

    private ArrayList<IEventObserver> mObservers;

    @Inject
    public EventBusController() {
        mObservers = new ArrayList<>();
    }

    @Override
    public void addObserver(IEventObserver iObserver) {
        if (!mObservers.contains(iObserver)) {
            mObservers.add(iObserver);
        }
    }

    @Override
    public void removeObserver(IEventObserver iObserver) {
        if (iObserver != null) {
            final int i = mObservers.indexOf(iObserver);
            if (i >= 0) {
                mObservers.remove(iObserver);
            }
        }
    }

    @Override
    public void removeAllObservers() {
        if (mObservers != null) {
            mObservers.clear();
        }
    }

    @Override
    public void notifyStartedWithAction(final int action, final int classUniqueId) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onEvent(new EventStartRequest(action, classUniqueId));
        }
    }

    @Override
    public void notifyFinishWithAction(int action, final int classUniqueId) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onEvent(new EventFinishRequest(action, classUniqueId));
        }
    }

    @Override
    public void notifySuccess(int action, Response response, final int classUniqueId) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onEvent(new EventSuccessRequest(action, classUniqueId, response));
        }
    }

    @Override
    public void notifyFailed(int action, RetrofitException e, String message, final int classUniqueId) {
        final int size = mObservers.size();
        for (int i = 0; i < size; i++) {
            mObservers.get(i).onEvent(new EventFailRequest(action, classUniqueId, e, message));
        }
    }

    @Override
    public void notifyEvent(Event event) {
        final int size = mObservers.size();
        for (int i = 0; i < size; i++) {
            mObservers.get(i).onEvent(event);
        }
    }

    @Override
    public boolean containObserver(IEventObserver iObserver) {
        return mObservers.contains(iObserver);
    }

}
