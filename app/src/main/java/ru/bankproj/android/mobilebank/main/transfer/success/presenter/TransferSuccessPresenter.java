package ru.bankproj.android.mobilebank.main.transfer.success.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventData;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.main.transfer.success.ITransferSuccessContract;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.rest.RequestParams;
import ru.bankproj.android.mobilebank.rest.response.TemplateResponse;
import ru.bankproj.android.mobilebank.rest.response.TemplateSaveResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferExecResponse;

import static ru.bankproj.android.mobilebank.app.Action.SAVE_TEMPLATE;
import static ru.bankproj.android.mobilebank.app.Action.SEND_EMAIL_ACTION;

/**
 * Created by Alexey Vereshchaga on 27.12.17.
 */

public class TransferSuccessPresenter extends BaseEventBusPresenter<ITransferSuccessContract.View>
        implements ITransferSuccessContract.Presenter, IBaseResponseCallback {

    private static final String TRANSFER_EXEC_RESPONSE = "TransferSuccessPresenter.TRANSFER_EXEC_RESPONSE";

    private RequestManager requestManager;
    private TransferExecResponse transferExecResponse;
    private Disposable disposable;

    @Inject
    public TransferSuccessPresenter(RequestManager requestManager) {
        this.requestManager = requestManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        }
        getMvpView().showCreateTemplate(transferExecResponse.getRecentOpId() != null);
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            transferExecResponse = (TransferExecResponse) params[0];
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(TRANSFER_EXEC_RESPONSE, transferExecResponse);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        transferExecResponse = savedInstanceState.getParcelable(TRANSFER_EXEC_RESPONSE);
    }

    @Override
    public void completeClicked() {
        mEventBusController.notifyEvent(new EventData<>(Action.TRANSFER_BRANCH_COMPLETED));
    }

    @Override
    public void sendReceiptClicked() {
        getMvpView().showSendEmailDialog();
    }

    @Override
    public void sendMail(String email) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
//                double fee = transferExecResponse.getFee() == null ? 0 : transferExecResponse.getFee();
                disposable = requestManager.sendMail(sessionId,
                        RequestParams.getSendMailParams(transferExecResponse.getTransRef(), email),
                        new BaseRequestController(mEventBusController, SEND_EMAIL_ACTION, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    @Override
    public void saveTemplate(String name) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                disposable = requestManager.saveTemplate(sessionId, name, transferExecResponse.getRecentOpId(), 1,
                        mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                        new BaseRequestController(mEventBusController, SAVE_TEMPLATE, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    private void updateTemplateList() {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                disposable = requestManager.getTemplates(sessionId,
                        new BaseRequestController(mEventBusController, Action.GET_TEMPLATES_ACTION, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case START_REQUEST:
                switch (event.getActionCode()) {
                    case SEND_EMAIL_ACTION:
                    case SAVE_TEMPLATE:
                        getMvpView().startProgressDialog();
                        break;
                }
                break;
            case SUCCESS_REQUEST:
                switch (event.getActionCode()) {
                    case SEND_EMAIL_ACTION:
                    case SAVE_TEMPLATE:
                        undisposable(disposable);
                        Response<TemplateSaveResponse> baseResponse = (Response<TemplateSaveResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                        break;
                    case Action.GET_TEMPLATES_ACTION:
                        undisposable(disposable);
                        Response<TemplateResponse> templateResponse = (Response<TemplateResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), templateResponse, this);
                        break;
                }
                break;
            case FAIL_REQUEST:
                switch (event.getActionCode()) {
                    case SEND_EMAIL_ACTION:
                    case SAVE_TEMPLATE:
                        undisposable(disposable);
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        break;
                    case Action.GET_TEMPLATES_ACTION:
                        undisposable(disposable);
                        mDataManager.resetTemplateList(null);
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        break;
                }
                break;
        }
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case SEND_EMAIL_ACTION:
                getMvpView().showMailSuccessMessage();
                getMvpView().completeProgressDialog();
                break;
            case SAVE_TEMPLATE:
                getMvpView().showTemplateSavedMessage();
                updateTemplateList();
                break;
            case Action.GET_TEMPLATES_ACTION:
                getMvpView().completeProgressDialog();
                mEventBusController.notifyEvent(new EventData<>(Action.TRANSFER_BRANCH_COMPLETED));
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                getMvpView().completeProgressDialog();
                if (actionCode == Action.GET_TEMPLATES_ACTION) {
                    mDataManager.resetTemplateList(null);
                }
                getMvpView().unAuthorized();
                break;
            case BAD_REQUEST_ERROR:
            case DEFAULT_ERROR:
                if (actionCode == Action.GET_TEMPLATES_ACTION) {
                    mDataManager.resetTemplateList(null);
                }
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
        }
    }

}