package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;

/**
 * Created by maxmobiles on 17.12.2017.
 */

public class ConfirmationStrategiesResponse extends BaseResponse{

    public static final String PASSWORD_CONFIRMATION_STRATEGY = "password";

    @SerializedName("confirmationStrategies")
    private ArrayList<String> confirmationStrategies;

    public ArrayList<String> getConfirmationStrategies() {
        return confirmationStrategies;
    }

    public void setConfirmationStrategies(ArrayList<String> confirmationStrategies) {
        this.confirmationStrategies = confirmationStrategies;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeStringList(this.confirmationStrategies);
    }

    public ConfirmationStrategiesResponse() {
    }

    protected ConfirmationStrategiesResponse(Parcel in) {
        super(in);
        this.confirmationStrategies = in.createStringArrayList();
    }

    public static final Creator<ConfirmationStrategiesResponse> CREATOR = new Creator<ConfirmationStrategiesResponse>() {
        @Override
        public ConfirmationStrategiesResponse createFromParcel(Parcel source) {
            return new ConfirmationStrategiesResponse(source);
        }

        @Override
        public ConfirmationStrategiesResponse[] newArray(int size) {
            return new ConfirmationStrategiesResponse[size];
        }
    };
}
