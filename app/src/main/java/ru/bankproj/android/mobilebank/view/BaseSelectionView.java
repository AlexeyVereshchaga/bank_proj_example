package ru.bankproj.android.mobilebank.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import ru.bankproj.android.mobilebank.R;

/**
 * Created by maxmobiles on 24.12.2017.
 */

public class BaseSelectionView extends FrameLayout {

    protected EditText etValue;
    protected ImageView ivIcon;
    protected LinearLayout llClear;

    private String hint;

    private OnSelectionViewCallback mOnSelectionViewCallback;

    public BaseSelectionView(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public BaseSelectionView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BaseSelectionView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setOnSelectionViewCallback(OnSelectionViewCallback mOnSelectionViewCallback) {
        this.mOnSelectionViewCallback = mOnSelectionViewCallback;
    }

    private void init(final Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_base_selection_view, this);

        TypedArray typedArray;
        typedArray = context
                .obtainStyledAttributes(attrs, R.styleable.SelectionView);
        hint = typedArray
                .getString(R.styleable.SelectionView_hintText);
        typedArray.recycle();


    }

    public void setCustomText(String text){
        etValue.setText(text);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        etValue = (EditText) this.findViewById(R.id.et_value);
        ivIcon = (ImageView) this.findViewById(R.id.iv_icon);
        llClear = (LinearLayout) this.findViewById(R.id.ll_clear);
        etValue.setHint(hint);

        setListeners();
    }

    private void setListeners() {
        etValue.addTextChangedListener(mTextWatcher);
        etValue.setOnClickListener(mInputClickListener);
        llClear.setOnClickListener(mClearClickListener);
    }

    OnClickListener mInputClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mOnSelectionViewCallback != null) {
                mOnSelectionViewCallback.onSelectClicked();
            }
        }
    };

    OnClickListener mClearClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            etValue.setText("");
            if(mOnSelectionViewCallback != null){
                mOnSelectionViewCallback.onClearSelectClicked();
            }
        }
    };

    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.toString().isEmpty()) {
                hideClearButton();
            } else {
                showClearButton();
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    public void hideClearButton() {
        this.llClear.setVisibility(GONE);
    }

    public void showClearButton() {
        this.llClear.setVisibility(VISIBLE);
    }

    public interface OnSelectionViewCallback{
        void onSelectClicked();
        void onClearSelectClicked();
    }



}
