package ru.bankproj.android.mobilebank.progressdialog.custom.observer;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.progressdialog.ProgressEvent;

/**
 * Created by j7ars on 09.11.2017.
 */

public class ProgressObservable implements ProgressSubject{

    private static ProgressObservable mInstance;
    private ArrayList<ProgressObserver> mObservableList;
    private boolean isStateChange;

    public static ProgressObservable newInstance(){
        if(mInstance == null){
            mInstance = new ProgressObservable();
        }
        return mInstance;
    }

    private ProgressObservable(){
        mObservableList = new ArrayList<>();
        isStateChange = false;
    }

    @Override
    public void registerObserver(ProgressObserver observer) {
        mObservableList.add(observer);
    }

    @Override
    public void notifyObservers(ProgressEvent progressEvent) {
        if(isStateChange) {
            for (ProgressObserver observer : mObservableList) {
                observer.event(progressEvent);
            }
        }
    }

    @Override
    public void unRegisterObserver(ProgressObserver observer) {
        mObservableList.remove(observer);
    }

    public void sendEvent(ProgressEvent progressEvent){
        isStateChange = true;
        notifyObservers(progressEvent);
    }
}
