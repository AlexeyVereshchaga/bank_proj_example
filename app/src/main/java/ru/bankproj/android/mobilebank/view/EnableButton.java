package ru.bankproj.android.mobilebank.view;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import ru.bankproj.android.mobilebank.R;

/**
 * Created by j7ars on 01.11.2017.
 */

public class EnableButton extends AppCompatButton {

    public EnableButton(Context context) {
        super(context);
        initView();
    }

    public EnableButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public EnableButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        enable(false);
    }

    public void enable(boolean isEnable){
        if(isEnable){
            this.setEnabled(true);
            setBackgroundResource(R.drawable.selector_blue_button);
        } else{
            this.setEnabled(false);
            setBackgroundResource(R.drawable.shape_grey_button);
        }

    }

}
