package ru.bankproj.android.mobilebank.main.transfer.check.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.app.ValidFields;
import ru.bankproj.android.mobilebank.app.session.ISessionValueCallback;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.dataclasses.authorization.PasswordError;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SourceType;
import ru.bankproj.android.mobilebank.dataclasses.transfer.TargetType;
import ru.bankproj.android.mobilebank.di.scope.ConfigPersistent;
import ru.bankproj.android.mobilebank.main.transfer.check.ITransferCheckContract;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.rest.RequestParams;
import ru.bankproj.android.mobilebank.rest.response.ExecutePaymentResponse;
import ru.bankproj.android.mobilebank.rest.response.TemplateCreateResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferExecResponse;
import ru.bankproj.android.mobilebank.utils.RequestConstants;

import static ru.bankproj.android.mobilebank.app.Action.CREATE_TEMPLATE;
import static ru.bankproj.android.mobilebank.app.Action.TEMPLATE_EXEC;
import static ru.bankproj.android.mobilebank.app.Action.TRANSFER_EXEC;

/**
 * Created by Alexey Vereshchaga on 22.12.17.
 */
@ConfigPersistent
public class TransferCheckPresenter extends BaseEventBusPresenter<ITransferCheckContract.View>
        implements ITransferCheckContract.Presenter, IBaseResponseCallback {

    private static final String CHECK_TRANSFER_DATA = "TransferCheckPresenter.CHECK_TRANSFER_DATA";
    private static final String TEMPLATE_MODE = "TransferCheckPresenter.TEMPLATE_MODE";

    private CheckTransferData checkTransferData;
    private boolean templateMode;

    private RequestManager requestManager;
    private Disposable disposable;


    @Inject
    public TransferCheckPresenter(RequestManager requestManager) {
        this.requestManager = requestManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            showTransferInfo();
        }
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            checkTransferData = (CheckTransferData) params[0];
            templateMode = (boolean) params[1];
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(CHECK_TRANSFER_DATA, checkTransferData);
        outState.putBoolean(TEMPLATE_MODE, templateMode);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        checkTransferData = savedInstanceState.getParcelable(CHECK_TRANSFER_DATA);
        templateMode = savedInstanceState.getBoolean(TEMPLATE_MODE);
    }

    private void showTransferInfo() {
        switch (checkTransferData.getType()) {
            case CheckTransferData.CLIENT_GPB:
            case CheckTransferData.OWN_TRANSFER:
            case CheckTransferData.LEGAL_TRANSFER:
            case CheckTransferData.INDIVIDUAL_TRANSFER:
            case CheckTransferData.BUDGET_TRANSFER:
                getMvpView().showTransferInfo(checkTransferData);
                break;
            case CheckTransferData.TEMPLATE_TRANSFER:
                getMvpView().showTemplateTransferInfo(checkTransferData);
                break;
        }
    }

    private void getTransferExec(String pass) {
        mDataManager.getSessionId(new ISessionValueCallback() {
            @Override
            public void sessionValue(boolean isUnAuthorize, String sessionId) {
                if (isUnAuthorize) {
                    getMvpView().unAuthorized();
                } else {
                    disposable = makeRequestByTransferType(sessionId, pass);
                    addDisposable(disposable);
                }
            }
        });
    }

    private void getTemplateExec(String pass) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                disposable = requestManager.executeTemplate(sessionId,
                        RequestParams.getExecuteTemplateParams(checkTransferData.getTemplate().getUid(),
                                checkTransferData.getTemplate().getCurrency(), checkTransferData.getTemplate().getAmount(),
                                checkTransferData.getTransferResponse().getTransRef(), null, pass,
                                mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate()),
                        new BaseRequestController(mEventBusController, Action.TEMPLATE_EXEC, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

     private void createTemplate(String pass) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                int actionType = RequestConstants.AFTER_TRANSFER_CHECK;
                int sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
                Integer targetType = null;
                String pid = null;
                Integer operationSubtype = null;
                Integer flags = RequestConstants.TRANSFER_BETWEEN_CLIENTS_PRODUCTS;
                String targetId = null;
                String fullName = null;

                BaseProduct sourceProduct = checkTransferData.getSourceProduct();
                Map<String, String> params = checkTransferData.getAdditionParams();
                String amount = String.valueOf(Float.valueOf(checkTransferData.getAmount()));
                String templateName = checkTransferData.getNewTemplateName();
                String transRef = checkTransferData.getTransferResponse().getTransRef();
                BaseProduct targetProduct = checkTransferData.getTargetProduct();

                switch (checkTransferData.getType()) {
                    case CheckTransferData.OWN_TRANSFER:
                        if (sourceProduct instanceof Card) {
                            sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
                        } else if (sourceProduct instanceof Account) {
                            sourceType = RequestConstants.SOURCE_CLIENTS_ACCOUNT;
                        }
                        if (actionType == RequestConstants.AFTER_TRANSFER_CHECK || actionType == RequestConstants.AFTER_REPAYMENT_CHECK) {
                            if (targetProduct instanceof Card) {
                                targetType = RequestConstants.CLIENTS_CARD;
                            } else if (targetProduct instanceof Account) {
                                targetType = RequestConstants.CLIENTS_ACCOUNT;
                            }
                        }
//                if (actionType == RequestConstants.AFTER_PAYMENT_CHECK) {
//                    pid = provider.getPid();
//                }
                        if (actionType == RequestConstants.AFTER_TRANSFER_CHECK) {
                            if (targetProduct instanceof Account && ((Account) targetProduct).getType() == Account.TYPE_DEBIT) {
                                operationSubtype = RequestConstants.DEPOSIT_REFILL;
                            } else {
                                operationSubtype = RequestConstants.NORMAL_OPERATION;
                            }
                        }
                        targetId = targetProduct.getId();
                        flags = RequestConstants.TRANSFER_BETWEEN_CLIENTS_PRODUCTS;
                        break;

                    case CheckTransferData.CLIENT_GPB:
                        if (sourceProduct instanceof Card) {
                            sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
                        } else if (sourceProduct instanceof Account) {
                            sourceType = RequestConstants.SOURCE_CLIENTS_ACCOUNT;
                        }
                        targetType = RequestConstants.EXTERNAL_CARD;
                        if (actionType == RequestConstants.AFTER_TRANSFER_CHECK) {
                            operationSubtype = RequestConstants.NORMAL_OPERATION;
                        }
                        fullName = checkTransferData.getFioRecipient();
                        targetId = checkTransferData.getNumberCard();
                        flags = RequestConstants.TRANSFER_TO_ALIEN_PRODUCT;
                        break;

                    case CheckTransferData.INDIVIDUAL_TRANSFER:
                    case CheckTransferData.LEGAL_TRANSFER:
                    case CheckTransferData.BUDGET_TRANSFER:
                        if (sourceProduct instanceof Card) {
                            sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
                        } else if (sourceProduct instanceof Account) {
                            sourceType = RequestConstants.SOURCE_CLIENTS_ACCOUNT;
                        }
                        targetType = RequestConstants.ADDITION_PARAMETERS;
                        if (actionType == RequestConstants.AFTER_TRANSFER_CHECK) {
                            operationSubtype = RequestConstants.NORMAL_OPERATION;
                        }
                        flags = RequestConstants.TRANSFER_TO_ALIEN_PRODUCT;
                        break;
                }

                disposable = requestManager.createTemplate(sessionId, actionType, sourceProduct.getCurrency(), amount,
                        sourceType, sourceProduct.getId(), targetType, targetId,
                        pid, templateName, fullName, params, null, operationSubtype, flags, transRef, pass,
                        mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                        new BaseRequestController(mEventBusController, CREATE_TEMPLATE, getMvpView().getClassUniqueDeviceId()));
                addDisposable(disposable);
            }
        });
    }

    private Disposable makeRequestByTransferType(String sessionId, String pass) {
        int sourceType = -1;
        int targetType = -1;
        String sourceId = null;
        String targetId = null;
        String fullName = null;
        Map<String, String> additionalParams = checkTransferData.getAdditionParams();

        if (checkTransferData.getSourceProduct() != null) {
            if (checkTransferData.getSourceProduct() instanceof Card) {
                sourceType = SourceType.CARD.getId();
            } else if (checkTransferData.getSourceProduct() instanceof Account) {
                sourceType = SourceType.ACCOUNT.getId();
            }
            sourceId = checkTransferData.getSourceProduct().getId();
        }

        if (checkTransferData.getTargetProduct() != null) {
            if (checkTransferData.getTargetProduct() instanceof Card) {
                targetType = TargetType.CLIENTS_CARD.getId();
            } else if (checkTransferData.getTargetProduct() instanceof Account) {
                targetType = TargetType.CLIENTS_ACCOUNTS.getId();
            }
        }

        switch (checkTransferData.getType()) {
            case CheckTransferData.OWN_TRANSFER:
                targetId = checkTransferData.getTargetProduct().getId();
                break;
            case CheckTransferData.CLIENT_GPB:
                targetType = TargetType.EXTERNAL_CARD.getId();
                fullName = checkTransferData.getFioRecipient();
                targetId = checkTransferData.getNumberCard();// TODO: 29.12.17 if not card?
                break;
            case CheckTransferData.LEGAL_TRANSFER:
            case CheckTransferData.BUDGET_TRANSFER:
            case CheckTransferData.INDIVIDUAL_TRANSFER:
                targetType = TargetType.ADDITIONAL.getId();
                break;
        }

        disposable = requestManager.transferExec(
                sessionId, Constants.RUR, checkTransferData.getAmount(), checkTransferData.getTransferResponse().getTransRef(),
                targetId, pass, "0", targetType, sourceId, "1",
                sourceType, fullName, additionalParams, null,
                mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                new BaseRequestController(mEventBusController, TRANSFER_EXEC, getMvpView().getClassUniqueDeviceId()));
        return disposable;
    }

    @Override
    public void passwordTextChanged(String pass) {
        getMvpView().enableConfirmButton(checkPassword(pass));
    }

    @Override
    public void onConfirmClicked(Editable text) {
        if (templateMode) {
            createTemplate(text.toString());
        } else {
            if (checkTransferData.getType().equals(CheckTransferData.TEMPLATE_TRANSFER)) {
                getTemplateExec(text.toString());
            } else {
                getTransferExec(text.toString());
            }
        }
    }

    private boolean checkPassword(String pass) {
        PasswordError passwordError = ValidFields.isValidPassword(pass);
        if (passwordError.getErrorCode() != PasswordError.DEFAULT_MESSAGE) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case START_REQUEST:
                if (event.getActionCode() == TRANSFER_EXEC
                        || event.getActionCode() == TEMPLATE_EXEC
                        || event.getActionCode() == CREATE_TEMPLATE) {
                    getMvpView().startProgressDialog();
                }
                break;
            case SUCCESS_REQUEST:
                if (event.getActionCode() == TRANSFER_EXEC) {
                    undisposable(disposable);
                    Response<TransferExecResponse> baseResponse = (Response<TransferExecResponse>) ((EventSuccessRequest) event).getData();
                    ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                }
                if (event.getActionCode() == TEMPLATE_EXEC) {
                    undisposable(disposable);
                    Response<ExecutePaymentResponse> baseResponse = (Response<ExecutePaymentResponse>) ((EventSuccessRequest) event).getData();
                    ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                }
                if (event.getActionCode() == CREATE_TEMPLATE) {
                    undisposable(disposable);
                    Response<TemplateCreateResponse> baseResponse = (Response<TemplateCreateResponse>) ((EventSuccessRequest) event).getData();
                    ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                }
                break;
            case FAIL_REQUEST:
                if (event.getActionCode() == TRANSFER_EXEC
                        || event.getActionCode() == TEMPLATE_EXEC
                        || event.getActionCode() == CREATE_TEMPLATE) {
                    undisposable(disposable);
                    getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                }
                break;
        }
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case TRANSFER_EXEC:
                TransferExecResponse transferExecResponse = (TransferExecResponse) data.getValue();
                getMvpView().openSuccessTransferScreen(transferExecResponse);
                getMvpView().completeProgressDialog();
                break;
            case TEMPLATE_EXEC:
                ExecutePaymentResponse executePaymentResponse = (ExecutePaymentResponse) data.getValue();
                getMvpView().openSuccessTransferScreen(new TransferExecResponse(executePaymentResponse));
                getMvpView().completeProgressDialog();
                break;
            case Action.CREATE_TEMPLATE:
                TemplateCreateResponse templateCreateResponse = (TemplateCreateResponse) data.getValue();
                getMvpView().completeProgressDialog();
                getMvpView().openCreateTemplateSuccessScreen(checkTransferData.getNewTemplateName(), templateCreateResponse.getId());
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                getMvpView().completeProgressDialog();
                getMvpView().unAuthorized();
                break;
            case BAD_REQUEST_ERROR:
            case DEFAULT_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
        }
        switch (errorCode) {
            // TODO: 27.12.17 handle these specific errors
            case ResponseHandler.MAX_CONFIRMATION_ATTEMPT_WAS_EXCEEDED_ERROR:
            case ResponseHandler.PROVIDED_PARAMETERS_VALUES_ERROR:
            case ResponseHandler.INCORRECT_TRANSREF:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));//temporary
                break;
        }
    }

}