package ru.bankproj.android.mobilebank.view.paymentfields;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.ValidFields;
import ru.bankproj.android.mobilebank.dataclasses.providers.PaymentParameter;
import ru.bankproj.android.mobilebank.view.paymentfields.base.BaseField;

/**
 * Created by Alexey Vereshchaga on 22.02.18.
 */

public class CustomDateField extends BaseField {

    private LinearLayout mRootView;
    private EditText mEtDigits;
    private PaymentParameter mPaymentParameter;
    private TextView mTvDescField;

    public CustomDateField(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        return mRootView;
    }

    @Override
    protected void initView() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mRootView = (LinearLayout) inflater.inflate(R.layout.view_type_digits_field, null, false);
        mEtDigits = mRootView.findViewById(R.id.et_digits);
        mTvDescField = mRootView.findViewById(R.id.tv_description_field);
    }

    public String getValue() {
        return mEtDigits.getEditableText().toString();
    }

    @Override
    public void setPaymentParameter(PaymentParameter paymentParameter) {
        mPaymentParameter = paymentParameter;
        initConfigField();
    }

    @Override
    public String getPaymentParameterId() {
        return mPaymentParameter != null ? mPaymentParameter.getId() : "";
    }

    @Override
    public void enableView(boolean isEnable) {
        mEtDigits.setEnabled(isEnable);
    }

    @Override
    public Observable<Boolean> getObservable() {
        return getEmptyFieldWatcherObservable(mEtDigits);
    }

    public Observable<Boolean> getEmptyFieldWatcherObservable(@NonNull final EditText editText) {
        final PublishSubject<Boolean> subject = PublishSubject.create();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                subject.onNext(ValidFields.isNotEmptyField(s.toString())
                        && isValid(s.toString())
                        && isValidByDate(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return subject;
    }

    private boolean isValid(String s) {
        return s.length() <= mPaymentParameter.getMax() && s.length() >= mPaymentParameter.getMin();
    }


    private boolean isValidByDate(String strDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMyyyy", new Locale("ru"));

        Date convertedDate = null;
        try {
            convertedDate = dateFormat.parse(strDate);
        } catch (ParseException e) {
            Log.e(CustomDateField.class.getSimpleName(), "isValidByDate: ", e);
        }
        if (convertedDate != null) {
            Calendar currentDateAfterMonth = Calendar.getInstance();
            currentDateAfterMonth.add(Calendar.MONTH, 1);
            Calendar currentDateBefore3Months = Calendar.getInstance();
            currentDateBefore3Months.add(Calendar.MONTH, -3);

            //ok everything is fine, date in range
            return convertedDate.before(currentDateAfterMonth.getTime())
                    && convertedDate.after(currentDateBefore3Months.getTime());
        }
        return false;
    }

    public void setValue(String value) {
        mEtDigits.setText(value);
    }

    private void initConfigField() {
        if (mPaymentParameter != null) {
            mRootView.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(mPaymentParameter.getName())) {
                mEtDigits.setHint(mPaymentParameter.getName());
            }
            if (!TextUtils.isEmpty(mPaymentParameter.getDescription())) {
                mTvDescField.setText(mPaymentParameter.getDescription());
                mTvDescField.setVisibility(View.VISIBLE);
            } else {
                mTvDescField.setVisibility(View.GONE);
            }
            if (mPaymentParameter.getMax() > 0) {
                InputFilter[] fArray = new InputFilter[1];
                fArray[0] = new InputFilter.LengthFilter(mPaymentParameter.getMax());
                mEtDigits.setFilters(fArray);
                mEtDigits.setInputType(InputType.TYPE_CLASS_NUMBER);
            }
        } else {
            mRootView.setVisibility(View.GONE);
        }
    }

}
