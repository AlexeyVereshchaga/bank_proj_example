package ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary;

import java.util.List;

import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;
import ru.bankproj.android.mobilebank.dataclasses.transfer.Subsidiary;

/**
 * ISubsidiaryContract.java 20.01.2018
 * Class description
 *
 * @author Alex.
 */

public interface ISubsidiaryContract {

    interface View extends IBaseProgressView {

        void initData(List<Subsidiary> selectDataList);
    }

    interface Presenter extends IBaseMvpPresenter<View> {
        void subsidiariesSelectItem(int position);
    }
}
