package ru.bankproj.android.mobilebank.di.module;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.di.qualifier.ApplicationContext;
import ru.bankproj.android.mobilebank.store.Store;

/**
 * Created by j7ars on 25.10.2017.
 */
@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Store provideStore(){
        Store store = new Store(mApplication, Constants.KEYSTORE_SAVING_FILE_NAME, Constants.KEYSTORE_PASSWORD.toCharArray());
        return store;
    }

}
