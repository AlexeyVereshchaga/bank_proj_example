package ru.bankproj.android.mobilebank.utils;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Alexey Vereshchaga on 25.01.18.
 */

public class RequestConstants {

    // ActionType
    public static final int PAYMENT = 0;
    public static final int TRANSFER = 1;
    public static final int CREDIT_REPAYMENT = 2;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({PAYMENT, TRANSFER, CREDIT_REPAYMENT})
    public @interface ActionType {
    }

    // Operation subtype
    public static final int NORMAL_OPERATION = 0;
    public static final int DEPOSIT_REFILL = 1;

    /**
     * When condition: actionType = 1
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({NORMAL_OPERATION, DEPOSIT_REFILL})
    public @interface OperationSubtype {
    }

    //flags
    // TODO: 09.01.18 need fill other flags constants (see business processes of mbt)
    /**
     * Attribute for the payment (from business processes of mbt)
     */
    public static final int PAYMENT_FLAG = 0;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({PAYMENT_FLAG})
    public @interface PaymentFlags {
    }

    public static final int TRANSFER_BETWEEN_CLIENTS_PRODUCTS = 0;
    public static final int TRANSFER_TO_ALIEN_PRODUCT = 1;

    /**
     * Attributes for the transfer. Contains bitwise OR of the following values:
     * 0 – Transfer between client’s cards/accounts
     * 1 – Transfer from client’s card/account to alien card/account
     */
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TRANSFER_BETWEEN_CLIENTS_PRODUCTS, TRANSFER_TO_ALIEN_PRODUCT})
    public @interface TransferFlags {
    }

    // TODO: 16.01.18 replace enums and hardcoded constants
    //target type
    public static final int ADDITION_PARAMETERS = 0;
    public static final int CLIENTS_CARD = 1;
    public static final int ACCOUNT_IN_SAME_BANK = 2;
    public static final int CLIENTS_ACCOUNT = 4;
    public static final int EXTERNAL_CARD = 11;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ADDITION_PARAMETERS, CLIENTS_CARD, ACCOUNT_IN_SAME_BANK, CLIENTS_ACCOUNT, EXTERNAL_CARD})
    public @interface TargetType {
    }

    public static final int AFTER_PAYMENT_CHECK = 0;
    public static final int AFTER_TRANSFER_CHECK = 1;
    public static final int AFTER_REPAYMENT_CHECK = 2;
    public static final int AFTER_TEMPLATES_CHECK = 4;
    public static final int AFTER_RECENT_OPERATIONS_CHECK = 5;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({AFTER_PAYMENT_CHECK, AFTER_TRANSFER_CHECK, AFTER_REPAYMENT_CHECK, AFTER_TEMPLATES_CHECK, AFTER_RECENT_OPERATIONS_CHECK})
    public @interface TemplateActionType {
    }

    public static final int SOURCE_CLIENTS_CARD = 1;
    public static final int SOURCE_CLIENTS_ACCOUNT = 2;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({SOURCE_CLIENTS_CARD, SOURCE_CLIENTS_ACCOUNT})
    public @interface SourceType {
    }
}
