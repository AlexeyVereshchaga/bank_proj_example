package ru.bankproj.android.mobilebank.main.transfer.main.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.main.view.TransferBudgetOrganizationActivity;
import ru.bankproj.android.mobilebank.main.transfer.clientgpb.view.TransferClientGpbActivity;
import ru.bankproj.android.mobilebank.main.transfer.individual.person.view.TransferIndividualPersonActivity;
import ru.bankproj.android.mobilebank.main.transfer.legal.person.view.TransferLegalPersonActivity;
import ru.bankproj.android.mobilebank.main.transfer.main.ITransferContract;
import ru.bankproj.android.mobilebank.main.transfer.own.account.view.TransferOwnAccountActivity;
import ru.bankproj.android.mobilebank.main.transfer.repayment.main.view.RepaymentActivity;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.RequestConstants;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public class TransferFragment extends BaseMvpFragment implements ITransferContract.View {

    public static final String SELECTED_PRODUCT = "TransferFragment.SELECTED_PRODUCT";
    public static final String CREATE_TEMPLATE_MODE = "TransferFragment.CREATE_TEMPLATE_MODE";

    @Inject
    ITransferContract.Presenter presenter;

    @BindView(R.id.tv_internal_client)
    TextView tvInternalClient;
    @BindView(R.id.tv_external_card)
    TextView tvExternalCard;
    @BindView(R.id.tv_repayment_card_credit)
    TextView tvRepaymentCreditCard;
    @BindView(R.id.tv_repayment_credit_account)
    TextView tvRepaymentCreditAccount;
    @BindView(R.id.tv_individual)
    TextView tvIndividual;
    @BindView(R.id.tv_legal_entity)
    TextView tvLegalEntity;
    @BindView(R.id.tv_public_sector)
    TextView tvPublicSector;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs() {
        if (getArguments() != null) {
            presenter.setArguments(getArguments().getParcelable(SELECTED_PRODUCT),
                    getArguments().getBoolean(CREATE_TEMPLATE_MODE));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.onCreate(savedInstanceState);
        Utils.hideKeyboard(mActivity);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        presenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        presenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_transfer;
    }

    @OnClick(R.id.tv_own_account)
    void onOwnAccountClick() {
        presenter.ownAccountClicked();
    }

    @OnClick(R.id.tv_internal_client)
    void onClientGpbClick() {
        presenter.clientGpbClicked();
    }

    @OnClick(R.id.tv_external_card)
    void onOtherClientClick() {
        presenter.otherClientClicked();
    }

    @OnClick(R.id.tv_repayment_card_credit)
    void onRepaymentCardCreditClick() {
        presenter.creditCardClicked();
    }

    @OnClick(R.id.tv_repayment_credit_account)
    void onRepaymentAccountCardClick() {
        presenter.creditAccountClicked();
    }

    @OnClick(R.id.tv_individual)
    void onindividualClick() {
        presenter.individualClicked();
    }

    @OnClick(R.id.tv_legal_entity)
    void onLegalClick() {
        presenter.legalClicked();
    }

    @OnClick(R.id.tv_public_sector)
    void onPublicSectorClick() {
        presenter.budgetClicked();
    }

    @Override
    public void showTransferOwnAccountScreen(BaseProduct baseProduct, boolean templateMode) {
        getScreenCreator().startActivity(this, mActivity, TransferOwnAccountActivity.class,
                getOwnAccountBundle(baseProduct, templateMode), Constants.START_TRANSFER_OWN_ACCOUNT_ACTIVITY);
    }

    @Override
    public void showTransferOtherClientScreen(BaseProduct baseProduct, boolean templateMode) {
        getScreenCreator().startActivity(this, mActivity, TransferClientGpbActivity.class,
                getOtherClientBundle(baseProduct, templateMode), Constants.START_TRANSFER_CLIENT_GPB_ACTIVITY);
    }

    @Override
    public void showRepaymentCreditCardScreen(BaseProduct baseProduct, boolean templateMode) {
        getScreenCreator().startActivity(this, mActivity, RepaymentActivity.class,
                getRepaymentBundle(baseProduct, RequestConstants.CLIENTS_CARD, templateMode), Constants.START_REPAYMENT_ACTIVITY);
    }

    @Override
    public void showRepaymentCreditAccountScreen(BaseProduct baseProduct, boolean templateMode) {
        getScreenCreator().startActivity(this, mActivity, RepaymentActivity.class,
                getRepaymentBundle(baseProduct, RequestConstants.ACCOUNT_IN_SAME_BANK, templateMode), Constants.START_REPAYMENT_ACTIVITY);
    }

    @Override
    public void showTransferIndividualScreen(BaseProduct baseProduct, boolean templateMode) {
        getScreenCreator().startActivity(this, mActivity, TransferIndividualPersonActivity.class,
                getIndividualBundle(baseProduct, templateMode), Constants.START_TRANSFER_INDIVIDUAL_PERSON_ACTIVITY);
    }

    @Override
    public void showTransferLegalScreen(BaseProduct baseProduct, boolean templateMode) {
        getScreenCreator().startActivity(this, mActivity, TransferLegalPersonActivity.class,
                getLegalBundle(baseProduct, templateMode), Constants.START_TRANSFER_LEGAL_PERSON_ACTIVITY);
    }

    @Override
    public void showTransferBudgetScreen(BaseProduct baseProduct, boolean templateMode) {
        getScreenCreator().startActivity(this, mActivity, TransferBudgetOrganizationActivity.class,
                getBudgetBundle(baseProduct, templateMode), Constants.START_BUDGET_ORGANIZATION_ACTIVITY);
    }

    @Override
    public void disableRepaymentAccounts() {
        tvRepaymentCreditAccount.setEnabled(false);
        tvRepaymentCreditAccount.setCompoundDrawablesRelativeWithIntrinsicBounds(FieldConverter.getDrawable(R.drawable.ic_transfer_credit_account_grey), null, null, null);
    }

    @Override
    public void disableRepaymentCards() {
        tvRepaymentCreditCard.setEnabled(false);
        tvRepaymentCreditCard.setCompoundDrawablesRelativeWithIntrinsicBounds(FieldConverter.getDrawable(R.drawable.ic_transfer_card_credit_grey), null, null, null);
    }

    @Override
    public void showTransferClientGpbScreen(BaseProduct baseProduct, boolean templateMode) {
        getScreenCreator().startActivity(this, mActivity, TransferClientGpbActivity.class,
                getClientGpbBundle(baseProduct, templateMode), Constants.START_TRANSFER_CLIENT_GPB_ACTIVITY);
    }

    @NonNull
    private Bundle getOwnAccountBundle(BaseProduct card, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferOwnAccountActivity.SOURCE_PRODUCT, card);
        bundle.putString(TransferOwnAccountActivity.TRANSFER_DESCRIPTION, getString(R.string.transfer_between_accounts));
        bundle.putBoolean(TransferOwnAccountActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    private Bundle getRepaymentBundle(BaseProduct selectedCard, @RequestConstants.TargetType int targetType, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(RepaymentActivity.SOURCE_CARD, selectedCard);
        bundle.putInt(RepaymentActivity.TARGET_TYPE, targetType);
        switch (targetType) {
            case RequestConstants.ACCOUNT_IN_SAME_BANK:
                bundle.putString(RepaymentActivity.TRANSFER_DESCRIPTION, getString(R.string.transfer_credit_account));
                break;
            case RequestConstants.CLIENTS_CARD:
                bundle.putString(RepaymentActivity.TRANSFER_DESCRIPTION, getString(R.string.transfer_card_credit));
                break;
        }
        bundle.putBoolean(RepaymentActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    @NonNull
    private Bundle getClientGpbBundle(BaseProduct card, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferClientGpbActivity.SOURCE_CARD, card);
        bundle.putString(TransferClientGpbActivity.TRANSFER_DESCRIPTION, getString(R.string.transfer_internal_client));
        bundle.putBoolean(TransferClientGpbActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    @NonNull
    private Bundle getOtherClientBundle(BaseProduct card, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferClientGpbActivity.SOURCE_CARD, card);
        bundle.putString(TransferClientGpbActivity.TRANSFER_DESCRIPTION, getString(R.string.transfer_external_card));
        bundle.putBoolean(TransferClientGpbActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    @NonNull
    private Bundle getLegalBundle(BaseProduct card, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferLegalPersonActivity.SOURCE_CARD, card);
        bundle.putString(TransferLegalPersonActivity.TRANSFER_DESCRIPTION, getString(R.string.transfer_individual));
        bundle.putBoolean(TransferLegalPersonActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    @NonNull
    private Bundle getIndividualBundle(BaseProduct card, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferIndividualPersonActivity.SOURCE_CARD, card);
        bundle.putString(TransferIndividualPersonActivity.TRANSFER_DESCRIPTION, getString(R.string.transfer_individual));
        bundle.putBoolean(TransferIndividualPersonActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    @NonNull
    private Bundle getBudgetBundle(BaseProduct card, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferBudgetOrganizationActivity.SOURCE_CARD, card);
        bundle.putString(TransferBudgetOrganizationActivity.TRANSFER_DESCRIPTION, getString(R.string.transfer_public_sector));
        bundle.putBoolean(TransferBudgetOrganizationActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.START_BUDGET_ORGANIZATION_ACTIVITY:
            case Constants.START_TRANSFER_CLIENT_GPB_ACTIVITY:
            case Constants.START_TRANSFER_INDIVIDUAL_PERSON_ACTIVITY:
            case Constants.START_TRANSFER_LEGAL_PERSON_ACTIVITY:
            case Constants.START_TRANSFER_OWN_ACCOUNT_ACTIVITY:
            case Constants.START_REPAYMENT_ACTIVITY:
                if (resultCode == Constants.RESULT_LOGOUT) {
                    unAuthorized();
                }
                break;
        }
    }
}
