package ru.bankproj.android.mobilebank.view;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.utils.FieldConverter;

/**
 * Created by j7ars on 06.12.2017.
 */

public class ErrorLabelContainer extends LinearLayout implements ViewGroup.OnHierarchyChangeListener {

    private static final int ERROR_LABEL_TEXT_SIZE = 12;
    private static final int ERROR_LABEL_PADDING = 4;

    private TextView mErrorLabel;
    private Drawable mDrawable;
    private int mErrorColor;

    public ErrorLabelContainer(Context context) {
        super(context);
        initView();
    }

    public ErrorLabelContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ErrorLabelContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        setOnHierarchyChangeListener(this);
        setOrientation(VERTICAL);
        mErrorColor = FieldConverter.getColor(R.color.tomato);
        initErrorLabel();
    }

    private void initErrorLabel() {
        mErrorLabel = new TextView(getContext());
        mErrorLabel.setFocusable(true);
        mErrorLabel.setFocusableInTouchMode(false);
        mErrorLabel.setTextSize(ERROR_LABEL_TEXT_SIZE);
        mErrorLabel.setTextColor(mErrorColor);
    }

    public void setErrorColor(int color) {
        mErrorColor = color;
        mErrorLabel.setTextColor(mErrorColor);
    }

    public void clearError() {
        mErrorLabel.setVisibility(INVISIBLE);
        mDrawable.clearColorFilter();
    }

    public void setError(String text) {
        mErrorLabel.setVisibility(VISIBLE);
        mErrorLabel.setText(text);
        mDrawable.setColorFilter(mErrorColor, PorterDuff.Mode.SRC_ATOP);
        mErrorLabel.requestFocus();
    }

    @Override
    public void onChildViewAdded(View parent, View child) {
        int childCount = getChildCount();
        if (childCount == 1) {
            mDrawable = getChildAt(0).getBackground();
            addView(mErrorLabel);
        }
    }

    @Override
    public void onChildViewRemoved(View parent, View child) {
    }

    private int dipsToPix(float dps) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dps, getResources().getDisplayMetrics());
    }

}
