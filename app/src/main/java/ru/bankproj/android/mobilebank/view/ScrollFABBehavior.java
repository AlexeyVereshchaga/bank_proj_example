package ru.bankproj.android.mobilebank.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

import com.github.clans.fab.FloatingActionButton;

/**
 * Created by maxmobiles on 11.12.2017.
 */

public class ScrollFABBehavior extends CoordinatorLayout.Behavior<FloatingActionButton> {

    public ScrollFABBehavior(Context context, AttributeSet attributeSet){
        super();
    }

    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull FloatingActionButton child, @NonNull View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int type) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, type);
        if(dyConsumed > 0 && child.getVisibility() == View.VISIBLE){
            child.hide(true);
        } else if(dyConsumed < 0 && child.getVisibility() != View.VISIBLE){
            child.show(true);
        }
    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull FloatingActionButton child, @NonNull View directTargetChild, @NonNull View target, int axes, int type) {
        return type == ViewCompat.SCROLL_AXIS_VERTICAL;
    }

}
