package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.AccountConditions;

/**
 * Created by j7ars on 16.12.2017.
 */

public class AccountConditionsResponse extends BaseResponse {

    @SerializedName("planDate")
    private String planDate;
    @SerializedName("conditions")
    private ArrayList<AccountConditions> conditionList;

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public ArrayList<AccountConditions> getConditionList() {
        return conditionList;
    }

    public void setConditionList(ArrayList<AccountConditions> conditionList) {
        this.conditionList = conditionList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.planDate);
        dest.writeTypedList(this.conditionList);
    }

    public AccountConditionsResponse() {
    }

    protected AccountConditionsResponse(Parcel in) {
        super(in);
        this.planDate = in.readString();
        this.conditionList = in.createTypedArrayList(AccountConditions.CREATOR);
    }

    public static final Creator<AccountConditionsResponse> CREATOR = new Creator<AccountConditionsResponse>() {
        @Override
        public AccountConditionsResponse createFromParcel(Parcel source) {
            return new AccountConditionsResponse(source);
        }

        @Override
        public AccountConditionsResponse[] newArray(int size) {
            return new AccountConditionsResponse[size];
        }
    };
}
