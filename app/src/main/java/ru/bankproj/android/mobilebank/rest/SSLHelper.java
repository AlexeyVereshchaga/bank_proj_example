package ru.bankproj.android.mobilebank.rest;

import android.content.Context;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import ru.bankproj.android.mobilebank.BuildConfig;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.App;

/**
 * Created by j7ars on 27.10.2017.
 */

public class SSLHelper {

    public static final HostnameVerifier BM_HOST_VERIFIER = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public static SSLSocketFactory getSocketFactory() {
        try {
            KeyStore serverKeyStore = loadKeyStore(App.mInstance);

            SSLContext sslContext = SSLContext.getInstance("TLS");

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(serverKeyStore);

            sslContext.init(null, tmf.getTrustManagers(), null);

            return sslContext.getSocketFactory();

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public static X509TrustManager getTrustManager() {

        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };

        return (X509TrustManager) trustAllCerts[0];
    }

    private static KeyStore loadKeyStore(Context context) throws Exception {
        KeyStore keyStore = KeyStore.getInstance("BKS");
        int pathCert = BuildConfig.BANK_SERVER_MODE  ? R.raw.gpb_cert : R.raw.gpb_cert_2;
        InputStream stream = context.getResources().openRawResource(pathCert);

        try {
            keyStore.load(stream, BuildConfig.BANK_SERVER_MODE  ? "".toCharArray(): "".toCharArray());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            stream.close();
        }
        return keyStore;
    }

}
