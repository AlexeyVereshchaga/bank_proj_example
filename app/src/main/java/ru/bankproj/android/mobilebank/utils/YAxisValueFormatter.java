package ru.bankproj.android.mobilebank.utils;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by ismet on 12/20/17.
 */

public class YAxisValueFormatter implements IAxisValueFormatter {

    private DecimalFormat mFormat;

    public YAxisValueFormatter() {
        mFormat = new DecimalFormat("#,###.00 \u20BD"); // use one decimal
    }


    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        if(value == 0f){
            return "" + (int)value;
        }else {
            return mFormat.format(value);
        }
    }
}
