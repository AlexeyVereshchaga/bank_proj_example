package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.providers.SupportedCurrencies;
import ru.bankproj.android.mobilebank.dataclasses.providers.SupportedFunctions;
import ru.bankproj.android.mobilebank.dataclasses.providers.SupportedRegions;

/**
 * Created by j7ars on 21.11.2017.
 */

public class CapabilitiesResponse extends BaseResponse{

    @SerializedName("supportedCurrencies")
    private List<SupportedCurrencies> supportedCurrencies;
    @SerializedName("supportedFunctions")
    private SupportedFunctions supportedFunctions;
    @SerializedName("supportedRegions")
    private List<SupportedRegions> supportedRegions;

    public List<SupportedCurrencies> getSupportedCurrencies() {
        return supportedCurrencies;
    }

    public void setSupportedCurrencies(List<SupportedCurrencies> supportedCurrencies) {
        this.supportedCurrencies = supportedCurrencies;
    }

    public SupportedFunctions getSupportedFunctions() {
        return supportedFunctions;
    }

    public void setSupportedFunctions(SupportedFunctions supportedFunctions) {
        this.supportedFunctions = supportedFunctions;
    }

    public List<SupportedRegions> getSupportedRegions() {
        return supportedRegions;
    }

    public void setSupportedRegions(List<SupportedRegions> supportedRegions) {
        this.supportedRegions = supportedRegions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.supportedCurrencies);
        dest.writeParcelable(this.supportedFunctions, flags);
        dest.writeTypedList(this.supportedRegions);
    }

    public CapabilitiesResponse() {
    }

    protected CapabilitiesResponse(Parcel in) {
        super(in);
        this.supportedCurrencies = in.createTypedArrayList(SupportedCurrencies.CREATOR);
        this.supportedFunctions = in.readParcelable(SupportedFunctions.class.getClassLoader());
        this.supportedRegions = in.createTypedArrayList(SupportedRegions.CREATOR);
    }

    public static final Creator<CapabilitiesResponse> CREATOR = new Creator<CapabilitiesResponse>() {
        @Override
        public CapabilitiesResponse createFromParcel(Parcel source) {
            return new CapabilitiesResponse(source);
        }

        @Override
        public CapabilitiesResponse[] newArray(int size) {
            return new CapabilitiesResponse[size];
        }
    };
}
