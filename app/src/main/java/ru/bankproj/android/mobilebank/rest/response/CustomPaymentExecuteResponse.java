package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.operationhistory.OperationStatus;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;

/**
 * Created by j7ars on 25.12.2017.
 */

public class CustomPaymentExecuteResponse extends BaseResponse{

    @SerializedName("flash")
    private String flash;
    @SerializedName("payload")
    private String payload;
    @SerializedName("transRef")
    private String transRef;
    @SerializedName("confirmReq")
    private ConfirmRequestDescriptor confirmationRequestDescriptor;
    @SerializedName("operationStatus")
    private OperationStatus operationStatus;

    public String getFlash() {
        return flash;
    }

    public void setFlash(String flash) {
        this.flash = flash;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getTransRef() {
        return transRef;
    }

    public void setTransRef(String transRef) {
        this.transRef = transRef;
    }

    public ConfirmRequestDescriptor getConfirmationRequestDescriptor() {
        return confirmationRequestDescriptor;
    }

    public void setConfirmationRequestDescriptor(ConfirmRequestDescriptor confirmationRequestDescriptor) {
        this.confirmationRequestDescriptor = confirmationRequestDescriptor;
    }

    public OperationStatus getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(OperationStatus operationStatus) {
        this.operationStatus = operationStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.flash);
        dest.writeString(this.payload);
        dest.writeString(this.transRef);
        dest.writeParcelable(this.confirmationRequestDescriptor, flags);
        dest.writeParcelable(this.operationStatus, flags);
    }

    public CustomPaymentExecuteResponse() {
    }

    protected CustomPaymentExecuteResponse(Parcel in) {
        super(in);
        this.flash = in.readString();
        this.payload = in.readString();
        this.transRef = in.readString();
        this.confirmationRequestDescriptor = in.readParcelable(ConfirmRequestDescriptor.class.getClassLoader());
        this.operationStatus = in.readParcelable(OperationStatus.class.getClassLoader());
    }

    public static final Creator<CustomPaymentExecuteResponse> CREATOR = new Creator<CustomPaymentExecuteResponse>() {
        @Override
        public CustomPaymentExecuteResponse createFromParcel(Parcel source) {
            return new CustomPaymentExecuteResponse(source);
        }

        @Override
        public CustomPaymentExecuteResponse[] newArray(int size) {
            return new CustomPaymentExecuteResponse[size];
        }
    };
}
