package ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.view;

import android.os.Bundle;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.activity.BaseBackContainerActivity;

/**
 * SubsidiaryActivity.java 20.01.2018
 * Class description
 *
 * @author Alex.
 */

public class SubsidiaryActivity extends BaseBackContainerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        visibleCancel();
        setTitle(getString(R.string.subsidiary_title));
        if(savedInstanceState == null) {
            openFragment();
        }
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, getScreenCreator()
                .newInstance(SubsidiaryFragment.class)).commitAllowingStateLoss();
    }
}
