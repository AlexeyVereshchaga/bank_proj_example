package ru.bankproj.android.mobilebank.view;

import android.text.TextPaint;
import android.text.style.ClickableSpan;

/**
 * Created by j7ars on 02.11.2017.
 */

public abstract class TouchableSpan extends ClickableSpan {

    private boolean mIsPressed;
    private int mPressedBackgroundColor;
    private int mNormalTextColor;
    private int mPressedTextColor;
    private int mBackgroundColor;

    public TouchableSpan(int normalTextColor,int backgroundColor, int pressedTextColor, int pressedBackgroundColor) {
        mBackgroundColor = backgroundColor;
        mNormalTextColor = normalTextColor;
        mPressedTextColor = pressedTextColor;
        mPressedBackgroundColor = pressedBackgroundColor;
    }

    public void setPressed(boolean isSelected) {
        mIsPressed = isSelected;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setColor(mIsPressed ? mPressedTextColor : mNormalTextColor);
        ds.bgColor = mIsPressed ? mPressedBackgroundColor : mBackgroundColor;
        ds.setUnderlineText(!mIsPressed);
    }
}
