package ru.bankproj.android.mobilebank.progressdialog.custom.observer;

import ru.bankproj.android.mobilebank.progressdialog.ProgressEvent;

/**
 * Created by j7ars on 09.11.2017.
 */

public interface ProgressObserver {

    void event(ProgressEvent progressEvent);

}
