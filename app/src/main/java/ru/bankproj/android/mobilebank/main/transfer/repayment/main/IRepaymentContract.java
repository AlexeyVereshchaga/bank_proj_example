package ru.bankproj.android.mobilebank.main.transfer.repayment.main;

import java.util.List;

import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.CreditAccountDetail;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.Subsidiary;
import ru.bankproj.android.mobilebank.utils.RequestConstants;

/**
 * Created by Alexey Vereshchaga on 10.01.18.
 */

public interface IRepaymentContract {
    interface View extends IBaseProgressView {
        void openSelectTargetCardScreen(FilterProductData data);

        void moveToTop();

        void showSourceProduct(@RequestConstants.TargetType int targetType, BaseProduct debitingCard);

        void showTargetProduct(BaseProduct recipientCard);

        void showAccountNumberField(boolean show);

        void openRepaymentCheckScreen(CheckTransferData checkTransferData, boolean templateMode);

        void initView(boolean templateMode, int targetType, BaseProduct targetProduct);

        void openSelectSourceCardScreen(FilterProductData data);

        void openSubsidiariesScreen();

        void showSubsidiary(Subsidiary subsidiary);

        void showErrorDialog(CreditAccountDetail b);

        void showCardLimitDialog();

        void fieldValidationSuccess(Boolean success);

        void showNotValidAmountMessage(int validationValue);
    }

    interface Presenter extends IBaseMvpPresenter<View> {
        void addAccountNumberObservable(Observable<Boolean> accountNumberObservable);

        void removeAccountNumberObservable();

        void addSubsidiaryObservable(Observable<Boolean> subsidiaryObservable);

        void removeSubsidiaryObservable();

        void onSourceClicked();

        void onTargetClicked();

        void onNextClicked(String amount);

        void onSubsidiaryClicked();

        void setListObservableField(List<Observable<Boolean>> listObservable);
    }
}
