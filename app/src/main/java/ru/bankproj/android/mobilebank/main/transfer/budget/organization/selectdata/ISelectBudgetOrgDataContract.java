package ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SelectBudgetOrgData;

/**
 * Created by j7ars on 28.12.2017.
 */

public interface ISelectBudgetOrgDataContract {

    interface View extends IBaseProgressView {

        void initData(ArrayList<SelectBudgetOrgData> selectDataList);

        void setResult(SelectBudgetOrgData selectBudgetOrgData);

    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void budgetOrgDataSelectItem(int position);

    }

}
