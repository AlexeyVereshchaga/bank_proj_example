package ru.bankproj.android.mobilebank.managers;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.internal.operators.completable.CompletableFromAction;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.database.AppDatabase;
import ru.bankproj.android.mobilebank.database.DatabaseColumnInfo;
import ru.bankproj.android.mobilebank.dataclasses.block.Block;
import ru.bankproj.android.mobilebank.dataclasses.providers.Category;
import ru.bankproj.android.mobilebank.dataclasses.providers.Provider;
import ru.bankproj.android.mobilebank.dataclasses.providers.SupportedCurrencies;
import ru.bankproj.android.mobilebank.dataclasses.providers.SupportedFunctions;
import ru.bankproj.android.mobilebank.dataclasses.providers.SupportedRegions;
import ru.bankproj.android.mobilebank.di.qualifier.ApplicationContext;
import ru.bankproj.android.mobilebank.utils.FieldConverter;

/**
 * Created by j7ars on 15.11.2017.
 */
@Singleton
public class DatabaseManager {

    private AppDatabase mAppDatabase;

    @Inject
    public DatabaseManager(@ApplicationContext Context context) {
        mAppDatabase = AppDatabase.getInstance(context);
    }

    //block
    public Single<List<Block>> getAllBlock() {
        return mAppDatabase.blockDao().getAllBlock();
    }

    public Single<List<Block>> getBlockListByCategoryId(int categoryId) {
        return mAppDatabase.blockDao().getBlockListByCategoryId(categoryId);
    }

    public Single<List<Block>> getBlockListByCategoryIdAndByStatus(int categoryId, boolean isOn) {
        return mAppDatabase.blockDao().getBlockListByCategoryIdAndByStatus(categoryId, isOn);
    }

    public Single<List<Block>> getUnAuthorizedBlockList() {
        return mAppDatabase.blockDao().getUnAuthorizedBlockList(true);
    }

    public Completable insertOrUpdateBlock(Block block) {
        return new CompletableFromAction(() -> {
            mAppDatabase.blockDao().deleteBlock(block);
            mAppDatabase.blockDao().insertBlock(block);
        });
    }

    public Completable initDefaultBlock() {
        return new CompletableFromAction(() -> {
            if (mAppDatabase.blockDao().blockCount() == 0) {
                mAppDatabase.blockDao().deleteAllBlock();
                mAppDatabase.blockDao().insertAllBlock(getDefaultBlock());
            }
        });
    }

    private ArrayList<Block> getDefaultBlock() {
        ArrayList<Block> blockList = new ArrayList<>();
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_NEWS, FieldConverter.getString(R.string.block_notification), FieldConverter.getString(R.string.block_see_all), 0, false, false, false, false, true, 3, 0, DatabaseColumnInfo.CATEGORY_BANK_PRODUCT, false));
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_OFFERS, FieldConverter.getString(R.string.block_offers), FieldConverter.getString(R.string.block_see_all), 196, false, false, false, false, true, 1, 1, DatabaseColumnInfo.CATEGORY_BANK_PRODUCT, true));
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_ATM, FieldConverter.getString(R.string.block_atm), null, 136, false, false, false, false, true, 2, 3, DatabaseColumnInfo.CATEGORY_BANK_PRODUCT, true));
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_MY_CARDS, FieldConverter.getString(R.string.block_my_card), null, 0, true, false, false, false, true, 1, 0, DatabaseColumnInfo.CATEGORY_MY_PRODUCT, false));
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_MY_ACCOUNTS, FieldConverter.getString(R.string.block_my_accounts), null, 0, true, false, false, false, true, 2, 0, DatabaseColumnInfo.CATEGORY_MY_PRODUCT, false));
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_MY_DEBITS, FieldConverter.getString(R.string.block_my_debits), null, 0, true, false, false, false, true, 3, 0, DatabaseColumnInfo.CATEGORY_MY_PRODUCT, false));
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_MY_CREDITS, FieldConverter.getString(R.string.block_my_credits), null, 0, true, false, false, false, true, 4, 0, DatabaseColumnInfo.CATEGORY_MY_PRODUCT, false));
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_PAYMENT, FieldConverter.getString(R.string.block_payment), null, 222, false, false, false, false, true, 2, 0, DatabaseColumnInfo.CATEGORY_PAYMENTS, false));
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_TEMPLATES, FieldConverter.getString(R.string.block_templates), null, 0, false, false, false, false, true, 1, 0, DatabaseColumnInfo.CATEGORY_PAYMENTS, false));
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_TRANSFER, FieldConverter.getString(R.string.block_transfer), null, 222, false, false, false, false, true, 3, 0, DatabaseColumnInfo.CATEGORY_PAYMENTS, false));
        blockList.add(new Block(DatabaseColumnInfo.BLOCK_AUTH, null, null, 40, false, false, false, false, true, 0, 2, DatabaseColumnInfo.CATEGORY_EMPTY, true));
        return blockList;
    }

    //category provider
    public Single<List<Category>> getAllCategories() {
        return mAppDatabase.categoryDao().getAllCategories();
    }

    public void insertOrReplaceCategory(Category category) {
        mAppDatabase.categoryDao().insertCategory(category);
    }

    public void insertOrReplaceCategories(List<Category> categories) {
        mAppDatabase.categoryDao().insertCategories(categories);
    }

    public void deleteAllCategories() {
        mAppDatabase.categoryDao().deleteAllCategories();
    }

    public Single<List<Category>> getCategoriesByQuery(String query) {
        return mAppDatabase.categoryDao().getCategoriesByQuery("%" + query + "%");
    }

    //provider
    public Single<List<Provider>> getAllProviders() {
        return mAppDatabase.providerDao().getAllProviders();
    }

    public void insertOrReplaceProvider(Provider provider) {
        mAppDatabase.providerDao().insertProvider(provider);
    }

    public void insertOrReplaceProviders(List<Provider> providers) {
        mAppDatabase.providerDao().insertProviders(providers);
    }

    public void deleteAllProviders() {
        mAppDatabase.providerDao().deleteAllProviders();
    }

    public Single<List<Provider>> getAllProvidersByCategory(String cid) {
        return mAppDatabase.providerDao().getAllPaymentProvidersByCategory(cid);
    }

    public Single<List<Provider>> getProvidersByQuery(String query) {
        return mAppDatabase.providerDao().getProvidersByQuery("%" + query + "%");
    }

    public Single<Provider> getProvidersById(String pid) {
        return mAppDatabase.providerDao().getProvidersById(pid);
    }

    //provider payment parameters
//    public Single<List<PaymentParameter>> getAllPaymentParameterRecords() {
//        return mAppDatabase.paymentParametersDao().getAllPaymentParameterRecords();
//    }
//
//    public Single<List<PaymentParameter>> getAllPaymentParameterRecordsByProvider(String pid) {
//        return mAppDatabase.paymentParametersDao().getAllPaymentParameterRecordsByProvider(pid);
//    }
//
//    public void insertOrReplacePaymentParameterRecord(PaymentParameter paymentParameterRecord) {
//        mAppDatabase.paymentParametersDao().insertOrReplacePaymentParameterRecord(paymentParameterRecord);
//    }
//
//    public void deleteAllPaymentParameterRecords() {
//        mAppDatabase.paymentParametersDao().deleteAllPaymentParameterRecords();
//    }

    //supported regions
    public Single<List<SupportedRegions>> getAllSupportedRegions() {
        return mAppDatabase.supportedRegionsDao().getAllSupportedRegions();
    }

    public void insertAllSupportedRegions(List<SupportedRegions> supportedRegionsList) {
        mAppDatabase.supportedRegionsDao().insertAllSupportedRegions(supportedRegionsList);
    }

    public void deleteAllSupportedRegions() {
        mAppDatabase.supportedRegionsDao().deleteAllSupportedRegions();
    }

    //supported currencies
    public Single<List<SupportedCurrencies>> getAllSupportedCurrencies() {
        return mAppDatabase.supportedCurrenciesDao().getAllSupportedCurrencies();
    }

    public void insertAllSupportedCurrencies(List<SupportedCurrencies> supportedCurrenciesList) {
        mAppDatabase.supportedCurrenciesDao().insertAllSupportedCurrencies(supportedCurrenciesList);
    }

    public void deleteAllSupportedCurrencies() {
        mAppDatabase.supportedCurrenciesDao().deleteAllSupportedCurrencies();
    }

    //supported functions
    public Single<SupportedFunctions> getSupportedFunctions() {
        return mAppDatabase.supportedFunctionsDao().getSupportedFunctions();
    }

    public void insertOrReplaceSupportedFunctions(SupportedFunctions supportedFunctions) {
        mAppDatabase.supportedFunctionsDao().insertOrReplaceSupportedFunctions(supportedFunctions);
    }

    public void deleteSupportedFunctions() {
        mAppDatabase.supportedFunctionsDao().deleteSupportedFunctions();
    }

}
