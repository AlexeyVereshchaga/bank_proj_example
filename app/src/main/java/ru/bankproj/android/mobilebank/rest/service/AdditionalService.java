package ru.bankproj.android.mobilebank.rest.service;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.rest.RequestField;
import ru.bankproj.android.mobilebank.rest.Urls;

/**
 * Created by Alexey Vereshchaga on 01.03.18.
 */

public interface AdditionalService {
    @FormUrlEncoded
    @POST(Urls.SUBSCRIBE_ON_PUSH_NOTIFICATIONS)
    Single<Response<BaseResponse>> subscribeOnPushNotifications(@Header(RequestField.JSESSION) String sessionId,
                                                                @FieldMap(encoded = true) Map<String, String> params);
}
