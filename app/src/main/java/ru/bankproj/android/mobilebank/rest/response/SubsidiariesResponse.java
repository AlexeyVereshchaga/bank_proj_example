package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.transfer.Subsidiary;

/**
 * SubsidiariesResponse.java 20.01.2018
 * Class description
 *
 * @author Alex.
 */

public class SubsidiariesResponse extends BaseResponse {
    @SerializedName("subsidiaries")
    private List<Subsidiary> subsidiaries;

    public List<Subsidiary> getSubsidiaries() {
        return subsidiaries;
    }

    public void setSubsidiaries(List<Subsidiary> subsidiaries) {
        this.subsidiaries = subsidiaries;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.subsidiaries);
    }

    public SubsidiariesResponse() {
    }

    protected SubsidiariesResponse(Parcel in) {
        super(in);
        this.subsidiaries = in.createTypedArrayList(Subsidiary.CREATOR);
    }

    public static final Creator<SubsidiariesResponse> CREATOR = new Creator<SubsidiariesResponse>() {
        @Override
        public SubsidiariesResponse createFromParcel(Parcel source) {
            return new SubsidiariesResponse(source);
        }

        @Override
        public SubsidiariesResponse[] newArray(int size) {
            return new SubsidiariesResponse[size];
        }
    };
}