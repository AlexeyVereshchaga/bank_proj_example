package ru.bankproj.android.mobilebank.main.transfer.main.view;

import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.activity.BaseBackContainerActivity;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public class TransferActivity extends BaseBackContainerActivity {

    public static final String SELECTED_PRODUCT = "TransferActivity.SELECTED_PRODUCT";
    public static final String CREATE_TEMPLATE_MODE = "TransferActivity.CREATE_TEMPLATE_MODE";

    private BaseProduct baseProduct;
    private boolean templateMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.transfer_title));
        getExtras();
        if (savedInstanceState == null) {
            openFragment();
        }
    }

    private void getExtras() {
        if (getIntent().getExtras() != null) {
            baseProduct = getIntent().getExtras().getParcelable(SELECTED_PRODUCT);
            templateMode = getIntent().getBooleanExtra(CREATE_TEMPLATE_MODE, false);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(SELECTED_PRODUCT, baseProduct);
        outState.putBoolean(CREATE_TEMPLATE_MODE, templateMode);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        baseProduct = savedInstanceState.getParcelable(SELECTED_PRODUCT);
        templateMode = savedInstanceState.getBoolean(CREATE_TEMPLATE_MODE);
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container, getScreenCreator().newInstance(TransferFragment.class, createBundle())).commitAllowingStateLoss();
    }

    private Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferFragment.SELECTED_PRODUCT, baseProduct);
        bundle.putBoolean(TransferFragment.CREATE_TEMPLATE_MODE, templateMode);
        return bundle;
    }
}