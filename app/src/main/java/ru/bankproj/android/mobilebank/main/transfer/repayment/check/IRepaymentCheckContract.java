package ru.bankproj.android.mobilebank.main.transfer.repayment.check;

import android.text.Editable;

import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.rest.response.TransferExecResponse;

/**
 * Created by Alexey Vereshchaga on 11.01.18.
 */

public interface IRepaymentCheckContract {
    interface View extends IBaseProgressView {

        void showRepaymentInfo(CheckTransferData checkTransferData);

        void enableConfirmButton(boolean b);

        void openSuccessTransferScreen(TransferExecResponse transferExecResponse);

        void openCreateTemplateSuccessScreen(String newTemplateName, String id);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void passwordTextChanged(String pass);

        void onConfirmClicked(Editable text);
    }
}
