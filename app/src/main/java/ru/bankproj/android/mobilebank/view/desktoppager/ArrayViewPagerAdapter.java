package ru.bankproj.android.mobilebank.view.desktoppager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.bankproj.android.mobilebank.R;

/**
 * Created by j7ars on 31.10.2017.
 */

public abstract class ArrayViewPagerAdapter <T> extends ArrayPagerAdapter<T> {

    public ArrayViewPagerAdapter() {
        super();
    }

    public ArrayViewPagerAdapter(T... items) {
        super(items);
    }

    public ArrayViewPagerAdapter(List<T> items) {
        super(items);
    }

    public abstract View getView(LayoutInflater inflater, ViewGroup container, T item, int position);

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        T item = getItem(position);
        View view = getView(LayoutInflater.from(container.getContext()), container, item, position);
        view.setTag(R.id.view_tag_key, getItemWithId(position));
        container.addView(view);
        return super.instantiateItem(container, position);
    }

    @Override
    public boolean isViewFromObject(View view, Object item) {
        return item.equals(view.getTag(R.id.view_tag_key));
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object item) {
        container.removeView(findViewWithTagInViewPager(container, item));
    }

    private View findViewWithTagInViewPager(ViewGroup container, Object item) {
        for (int i = 0; i < container.getChildCount(); i++) {
            View view = container.getChildAt(i);
            Object tag = view.getTag(R.id.view_tag_key);
            if (item.equals(tag)) {
                return view;
            }
        }
        throw new NullPointerException("view's tag is not found for some reason.");
    }
}
