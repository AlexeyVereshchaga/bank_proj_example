package ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.dataclasses.transfer.Subsidiary;

/**
 * SubsidiaryAdapter.java 20.01.2018
 * Class description
 *
 * @author Alex.
 */

public class SubsidiaryAdapter extends RecyclerView.Adapter<SubsidiaryAdapter.SubsidiaryViewHolder> {

    private List<Subsidiary> subsidiaries;

    private OnItemClickListener onItemClickListener;

    public SubsidiaryAdapter() {
        subsidiaries = new ArrayList<>();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setData(List<Subsidiary> subsidiaries) {
        this.subsidiaries = subsidiaries;
        notifyDataSetChanged();
    }

    @Override
    public SubsidiaryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_subsidiary_item, parent, false);
        return new SubsidiaryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SubsidiaryViewHolder holder, int position) {
        Subsidiary subsidiary = subsidiaries.get(position);
        holder.tvSubsidiaryName.setText(subsidiary.getName());
    }

    @Override
    public int getItemCount() {
        return subsidiaries.size();
    }

    class SubsidiaryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_subsidiary)
        TextView tvSubsidiaryName;

        SubsidiaryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.ll_subsidiary_container)
        void onClickItem() {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

}