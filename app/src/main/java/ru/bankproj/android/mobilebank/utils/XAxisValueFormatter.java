package ru.bankproj.android.mobilebank.utils;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.List;

import ru.bankproj.android.mobilebank.dataclasses.paymentscheduler.CreditPaymentSchedule;

/**
 * Created by ismet on 12/20/17.
 */

public class XAxisValueFormatter implements IAxisValueFormatter {

    private List<CreditPaymentSchedule> mCreditPaymentSchedulesList;

    public XAxisValueFormatter(List<CreditPaymentSchedule> creditPaymentSchedulesList) {
        mCreditPaymentSchedulesList = creditPaymentSchedulesList;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        if (value % 1 == 0) {
            return FieldConverter.formatDateToMonthWithDay(mCreditPaymentSchedulesList.get((int) value).getTimestamp());
        }
        return "";
    }
}