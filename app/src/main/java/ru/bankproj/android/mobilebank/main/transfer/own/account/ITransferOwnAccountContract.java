package ru.bankproj.android.mobilebank.main.transfer.own.account;

import java.util.List;

import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public interface ITransferOwnAccountContract {
    interface View extends IBaseProgressView {
        void openSelectTargetCardScreen(FilterProductData data);

        void moveToTop();

        void showSourceProduct(BaseProduct sourceProduct);

        void showTargetProduct(BaseProduct targetProduct);

        void showDialogBySourceTargetType(BaseProduct sourceProduct, BaseProduct targetProduct);

        void openTransferCheckScreen(CheckTransferData checkTransferData, boolean templateMode);

        void openSelectSourceCardScreen(FilterProductData data);

        void enableNextButton(boolean enable);

        void initTemplateMode(boolean templateMode);

        void showCardLimitDialog();

        void showNotValidAmountMessage(int validationValue);

        void fieldValidationSuccess(Boolean success);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void setListObservableField(List<Observable<Boolean>> mListObservable);

        void onSourceClicked();

        void onTargetClicked();

        void onNextClicked(String amount);

        void clearTargetProduct();

        void onResume();
    }
}


