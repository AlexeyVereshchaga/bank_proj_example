package ru.bankproj.android.mobilebank.view.paymentfields;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.PolyMaskTextChangedListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.ValidFields;
import ru.bankproj.android.mobilebank.dataclasses.providers.PaymentParameter;
import ru.bankproj.android.mobilebank.view.paymentfields.base.BaseField;

/**
 * Created by rrust on 17.12.2017.
 */

public class CustomPhoneNumberField extends BaseField implements View.OnClickListener {

    private RelativeLayout mRootView;
    private EditText mEtPhoneNumber;
    private ImageView mIvPhoneBook;
    private OnBtnPhoneBookClickListener mListener;
    private PaymentParameter mPaymentParameter;

    private String mPhoneValue;

    public CustomPhoneNumberField(Context context) {
        super(context);
    }

    public void setOnBtnPhoneBookClickListener(OnBtnPhoneBookClickListener listener) {
        mListener = listener;
    }

    @Override
    public View getView() {
        return mRootView;
    }

    private Observable<Boolean> getEmptyFieldWatcherObservable(@NonNull final EditText editText) {
        final PublishSubject<Boolean> subject = PublishSubject.create();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                subject.onNext(ValidFields.isNotEmptyField(s.toString()) && isValid(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (ValidFields.isValidPhone(s.toString())) {

                }
            }
        });
        return subject;
    }

    private boolean isValid(String s) {
        return s.length() <= mPaymentParameter.getMax() + 7 && s.length() >= mPaymentParameter.getMin() + 7;
    }

    @Override
    public void setValue(String value) {
        mEtPhoneNumber.setText(value);
    }

    public String getValue() {
        return mPhoneValue;
    }

    @Override
    public void enableView(boolean isEnable) {
        mEtPhoneNumber.setEnabled(isEnable);
        mIvPhoneBook.setEnabled(isEnable);
    }

    @Override
    protected void initView() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mRootView = (RelativeLayout) inflater.inflate(R.layout.view_type_custom_phone_field, null, false);
        mEtPhoneNumber = mRootView.findViewById(R.id.et_custom_phone_number);
        mIvPhoneBook = mRootView.findViewById(R.id.iv_phone_book);
        mIvPhoneBook.setOnClickListener(this);

        initMasckedPhone();
    }

    @Override
    public void setPaymentParameter(PaymentParameter paymentParameter) {
        mPaymentParameter = paymentParameter;
    }

    @Override
    public String getPaymentParameterId() {
        return mPaymentParameter != null ? mPaymentParameter.getId() : "";
    }


    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.OnBtnPhoneBookClick();
        }
    }

    @Override
    public Observable<Boolean> getObservable() {
        return getEmptyFieldWatcherObservable(mEtPhoneNumber);
    }

    public interface OnBtnPhoneBookClickListener {
        void OnBtnPhoneBookClick();
    }

    public void initMasckedPhone() {
        List<String> affineFormats = new ArrayList<>();
        affineFormats.add("8 ([000]) [000] [00] [00]");
        affineFormats.add("+7 ([000]) [000] [00] [00]");
        MaskedTextChangedListener listener = new PolyMaskTextChangedListener(
                "+7 ([000]) [000] [00] [00]",
                affineFormats,
                true,
                mEtPhoneNumber,
                null,
                (b, s) -> {
                    if (b && ValidFields.isValidPhone(s)) {
                        mPhoneValue = s;
                    } else {
                        mPhoneValue = null;
                    }
                });
        mEtPhoneNumber.addTextChangedListener(listener);
    }
}
