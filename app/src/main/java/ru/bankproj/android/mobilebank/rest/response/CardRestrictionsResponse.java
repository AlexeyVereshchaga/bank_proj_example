package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.OpTypes;
import ru.bankproj.android.mobilebank.dataclasses.product.Region;
import ru.bankproj.android.mobilebank.dataclasses.product.Restriction;

/**
 * Created by j7ars on 12.12.2017.
 */

public class CardRestrictionsResponse extends BaseResponse {

    @SerializedName("opTypes")
    private ArrayList<OpTypes> opTypesList;
    @SerializedName("regions")
    private ArrayList<Region> regionsList;
    @SerializedName("restrictions")
    private ArrayList<Restriction> restrictionList;

    public ArrayList<OpTypes> getOpTypesList() {
        return opTypesList;
    }

    public void setOpTypesList(ArrayList<OpTypes> opTypesList) {
        this.opTypesList = opTypesList;
    }

    public ArrayList<Region> getRegionsList() {
        return regionsList;
    }

    public void setRegionsList(ArrayList<Region> regionsList) {
        this.regionsList = regionsList;
    }

    public ArrayList<Restriction> getRestrictionList() {
        return restrictionList;
    }

    public void setRestrictionList(ArrayList<Restriction> restrictionList) {
        this.restrictionList = restrictionList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.opTypesList);
        dest.writeTypedList(this.regionsList);
        dest.writeTypedList(this.restrictionList);
    }

    public CardRestrictionsResponse() {
    }

    protected CardRestrictionsResponse(Parcel in) {
        super(in);
        this.opTypesList = in.createTypedArrayList(OpTypes.CREATOR);
        this.regionsList = in.createTypedArrayList(Region.CREATOR);
        this.restrictionList = in.createTypedArrayList(Restriction.CREATOR);
    }

    public static final Creator<CardRestrictionsResponse> CREATOR = new Creator<CardRestrictionsResponse>() {
        @Override
        public CardRestrictionsResponse createFromParcel(Parcel source) {
            return new CardRestrictionsResponse(source);
        }

        @Override
        public CardRestrictionsResponse[] newArray(int size) {
            return new CardRestrictionsResponse[size];
        }
    };
}
