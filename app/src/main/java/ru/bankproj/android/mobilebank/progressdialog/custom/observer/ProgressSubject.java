package ru.bankproj.android.mobilebank.progressdialog.custom.observer;

import ru.bankproj.android.mobilebank.progressdialog.ProgressEvent;

/**
 * Created by j7ars on 09.11.2017.
 */

public interface ProgressSubject {

    public void registerObserver(ProgressObserver observer);

    public void notifyObservers(ProgressEvent progressEvent);

    public void unRegisterObserver(ProgressObserver observer);

}
