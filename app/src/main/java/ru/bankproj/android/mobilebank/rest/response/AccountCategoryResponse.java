package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.AccountCategory;

/**
 * Created by j7ars on 16.12.2017.
 */

public class AccountCategoryResponse extends BaseResponse{

    @SerializedName("categories")
    private ArrayList<AccountCategory> accountCategoryList;

    public ArrayList<AccountCategory> getAccountCategoryList() {
        return accountCategoryList;
    }

    public void setAccountCategoryList(ArrayList<AccountCategory> accountCategoryList) {
        this.accountCategoryList = accountCategoryList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeList(this.accountCategoryList);
    }

    public AccountCategoryResponse() {
    }

    protected AccountCategoryResponse(Parcel in) {
        super(in);
        this.accountCategoryList = new ArrayList<AccountCategory>();
        in.readList(this.accountCategoryList, AccountCategory.class.getClassLoader());
    }

    public static final Creator<AccountCategoryResponse> CREATOR = new Creator<AccountCategoryResponse>() {
        @Override
        public AccountCategoryResponse createFromParcel(Parcel source) {
            return new AccountCategoryResponse(source);
        }

        @Override
        public AccountCategoryResponse[] newArray(int size) {
            return new AccountCategoryResponse[size];
        }
    };
}
