package ru.bankproj.android.mobilebank.main.transfer.repayment.main.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.app.ValidFields;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.product.CreditAccountDetail;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.Subsidiary;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.block.dialogs.BalanceDialog;
import ru.bankproj.android.mobilebank.main.dialogs.InfoDialog;
import ru.bankproj.android.mobilebank.main.payment.selectcardoraccount.view.SelectCardOrAccountActivity;
import ru.bankproj.android.mobilebank.main.transfer.repayment.check.view.RepaymentCheckActivity;
import ru.bankproj.android.mobilebank.main.transfer.repayment.dialog.RepaymentMessageDialog;
import ru.bankproj.android.mobilebank.main.transfer.repayment.main.IRepaymentContract;
import ru.bankproj.android.mobilebank.main.transfer.repayment.subsidiary.view.SubsidiaryActivity;
import ru.bankproj.android.mobilebank.utils.AmountDigitsInputFilter;
import ru.bankproj.android.mobilebank.utils.CheckUtils;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.RequestConstants;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.utils.storage.AppStorage;
import ru.bankproj.android.mobilebank.view.EnableButton;
import ru.bankproj.android.mobilebank.view.ErrorLabelContainer;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class RepaymentFragment extends BaseMvpFragment implements IRepaymentContract.View {

    public static final String SOURCE_CARD = "RepaymentFragment.SOURCE_CARD";
    public static final String DESCRIPTION_TRANSFER = "RepaymentFragment.TRANSFER_DESCRIPTION";
    public static final String TARGET_TYPE = "RepaymentFragment.TARGET_TYPE";
    public static final String TARGET_ACCOUNT = "RepaymentFragment.TARGET_ACCOUNT";
    public static final String TEMPLATE_MODE = "RepaymentFragment.TEMPLATE_MODE";

    @BindView(R.id.source_card)
    View sourceCardInclude;
    @BindView(R.id.target_card)
    View targetCardInclude;
    @BindView(R.id.et_subsidiary)
    EditText etSubsidiary;
    @BindView(R.id.ll_subsidiary)
    LinearLayout llSubsidiary;
    @BindView(R.id.subsidiary_separator)
    View subsidiarySeparator;
    @BindView(R.id.et_limit_assignment)
    EditText etAmount;
    @BindView(R.id.btn_transfer_next_step)
    EnableButton btnNext;
     @BindView(R.id.fl_focusable_source)
    View flSource;
    @BindView(R.id.fl_focusable_target)
    View flTarget;
    @BindView(R.id.transfer_target)
    View transferRecipient;
    @BindView(R.id.ll_account_number)
    LinearLayout llAccountNumber;
    @BindView(R.id.el_repayment_account_number)
    ErrorLabelContainer elAccountNumber;
    @BindView(R.id.et_repayment_account_number)
    EditText etAccountNumber;

    //Template views
    @BindView(R.id.ll_template_name_container)
    LinearLayout llTemplateNameContainer;
    @BindView(R.id.el_template_name)
    ErrorLabelContainer elTemplateName;
    @BindView(R.id.et_template_name)
    EditText etTemplateName;

    private IncludedCardView debitingView;
    private IncludedCardView targetView;

    @BindViews({R.id.transfer_source, R.id.source_card})
    List<View> sourceViews;
    @BindViews({R.id.transfer_target, R.id.target_card})
    List<View> targetViews;

    static final ButterKnife.Setter<View, Boolean> SHOW_PRODUCT = (view, value, index) -> {
        switch (view.getId()) {
            case R.id.source_card:
            case R.id.target_card:
                view.setVisibility(value ? View.VISIBLE : View.GONE);
                break;
            case R.id.transfer_source:
            case R.id.transfer_target:
                view.setVisibility(value ? View.GONE : View.VISIBLE);
                break;
        }
    };

    static class IncludedCardView {
        @BindView(R.id.tv_card_name)
        TextView tvCardName;
        @BindView(R.id.tv_card_number)
        TextView tvCardNumber;
        @BindView(R.id.tv_card_amount)
        TextView tvCardAmount;
        @BindView(R.id.tv_card_currency)
        TextView tvCardCurrency;
        @BindView(R.id.iv_card_icon)
        ImageView ivCardIcon;
        @BindView(R.id.tv_card_balance)
        TextView tvCardBalance;
        @BindView(R.id.iv_card_balance)
        ImageView ivCardBalance;
        @BindView(R.id.ll_amount_container)
        LinearLayout llAmountContainer;
        @BindView(R.id.ll_amount_balance)
        LinearLayout llAmountBalance;
    }

    @Inject
    IRepaymentContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs() {
        if (getArguments() != null) {
            presenter.setArguments(getArguments().getParcelable(SOURCE_CARD),
                    getArguments().getString(DESCRIPTION_TRANSFER),
                    getArguments().getInt(TARGET_TYPE),
                    getArguments().getParcelable(TARGET_ACCOUNT),
                    getArguments().getBoolean(TEMPLATE_MODE));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bindIncluded();
        presenter.onCreate(savedInstanceState);
        Utils.hideKeyboard(mActivity);
    }

    @Override
    public void initView(boolean templateMode, int targetType, BaseProduct targetProduct) {
        ((TextView) transferRecipient.findViewById(R.id.tv_select_card_text)).setText(FieldConverter.getString(R.string.debit_select_card_to_title));
        llTemplateNameContainer.setVisibility(templateMode ? VISIBLE : GONE);
        etAmount.setFilters(new InputFilter[]{new AmountDigitsInputFilter()});
        if (targetType == RequestConstants.ACCOUNT_IN_SAME_BANK && targetProduct != null) {
            showRepaymentCreditAccountAlert();
        }

        List<Observable<Boolean>> listObservable = new ArrayList<>();
        if (templateMode) {
            listObservable.add(Utils.getRegexObservable(etTemplateName, ValidFields.TEMPLATE_NAME_REGEX, getString(R.string.transfer_invalid_value)));
        }
        presenter.setListObservableField(listObservable);
    }

    private void showRepaymentCreditAccountAlert() {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(R.string.dialog_repayment_account));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    private void bindIncluded() {
        debitingView = new IncludedCardView();
        targetView = new IncludedCardView();
        ButterKnife.bind(debitingView, sourceCardInclude);
        ButterKnife.bind(targetView, targetCardInclude);
        initIncluded(debitingView);
        initIncluded(targetView);
    }

    private void initIncluded(IncludedCardView includedCardView) {
        SpannableString sb1 = new SpannableString(FieldConverter.getString(R.string.card_balance));
        sb1.setSpan(new UnderlineSpan(), 0, FieldConverter.getString(R.string.card_balance).length(), 0);
        includedCardView.tvCardBalance.setText(sb1);
        if (AppStorage.getBooleanValue(Constants.IS_HIDE_BALANCE, mActivity)) {
            includedCardView.llAmountBalance.setVisibility(VISIBLE);
            includedCardView.llAmountContainer.setVisibility(GONE);
            includedCardView.tvCardBalance.setOnClickListener(view
                    -> crossFade(includedCardView.llAmountContainer, includedCardView.llAmountBalance));
            includedCardView.llAmountContainer.setOnClickListener(view
                    -> crossFade(includedCardView.llAmountBalance, includedCardView.llAmountContainer));
            includedCardView.ivCardBalance.setOnClickListener(view -> showBalanceDialog());
        } else {
            includedCardView.llAmountBalance.setVisibility(GONE);
            includedCardView.llAmountContainer.setVisibility(VISIBLE);
        }
    }

    private void crossFade(LinearLayout showLayout, LinearLayout hideLayout) {
        hideLayout.animate()
                .alpha(0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        hideLayout.setVisibility(View.GONE);
                        showLayout.setAlpha(0f);
                        showLayout.setVisibility(View.VISIBLE);
                        showLayout.animate()
                                .alpha(1f)
                                .setDuration(300)
                                .setListener(null);
                    }
                });
    }

    public void showBalanceDialog() {
        BalanceDialog dialogFragment = BalanceDialog.newInstance();
        dialogFragment.setOnResultListener(new BalanceDialog.OnDialogResultListener() {
            @Override
            public void onResultDialog(int statusCode) {
                if (statusCode == Activity.RESULT_OK) {
                    dialogFragment.dismiss();
                } else {
                    dialogFragment.dismiss();
                }
            }
        });
        dialogFragment.show(getActivity().getSupportFragmentManager(), BalanceDialog.TAG);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        presenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        presenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return mProgressView;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_repayment;
    }

    @OnClick(R.id.fl_source)
    void onSourceClick() {
        presenter.onSourceClicked();
    }

    @OnClick(R.id.fl_target)
    void onTargetClick() {
        presenter.onTargetClicked();
    }

    @Override
    public void openSelectSourceCardScreen(FilterProductData data) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SelectCardOrAccountActivity.FILTER_PRODUCTS, data);
        getScreenCreator().startActivity(this, mActivity, SelectCardOrAccountActivity.class, bundle);
    }

    @Override
    public void openSubsidiariesScreen() {
        getScreenCreator().startActivity(this, mActivity, SubsidiaryActivity.class);
    }

    @Override
    public void showSubsidiary(Subsidiary subsidiary) {
        etSubsidiary.setText(subsidiary.getName());
    }

    @Override
    public void showErrorDialog(CreditAccountDetail creditAccountDetail) {
        Calendar calendar = Calendar.getInstance();
        if (Utils.getMilleFromDate(Utils.formatSelectedDate(creditAccountDetail.getEndDate())) + Utils.get30daysInMilli() >= Utils.getMilleFromDate(calendar.getTime())
                && Utils.getMilleFromDate(Utils.formatSelectedDate(creditAccountDetail.getEndDate())) <= Utils.getMilleFromDate(calendar.getTime())) {
            showDialog(FieldConverter.getString(R.string.dialog_repayment_message_in30days)); //период 30 дней
        } else {
            showDialog(FieldConverter.getString(R.string.dialog_repayment_message_is_impossible));
        }
    }

    @Override
    public void showCardLimitDialog() {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(R.string.limit_transfer_card_dialog_text));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @Override
    public void fieldValidationSuccess(Boolean success) {
        btnNext.enable(success);
    }

    public void showDialog(String message) {
        RepaymentMessageDialog dialogFragment = RepaymentMessageDialog.newInstance(message);
        dialogFragment.setOnResultListener(new RepaymentMessageDialog.OnDialogResultListener() {
            @Override
            public void onResultDialog(int statusCode) {
                if (statusCode == Activity.RESULT_OK) {
                    dialogFragment.dismiss();
                } else {
                    dialogFragment.dismiss();
                }
            }
        });
        dialogFragment.show(getActivity().getSupportFragmentManager(), RepaymentMessageDialog.TAG);
    }

    @Override
    public void openSelectTargetCardScreen(FilterProductData data) {
        Bundle bundle = new Bundle();
//        switch (targetType) {
//            case RequestConstants.CLIENTS_CARD:
//                bundle.putInt(SelectCardOrAccountActivity.PURPOSE_TYPE, SelectCardOrAccountActivity.TARGET_CREDIT_CARD);
//                break;
//            case RequestConstants.ACCOUNT_IN_SAME_BANK:
//                bundle.putInt(SelectCardOrAccountActivity.PURPOSE_TYPE, SelectCardOrAccountActivity.TARGET_ACCOUNT_CREDIT);
//                break;
//        }
        bundle.putParcelable(SelectCardOrAccountActivity.FILTER_PRODUCTS, data);
        getScreenCreator().startActivity(this, mActivity, SelectCardOrAccountActivity.class, bundle);
    }

    @Override
    public void moveToTop() {
        Utils.moveToTopCurrent(mActivity);
    }

    @Override
    public void showSourceProduct(int targetType, BaseProduct sourceProduct) {
        boolean notNullProduct = sourceProduct != null;
        ButterKnife.apply(sourceViews, SHOW_PRODUCT, notNullProduct);
        if (notNullProduct) {
            initProduct(debitingView, sourceProduct);
        }
        switch (targetType) {
            case RequestConstants.CLIENTS_CARD:
                llSubsidiary.setVisibility(GONE);
                subsidiarySeparator.setVisibility(GONE);
                presenter.addSubsidiaryObservable(Utils.getPredicateObservable(etSubsidiary, new CheckUtils.RegexPredicateWithoutError(ValidFields.NOT_EMPTY_REGEX)));
                break;
            case RequestConstants.ACCOUNT_IN_SAME_BANK:
                llSubsidiary.setVisibility(VISIBLE);
                subsidiarySeparator.setVisibility(VISIBLE);
                presenter.removeSubsidiaryObservable();
                break;
        }
    }

    @Override
    public void showTargetProduct(BaseProduct targetProduct) {
        boolean notNullProduct = targetProduct != null;
        flTarget.setTag(notNullProduct);
        ButterKnife.apply(targetViews, SHOW_PRODUCT, notNullProduct);
        if (notNullProduct) {
            if (targetProduct instanceof Card) {
                showAccountNumberField(false);
            }
            initProduct(targetView, targetProduct);
        }
    }

    @Override
    public void showAccountNumberField(boolean show) {
        if (show) {
            presenter.addAccountNumberObservable(Utils.getRegexObservable(etAccountNumber, ValidFields.ACCOUNT_NUMBER_REGEX, getString(R.string.transfer_account_number)));
        } else {
            presenter.removeAccountNumberObservable();
        }
        llAccountNumber.setVisibility(show ? VISIBLE : GONE);
    }

    @Override
    public void showNotValidAmountMessage(int validationValue) {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(validationValue));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @Override
    public void openRepaymentCheckScreen(CheckTransferData checkTransferData, boolean templateMode) {
        if (templateMode) {
            checkTransferData.setNewTemplateName(etTemplateName.getText().toString());
        }
        getScreenCreator().startActivity(this, mActivity, RepaymentCheckActivity.class,
                createTransferCheckBundle(checkTransferData, templateMode), Constants.START_REPAYMENT_CHECK_ACTIVITY);
    }

    @NonNull
    private Bundle createTransferCheckBundle(CheckTransferData checkTransferData, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(RepaymentCheckActivity.CHECK_TRANSFER_DATA, checkTransferData);
        bundle.putBoolean(RepaymentCheckActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    private void initProduct(IncludedCardView includedCardView, BaseProduct baseProduct) {
        includedCardView.tvCardName.setText(baseProduct.getName());
        includedCardView.tvCardNumber.setText(Utils.getMaskedAccountNumber(baseProduct.getNumber()));
        includedCardView.tvCardAmount.setText(Utils.formatMoneyValue(baseProduct.getBalance()));
        includedCardView.tvCardCurrency.setText(FieldConverter.getCurrency(baseProduct.getCurrency()));
        if (baseProduct instanceof Card) {
            includedCardView.ivCardIcon.setImageResource(((Card) baseProduct).getCardBrand());
        } else {
            includedCardView.ivCardIcon.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.btn_transfer_next_step)
    void onNextClick() {
        presenter.onNextClicked(etAmount.getText().toString());
    }

    @OnClick(R.id.et_subsidiary)
    void onSubsidiaryClick() {
        presenter.onSubsidiaryClicked();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.START_REPAYMENT_CHECK_ACTIVITY:
                if (resultCode == Constants.RESULT_LOGOUT) {
                    unAuthorized();
                }
                break;
        }
    }
}
