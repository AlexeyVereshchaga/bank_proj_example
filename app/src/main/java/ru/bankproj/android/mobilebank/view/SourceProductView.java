package ru.bankproj.android.mobilebank.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.utils.storage.AppStorage;

import static ru.bankproj.android.mobilebank.utils.UI.crossFade;

/**
 * Created by j7ars on 22.12.2017.
 */

public class SourceProductView extends RelativeLayout implements View.OnClickListener{

    TextView tvProductName;
    ImageView ivProductIcon;
    TextView tvProductNumber;
    TextView tvProductBalance;
    ImageView ivProductBalance;
    LinearLayout llAmountBalance;
    TextView tvProductAmount;
    TextView tvProductCurrency;
    LinearLayout llAmountContainer;

    private IProductViewCallback mProductViewCallback;

    public SourceProductView(Context context) {
        super(context);
        init(context);
    }

    public SourceProductView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SourceProductView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void setProductViewCallback(IProductViewCallback mProductViewCallback) {
        this.mProductViewCallback = mProductViewCallback;
    }

    private void init(Context context){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_source_product, this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_product_balance:
                if(mProductViewCallback != null){
                    mProductViewCallback.showBalanceDialog();
                }
                break;
            case R.id.tv_product_amount:
            case R.id.tv_product_currency:
                crossFade(llAmountBalance, llAmountContainer);
                break;
            case R.id.tv_product_balance:
                crossFade(llAmountContainer, llAmountBalance);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        tvProductName = (TextView) this.findViewById(R.id.tv_product_name);
        ivProductIcon = (ImageView) this.findViewById(R.id.iv_product_icon);
        tvProductNumber = (TextView) this.findViewById(R.id.tv_product_number);
        tvProductBalance = (TextView) this.findViewById(R.id.tv_product_balance);
        ivProductBalance = (ImageView) this.findViewById(R.id.iv_product_balance);
        llAmountBalance = (LinearLayout) this.findViewById(R.id.ll_amount_balance);
        tvProductAmount = (TextView) this.findViewById(R.id.tv_product_amount);
        tvProductCurrency = (TextView) this.findViewById(R.id.tv_product_currency);
        llAmountContainer = (LinearLayout) this.findViewById(R.id.ll_amount_container);
        ivProductBalance.setOnClickListener(this);
        tvProductAmount.setOnClickListener(this);
        tvProductCurrency.setOnClickListener(this);
        tvProductBalance.setOnClickListener(this);
    }

    public void initProductData(Context context, BaseProduct baseProduct) {
        if (baseProduct != null) {
            SpannableString sb1 = new SpannableString(FieldConverter.getString(R.string.card_balance));
            sb1.setSpan(new UnderlineSpan(), 0, FieldConverter.getString(R.string.card_balance).length(), 0);
            tvProductBalance.setText(sb1);
            if (AppStorage.getBooleanValue(Constants.IS_HIDE_BALANCE, context)) {
                llAmountContainer.setVisibility(GONE);
                llAmountBalance.setAlpha(1f);
                llAmountBalance.setVisibility(VISIBLE);
            } else {
                llAmountBalance.setVisibility(GONE);
                llAmountContainer.setAlpha(1f);
                llAmountContainer.setVisibility(VISIBLE);
            }
            if (baseProduct instanceof Card) {
                Card card = (Card) baseProduct;
                tvProductName.setText(card.getCardName());
                tvProductNumber.setText(Utils.getMaskedPan(card.getNumber()));
                tvProductAmount.setText(Utils.formatMoneyValue(card.getBalance()));
                tvProductCurrency.setText(FieldConverter.getCurrency(card.getCurrency()));
                ivProductIcon.setImageResource(card.getCardBrand());
            } else {
                Account account = (Account) baseProduct;
                ivProductIcon.setVisibility(GONE);
                tvProductName.setText(account.getAccountName());
                tvProductNumber.setText(Utils.getMaskedPan(account.getNumber()));
                tvProductAmount.setText(Utils.formatMoneyValue(account.getBalance()));
                tvProductCurrency.setText(FieldConverter.getCurrency(account.getCurrency()));
            }
        }
    }


    public interface IProductViewCallback{
        void showBalanceDialog();
    }

}
