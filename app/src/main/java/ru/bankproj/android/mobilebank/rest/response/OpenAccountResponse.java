package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.providers.OutputData;

/**
 * Created by maxmobiles on 17.12.2017.
 */

public class OpenAccountResponse extends BaseResponse {

    @SerializedName("transRef")
    private String transRef;
    @SerializedName("fee")
    private double fee;
    @SerializedName("currency")
    private String currency;
    @SerializedName("confirmReq")
    private ConfirmRequestDescriptor confirmRequestDescriptor;
    @SerializedName("details")
    private ArrayList<OutputData> detailList;

    public String getTransRef() {
        return transRef;
    }

    public void setTransRef(String transRef) {
        this.transRef = transRef;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ConfirmRequestDescriptor getConfirmRequestDescriptor() {
        return confirmRequestDescriptor;
    }

    public void setConfirmRequestDescriptor(ConfirmRequestDescriptor confirmRequestDescriptor) {
        this.confirmRequestDescriptor = confirmRequestDescriptor;
    }

    public ArrayList<OutputData> getDetailList() {
        return detailList;
    }

    public void setDetailList(ArrayList<OutputData> detailList) {
        this.detailList = detailList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.transRef);
        dest.writeDouble(this.fee);
        dest.writeString(this.currency);
        dest.writeParcelable(this.confirmRequestDescriptor, flags);
        dest.writeTypedList(this.detailList);
    }

    public OpenAccountResponse() {
    }

    protected OpenAccountResponse(Parcel in) {
        super(in);
        this.transRef = in.readString();
        this.fee = in.readDouble();
        this.currency = in.readString();
        this.confirmRequestDescriptor = in.readParcelable(ConfirmRequestDescriptor.class.getClassLoader());
        this.detailList = in.createTypedArrayList(OutputData.CREATOR);
    }

    public static final Creator<OpenAccountResponse> CREATOR = new Creator<OpenAccountResponse>() {
        @Override
        public OpenAccountResponse createFromParcel(Parcel source) {
            return new OpenAccountResponse(source);
        }

        @Override
        public OpenAccountResponse[] newArray(int size) {
            return new OpenAccountResponse[size];
        }
    };
}
