package ru.bankproj.android.mobilebank.di.qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by j7ars on 25.10.2017.
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ViewContext {
}
