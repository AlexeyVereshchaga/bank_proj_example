package ru.bankproj.android.mobilebank.main.transfer.success.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.dialogs.SendMailDialog;
import ru.bankproj.android.mobilebank.main.template.dialog.TemplateDialog;
import ru.bankproj.android.mobilebank.main.transfer.success.ITransferSuccessContract;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

/**
 * Created by Alexey Vereshchaga on 27.12.17.
 */

public class TransferSuccessFragment extends BaseMvpFragment implements ITransferSuccessContract.View {

    public static final String TRANSFER_EXEC_RESPONSE = "TransferSuccessFragment.TRANSFER_EXEC_RESPONSE";

    @BindView(R.id.cb_template)
    CheckBox cbTemplate;

    @Inject
    ITransferSuccessContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs() {
        if (getArguments() != null) {
            presenter.setArguments(getArguments().getParcelable(TRANSFER_EXEC_RESPONSE)); //getArguments().getString(TRANSFER_PARAMS)
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.onCreate(savedInstanceState);
        Utils.hideKeyboard(mActivity);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        presenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        presenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_transfer_success;
    }

    @OnClick(R.id.btn_transfer_complete)
    void onCompleteClick() {
        if (cbTemplate.isChecked()) {
            showTemplateDialog();
        } else {
            presenter.completeClicked();
        }
    }

    private void showTemplateDialog() {
        TemplateDialog dialogFragment = TemplateDialog.newInstance(TemplateDialog.CREATE, null);
        dialogFragment.setOnResultListener((statusCode, name) -> {
            dialogFragment.dismiss();
            if (statusCode == TemplateDialog.CREATE) {
                presenter.saveTemplate(name);
            }
        });
        dialogFragment.show(getActivity().getSupportFragmentManager(), TemplateDialog.TAG);
    }

    @OnClick(R.id.btn_transfer_send_receipt)
    void onSendReceipt() {
        presenter.sendReceiptClicked();
    }

    @Override
    public void showSendEmailDialog() {
        SendMailDialog dialogFragment = SendMailDialog.newInstance();
        dialogFragment.setOnResultListener(new SendMailDialog.OnDialogResultListener() {
            @Override
            public void onResultDialog(int statusCode, String email) {
                if (statusCode == Activity.RESULT_OK) {
                    dialogFragment.dismissAllowingStateLoss();
                    presenter.sendMail(email);
                } else {
                    dialogFragment.dismiss();
                }
            }
        });
        dialogFragment.show(getActivity().getSupportFragmentManager(), SendMailDialog.TAG);
    }

    @Override
    public void showTemplateSavedMessage() {
        Toast.makeText(mActivity, R.string.template_success_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMailSuccessMessage() {
        Toast.makeText(mActivity, R.string.mail_success_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showCreateTemplate(boolean show) {
        cbTemplate.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}