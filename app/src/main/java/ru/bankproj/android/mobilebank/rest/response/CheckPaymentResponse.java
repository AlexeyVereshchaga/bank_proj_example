package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.providers.OutputData;

/**
 * Created by arsen on 23.12.17.
 */
public class CheckPaymentResponse extends BaseResponse {
    @SerializedName("transRef")
    private String transRef;
    @SerializedName("fee")
    private float fee;
    @SerializedName("currency")
    private String currency;
    @SerializedName("confirmReq")
    private ConfirmRequestDescriptor confirmReq;
    @SerializedName("details")
    private ArrayList<OutputData> details;
    private String confirmationStrategy;

    public String getTransRef() {
        return transRef;
    }

    public void setTransRef(String transRef) {
        this.transRef = transRef;
    }

    public float getFee() {
        return fee;
    }

    public void setFee(float fee) {
        this.fee = fee;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ConfirmRequestDescriptor getConfirmReq() {
        return confirmReq;
    }

    public void setConfirmReq(ConfirmRequestDescriptor confirmReq) {
        this.confirmReq = confirmReq;
    }

    public ArrayList<OutputData> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<OutputData> details) {
        this.details = details;
    }

    public String getConfirmationStrategy() {
        return confirmationStrategy;
    }

    public void setConfirmationStrategy(String confirmationStrategy) {
        this.confirmationStrategy = confirmationStrategy;
    }

    public static Creator<CheckPaymentResponse> getCREATOR() {
        return CREATOR;
    }

    public CheckPaymentResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.transRef);
        dest.writeFloat(this.fee);
        dest.writeString(this.currency);
        dest.writeParcelable(this.confirmReq, flags);
        dest.writeTypedList(this.details);
        dest.writeString(this.confirmationStrategy);
    }

    protected CheckPaymentResponse(Parcel in) {
        super(in);
        this.transRef = in.readString();
        this.fee = in.readFloat();
        this.currency = in.readString();
        this.confirmReq = in.readParcelable(ConfirmRequestDescriptor.class.getClassLoader());
        this.details = in.createTypedArrayList(OutputData.CREATOR);
        this.confirmationStrategy = in.readString();
    }

    public static final Creator<CheckPaymentResponse> CREATOR = new Creator<CheckPaymentResponse>() {
        @Override
        public CheckPaymentResponse createFromParcel(Parcel source) {
            return new CheckPaymentResponse(source);
        }

        @Override
        public CheckPaymentResponse[] newArray(int size) {
            return new CheckPaymentResponse[size];
        }
    };
}
