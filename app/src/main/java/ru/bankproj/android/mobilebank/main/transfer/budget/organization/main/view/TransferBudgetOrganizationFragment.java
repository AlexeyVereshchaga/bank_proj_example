package ru.bankproj.android.mobilebank.main.transfer.budget.organization.main.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.app.ValidFields;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SelectBudgetOrgData;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.dialogs.InfoDialog;
import ru.bankproj.android.mobilebank.main.payment.selectcardoraccount.view.SelectCardOrAccountActivity;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.main.ITransferBudgetOrganizationContract;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.view.SelectBudgetOrgDataActivity;
import ru.bankproj.android.mobilebank.main.transfer.check.view.TransferCheckActivity;
import ru.bankproj.android.mobilebank.utils.AmountDigitsInputFilter;
import ru.bankproj.android.mobilebank.utils.CheckUtils;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.view.EnableButton;
import ru.bankproj.android.mobilebank.view.ErrorLabelContainer;
import ru.bankproj.android.mobilebank.view.SourceProductView;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static ru.bankproj.android.mobilebank.utils.UI.crossFade;
import static ru.bankproj.android.mobilebank.utils.Utils.initUinValidation;

/**
 * Created by j7ars on 28.12.2017.
 */

public class TransferBudgetOrganizationFragment extends BaseMvpFragment implements ITransferBudgetOrganizationContract.View, DatePickerDialog.OnDateSetListener {

    public static final String SOURCE_PRODUCT = "TransferIndividualPersonFragment.SOURCE_PRODUCT";
    public static final String TEMPLATE_MODE = "TransferIndividualPersonFragment.TEMPLATE_MODE";

    private static final String DATE_PICKER_DIALOG = "TransferBudgetOrganizationFragment.DATE_PICKER_DIALOG";

    @BindView(R.id.ll_source_container)
    LinearLayout llSourceContainer;
    @BindView(R.id.rl_select_product)
    RelativeLayout rlSelectProduct;
    @BindView(R.id.v_source_product)
    SourceProductView sourceProductView;
    @BindView(R.id.el_payer_inn)
    ErrorLabelContainer elCustomerInn;
    @BindView(R.id.et_payer_inn)
    EditText etCustomerInn;
    @BindView(R.id.el_recipient_name)
    ErrorLabelContainer elReceiverName;
    @BindView(R.id.et_recipient_name)
    EditText etReceiverName;
    @BindView(R.id.el_recipient_inn)
    ErrorLabelContainer elReceiverInn;
    @BindView(R.id.et_recipient_inn)
    EditText etReceiverInn;
    @BindView(R.id.el_kpp)
    ErrorLabelContainer elReceiverKpp;
    @BindView(R.id.et_kpp)
    EditText etReceiverKpp;
    @BindView(R.id.el_account_number_receiver)
    ErrorLabelContainer elReceiverAccount;
    @BindView(R.id.et_account_number_receiver)
    EditText etReceiverAccount;
    @BindView(R.id.el_bik)
    ErrorLabelContainer elReceiverBik;
    @BindView(R.id.et_bik)
    EditText etReceiverBik;
    @BindView(R.id.el_transfer_target)
    ErrorLabelContainer elTransferTarget;
    @BindView(R.id.et_transfer_target)
    EditText etTransferTarget;
    @BindView(R.id.el_uin)
    ErrorLabelContainer elUin;
    @BindView(R.id.et_uin)
    EditText etUin;
    @BindView(R.id.rl_transfer_payer_status)
    RelativeLayout rlTransferPayerStatus;
    @BindView(R.id.rl_transfer_type)
    RelativeLayout rlTransferType;
    @BindView(R.id.rl_transfer_reason)
    RelativeLayout rlTransferReason;
    @BindView(R.id.el_kbk)
    ErrorLabelContainer elKbk;
    @BindView(R.id.et_kbk)
    EditText etKbk;
    @BindView(R.id.el_oktm)
    ErrorLabelContainer elOktm;
    @BindView(R.id.et_oktm)
    EditText etOktm;
    @BindView(R.id.et_document_date)
    EditText etDocumentDate;
    @BindView(R.id.rl_transfer_document_type)
    RelativeLayout rlTransferDocumentType;
    @BindView(R.id.el_document_number)
    ErrorLabelContainer elDocumentNumber;
    @BindView(R.id.et_document_number)
    EditText etDocumentNumber;
    @BindView(R.id.et_code_custom_organ)
    EditText etCustCode;
    @BindView(R.id.rl_transfer_nalog_period1)
    RelativeLayout rlTransferNalogPeriod1;
    @BindView(R.id.et_nalog_period2)
    EditText etTaxPeriodParam2;
    @BindView(R.id.et_nalog_period3)
    EditText etTaxPeriodParam3;
    @BindView(R.id.et_passport_deal)
    EditText etPassportDeal;
    @BindView(R.id.et_limit_assignment)
    EditText etAmount;
    @BindView(R.id.tv_currency_sign)
    TextView tvCurrencySign;
    @BindView(R.id.et_transfer_payer_status)
    EditText etTransferPayerStatus;
    @BindView(R.id.et_transfer_type)
    EditText etTransferType;
    @BindView(R.id.et_transfer_reason)
    EditText etTransferReason;
    @BindView(R.id.et_document_type)
    EditText etDocumentType;
    @BindView(R.id.et_nalog_period)
    EditText etNalogPeriod;
    @BindView(R.id.btn_further)
    EnableButton btnFurther;
    @BindView(R.id.tv_select_card_text)
    TextView tvSelectCardText;
    //Template views
    @BindView(R.id.ll_template_name_container)
    LinearLayout llTemplateNameContainer;
    @BindView(R.id.el_template_name)
    ErrorLabelContainer elTemplateName;
    @BindView(R.id.et_template_name)
    EditText etTemplateName;

    @Inject
    ITransferBudgetOrganizationContract.Presenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs() {
        if (getArguments() != null) {
            mPresenter.setArguments(getArguments().getParcelable(SOURCE_PRODUCT),
                    getArguments().getBoolean(TEMPLATE_MODE));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.onCreate(savedInstanceState);
        tvSelectCardText.setText(FieldConverter.getString(R.string.debit_select_card_from_title));
        etAmount.setFilters(new InputFilter[]{new AmountDigitsInputFilter()});
    }

    @Override
    public void initView(boolean templateMode) {
        llTemplateNameContainer.setVisibility(templateMode ? VISIBLE : GONE);

        List<Observable<Boolean>> listObservable = new ArrayList<>();
        if (templateMode) {
            listObservable.add(Utils.getRegexObservable(etTemplateName, ValidFields.TEMPLATE_NAME_REGEX, getString(R.string.transfer_invalid_value)));
        }

        listObservable.add(Utils.getRegexObservable(etReceiverAccount, ValidFields.ACCOUNT_NUMBER_REGEX, getString(R.string.transfer_error_account)));
        listObservable.add(Utils.getRegexObservable(etReceiverBik, ValidFields.BIC_REGEX, getString(R.string.transfer_error_bic)));
        listObservable.add(Utils.getMultiplePredicatesObservable(etTransferTarget, new ArrayList<CheckUtils.CheckEditTextPredicate>() {
            {
                add(new CheckUtils.RegexPredicate(ValidFields.TRANSFER_TARGET_REGEX, getString(R.string.transfer_invalid_value)));
                add(new CheckUtils.RegexPredicate(ValidFields.SPECIFY_TRANSFER_TARGET_REGEX, getString(R.string.transfer_invalid_value)));
            }
        }));
        listObservable.add(Utils.getRegexObservable(etReceiverName, ValidFields.TARGET_NAME_REGEX, getString(R.string.transfer_invalid_value)));
        listObservable.add(Utils.getMultiplePredicatesObservable(etCustomerInn, new ArrayList<CheckUtils.CheckEditTextPredicate>() {
            {
                add(new CheckUtils.RegexPredicate(ValidFields.BUDGET_INN_REGEX, getString(R.string.transfer_invalid_value)));
                add(new CheckUtils.IndividualPersonInnPredicate());
            }
        }));
        listObservable.add(Utils.getMultiplePredicatesObservable(etReceiverKpp, new ArrayList<CheckUtils.CheckEditTextPredicate>() {
            {
                add(new CheckUtils.RegexPredicate(ValidFields.KPP_REGEX, getString(R.string.transfer_invalid_value)));
                add(new CheckUtils.RegexPredicate(ValidFields.SPECIFY_KPP_REGEX, getString(R.string.transfer_invalid_value)));
            }
        }));
        listObservable.add(Utils.getMultiplePredicatesObservable(etReceiverInn, new ArrayList<CheckUtils.CheckEditTextPredicate>() {
            {
                add(new CheckUtils.RegexPredicate(ValidFields.LEGAL_PERSONS_INN_REGEX, getString(R.string.transfer_error_target_inn)));
                add(new CheckUtils.LegalPersonInnPredicate());
            }
        }));
        listObservable.add(Utils.getRegexObservable(etKbk, ValidFields.KBK_REGEX, getString(R.string.transfer_error_account)));
        listObservable.add(Utils.getMultiplePredicatesObservable(etOktm, new ArrayList<CheckUtils.CheckEditTextPredicate>() {
            {
                add(new CheckUtils.RegexPredicate(ValidFields.OKTMO_REGEX, getString(R.string.transfer_error_oktmo)));
                add(new CheckUtils.RegexPredicate(ValidFields.SPECIFY_OKTMO_REGEX, getString(R.string.transfer_invalid_value)));
            }
        }));
        listObservable.add(Utils.getRegexObservable(etDocumentNumber, ValidFields.DOCUMENT_NUMBER, getString(R.string.transfer_invalid_value)));

        listObservable.add(Utils.getPredicateObservable(etTransferPayerStatus, new CheckUtils.RegexPredicateWithoutError(ValidFields.NOT_EMPTY_REGEX)));
        listObservable.add(Utils.getPredicateObservable(etDocumentDate, new CheckUtils.RegexPredicateWithoutError(ValidFields.NOT_EMPTY_REGEX)));
        listObservable.add(Utils.getPredicateObservable(etDocumentType, new CheckUtils.RegexPredicateWithoutError(ValidFields.NOT_EMPTY_REGEX)));
        listObservable.add(Utils.getPredicateObservable(etTransferType, new CheckUtils.RegexPredicateWithoutError(ValidFields.NOT_EMPTY_REGEX)));
        listObservable.add(Utils.getPredicateObservable(etNalogPeriod, new CheckUtils.RegexPredicateWithoutError(ValidFields.NOT_EMPTY_REGEX)));
        listObservable.add(Utils.getPredicateObservable(etTransferReason, new CheckUtils.RegexPredicateWithoutError(ValidFields.NOT_EMPTY_REGEX)));

        initUinValidation(etUin);//
        etReceiverAccount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0 && !(editable.charAt(0) == '3' || editable.charAt(0) == '4')) {
                    editable.replace(0, 1, "");
                }
            }
        });
        etReceiverName.setOnFocusChangeListener((view, hasFocus) -> {
            if (!hasFocus) {
                etReceiverName.setText(etReceiverName.getText().toString().trim());
            }
        });

        mPresenter.setListObservableField(listObservable);
    }

    @Override
    public void initProductData(BaseProduct baseProduct) {
        if (baseProduct != null) {
            if (sourceProductView.getVisibility() == GONE) {
                crossFade(sourceProductView, rlSelectProduct);
            }
            sourceProductView.initProductData(mActivity, baseProduct);
        } else {
            crossFade(rlSelectProduct, sourceProductView);
        }
    }

    @OnClick({R.id.rl_select_product, R.id.v_source_product, R.id.rl_transfer_payer_status, R.id.rl_transfer_type, R.id.rl_transfer_reason, R.id.rl_transfer_document_type, R.id.btn_further, R.id.rl_transfer_nalog_period1, R.id.et_document_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_select_product:
            case R.id.v_source_product:
                FilterProductData data = new FilterProductData();
                data.setFilterMode(FilterProductData.CARDS);
                Bundle bundle = new Bundle();
                bundle.putParcelable(SelectCardOrAccountActivity.FILTER_PRODUCTS, data);
                getScreenCreator().startActivity(this, mActivity, SelectCardOrAccountActivity.class, bundle, Constants.START_SELECT_PRODUCT_ACTIVITY);
                break;
            case R.id.rl_transfer_payer_status:
                getScreenCreator().startActivity(this, mActivity, SelectBudgetOrgDataActivity.class, getSelectBudgetOrgDataBundle(SelectBudgetOrgData.PAYER_STATUS), Constants.START_SELECT_BUDGET_DATA_ACTIVITY);
                break;
            case R.id.rl_transfer_nalog_period1:
                getScreenCreator().startActivity(this, mActivity, SelectBudgetOrgDataActivity.class, getSelectBudgetOrgDataBundle(SelectBudgetOrgData.NALOG_PERIOD), Constants.START_SELECT_BUDGET_DATA_ACTIVITY);
                break;
            case R.id.rl_transfer_type:
                getScreenCreator().startActivity(this, mActivity, SelectBudgetOrgDataActivity.class, getSelectBudgetOrgDataBundle(SelectBudgetOrgData.TRANSFER_TYPE), Constants.START_SELECT_BUDGET_DATA_ACTIVITY);
                break;
            case R.id.rl_transfer_reason:
                getScreenCreator().startActivity(this, mActivity, SelectBudgetOrgDataActivity.class, getSelectBudgetOrgDataBundle(SelectBudgetOrgData.TRANSFER_REASON), Constants.START_SELECT_BUDGET_DATA_ACTIVITY);
                break;
            case R.id.rl_transfer_document_type:
                getScreenCreator().startActivity(this, mActivity, SelectBudgetOrgDataActivity.class, getSelectBudgetOrgDataBundle(SelectBudgetOrgData.DOCUMENT_TYPE), Constants.START_SELECT_BUDGET_DATA_ACTIVITY);
                break;
            case R.id.et_document_date:
                Calendar initDate = Calendar.getInstance();
                DatePickerDialog dataPickerData = DatePickerDialog.newInstance(this,
                        initDate.get(Calendar.YEAR),
                        initDate.get(Calendar.MONTH),
                        initDate.get(Calendar.DAY_OF_MONTH)
                );
                dataPickerData.setOkText(FieldConverter.getString(R.string.ok));
                dataPickerData.setCancelText(FieldConverter.getString(R.string.cancel));
                dataPickerData.setAccentColor(FieldConverter.getColor(R.color.colorAccent));
                dataPickerData.setVersion(DatePickerDialog.Version.VERSION_1);
                dataPickerData.dismissOnPause(true);
                dataPickerData.show(mActivity.getFragmentManager(), DATE_PICKER_DIALOG);
                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        mPresenter.setDocumentDate(calendar.getTime());
    }

    @Override
    public void showDate(String date) {
        etDocumentDate.setText(date);
    }

    private Bundle getSelectBudgetOrgDataBundle(int type) {
        Bundle bundle = new Bundle();
        bundle.putInt(SelectBudgetOrgDataActivity.SELECT_TYPE, type);
        return bundle;
    }

    @Override
    public void setDocumentType(String documentType) {
        etDocumentType.setText(documentType);
    }

    @Override
    public void setTransferType(String transferType) {
        etTransferType.setText(transferType);
    }

    @Override
    public void setNalogPeriod(String nalogPeriod) {
        etNalogPeriod.setText(nalogPeriod);
    }

    @Override
    public void setTransferReason(String transferReason) {
        etTransferReason.setText(transferReason);
    }

    @Override
    public void setPayerStatus(String payerStatus) {
        etTransferPayerStatus.setText(payerStatus);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_transfer_budget_organization;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.START_SELECT_PRODUCT_ACTIVITY:
                if (resultCode == Constants.RESULT_SELECT_PRODUCT_ACTIVITY) {
                    if (data.getExtras() != null) {
                        if (data.getExtras().getParcelable(Constants.SELECTED_CARD_CONTENT) != null) {
                            mPresenter.setSelectedProduct(data.getExtras().getParcelable(Constants.SELECTED_CARD_CONTENT));
                        } else {
                            mPresenter.setSelectedProduct(data.getExtras().getParcelable(Constants.SELECTED_ACCOUNT_CONTENT));
                        }
                    }
                } else if (resultCode == Constants.RESULT_LOGOUT) {
                    unAuthorized();
                }
                break;
            case Constants.START_SELECT_BUDGET_DATA_ACTIVITY:
                if (resultCode == Constants.RESULT_SELECT_BUDGET_DATA_ACTIVITY) {
                    if (data.getExtras() != null) {
                        if (data.getExtras().getParcelable(Constants.SELECTED_BUDGET_ORG_DATA) != null) {
                            SelectBudgetOrgData selectBudgetOrgData = data.getExtras().getParcelable(Constants.SELECTED_BUDGET_ORG_DATA);
                            mPresenter.setSelectedBudgetOrgData(selectBudgetOrgData);
                        }
                    }
                } else if (resultCode == Constants.RESULT_LOGOUT) {
                    unAuthorized();
                }
                break;
            case Constants.START_TRANSFER_CHECK_ACTIVITY:
                if (resultCode == Constants.RESULT_LOGOUT) {
                    unAuthorized();
                }
                break;
        }
    }

    @Override
    public void openTransferCheckScreen(CheckTransferData checkTransferData, boolean templateMode) {
        if (templateMode) {
            checkTransferData.setNewTemplateName(etTemplateName.getText().toString());
        }
        getScreenCreator().startActivity(this, mActivity, TransferCheckActivity.class,
                createTransferCheckBundle(checkTransferData, templateMode), Constants.START_TRANSFER_CHECK_ACTIVITY);
    }

    @Override
    public void showAccountNumberError(boolean showError) {
        if (showError) {
            elReceiverAccount.setError(getString(R.string.transfer_error_bic_and_account));
            elReceiverBik.setError(getString(R.string.transfer_error_bic_and_account));
        } else {
            elReceiverAccount.clearError();
            elReceiverBik.clearError();
        }
    }

    @Override
    public void showCardLimitDialog() {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(R.string.limit_transfer_card_dialog_text));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @Override
    public void fieldValidationSuccess(Boolean success) {
        btnFurther.enable(success);
    }

    @Override
    public void showNotValidAmountMessage(int validationValue) {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(validationValue));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @NonNull
    private Bundle createTransferCheckBundle(CheckTransferData checkTransferData, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferCheckActivity.CHECK_TRANSFER_DATA, checkTransferData);
        bundle.putBoolean(TransferCheckActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    @OnClick(R.id.btn_further)
    void onNextClick() {
        mPresenter.onNextClicked(etAmount.getText().toString(), etCustomerInn.getText().toString(), etReceiverName.getText().toString(), etReceiverInn.getText().toString(),
                etReceiverKpp.getText().toString(), etReceiverAccount.getText().toString(), etReceiverBik.getText().toString(), etTransferTarget.getText().toString()/*param.Ground*/,
                etUin.getText().toString(), etKbk.getText().toString(), etOktm.getText().toString(), etDocumentDate.getText().toString(), etDocumentNumber.getText().toString(),
                etCustCode.getText().toString(), etTaxPeriodParam2.getText().toString(), etTaxPeriodParam3.getText().toString(), etPassportDeal.getText().toString());
    }
}
