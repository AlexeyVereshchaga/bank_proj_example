package ru.bankproj.android.mobilebank.view.paymentfields;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.ValidFields;
import ru.bankproj.android.mobilebank.dataclasses.providers.PaymentParameter;
import ru.bankproj.android.mobilebank.view.paymentfields.base.BaseField;

/**
 * Created by Alexey Vereshchaga on 07.03.18.
 */

public class SelectField extends BaseField implements View.OnClickListener {

    private LinearLayout llRootView;
    private TextView tvLabel;
    private OnFieldClickListener onFieldClickListener;
    private PaymentParameter paymentParameter;

    private String value;

    public SelectField(Context context) {
        super(context);
    }

    public void setOnFieldClickListener(OnFieldClickListener listener) {
        onFieldClickListener = listener;
    }

    @Override
    public View getView() {
        return llRootView;
    }

    private Observable<Boolean> getEmptyFieldWatcherObservable(@NonNull final TextView textView) {
        final PublishSubject<Boolean> subject = PublishSubject.create();
        textView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                subject.onNext(ValidFields.isNotEmptyField(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        return subject;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    public void setSelectedField(String label) {
        tvLabel.setText(label);
    }

    public String getValue() {
        return value;
    }

    @Override
    public void enableView(boolean isEnable) {
        llRootView.setEnabled(isEnable);
    }

    @Override
    protected void initView() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        llRootView = (LinearLayout) inflater.inflate(R.layout.view_type_select_field, null, false);
        tvLabel = llRootView.findViewById(R.id.tv_select_value);
        llRootView.setOnClickListener(this);
    }

    @Override
    public void setPaymentParameter(PaymentParameter paymentParameter) {
        this.paymentParameter = paymentParameter;
    }

    @Override
    public String getPaymentParameterId() {
        return paymentParameter != null ? paymentParameter.getId() : "";
    }


    @Override
    public void onClick(View view) {
        if (onFieldClickListener != null) {
            onFieldClickListener.onFieldClick(getPaymentParameterId());
        }
    }

    @Override
    public Observable<Boolean> getObservable() {
        return getEmptyFieldWatcherObservable(tvLabel);
    }

    public interface OnFieldClickListener {
        void onFieldClick(String parameterId);
    }
}

