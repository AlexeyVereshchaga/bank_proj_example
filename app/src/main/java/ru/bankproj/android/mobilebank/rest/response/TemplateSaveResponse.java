package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;

/**
 * TemplateSaveResponse.java 28.12.2017
 * Class description
 *
 * @author Alex.
 */

public class TemplateSaveResponse extends BaseResponse {
    @SerializedName("uid")
    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.uid);
    }

    public TemplateSaveResponse() {
    }

    protected TemplateSaveResponse(Parcel in) {
        super(in);
        this.uid = in.readString();
    }

    public static final Creator<TemplateSaveResponse> CREATOR = new Creator<TemplateSaveResponse>() {
        @Override
        public TemplateSaveResponse createFromParcel(Parcel source) {
            return new TemplateSaveResponse(source);
        }

        @Override
        public TemplateSaveResponse[] newArray(int size) {
            return new TemplateSaveResponse[size];
        }
    };
}
