package ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import javax.inject.Inject;

import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SelectBudgetOrgData;
import ru.bankproj.android.mobilebank.di.scope.ConfigPersistent;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.ISelectBudgetOrgDataContract;

/**
 * Created by j7ars on 28.12.2017.
 */
@ConfigPersistent
public class SelectBudgetOrgDataPresenter extends BaseEventBusPresenter<ISelectBudgetOrgDataContract.View> implements ISelectBudgetOrgDataContract.Presenter {

    private static final String SAVE_SELECT_TYPE = "SelectBudgetOrgDataPresenter.SAVE_SELECT_TYPE";
    private static final String SAVE_SELECT_BUDGET_ORG_DATA_LIST = "SelectBudgetOrgDataPresenter.SAVE_SELECT_BUDGET_ORG_DATA_LIST";

    private int mSelectType;
    private ArrayList<SelectBudgetOrgData> mSelectBudgetOrgDataList;

    @Inject
    public SelectBudgetOrgDataPresenter(){
        mSelectBudgetOrgDataList = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            restoreInstanceState(savedInstanceState);
        } else{
            initView();
        }
    }

    @Override
    public void setArguments(Object... params) {
        if(params != null){
            mSelectType = (int) params[0];
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putInt(SAVE_SELECT_TYPE, mSelectType);
        outState.putParcelableArrayList(SAVE_SELECT_BUDGET_ORG_DATA_LIST, mSelectBudgetOrgDataList);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        mSelectType = savedInstanceState.getInt(SAVE_SELECT_TYPE);
        mSelectBudgetOrgDataList = savedInstanceState.getParcelableArrayList(SAVE_SELECT_BUDGET_ORG_DATA_LIST);
    }

    private void initView(){
        switch (mSelectType){
            case SelectBudgetOrgData.DOCUMENT_TYPE:
                mSelectBudgetOrgDataList = mDataManager.getDocumentType();
                break;
            case SelectBudgetOrgData.TRANSFER_TYPE:
                mSelectBudgetOrgDataList = mDataManager.getTransferType();
                break;
            case SelectBudgetOrgData.NALOG_PERIOD:
                mSelectBudgetOrgDataList = mDataManager.getNalogPeriod();
                break;
            case SelectBudgetOrgData.PAYER_STATUS:
                mSelectBudgetOrgDataList = mDataManager.getPayerStatus();
                break;
            case SelectBudgetOrgData.TRANSFER_REASON:
                mSelectBudgetOrgDataList = mDataManager.getReasonTransfer();
                break;
        }
        getMvpView().initData(mSelectBudgetOrgDataList);
    }

    @Override
    public void budgetOrgDataSelectItem(int position) {
        SelectBudgetOrgData selectBudgetOrgData = mSelectBudgetOrgDataList.get(position);
        if(selectBudgetOrgData != null){
            selectBudgetOrgData.setType(mSelectType);
            getMvpView().setResult(selectBudgetOrgData);
        }
    }

    @Override
    public void onEvent(Event event) {

    }
}
