package ru.bankproj.android.mobilebank.view.desktoppager;

import android.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.util.SparseBooleanArray;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by j7ars on 31.10.2017.
 */

public abstract class ArrayPagerAdapter<T> extends PagerAdapter {
    private ArrayList<IdentifiedItem<T>> items;
    private final Object lock = new Object();
    private final IdentifiedItemFactory<T> identifiedItemFactory;

    protected List<WeakReference<Fragment>> fragmentList = new ArrayList<>();

    public ArrayPagerAdapter() {
        this(new ArrayList<T>());
    }

    @SafeVarargs
    public ArrayPagerAdapter(T... items) {
        this(new ArrayList<>(Arrays.asList(items)));
    }

    public ArrayPagerAdapter(List<T> items) {
        identifiedItemFactory = new IdentifiedItemFactory<>(0);
        this.items = identifiedItemFactory.createList(items);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return items.get(position);
    }

    public void add(T item) {
        synchronized (lock) {
            items.add(identifiedItemFactory.create(item));
        }
        notifyDataSetChanged();
    }

    public void add(int index, T item) {
        synchronized (lock) {
            items.add(index, identifiedItemFactory.create(item));
        }
        itemPositionChangeChecked = new SparseBooleanArray(this.items.size());
        notifyDataSetChanged();
    }

    public void addAll(T... items) {
        synchronized (lock) {
            this.items.addAll(identifiedItemFactory.createList(items));
        }
        itemPositionChangeChecked = new SparseBooleanArray(this.items.size());
        notifyDataSetChanged();
    }

    public void addAll(List<T> items) {
        synchronized (lock) {
            this.items.addAll(identifiedItemFactory.createList(items));
        }
        itemPositionChangeChecked = new SparseBooleanArray(this.items.size());
        notifyDataSetChanged();
    }

    public void remove(int position) throws IndexOutOfBoundsException {
        synchronized (lock) {
            items.remove(position);
        }
        itemPositionChangeChecked = new SparseBooleanArray(items.size());
        notifyDataSetChanged();
    }

    public void clear() {
        synchronized (lock) {
            items.clear();
        }
        itemPositionChangeChecked = new SparseBooleanArray(items.size());
        notifyDataSetChanged();
    }

    public T getItem(int position) {
        return items.get(position).item;
    }

    public int getPosition(T item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).item == item) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int getItemPosition(Object item) {
        if (!items.contains(item)) {
            return POSITION_NONE;
        } else if (itemPositionChangeChecked.size() != items.size()) {
            int newPos = items.indexOf(item);
            int ret = itemPositionChangeChecked.get(newPos) ? POSITION_UNCHANGED : newPos;
            itemPositionChangeChecked.put(newPos, true);
            return ret;
        } else {
            return POSITION_UNCHANGED;
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    IdentifiedItem<T> getItemWithId(int position) {
        return items.get(position);
    }

    ArrayList<IdentifiedItem<T>> getItemsWithId() {
        return items;
    }

    ArrayList<T> getItems() {
        ArrayList<T> list = new ArrayList<>();
        for (IdentifiedItem<T> item : items) {
            list.add(item.item);
        }
        return list;
    }

    void setItems(List<T> items) {
        this.items = identifiedItemFactory.createList(items);
        notifyDataSetChanged();
    }

    private SparseBooleanArray itemPositionChangeChecked = new SparseBooleanArray();

    static class IdentifiedItemFactory<T> {
        private long lastId;

        IdentifiedItemFactory(long firstId) {
            lastId = firstId;
        }

        IdentifiedItem<T> create(T item) {
            return new IdentifiedItem(lastId++, item);
        }

        ArrayList<IdentifiedItem<T>> createList(List<T> items) {
            ArrayList<IdentifiedItem<T>> list = new ArrayList();
            for (T item : items) {
                list.add(create(item));
            }
            return list;
        }

        @SafeVarargs
        final ArrayList createList(T... items) {
            return createList(new ArrayList<>(Arrays.asList(items)));
        }
    }

    static class IdentifiedItem<T> {
        long id;
        T item;

        public IdentifiedItem(long id, T item) {
            this.id = id;
            this.item = item;
        }

    }
}
