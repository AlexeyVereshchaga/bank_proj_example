package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.attachdevice.AttachDevice;

/**
 * Created by maxmobiles on 10.12.2017.
 */

public class AttachedDeviceResponse extends BaseResponse {

    @SerializedName("devices")
    private ArrayList<AttachDevice> mAttachedDeviceList;

    public ArrayList<AttachDevice> getAttachedDeviceList() {
        return mAttachedDeviceList;
    }

    public void setAttachedDeviceList(ArrayList<AttachDevice> mAttachedDeviceList) {
        this.mAttachedDeviceList = mAttachedDeviceList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.mAttachedDeviceList);
    }

    public AttachedDeviceResponse() {
    }

    protected AttachedDeviceResponse(Parcel in) {
        super(in);
        this.mAttachedDeviceList = in.createTypedArrayList(AttachDevice.CREATOR);
    }

    public static final Creator<AttachedDeviceResponse> CREATOR = new Creator<AttachedDeviceResponse>() {
        @Override
        public AttachedDeviceResponse createFromParcel(Parcel source) {
            return new AttachedDeviceResponse(source);
        }

        @Override
        public AttachedDeviceResponse[] newArray(int size) {
            return new AttachedDeviceResponse[size];
        }
    };
}
