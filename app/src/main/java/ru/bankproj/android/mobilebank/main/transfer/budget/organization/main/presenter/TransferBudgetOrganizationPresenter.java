package ru.bankproj.android.mobilebank.main.transfer.budget.organization.main.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Response;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Action;
import ru.bankproj.android.mobilebank.base.event.Event;
import ru.bankproj.android.mobilebank.base.event.EventFailRequest;
import ru.bankproj.android.mobilebank.base.event.EventSuccessRequest;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.base.mvp.BaseEventBusPresenter;
import ru.bankproj.android.mobilebank.base.request.BaseRequestController;
import ru.bankproj.android.mobilebank.base.response.ErrorKind;
import ru.bankproj.android.mobilebank.base.response.IBaseResponseCallback;
import ru.bankproj.android.mobilebank.base.response.ResponseHandler;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.product.CardLimit;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SelectBudgetOrgData;
import ru.bankproj.android.mobilebank.di.scope.ConfigPersistent;
import ru.bankproj.android.mobilebank.main.transfer.budget.organization.main.ITransferBudgetOrganizationContract;
import ru.bankproj.android.mobilebank.managers.RequestManager;
import ru.bankproj.android.mobilebank.rest.RequestParams;
import ru.bankproj.android.mobilebank.rest.response.CardLimitResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferResponse;
import ru.bankproj.android.mobilebank.utils.RequestConstants;
import ru.bankproj.android.mobilebank.utils.Utils;

import static ru.bankproj.android.mobilebank.app.Constants.TAG;

/**
 * Created by j7ars on 28.12.2017.
 */
@ConfigPersistent
public class TransferBudgetOrganizationPresenter extends BaseEventBusPresenter<ITransferBudgetOrganizationContract.View>
        implements ITransferBudgetOrganizationContract.Presenter, IBaseResponseCallback {

    private static final String SAVE_SELECTED_PRODUCT = "TransferBudgetOrganizationPresenter.SAVE_SELECTED_PRODUCT";
    private static final String SAVE_SELECTED_PAYER_STATUS = "TransferBudgetOrganizationPresenter.SAVE_SELECTED_PAYER_STATUS";
    private static final String SAVE_SELECTED_TRANSFER_STATUS = "TransferBudgetOrganizationPresenter.SAVE_SELECTED_TRANSFER_STATUS";
    private static final String SAVE_SELECTED_TRANSFER_REASON = "TransferBudgetOrganizationPresenter.SAVE_SELECTED_TRANSFER_REASON";
    private static final String SAVE_SELECTED_DOCUMENT_TYPE = "TransferBudgetOrganizationPresenter.SAVE_SELECTED_DOCUMENT_TYPE";
    private static final String SAVE_SELECTED_NALOG_PERIOD = "TransferBudgetOrganizationPresenter.SAVE_SELECTED_NALOG_PERIOD";
    private static final String PARAM_TYPE = "budget";
    private static final String PARAM_ID = "2";
    private static final String AMOUNT = "TransferIndividualPersonPresenter.AMOUNT";
    private static final String TRANSFER_RESPONSE = "TransferIndividualPersonPresenter.TRANSFER_RESPONSE";
    private static final String TEMPLATE_MODE = "TransferIndividualPersonPresenter.TEMPLATE_MODE";

    private RequestManager mRequestManager;

    private Disposable mDisposable;

    private BaseProduct mSelectedProduct;

    private SelectBudgetOrgData mSelectedPayerStatus;
    private SelectBudgetOrgData mSelectedTaxType;
    private SelectBudgetOrgData mSelectedTransferReason;
    private SelectBudgetOrgData mSelectedDocumentType;
    private SelectBudgetOrgData mSelectedNalogPeriod;

    private Date mSelectedDate;

    private PublishSubject<Boolean> subject = PublishSubject.create();
    private String amount;
    private TransferResponse transferResponse;
    private boolean templateMode;
    private String customerInn;
    private String receiverName;
    private String receiverInn;
    private String receiverKpp;
    private String receiverAccount;
    private String receiverBic;
    private String transferTarget;
    private String uin;
    private String cbc;
    private String oktm;
    private String documentDate;
    private String documentNumber;
    private String custCode;
    private String taxPeriodParam2;
    private String taxPeriodParam3;
    private String passportDeal;

    private Double productLimit;

    @Inject
    public TransferBudgetOrganizationPresenter(RequestManager requestManager) {
        this.mRequestManager = requestManager;
        mDisposable = Disposables.empty();
    }

    @Override
    public void setListObservableField(List<Observable<Boolean>> observables) {
        observables.add(subject);
        Disposable validationDisposable = Observable.combineLatest(observables, objects -> {
            boolean state = true;
            for (Object object : objects) {
                state &= (Boolean) object;
            }
            return state;
        }).subscribe(s -> getMvpView().fieldValidationSuccess(s));
        addDisposable(validationDisposable);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            getMvpView().initView(templateMode);
            setProductData(mSelectedProduct);
        }
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            mSelectedProduct = (BaseProduct) params[0];
            templateMode = (boolean) params[1];
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(SAVE_SELECTED_PRODUCT, mSelectedProduct);
        outState.putParcelable(SAVE_SELECTED_PAYER_STATUS, mSelectedPayerStatus);
        outState.putParcelable(SAVE_SELECTED_TRANSFER_STATUS, mSelectedTaxType);
        outState.putParcelable(SAVE_SELECTED_TRANSFER_REASON, mSelectedTransferReason);
        outState.putParcelable(SAVE_SELECTED_DOCUMENT_TYPE, mSelectedDocumentType);
        outState.putParcelable(SAVE_SELECTED_NALOG_PERIOD, mSelectedNalogPeriod);
        outState.putString(AMOUNT, amount);
        outState.putParcelable(TRANSFER_RESPONSE, transferResponse);
        outState.putBoolean(TEMPLATE_MODE, templateMode);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        mSelectedProduct = savedInstanceState.getParcelable(SAVE_SELECTED_PRODUCT);
        mSelectedPayerStatus = savedInstanceState.getParcelable(SAVE_SELECTED_PAYER_STATUS);
        mSelectedTaxType = savedInstanceState.getParcelable(SAVE_SELECTED_TRANSFER_STATUS);
        mSelectedTransferReason = savedInstanceState.getParcelable(SAVE_SELECTED_TRANSFER_REASON);
        mSelectedDocumentType = savedInstanceState.getParcelable(SAVE_SELECTED_DOCUMENT_TYPE);
        mSelectedNalogPeriod = savedInstanceState.getParcelable(SAVE_SELECTED_NALOG_PERIOD);
        amount = savedInstanceState.getString(AMOUNT);
        transferResponse = savedInstanceState.getParcelable(TRANSFER_RESPONSE);
        templateMode = savedInstanceState.getBoolean(TEMPLATE_MODE);
    }

    @Override
    public void setSelectedProduct(BaseProduct baseProduct) {
        setProductData(baseProduct);
    }

    private void setProductData(BaseProduct baseProduct) {
        this.mSelectedProduct = baseProduct;
        subject.onNext(mSelectedProduct != null);
        if (mSelectedProduct != null && mSelectedProduct instanceof Card) {
            getCardLimits(mSelectedProduct);
        }
        getMvpView().initProductData(mSelectedProduct);
    }

    private void getCardLimits(BaseProduct sourceProduct) {
        mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
            if (isUnAuthorize) {
                getMvpView().unAuthorized();
            } else {
                productLimit = null;
                mDisposable = mRequestManager.getCardLimits(sessionId, sourceProduct.getId(),
                        new BaseRequestController(mEventBusController, Action.GET_CARD_LIMITS_ACTION, getMvpView().getClassUniqueDeviceId()));
            }
        });
    }

    @Override
    public void setSelectedBudgetOrgData(SelectBudgetOrgData selectedBudgetOrgData) {
        switch (selectedBudgetOrgData.getType()) {
            case SelectBudgetOrgData.DOCUMENT_TYPE:
                mSelectedDocumentType = selectedBudgetOrgData;
                getMvpView().setDocumentType(mSelectedDocumentType.getValue());
                break;
            case SelectBudgetOrgData.TRANSFER_TYPE:
                mSelectedTaxType = selectedBudgetOrgData;
                getMvpView().setTransferType(mSelectedTaxType.getValue());
                break;
            case SelectBudgetOrgData.NALOG_PERIOD:
                mSelectedNalogPeriod = selectedBudgetOrgData;
                getMvpView().setNalogPeriod(mSelectedNalogPeriod.getValue());
                break;
            case SelectBudgetOrgData.PAYER_STATUS:
                mSelectedPayerStatus = selectedBudgetOrgData;
                getMvpView().setPayerStatus(mSelectedPayerStatus.getValue());
                break;
            case SelectBudgetOrgData.TRANSFER_REASON:
                mSelectedTransferReason = selectedBudgetOrgData;
                getMvpView().setTransferReason(mSelectedTransferReason.getValue());
                break;
        }
    }

    @Override
    public void setDocumentDate(Date date) {
        this.mSelectedDate = date;
        getMvpView().showDate(Utils.formatSelectedDate(mSelectedDate));
    }

    private boolean validateFields(String accountNumberRecipient, String bic) {
        boolean accNumberValid = Utils.accountNumberValidation(bic, accountNumberRecipient);
        getMvpView().showAccountNumberError(!accNumberValid);
        return accNumberValid;
    }

    @Override
    public void onNextClicked(String amount, String customerInn, String receiverName, String receiverInn, String receiverKpp, String receiverAccount,
                              String receiverBic, String transferTarget, String uin, String cbc, String oktm, String documentDate, String documentNumber,
                              String custCode, String taxPeriodParam2, String taxPeriodParam3, String passportDeal) {
        this.amount = amount;
        this.customerInn = customerInn;
        this.receiverName = receiverName;
        this.receiverInn = receiverInn;
        this.receiverKpp = receiverKpp;
        this.receiverAccount = receiverAccount;
        this.receiverBic = receiverBic;
        this.transferTarget = transferTarget;
        this.uin = uin;
        this.cbc = cbc;
        this.oktm = oktm;
        this.documentDate = documentDate;
        this.documentNumber = documentNumber;
        this.custCode = custCode;
        this.taxPeriodParam2 = taxPeriodParam2;
        this.taxPeriodParam3 = taxPeriodParam3;
        this.passportDeal = passportDeal;

        int validationValue = validateAmount(amount);
        if (validationValue == -1) {
            checkTransferRequest(amount, customerInn, receiverName, receiverInn, receiverKpp, receiverAccount, receiverBic, transferTarget, uin, cbc, oktm,
                    documentDate, documentNumber, custCode, taxPeriodParam2, taxPeriodParam3, passportDeal);
        } else {
            getMvpView().showNotValidAmountMessage(validationValue);
        }
    }

    private int validateAmount(String amountStr) {
        Float amount = -1f;
        try {
            amount = Float.valueOf(amountStr);
        } catch (NumberFormatException e) {
            Log.e(TAG, "validateAmount: ", e);
        }
        if (productLimit != null && productLimit < amount) {
            return R.string.payment_restriction_exceeded;
        }
        if (amount <= 0) {
            return R.string.amount_error_message;
        }
        return -1;
    }

    private void checkTransferRequest(String amount, String customerInn, String receiverName, String receiverInn, String receiverKpp, String receiverAccount,
                                      String receiverBic, String transferTarget, String uin, String cbc, String oktm, String documentDate, String documentNumber,
                                      String custCode, String taxPeriodParam2, String taxPeriodParam3, String passportDeal) {
        if (validateFields(receiverAccount, receiverBic)) {
            undisposable(mDisposable);
            mDataManager.getSessionId((isUnAuthorize, sessionId) -> {
                if (isUnAuthorize) {
                    getMvpView().unAuthorized();
                } else {
                    Integer sourceType = -1;
                    String sourceId = mSelectedProduct.getId();
                    Integer targetType = RequestConstants.ADDITION_PARAMETERS;
                    Map<String, String> additionParams = RequestParams.getAdditionalParamsBudget(customerInn, receiverName, receiverInn, receiverKpp, receiverAccount,
                            receiverBic, transferTarget, uin, mSelectedPayerStatus.getKey(), mSelectedTaxType.getKey(), mSelectedTransferReason.getKey(),
                            cbc, oktm, documentDate, mSelectedDocumentType.getKey(), documentNumber, custCode,
                            mSelectedNalogPeriod.getKey(), taxPeriodParam2, taxPeriodParam3, passportDeal, PARAM_TYPE, PARAM_ID);
                    String targetId = null;
                    if (mSelectedProduct instanceof Card) {
                        sourceType = RequestConstants.SOURCE_CLIENTS_CARD;
                    } else if (mSelectedProduct instanceof Account) {
                        sourceType = RequestConstants.SOURCE_CLIENTS_ACCOUNT;
                    }
                    mDisposable = mRequestManager.transferCheck(
                            sessionId,
                            RequestParams.getConfirmationStrategiesParams(ConfirmRequestDescriptor.TRANSFER, null, amount, mSelectedProduct.getCurrency(), null, sourceType, targetType, -1),
                            mSelectedProduct.getCurrency(), amount, sourceType, sourceId, targetType, targetId, null, additionParams, mDataManager.getKeyPair(mDataManager.getLogin()).getPrivate(),
                            new BaseRequestController(mEventBusController, Action.CHECK_TRANSFER, getMvpView().getClassUniqueDeviceId()));
                    addDisposable(mDisposable);
                }
            });
        }
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case START_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                    case Action.GET_CARD_LIMITS_ACTION:
                        getMvpView().startProgressDialog();
                        break;
                }
                break;
            case SUCCESS_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                        undisposable(mDisposable);
                        Response<TransferResponse> responseResponse = (Response<TransferResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), responseResponse, this);
                        break;
                    case Action.GET_CARD_LIMITS_ACTION:
                        undisposable(mDisposable);
                        Response<CardLimitResponse> baseResponse = (Response<CardLimitResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), baseResponse, this);
                        break;
                }
                break;
            case FAIL_REQUEST:
                switch (event.getActionCode()) {
                    case Action.CHECK_TRANSFER:
                        undisposable(mDisposable);
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        break;
                    case Action.GET_CARD_LIMITS_ACTION:
                        undisposable(mDisposable);
                        getMvpView().errorProgressDialog(((EventFailRequest) event).getThrowable());
                        setProductData(null);
                        break;
                }
                break;
        }
    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Pair data) {
        switch (actionCode) {
            case Action.CHECK_TRANSFER:
                transferResponse = (TransferResponse) data.getValue();
                CheckTransferData checkTransferData = new CheckTransferData();
                checkTransferData.setAmount(amount);
                checkTransferData.setSourceProduct(mSelectedProduct);
                checkTransferData.setTransferDescription("В бюджетную организацию");
                checkTransferData.setTransferResponse(transferResponse);
                checkTransferData.setType(CheckTransferData.BUDGET_TRANSFER);
                Map<String, String> additionParams = RequestParams.getAdditionalParamsBudget(customerInn, receiverName, receiverInn, receiverKpp, receiverAccount,
                        receiverBic, transferTarget, uin, mSelectedPayerStatus.getKey(), mSelectedTaxType.getKey(), mSelectedTransferReason.getKey(),
                        cbc, oktm, documentDate, mSelectedDocumentType.getKey(), documentNumber, custCode,
                        mSelectedNalogPeriod.getKey(), taxPeriodParam2, taxPeriodParam3, passportDeal, PARAM_TYPE, PARAM_ID);
                checkTransferData.setAdditionParams(additionParams);
                getMvpView().openTransferCheckScreen(checkTransferData, templateMode);
                break;
            case Action.GET_CARD_LIMITS_ACTION:
                getMvpView().completeProgressDialog();
                CardLimitResponse cardLimitResponse = (CardLimitResponse) data.getValue();
                if (cardLimitResponse != null) {
                    ArrayList<CardLimit> cardLimits = cardLimitResponse.getLimitList();
                    productLimit = null;
                    for (CardLimit cardLimit :
                            cardLimits) {
                        double localLimit = cardLimit.getAmount() - cardLimit.getUsedAmount();
                        if (productLimit == null || productLimit > localLimit) {
                            productLimit = localLimit;
                        }
                        if (productLimit <= 0) {
                            setProductData(null);
                            getMvpView().showCardLimitDialog();
                            return;
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                getMvpView().completeProgressDialog();
                getMvpView().unAuthorized();
                break;
            case BAD_REQUEST_ERROR:
            case DEFAULT_ERROR:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
            default:
                getMvpView().errorProgressDialog(mDataManager.getErrorMessage(errorCode));
                break;
        }
        switch (actionCode) {
            case Action.GET_CARD_LIMITS_ACTION:
                setProductData(null);
                break;
        }
    }
}
