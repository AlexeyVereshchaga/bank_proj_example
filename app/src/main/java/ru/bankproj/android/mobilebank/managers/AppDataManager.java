package ru.bankproj.android.mobilebank.managers;

import java.io.IOException;
import java.security.KeyPair;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.dataclasses.MenuData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SelectBudgetOrgData;
import ru.bankproj.android.mobilebank.store.StoreHelper;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.Utils;

/**
 * Created by j7ars on 25.10.2017.
 */
@Singleton
public class AppDataManager {

    private StoreHelper mStoreHelper;

    @Inject
    public AppDataManager(StoreHelper storeHelper) {
        this.mStoreHelper = storeHelper;
    }

    public String getDevKey(String alias) throws IOException {
        return mStoreHelper.getDevKey(Utils.formatNumberForSavingKeystore(alias));
    }

    public KeyPair getKeyPair(String alias) {
        return mStoreHelper.getKeyPair(Utils.formatNumberForSavingKeystore(alias));
    }

    public KeyPair generateKeyPair(String alias) {
        return mStoreHelper.generateKeyPair(Utils.formatNumberForSavingKeystore(alias));
    }

    public void deleteKeyPair(String alias) {
        mStoreHelper.deleteKeyPair(Utils.formatNumberForSavingKeystore(alias));
    }

    public ArrayList<MenuData> getMenuList(String sessionId) {
        ArrayList<MenuData> data = new ArrayList<>();
        data.add(new MenuData(MenuData.HEADER_TYPE, null, null, MenuData.HEADER_TYPE));
        if(sessionId == null || sessionId.isEmpty()){
            data.add(new MenuData(MenuData.MAIN, FieldConverter.getString(R.string.drawer_menu_main), FieldConverter.getString(R.string.drawer_menu_main), MenuData.ITEM_TYPE));
            data.add(new MenuData(MenuData.CONTACTS, FieldConverter.getString(R.string.drawer_menu_contacts), FieldConverter.getString(R.string.drawer_menu_contacts), MenuData.ITEM_TYPE));
        } else{
            data.add(new MenuData(MenuData.MY_PRODUCT, FieldConverter.getString(R.string.drawer_menu_my_product), FieldConverter.getString(R.string.drawer_menu_my_product), MenuData.ITEM_TYPE));
            data.add(new MenuData(MenuData.PAYMENT, FieldConverter.getString(R.string.drawer_menu_payment), FieldConverter.getString(R.string.drawer_menu_payment), MenuData.ITEM_TYPE));
            data.add(new MenuData(MenuData.BANK_PRODUCT, FieldConverter.getString(R.string.drawer_menu_bank_product), FieldConverter.getString(R.string.drawer_menu_bank_product), MenuData.ITEM_TYPE));
            data.add(new MenuData(MenuData.NOTIFICATION, FieldConverter.getString(R.string.drawer_menu_notification), FieldConverter.getString(R.string.drawer_menu_notification), MenuData.ITEM_TYPE));
            data.add(new MenuData(MenuData.CONTACTS, FieldConverter.getString(R.string.drawer_menu_contacts), FieldConverter.getString(R.string.drawer_menu_contacts), MenuData.ITEM_TYPE));
            data.add(new MenuData(MenuData.SEPARATOR_TYPE, null, null, MenuData.SEPARATOR_TYPE));
            data.add(new MenuData(MenuData.SETTINGS, FieldConverter.getString(R.string.drawer_menu_settings), FieldConverter.getString(R.string.drawer_menu_settings), MenuData.ITEM_TYPE));
            data.add(new MenuData(MenuData.LOGOUT, FieldConverter.getString(R.string.drawer_menu_logout), FieldConverter.getString(R.string.drawer_menu_logout), MenuData.ITEM_TYPE));
        }

        return data;
    }

    public ArrayList<SelectBudgetOrgData> getDocumentType() {
        ArrayList<SelectBudgetOrgData> data = new ArrayList<>();
        String [] key = FieldConverter.getStringArray(R.array.document_type_key);
        String [] value = FieldConverter.getStringArray(R.array.document_type_value);
        for(int i = 0; i < key.length ; i ++ ){
            data.add(new SelectBudgetOrgData(key[i], value[i]));
        }
        return data;
    }

    public ArrayList<SelectBudgetOrgData> getTransferType() {
        ArrayList<SelectBudgetOrgData> data = new ArrayList<>();
        String [] key = FieldConverter.getStringArray(R.array.transfer_type_key);
        String [] value = FieldConverter.getStringArray(R.array.transfer_type_value);
        for(int i = 0; i < key.length ; i ++ ){
            data.add(new SelectBudgetOrgData(key[i], value[i]));
        }
        return data;
    }

    public ArrayList<SelectBudgetOrgData> getNalogPeriod() {
        ArrayList<SelectBudgetOrgData> data = new ArrayList<>();
        String [] key = FieldConverter.getStringArray(R.array.nalog_period_key);
        String [] value = FieldConverter.getStringArray(R.array.nalog_period_value);
        for(int i = 0; i < key.length ; i ++ ){
            data.add(new SelectBudgetOrgData(key[i], value[i]));
        }
        return data;
    }

    public ArrayList<SelectBudgetOrgData> getReasonTransfer() {
        ArrayList<SelectBudgetOrgData> data = new ArrayList<>();
        String [] key = FieldConverter.getStringArray(R.array.reason_transfer_key);
        String [] value = FieldConverter.getStringArray(R.array.reason_transfer_value);
        for(int i = 0; i < key.length ; i ++ ){
            data.add(new SelectBudgetOrgData(key[i], value[i]));
        }
        return data;
    }

    public ArrayList<SelectBudgetOrgData> getPayerStatus() {
        ArrayList<SelectBudgetOrgData> data = new ArrayList<>();
        String [] key = FieldConverter.getStringArray(R.array.status_transfer_key);
        String [] value = FieldConverter.getStringArray(R.array.status_transfer_value);
        for(int i = 0; i < key.length ; i ++ ){
            data.add(new SelectBudgetOrgData(key[i], value[i]));
        }
        return data;
    }

}
