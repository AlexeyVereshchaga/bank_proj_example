package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.providers.OutputData;

/**
 * Created by Alexey Vereshchaga on 23.12.17.
 */

public class TransferResponse extends BaseResponse {
    @SerializedName("transRef")
    private String transRef;
    @SerializedName("planDate")
    private String planDate;
    @SerializedName("fee")
    private float fee;
    @SerializedName("currency")
    private String currency;
    @SerializedName("confirmReq")
    private ConfirmRequestDescriptor confirmReq;
    @SerializedName("details")
    private ArrayList<OutputData> detailList;
    private String confirmationStrategy;

    public String getTransRef() {
        return transRef;
    }

    public void setTransRef(String transRef) {
        this.transRef = transRef;
    }

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public float getFee() {
        return fee;
    }

    public void setFee(float fee) {
        this.fee = fee;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ConfirmRequestDescriptor getConfirmReq() {
        return confirmReq;
    }

    public void setConfirmReq(ConfirmRequestDescriptor confirmReq) {
        this.confirmReq = confirmReq;
    }

    public ArrayList<OutputData> getDetailList() {
        return detailList;
    }

    public void setDetailList(ArrayList<OutputData> detailList) {
        this.detailList = detailList;
    }

    public String getConfirmationStrategy() {
        return confirmationStrategy;
    }

    public void setConfirmationStrategy(String confirmationStrategy) {
        this.confirmationStrategy = confirmationStrategy;
    }

    public TransferResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.transRef);
        dest.writeString(this.planDate);
        dest.writeFloat(this.fee);
        dest.writeString(this.currency);
        dest.writeParcelable(this.confirmReq, flags);
        dest.writeTypedList(this.detailList);
        dest.writeString(this.confirmationStrategy);
    }

    protected TransferResponse(Parcel in) {
        super(in);
        this.transRef = in.readString();
        this.planDate = in.readString();
        this.fee = in.readFloat();
        this.currency = in.readString();
        this.confirmReq = in.readParcelable(ConfirmRequestDescriptor.class.getClassLoader());
        this.detailList = in.createTypedArrayList(OutputData.CREATOR);
        this.confirmationStrategy = in.readString();
    }

    public static final Creator<TransferResponse> CREATOR = new Creator<TransferResponse>() {
        @Override
        public TransferResponse createFromParcel(Parcel source) {
            return new TransferResponse(source);
        }

        @Override
        public TransferResponse[] newArray(int size) {
            return new TransferResponse[size];
        }
    };
}
