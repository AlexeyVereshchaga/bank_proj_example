package ru.bankproj.android.mobilebank.main.transfer.template;

import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;
import ru.bankproj.android.mobilebank.dataclasses.templates.Template;
import ru.bankproj.android.mobilebank.rest.response.TransferResponse;

/**
 * Created by Alexey Vereshchaga on 16.01.18.
 */

public interface ITransferTemplateContract {

    interface View extends IBaseProgressView {

        void showTemplate(Template sourceProduct);

        void openTransferCheckScreen(Template template, TransferResponse transferResponse, String amount);

        void showEmptySourceCardDialog();

        void showBlockedSourceCardDialog();

        void showExpiredSourceCardDialog();

        void showTemporaryBlockedCardDialog();

        void showTemplateLimitSourceCardDialog();

        void showTargetProductBlockedDialog();

        void showTargetAccountTemporaryBlockedDialog();

        void showTargetAccountBlockedDialog();

        void showBlockedTargetCardDialog();

        void showTemporaryBlockedTargetCardDialog();

        void showNotValidAmountMessage(int validationValue);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void onNextClicked(String amount);
    }
}
