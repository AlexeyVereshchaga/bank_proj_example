package ru.bankproj.android.mobilebank.main.transfer.budget.organization.selectdata.view;

import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.activity.BaseBackContainerActivity;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SelectBudgetOrgData;
import ru.bankproj.android.mobilebank.utils.FieldConverter;

/**
 * Created by j7ars on 28.12.2017.
 */

public class SelectBudgetOrgDataActivity extends BaseBackContainerActivity {

    private static final String SAVE_SELECT_TYPE = "SelectBudgetOrgDataActivity.SAVE_SELECT_TYPE";

    public static final String SELECT_TYPE = "SelectBudgetOrgDataActivity.SELECT_TYPE";

    private int mType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        visibleCancel();
        getExtras();
        setScreenTitle();
        if(savedInstanceState == null) {
            openFragment();
        } else{
            onRestoreInstanceState(savedInstanceState);
        }
    }

    private void setScreenTitle(){
        switch (mType){
            case SelectBudgetOrgData.DOCUMENT_TYPE:
                setTitle(FieldConverter.getString(R.string.transfer_type_document_screen_title));
                break;
            case SelectBudgetOrgData.TRANSFER_TYPE:
                setTitle(FieldConverter.getString(R.string.transfer_type_transfer_screen_title));
                break;
            case SelectBudgetOrgData.NALOG_PERIOD:
                setTitle(FieldConverter.getString(R.string.transfer_nalog_period_screen_title));
                break;
            case SelectBudgetOrgData.PAYER_STATUS:
                setTitle(FieldConverter.getString(R.string.transfer_payer_status_screen_title));
                break;
            case SelectBudgetOrgData.TRANSFER_REASON:
                setTitle(FieldConverter.getString(R.string.transfer_transfer_reason_screen_title));
                break;
        }
    }

    private void getExtras(){
        if(getIntent().getExtras() != null){
            mType = getIntent().getExtras().getInt(SELECT_TYPE);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt(SAVE_SELECT_TYPE, mType);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mType = savedInstanceState.getInt(SAVE_SELECT_TYPE);
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, getScreenCreator().newInstance(SelectBudgetOrgDataFragment.class, getBundle())).commitAllowingStateLoss();
    }

    private Bundle getBundle(){
        Bundle bundle = new Bundle();
        bundle.putInt(SelectBudgetOrgDataFragment.SELECT_TYPE, mType);
        return bundle;
    }

}
