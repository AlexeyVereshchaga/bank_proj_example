package ru.bankproj.android.mobilebank.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.App;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by j7ars on 25.10.2017.
 */

public class UI {

    public static void animationOpenActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    public static void animationCloseActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
    }

    public static void openActivityWithoutAnimation(Activity activity) {
        activity.overridePendingTransition(0, 0);
    }

    public static void startRotate180LeftDownAnimation(ImageView iv) {
        Animation a = AnimationUtils.loadAnimation(App.mInstance, R.anim.rotate_180_deg_left_down);
        iv.startAnimation(a);
    }

    public static void startRotate180LeftUpAnimation(ImageView iv) {
        Animation a = AnimationUtils.loadAnimation(App.mInstance, R.anim.rotate_180_deg_left_up);
        iv.startAnimation(a);
    }

    public static void crossFade(ViewGroup showLayout, ViewGroup hideLayout) {
        hideLayout.animate()
                .alpha(0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        hideLayout.setVisibility(GONE);
                        showLayout.setAlpha(0f);
                        showLayout.setVisibility(VISIBLE);
                        showLayout.animate()
                                .alpha(1f)
                                .setDuration(300)
                                .setListener(null);
                    }
                });
    }

    interface IProductViewCallback{
        void showBalanceDialog();
    }


}
