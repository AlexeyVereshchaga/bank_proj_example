package ru.bankproj.android.mobilebank.view.pagescrollview;

import ru.bankproj.android.mobilebank.view.pagescrollview.helper.Helper;
import ru.bankproj.android.mobilebank.view.pagescrollview.helper.HorizontalHelper;
import ru.bankproj.android.mobilebank.view.pagescrollview.helper.VerticalHelper;

/**
 * Created by j7ars on 25.10.2017.
 */

public enum Orientation {

    HORIZONTAL {
        @Override
        Helper createHelper() {
            return new HorizontalHelper();
        }
    },
    VERTICAL {
        @Override
        Helper createHelper() {
            return new VerticalHelper();
        }
    };

    abstract Helper createHelper();



}
