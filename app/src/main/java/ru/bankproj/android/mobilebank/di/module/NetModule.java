package ru.bankproj.android.mobilebank.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.net.ssl.SSLSocketFactory;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.bankproj.android.mobilebank.BuildConfig;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.dataclasses.providers.OutputData;
import ru.bankproj.android.mobilebank.rest.typeadapter.OutputDataTypeAdapter;
import ru.bankproj.android.mobilebank.rest.SSLHelper;
import ru.bankproj.android.mobilebank.rest.Urls;
import ru.bankproj.android.mobilebank.rest.error.ErrorHandlingCallAdapterFactory;
import ru.bankproj.android.mobilebank.utils.interceptor.LogInterceptor;

/**
 * Created by j7ars on 25.10.2017.
 */
@Module
public class NetModule {

    @Provides
    @Singleton
    @Named("main")
    OkHttpClient provideMainOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        LogInterceptor netLogging = new LogInterceptor();
        netLogging.setLevel(LogInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        //ssl
        SSLSocketFactory socketFactory = SSLHelper.getSocketFactory();

        if (socketFactory != null) {
            builder.sslSocketFactory(socketFactory, SSLHelper.getTrustManager());
            builder.hostnameVerifier(SSLHelper.BM_HOST_VERIFIER);
        }

        builder.connectTimeout(Constants.CONNECT_TIMEOUT, TimeUnit.SECONDS);
        builder.writeTimeout(Constants.WRITE_TIMEOUT, TimeUnit.SECONDS);
        builder.readTimeout(Constants.TIMEOUT, TimeUnit.SECONDS);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                request = request.newBuilder()
                        .addHeader("Content-Type", "text/plain")
                        .addHeader("Spec-Revision", "1408")
                        .build();
                Response response = chain.proceed(request);
                return response;
            }
        });
//        builder.addNetworkInterceptor(logging);
        if (BuildConfig.DEBUG_) {
            builder.addNetworkInterceptor(netLogging);
        }
        return builder.build();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .registerTypeAdapter(OutputData.class, new OutputDataTypeAdapter())
                .enableComplexMapKeySerialization()
                .serializeNulls()
                .setPrettyPrinting()
                .setVersion(1.0)
                .create();
    }

    @Provides
    @Singleton
    @Named("main")
    Retrofit provideMainRetrofit(@Named("main") OkHttpClient okHttpClient, Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(ErrorHandlingCallAdapterFactory.create())
                .baseUrl(Urls.BASE_BPC_API)
                .client(okHttpClient)
                .build();
        return retrofit;
    }


}
