package ru.bankproj.android.mobilebank.view.paymentfields;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.dataclasses.providers.PaymentParameter;
import ru.bankproj.android.mobilebank.view.BaseSelectionView;
import ru.bankproj.android.mobilebank.view.paymentfields.base.BaseField;

/**
 * Created by rrust on 03.01.2018.
 */

public class SelectDateField extends BaseField{

    private BaseSelectionView mRootView;

    private PaymentParameter mPaymentParameter;
    private String mValue;

    public SelectDateField(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        return mRootView;
    }

    @Override
    protected void initView() {
        mRootView = (BaseSelectionView) LayoutInflater.from(mContext).inflate(R.layout.view_base_selection_view, null, false);
        setListeners();
    }

    private void initConfigField(){
        if(mPaymentParameter != null){
            mRootView.setVisibility(View.VISIBLE);
            if(!TextUtils.isEmpty(mPaymentParameter.getName())){
                mRootView.setCustomText(mPaymentParameter.getName());
            }
        }
        else{
            mRootView.setVisibility(View.GONE);
        }
    }

    private void setListeners(){
        mRootView.setOnSelectionViewCallback(new BaseSelectionView.OnSelectionViewCallback() {
            @Override
            public void onSelectClicked() {

            }

            @Override
            public void onClearSelectClicked() {
            }
        });

        mRootView.setOnSelectionViewCallback(new BaseSelectionView.OnSelectionViewCallback() {
            @Override
            public void onSelectClicked() {

            }

            @Override
            public void onClearSelectClicked() {
            }
        });
    }

    @Override
    public void setValue(String value) {
        mValue = value;
    }

    @Override
    public String getValue() {
        return mValue;
    }

    @Override
    public void enableView(boolean isEnable) {
        mRootView.setEnabled(isEnable);
    }

    @Override
    public Observable<Boolean> getObservable() {
        return null;
    }

    @Override
    public void setPaymentParameter(PaymentParameter paymentParameter) {
        mPaymentParameter = paymentParameter;
        initConfigField();
    }

    @Override
    public String getPaymentParameterId() {
        return mPaymentParameter != null ? mPaymentParameter.getId() : "";
    }
}
