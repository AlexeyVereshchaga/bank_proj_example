package ru.bankproj.android.mobilebank.main.transfer.legal.person;

import java.util.List;

import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.base.mvp.IBaseMvpPresenter;
import ru.bankproj.android.mobilebank.base.mvp.IBaseProgressView;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;

/**
 * Created by maxmobiles on 27.12.2017.
 */

public interface ITransferLegalPersonContract {

    interface View extends IBaseProgressView {

        void initView(boolean templateMode);

        void initProductData(BaseProduct baseProduct);

        void openTransferCheckScreen(CheckTransferData checkTransferData, boolean templateMode);

        void showAccountNumberError(boolean templateMode);

        void showCardLimitDialog();

        void fieldValidationSuccess(Boolean success);

        void showNotValidAmountMessage(int validationValue);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void setListObservableField(List<Observable<Boolean>> observables);

        void setSelectedProduct(BaseProduct baseProduct);

        void onNextClicked(String amount, String recipientName, String inn, String accountNumberRecipient, String kpp, String bik,
                           String passportDeal, String transferTarget);
    }
}
