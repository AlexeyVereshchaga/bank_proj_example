package ru.bankproj.android.mobilebank.view.pagescrollview.transform;

import android.view.View;

/**
 * Created by j7ars on 25.10.2017.
 */

public interface PageScrollItemTransformer {
    void transformItem(View item, float position);
}
