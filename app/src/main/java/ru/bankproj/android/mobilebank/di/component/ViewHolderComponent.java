package ru.bankproj.android.mobilebank.di.component;

import dagger.Subcomponent;
import ru.bankproj.android.mobilebank.di.module.PresenterModule;
import ru.bankproj.android.mobilebank.di.module.ViewHolderModule;
import ru.bankproj.android.mobilebank.di.scope.PerViewHolder;

/**
 * Created by j7ars on 25.10.2017.
 */
@PerViewHolder
@Subcomponent(modules = {ViewHolderModule.class, PresenterModule.class})
public interface ViewHolderComponent {
}
