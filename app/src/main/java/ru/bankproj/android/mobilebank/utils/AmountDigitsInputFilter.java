package ru.bankproj.android.mobilebank.utils;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * AmountDigitsInputFilter.java 30.01.2018
 * Class description
 *
 * @author Alex.
 */

public class AmountDigitsInputFilter implements InputFilter {
    private Pattern mPattern;

    public AmountDigitsInputFilter() {
        mPattern = Pattern.compile("(([1-9][0-9]*)|([0]))((\\.[0-9]{1,2})|(\\.)?)");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        String stringToMatch = dest.toString() + source.toString();
        Matcher matcher = mPattern.matcher(stringToMatch);
        if (!matcher.matches()) {
            return "";
        }
        if (stringToMatch.equals("0.00")) {
            return "";
        }
        return null;
    }
}