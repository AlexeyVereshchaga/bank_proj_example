package ru.bankproj.android.mobilebank.di.component;

import dagger.Subcomponent;
import ru.bankproj.android.mobilebank.di.module.ActivityModule;
import ru.bankproj.android.mobilebank.di.module.FragmentModule;
import ru.bankproj.android.mobilebank.di.module.ServiceModule;
import ru.bankproj.android.mobilebank.di.module.ViewHolderModule;
import ru.bankproj.android.mobilebank.di.module.ViewModule;
import ru.bankproj.android.mobilebank.di.scope.ConfigPersistent;

/**
 * Created by j7ars on 25.10.2017.
 */
@ConfigPersistent
@Subcomponent
public interface ConfigPersistentComponent {

    ActivityComponent activityComponent(ActivityModule activityModule);

    FragmentComponent fragmentComponent(FragmentModule fragmentModule);

    ServiceComponent serviceComponent(ServiceModule serviceModule);

    ViewHolderComponent viewHolderComponent(ViewHolderModule viewHolderModule);

    ViewComponent viewComponent(ViewModule viewModule);

}
