package ru.bankproj.android.mobilebank.rest.service;

import java.util.LinkedHashMap;
import java.util.Map;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.rest.RequestField;
import ru.bankproj.android.mobilebank.rest.Urls;
import ru.bankproj.android.mobilebank.rest.response.ContactsResponse;
import ru.bankproj.android.mobilebank.rest.response.RequisitesResponse;
import ru.bankproj.android.mobilebank.rest.response.TemplateCreateResponse;
import ru.bankproj.android.mobilebank.rest.response.TemplateSaveResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferExecResponse;
import ru.bankproj.android.mobilebank.rest.response.TransferResponse;

/**
 * Created by Alexey Vereshchaga on 23.12.17.
 */

public interface FunctionalService {

    @FormUrlEncoded
    @POST(Urls.GET_TRANSFER_CHECK)
    Single<Response<TransferResponse>> transferCheck(@Header(RequestField.JSESSION) String sessionId,
                                                     @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST(Urls.GET_TRANSFER_EXEC)
    Single<Response<TransferExecResponse>> transferExec(@Header(RequestField.JSESSION) String sessionId,
                                                        @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST(Urls.CREATE_TEMPLATE)
    Single<Response<TemplateCreateResponse>> createTemplate(@Header(RequestField.JSESSION) String sessionId,
                                                            @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST(Urls.SAVE_TEMPLATE)
    Single<Response<TemplateSaveResponse>> saveTemplate(@Header(RequestField.JSESSION) String sessionId,
                                                        @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST(Urls.RENAME_TEMPLATE)
    Single<Response<BaseResponse>> renameTemplate(@Header(RequestField.JSESSION) String sessionId,
                                                  @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST(Urls.DELETE_TEMPLATE)
    Single<Response<BaseResponse>> deleteTemplate(@Header(RequestField.JSESSION) String sessionId,
                                                  @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST(Urls.REPAYMENT_CHECK)
    Single<Response<TransferResponse>> repaymentCheck(@Header(RequestField.JSESSION) String sessionId,
                                                      @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST(Urls.REPAYMENT_EXEC)
    Single<Response<TransferExecResponse>> repaymentExec(@Header(RequestField.JSESSION) String sessionId,
                                                         @FieldMap(encoded = true) Map<String, String> params);

    @GET(Urls.GET_CONTACTS_INFORMATION)
    Single<Response<ContactsResponse>> getContactsInformation(@QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.CARD_REQUISITES)
    Single<Response<RequisitesResponse>> getCardRequisites(@Header(RequestField.JSESSION) String sessionId,
                                                           @Query(RequestField.ID) String cardId);

    @FormUrlEncoded
    @POST(Urls.DEVICE_INFORMATION)
    Single<Response<BaseResponse>> postDeviceInformation(@Header(RequestField.JSESSION) String sessionId,
                                                         @FieldMap(encoded = true) Map<String, String> params);
}