package ru.bankproj.android.mobilebank.main.transfer.own.account.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.base.fragment.BaseMvpFragment;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.BaseProduct;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.transfer.CheckTransferData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.FilterProductData;
import ru.bankproj.android.mobilebank.di.component.FragmentComponent;
import ru.bankproj.android.mobilebank.main.block.dialogs.BalanceDialog;
import ru.bankproj.android.mobilebank.main.dialogs.InfoDialog;
import ru.bankproj.android.mobilebank.main.payment.selectcardoraccount.view.SelectCardOrAccountActivity;
import ru.bankproj.android.mobilebank.main.transfer.check.view.TransferCheckActivity;
import ru.bankproj.android.mobilebank.main.transfer.own.account.ITransferOwnAccountContract;
import ru.bankproj.android.mobilebank.utils.AmountDigitsInputFilter;
import ru.bankproj.android.mobilebank.utils.FieldConverter;
import ru.bankproj.android.mobilebank.utils.Utils;
import ru.bankproj.android.mobilebank.utils.storage.AppStorage;
import ru.bankproj.android.mobilebank.view.EnableButton;
import ru.bankproj.android.mobilebank.view.ErrorLabelContainer;
import ru.bankproj.android.mobilebank.view.progress.ProgressView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Alexey Vereshchaga on 21.12.17.
 */

public class TransferOwnAccountFragment extends BaseMvpFragment implements ITransferOwnAccountContract.View {

    public static final String SOURCE_PRODUCT = "TransferOwnAccountFragment.SOURCE_PRODUCT";
    public static final String DESCRIPTION_TRANSFER = "TransferOwnAccountFragment.TRANSFER_DESCRIPTION";
    public static final String TARGET_PRODUCT = "TransferOwnAccountFragment.TARGET_PRODUCT";
    public static final String TEMPLATE_MODE = "TransferOwnAccountFragment.TEMPLATE_MODE";

    @BindView(R.id.transfer_source)
    View transferSource;
    @BindView(R.id.transfer_target)
    View transferTarget;
    @BindView(R.id.source_card)
    View sourceCardInclude;
    @BindView(R.id.target_card)
    View targetProductInclude;
    @BindView(R.id.et_limit_assignment)
    EditText etAmount;
    @BindView(R.id.btn_transfer_next_step)
    EnableButton btnNext;
    //Template views
    @BindView(R.id.ll_template_name_container)
    LinearLayout llTemplateNameContainer;
    @BindView(R.id.el_template_name)
    ErrorLabelContainer elTemplateName;
    @BindView(R.id.et_template_name)
    EditText etTemplateName;

    private IncludedCardView sourceView;
    private IncludedCardView targetView;

    @BindViews({R.id.transfer_source, R.id.source_card})
    List<View> sourceViews;
    @BindViews({R.id.transfer_target, R.id.target_card})
    List<View> targetViews;

    static final ButterKnife.Setter<View, Boolean> SHOW_PRODUCT = (view, value, index) -> {
        switch (view.getId()) {
            case R.id.source_card:
            case R.id.target_card:
                view.setVisibility(value ? View.VISIBLE : View.GONE);
                break;
            case R.id.transfer_source:
            case R.id.transfer_target:
                view.setVisibility(value ? View.GONE : View.VISIBLE);
                break;
        }
    };

    static class IncludedCardView {
        @BindView(R.id.tv_card_name)
        TextView tvCardName;
        @BindView(R.id.tv_card_number)
        TextView tvCardNumber;
        @BindView(R.id.tv_card_amount)
        TextView tvCardAmount;
        @BindView(R.id.tv_card_currency)
        TextView tvCardCurrency;
        @BindView(R.id.iv_card_icon)
        ImageView ivCardIcon;
        @BindView(R.id.tv_card_balance)
        TextView tvCardBalance;
        @BindView(R.id.iv_card_balance)
        ImageView ivCardBalance;
        @BindView(R.id.ll_amount_container)
        LinearLayout llAmountContainer;
        @BindView(R.id.ll_amount_balance)
        LinearLayout llAmountBalance;
    }

    @Inject
    ITransferOwnAccountContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs() {
        if (getArguments() != null) {
            presenter.setArguments(getArguments().getParcelable(SOURCE_PRODUCT),
                    getArguments().getString(DESCRIPTION_TRANSFER),
                    getArguments().getParcelable(TARGET_PRODUCT),
                    getArguments().getBoolean(TEMPLATE_MODE));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bindIncluded();
        initView();
        presenter.onCreate(savedInstanceState);
        Utils.hideKeyboard(mActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    private void initView() {
        btnNext.enable(true);
        etAmount.setFilters(new InputFilter[]{new AmountDigitsInputFilter()});
    }

    private void bindIncluded() {
        sourceView = new IncludedCardView();
        targetView = new IncludedCardView();
        ButterKnife.bind(sourceView, sourceCardInclude);
        ButterKnife.bind(targetView, targetProductInclude);
        ((TextView) ((RelativeLayout) transferTarget).getChildAt(0)).setText(FieldConverter.getString(R.string.debit_select_card_to_title));
        initIncluded(sourceView);
        initIncluded(targetView);
    }

    private void initIncluded(IncludedCardView includedCardView) {
        SpannableString sb1 = new SpannableString(FieldConverter.getString(R.string.card_balance));
        sb1.setSpan(new UnderlineSpan(), 0, FieldConverter.getString(R.string.card_balance).length(), 0);
        includedCardView.tvCardBalance.setText(sb1);
        if (AppStorage.getBooleanValue(Constants.IS_HIDE_BALANCE, mActivity)) {
            includedCardView.llAmountBalance.setVisibility(VISIBLE);
            includedCardView.llAmountContainer.setVisibility(GONE);
            includedCardView.tvCardBalance.setOnClickListener(view
                    -> crossFade(includedCardView.llAmountContainer, includedCardView.llAmountBalance));
            includedCardView.llAmountContainer.setOnClickListener(view
                    -> crossFade(includedCardView.llAmountBalance, includedCardView.llAmountContainer));
            includedCardView.ivCardBalance.setOnClickListener(view -> showBalanceDialog());
        } else {
            includedCardView.llAmountBalance.setVisibility(GONE);
            includedCardView.llAmountContainer.setVisibility(VISIBLE);
        }
    }

    private void crossFade(LinearLayout showLayout, LinearLayout hideLayout) {
        hideLayout.animate()
                .alpha(0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        hideLayout.setVisibility(View.GONE);
                        showLayout.setAlpha(0f);
                        showLayout.setVisibility(View.VISIBLE);
                        showLayout.animate()
                                .alpha(1f)
                                .setDuration(300)
                                .setListener(null);
                    }
                });
    }

    public void showBalanceDialog() {
        BalanceDialog dialogFragment = BalanceDialog.newInstance();
        dialogFragment.setOnResultListener(new BalanceDialog.OnDialogResultListener() {
            @Override
            public void onResultDialog(int statusCode) {
                if (statusCode == Activity.RESULT_OK) {
                    dialogFragment.dismiss();
                } else {
                    dialogFragment.dismiss();
                }
            }
        });
        dialogFragment.show(getActivity().getSupportFragmentManager(), BalanceDialog.TAG);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        presenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        presenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return mProgressView;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_transfer_own_account;
    }

    @OnClick(R.id.fl_source)
    void onSourceClick() {
        presenter.onSourceClicked();
    }

    @OnClick(R.id.fl_target)
    void onTargetClick() {
        presenter.onTargetClicked();
    }

    @Override
    public void openSelectSourceCardScreen(FilterProductData data) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SelectCardOrAccountActivity.FILTER_PRODUCTS, data);
        getScreenCreator().startActivity(this, mActivity, SelectCardOrAccountActivity.class, bundle);
    }

    @Override
    public void enableNextButton(boolean enable) {
        btnNext.enable(enable);
    }

    @Override
    public void initTemplateMode(boolean templateMode) {
        List<Observable<Boolean>> listObservable = new ArrayList<>();
        if (templateMode) {
            listObservable.add(Utils.getRegexObservable(etTemplateName, "^.{1,300}$", getString(R.string.transfer_invalid_value)));
        }
        presenter.setListObservableField(listObservable);
        llTemplateNameContainer.setVisibility(templateMode ? VISIBLE : GONE);
    }

    @Override
    public void showCardLimitDialog() {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(R.string.limit_transfer_card_dialog_text));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @Override
    public void showNotValidAmountMessage(int validationValue) {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(validationValue));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @Override
    public void fieldValidationSuccess(Boolean success) {
        btnNext.enable(success);
    }

    @Override
    public void openSelectTargetCardScreen(FilterProductData data) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SelectCardOrAccountActivity.FILTER_PRODUCTS, data);
        getScreenCreator().startActivity(this, mActivity, SelectCardOrAccountActivity.class, bundle);
    }

    @Override
    public void moveToTop() {
        Utils.moveToTopCurrent(mActivity);
    }

    @Override
    public void showSourceProduct(BaseProduct sourceProduct) {
        presenter.clearTargetProduct();
        boolean showProduct = sourceProduct != null;
        ButterKnife.apply(sourceViews, SHOW_PRODUCT, showProduct);
        if (showProduct) {
            initProduct(sourceView, sourceProduct);
        }
    }

    @Override
    public void showTargetProduct(BaseProduct targetProduct) {
        boolean showProduct = targetProduct != null;
        ButterKnife.apply(targetViews, SHOW_PRODUCT, showProduct);
        if (showProduct) {
            initProduct(targetView, targetProduct);
        }
    }

    @Override
    public void showDialogBySourceTargetType(BaseProduct sourceProduct, BaseProduct targetProduct) {
        if (sourceProduct != null
                && sourceProduct instanceof Card
                && targetProduct instanceof Account
                && ((Account) targetProduct).getType() == Account.TYPE_DEBIT) {
            showCardDebitDialog();
        }
        if (sourceProduct != null
                && sourceProduct instanceof Account
                && ((Account) sourceProduct).getType() == Account.TYPE_DEBIT
                && targetProduct instanceof Card) {
            showDebitCardDialog();
        }
    }

    public void showCardDebitDialog() {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(R.string.transfer_card_debit_dialog_text));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    public void showDebitCardDialog() {
        InfoDialog dialogFragment = InfoDialog.newInstance(getString(R.string.transfer_debit_card_dialog_text));
        dialogFragment.show(getActivity().getSupportFragmentManager(), InfoDialog.TAG);
    }

    @Override
    public void openTransferCheckScreen(CheckTransferData checkTransferData, boolean templateMode) {
        if (templateMode) {
            checkTransferData.setNewTemplateName(etTemplateName.getText().toString());
        }
        getScreenCreator().startActivity(this, mActivity, TransferCheckActivity.class,
                createTransferCheckBundle(checkTransferData, templateMode), Constants.START_TRANSFER_CHECK_ACTIVITY);
    }

    @NonNull
    private Bundle createTransferCheckBundle(CheckTransferData checkTransferData, boolean templateMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferCheckActivity.CHECK_TRANSFER_DATA, checkTransferData);
        bundle.putBoolean(TransferCheckActivity.TEMPLATE_MODE, templateMode);
        return bundle;
    }

    private void initProduct(IncludedCardView includedCardView, BaseProduct baseProduct) {
        String name = "";
        if (baseProduct instanceof Card) {
            name = ((Card) baseProduct).getCardName();
        } else if (baseProduct instanceof Account) {
            name = ((Account) baseProduct).getAccountName();
        }
        includedCardView.tvCardName.setText(name);
        includedCardView.tvCardNumber.setText(Utils.getMaskedAccountNumber(baseProduct.getNumber()));
        includedCardView.tvCardAmount.setText(Utils.formatMoneyValue(baseProduct.getBalance()));
        includedCardView.tvCardCurrency.setText(FieldConverter.getCurrency(baseProduct.getCurrency()));
        if (baseProduct instanceof Card) {
            includedCardView.ivCardIcon.setImageResource(((Card) baseProduct).getCardBrand());
        } else {
            includedCardView.ivCardIcon.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.btn_transfer_next_step)
    void onNextClick() {
        presenter.onNextClicked(etAmount.getText().toString());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.START_TRANSFER_CHECK_ACTIVITY:
                if (resultCode == Constants.RESULT_LOGOUT) {
                    unAuthorized();
                }
                break;
        }
    }
}
