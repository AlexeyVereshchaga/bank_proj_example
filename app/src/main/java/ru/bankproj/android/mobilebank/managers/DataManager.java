package ru.bankproj.android.mobilebank.managers;

import android.location.Location;
import android.text.TextUtils;

import java.io.IOException;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.app.session.ISessionValueCallback;
import ru.bankproj.android.mobilebank.base.callback.IDataCallback;
import ru.bankproj.android.mobilebank.base.event.Pair;
import ru.bankproj.android.mobilebank.dataclasses.ErrorData;
import ru.bankproj.android.mobilebank.dataclasses.MenuData;
import ru.bankproj.android.mobilebank.dataclasses.atm.ATM;
import ru.bankproj.android.mobilebank.dataclasses.atm.AtmFilterData;
import ru.bankproj.android.mobilebank.dataclasses.block.Block;
import ru.bankproj.android.mobilebank.dataclasses.news.News;
import ru.bankproj.android.mobilebank.dataclasses.offers.Offer;
import ru.bankproj.android.mobilebank.dataclasses.product.Account;
import ru.bankproj.android.mobilebank.dataclasses.product.Card;
import ru.bankproj.android.mobilebank.dataclasses.providers.Category;
import ru.bankproj.android.mobilebank.dataclasses.providers.Provider;
import ru.bankproj.android.mobilebank.dataclasses.providers.SupportedCurrencies;
import ru.bankproj.android.mobilebank.dataclasses.providers.SupportedFunctions;
import ru.bankproj.android.mobilebank.dataclasses.providers.SupportedRegions;
import ru.bankproj.android.mobilebank.dataclasses.templates.Template;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SelectBudgetOrgData;
import ru.bankproj.android.mobilebank.dataclasses.transfer.SourceType;

import static ru.bankproj.android.mobilebank.dataclasses.atm.ATM.BRANCH_TYPE;

/**
 * Created by j7ars on 25.10.2017.
 */
@Singleton
public class DataManager {

    private ErrorMessageManager mErrorMessageManager;
    private AppDataManager mAppDataManager;
    private DatabaseManager mDatabaseManager;
    private DataSourceManager mDataSourceManager;

    private String mSessionId;

    @Inject
    public DataManager(ErrorMessageManager errorMessageManager, AppDataManager appDataManager, DatabaseManager databaseManager, DataSourceManager dataSourceManager) {
        this.mErrorMessageManager = errorMessageManager;
        this.mAppDataManager = appDataManager;
        this.mDatabaseManager = databaseManager;
        this.mDataSourceManager = dataSourceManager;
    }

    public void getSessionId(ISessionValueCallback sessionValueCallback) {
        if (mSessionId == null) {
            if (sessionValueCallback != null) {
                sessionValueCallback.sessionValue(true, mSessionId);
            }
        } else {
            if (sessionValueCallback != null) {
                sessionValueCallback.sessionValue(false, mSessionId);
            }
        }
    }

    public void setSessionId(String mSessionId) {
        this.mSessionId = mSessionId;
    }

    public String getLogin() {
        return mDataSourceManager.getLogin();
    }

    public void setLogin(String login) {
        mDataSourceManager.setLogin(login);
    }

    public void setCurrentLocation(Location location) {
        mDataSourceManager.setCurrentLocation(location);
    }

    public Location getCurrentLocation() {
        return mDataSourceManager.getCurrentLocation();
    }

    public void logout() {
        mSessionId = null;
        mDataSourceManager.logout();
    }

    public String getDevKey(String alias) throws IOException {
        return mAppDataManager.getDevKey(alias);
    }

    public KeyPair getKeyPair(String alias) {
        return mAppDataManager.getKeyPair(alias);
    }

    public KeyPair generateKeyPair(String alias) {
        return mAppDataManager.generateKeyPair(alias);
    }

    public void deleteKeyPair(String alias) {
        mAppDataManager.deleteKeyPair(alias);
    }

    public ErrorData getErrorMessage(int errorCode) {
        return mErrorMessageManager.getErrorMessage(errorCode);
    }

    public ArrayList<MenuData> getMenuList() {
        return mAppDataManager.getMenuList(mSessionId);
    }

    public ArrayList<SelectBudgetOrgData> getDocumentType() {
        return mAppDataManager.getDocumentType();
    }

    public ArrayList<SelectBudgetOrgData> getTransferType() {
        return mAppDataManager.getTransferType();
    }

    public ArrayList<SelectBudgetOrgData> getNalogPeriod() {
        return mAppDataManager.getNalogPeriod();
    }

    public ArrayList<SelectBudgetOrgData> getReasonTransfer() {
        return mAppDataManager.getReasonTransfer();
    }

    public ArrayList<SelectBudgetOrgData> getPayerStatus() {
        return mAppDataManager.getPayerStatus();
    }

    //Block
    public Single<List<Block>> getAllBlock() {
        return mDatabaseManager.getAllBlock();
    }

    public Single<List<Block>> getBlockListByCategoryId(int categoryId) {
        return mDatabaseManager.getBlockListByCategoryId(categoryId);
    }

    public Single<List<Block>> getBlockListByCategoryIdAndByStatus(int categoryId, boolean isOn) {
        return mDatabaseManager.getBlockListByCategoryIdAndByStatus(categoryId, isOn);
    }

    public Single<List<Block>> getUnAuthorizedBlockList() {
        return mDatabaseManager.getUnAuthorizedBlockList();
    }

    public Completable insertOrUpdateBlock(Block block) {
        return mDatabaseManager.insertOrUpdateBlock(block);
    }

    public Completable initDefaultBlock() {
        return mDatabaseManager.initDefaultBlock();
    }

    //categories provider
    public Single<List<Category>> getAllCategories() {
        return mDatabaseManager.getAllCategories();
    }

    //providers
    public Single<List<Provider>> getAllProviders() {
        return mDatabaseManager.getAllProviders();
    }

    //search categories
    public Single<List<Provider>> getProvidersByQuery(String query) {
        return mDatabaseManager.getProvidersByQuery(query);
    }

    //search provider by id
    public Single<Provider> getProvidersById(String pid) {
        return mDatabaseManager.getProvidersById(pid);
    }

    //search category;
    public Single<List<Category>> getCategoriesByQuery(String query) {
        return mDatabaseManager.getCategoriesByQuery(query);
    }

//    //payment parameter
//    public Single<List<PaymentParameter>> getPaymentParameterRecords() {
//        return mDatabaseManager.getAllPaymentParameterRecords();
//    }

    public Single<List<Provider>> getAllProvidersByCategory(String cid) {
        return mDatabaseManager.getAllProvidersByCategory(cid);
    }

    //support regions
    public Single<List<SupportedRegions>> getSupportRegions() {
        return mDatabaseManager.getAllSupportedRegions();
    }

    //support currencies
    public Single<List<SupportedCurrencies>> getSupportCurrencies() {
        return mDatabaseManager.getAllSupportedCurrencies();
    }

    //support functions
    public Single<SupportedFunctions> getSupportFunctions() {
        return mDatabaseManager.getSupportedFunctions();
    }

//    public Single<List<PaymentParameter>> getPaymentParameterRecordsByProvider(String pid) {
//        return mDatabaseManager.getAllPaymentParameterRecordsByProvider(pid);
//    }

    public Observable getTimerSplash() {
        return Observable.timer(Constants.SPLASH_TIMEOUT, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public void getNewsData(boolean isRefreshData, IDataCallback dataCallback) {
        if (dataCallback != null) {
            if (!isRefreshData) {
                if (mDataSourceManager.getNewsList() == null || mDataSourceManager.getNewsList().isEmpty()) {
                    dataCallback.makeRequest();
                } else {
                    dataCallback.setData(new Pair(mDataSourceManager.getNewsList()));
                }
            } else {
                dataCallback.makeRequest();
            }
        }
    }

    public ArrayList<News> getNewsList() {
        return mDataSourceManager.getNewsList();
    }

    public void getOffersData(boolean isRefreshData, IDataCallback dataCallback) {
        if (dataCallback != null) {
            if (!isRefreshData) {
                if (mDataSourceManager.getOfferList() == null || mDataSourceManager.getOfferList().isEmpty()) {
                    dataCallback.makeRequest();
                } else {
                    dataCallback.setData(new Pair(mDataSourceManager.getOfferList()));
                }
            } else {
                dataCallback.makeRequest();
            }
        }
    }

    public ArrayList<Offer> getOfferList() {
        return mDataSourceManager.getOfferList();
    }

    public void getAtmData(boolean isRefreshData, IDataCallback dataCallback) {
        if (dataCallback != null) {
            if (!isRefreshData) {
                if (mDataSourceManager.getAtmList() == null || mDataSourceManager.getAtmList().isEmpty()) {
                    dataCallback.makeRequest();
                } else {
                    dataCallback.setData(new Pair(mDataSourceManager.getAtmList()));
                }
            } else {
                dataCallback.makeRequest();
            }
        }
    }

    public ArrayList<ATM> getAtmList() {
        return mDataSourceManager.getAtmList();
    }

    public ATM getNearestATM(ArrayList<ATM> atmList) {
        if (atmList != null && atmList.size() != 0) {
            for (ATM atm : atmList) {
                if (atm.getItemType() != ATM.TYPE_HEADER) {
                    if (atm.getType() == BRANCH_TYPE) {
                        return atm;
                    }
                }
            }
        }
        return null;
    }

    public void getCardData(boolean isRefreshData, IDataCallback dataCallback) {
        if (dataCallback != null) {
            if (!isRefreshData) {
                if (mDataSourceManager.getCardList() == null || mDataSourceManager.getCardList().isEmpty()) {
                    dataCallback.makeRequest();
                } else {
                    dataCallback.setData(new Pair(mDataSourceManager.getCardList()));
                }
            } else {
                dataCallback.makeRequest();
            }
        }
    }

    public ArrayList<Template> getTemplateList() {
        return mDataSourceManager.getTemplateList();
    }

    public void getTemplateData(boolean isRefreshData, IDataCallback dataCallback) {
        if (dataCallback != null) {
            if (!isRefreshData) {
                if (mDataSourceManager.getTemplateList() == null || mDataSourceManager.getTemplateList().isEmpty()) {
                    dataCallback.makeRequest();
                } else {
                    dataCallback.setData(new Pair(mDataSourceManager.getTemplateList()));
                }
            } else {
                dataCallback.makeRequest();
            }
        }
    }

    public void resetTemplateList(ArrayList<Template> templateList) {
        mDataSourceManager.resetTemplateList(templateList);
    }

    public void updateTemplateList(int position, Template template) {
        mDataSourceManager.updateTemplateList(position, template);
    }

    public void removeTemplate(String uid) {
        mDataSourceManager.removeTemplateByUid(uid);
    }

    public Template getTemplateWithSourceProduct(Template template) {
        if (template != null) {
            if (template.getSourceType() == SourceType.CARD.getId()) {
                template.setSourceProduct(getCardById(template.getSourceId()));
            } else {
                template.setSourceProduct(getAccountById(template.getSourceId()));
            }
        }
        return template;
    }

    public Template getTemplateWithTargetProduct(Template template) {
        if (template != null && !TextUtils.isEmpty(template.getTargetId())) {
            if (template.getTargetType() == SourceType.CARD.getId()) {
                template.setTargetProduct(getCardById(template.getTargetId()));
            } else {
                template.setTargetProduct(getAccountById(template.getTargetId()));
            }
        }
        return template;
    }

    public ArrayList<Card> getCardList() {
        return mDataSourceManager.getCardList();
    }

    public ArrayList<Card> getCardListByStatus(int status) {
        return mDataSourceManager.getCardListByStatus(status);
    }

    public ArrayList<Card> getCardListByType(int type) {
        return mDataSourceManager.getCardListByType(type);
    }

    public void updateCardList(int position, Card card) {
        mDataSourceManager.updateCardList(position, card);
    }

    public void resetCardList(ArrayList<Card> cardList) {
        mDataSourceManager.resetCardList(cardList);
    }

    public Card getCardById(String id) {
        return mDataSourceManager.getCardById(id);
    }

    public void getAccountData(boolean isRefreshData, IDataCallback dataCallback) {
        if (dataCallback != null) {
            if (!isRefreshData) {
                if (mDataSourceManager.getAccountList() == null || mDataSourceManager.getAccountList().isEmpty()) {
                    dataCallback.makeRequest();
                } else {
                    dataCallback.setData(new Pair(mDataSourceManager.getAccountList()));
                }
            } else {
                dataCallback.makeRequest();
            }
        }
    }

    public ArrayList<Account> getAccountList() {
        return mDataSourceManager.getAccountList();
    }

    public ArrayList<Account> getAccountListByStatus(int status) {
        return mDataSourceManager.getAccountListByStatus(status);
    }

    public ArrayList<Account> getDebitAccountListByStatus(int status) {
        return mDataSourceManager.getDebitAccountListByStatus(status);
    }

    public ArrayList<Account> getCreditAccountListByStatus(int status) {
        return mDataSourceManager.getCreditAccountListByStatus(status);
    }

    public ArrayList<Account> getAccountListByType(int type) {
        return mDataSourceManager.getAccountListByType(type);
    }

    public void updateAccountList(int position, Account account) {
        mDataSourceManager.updateAccountList(position, account);
    }

    public void resetAccountList(ArrayList<Account> accountList) {
        mDataSourceManager.resetAccountList(accountList);
    }

    public Account getAccountById(String id) {
        return mDataSourceManager.getAccountById(id);
    }


    public void getCreditData(boolean isRefreshData, IDataCallback dataCallback) {
        if (dataCallback != null) {
            if (!isRefreshData) {
                if (mDataSourceManager.getCreditList() == null || mDataSourceManager.getCreditList().isEmpty()) {
                    dataCallback.makeRequest();
                } else {
                    dataCallback.setData(new Pair(mDataSourceManager.getCreditList()));
                }
            } else {
                dataCallback.makeRequest();
            }
        }
    }

    public ArrayList<Account> getCreditList() {
        return mDataSourceManager.getCreditList();
    }

    public void updateCreditList(int position, Account account) {
        mDataSourceManager.updateCreditList(position, account);
    }

    public void resetCreditList(ArrayList<Account> accountList) {
        mDataSourceManager.resetCreditList(accountList);
    }

    public void getDebitData(boolean isRefreshData, IDataCallback dataCallback) {
        if (dataCallback != null) {
            if (!isRefreshData) {
                if (mDataSourceManager.getDebitList() == null || mDataSourceManager.getDebitList().isEmpty()) {
                    dataCallback.makeRequest();
                } else {
                    dataCallback.setData(new Pair(mDataSourceManager.getDebitList()));
                }
            } else {
                dataCallback.makeRequest();
            }
        }
    }

//    public void getPaymentScheduleData(boolean isRefreshData, IDataCallback dataCallback) {
//        if (dataCallback != null) {
//            if(!isRefreshData) {
//                if (mDataSourceManager.() == null || mDataSourceManager.getAtmList().isEmpty()) {
//                    dataCallback.makeRequest();
//                } else {
//                    dataCallback.setData(new Pair(mDataSourceManager.getAtmList()));
//                }
//            } else{
//                dataCallback.makeRequest();
//            }
//        }
//    }

    public ArrayList<Account> getDebitList() {
        return mDataSourceManager.getDebitList();
    }

    public void updateDebitList(int position, Account account) {
        mDataSourceManager.updateDebitList(position, account);
    }

    public void resetDebitList(ArrayList<Account> accountList) {
        mDataSourceManager.resetDebitList(accountList);
    }

    public AtmFilterData getAtmFilterData() {
        return mDataSourceManager.generateFilterData();
    }

    public ArrayList<ATM> addAtmHeader(ArrayList<ATM> filteredAtmList) {
        return mDataSourceManager.handlingATM(filteredAtmList);
    }
}
