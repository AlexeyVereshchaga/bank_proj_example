
package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.paymentscheduler.CreditPaymentSchedule;

public class CreditPaymentScheduleResponse extends BaseResponse{

    @SerializedName("page")
    private Integer page;
    @SerializedName("creditpaymentschedule")
    private List<CreditPaymentSchedule> creditPaymentSchedule;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<CreditPaymentSchedule> getCreditPaymentSchedule() {
        return creditPaymentSchedule;
    }

    public void setCreditPaymentSchedule(List<CreditPaymentSchedule> creditPaymentSchedule) {
        this.creditPaymentSchedule = creditPaymentSchedule;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.page);
        dest.writeTypedList(this.creditPaymentSchedule);
    }

    public CreditPaymentScheduleResponse() {
    }

    protected CreditPaymentScheduleResponse(Parcel in) {
        super(in);
        this.page = (Integer) in.readValue(Integer.class.getClassLoader());
        this.creditPaymentSchedule = in.createTypedArrayList(CreditPaymentSchedule.CREATOR);
    }

    public static final Creator<CreditPaymentScheduleResponse> CREATOR = new Creator<CreditPaymentScheduleResponse>() {
        @Override
        public CreditPaymentScheduleResponse createFromParcel(Parcel source) {
            return new CreditPaymentScheduleResponse(source);
        }

        @Override
        public CreditPaymentScheduleResponse[] newArray(int size) {
            return new CreditPaymentScheduleResponse[size];
        }
    };
}
