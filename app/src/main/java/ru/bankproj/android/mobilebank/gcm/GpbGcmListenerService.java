package ru.bankproj.android.mobilebank.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.app.Constants;
import ru.bankproj.android.mobilebank.utils.storage.AppStorage;

/**
 * Created by Alexey Vereshchaga on 28.02.18.
 */

public class GpbGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    public static final String CHANNEL_ID = "CHANNEL_ID";
    public static final String BODY = "body";
    public static final String SOUND = "sound";
    public static final String TYPE = "type";
    public static final String PID = "pid";
    public static final String DISCOUNT_EXPIRY_DAYS = "discountExpiryDays";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Boolean pushNotificationsEnabled = AppStorage.getBoolean(Constants.PUSH_ENABLED, getApplicationContext());
        if (pushNotificationsEnabled == null || pushNotificationsEnabled) {
            String message = data.getString(BODY);
            String sound = data.getString(SOUND);
            String dataType = data.getString(TYPE);
            String pid = data.getString(PID);
            String discountExpiryDays = data.getString(DISCOUNT_EXPIRY_DAYS);

            Log.d(TAG, "From: " + from);
            Log.d(TAG, "Message: " + message);

//            if (from.startsWith("/topics/")) {
//                // message received from some topic.
//            } else {
//                // normal downstream message.
//            }

            // [START_EXCLUDE]
            /**
             * Production applications would usually process the message here.
             * Eg: - Syncing with server.
             *     - Store message in local database.
             *     - Update UI.
             */

            /**
             * In some cases it may be useful to show a notification indicating to the user
             * that a message was received.
             */
            sendNotification(message);
            // [END_EXCLUDE]
        }
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {
        Intent intent = new Intent(); //this, SplashScreenActivity.class
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_logo)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
