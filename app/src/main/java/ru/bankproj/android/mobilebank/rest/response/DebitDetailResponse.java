package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.product.DebitAccountDetail;

/**
 * Created by j7ars on 15.12.2017.
 */

public class DebitDetailResponse extends BaseResponse {

    @SerializedName("account")
    private DebitAccountDetail debitAccountDetail;

    public DebitAccountDetail getDebitAccountDetail() {
        return debitAccountDetail;
    }

    public void setDebitAccountDetail(DebitAccountDetail debitAccountDetail) {
        this.debitAccountDetail = debitAccountDetail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(this.debitAccountDetail, flags);
    }

    public DebitDetailResponse() {
    }

    protected DebitDetailResponse(Parcel in) {
        super(in);
        this.debitAccountDetail = in.readParcelable(DebitAccountDetail.class.getClassLoader());
    }

    public static final Creator<DebitDetailResponse> CREATOR = new Creator<DebitDetailResponse>() {
        @Override
        public DebitDetailResponse createFromParcel(Parcel source) {
            return new DebitDetailResponse(source);
        }

        @Override
        public DebitDetailResponse[] newArray(int size) {
            return new DebitDetailResponse[size];
        }
    };
}
