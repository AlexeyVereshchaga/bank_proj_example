package ru.bankproj.android.mobilebank.main.transfer.template.view;

import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.bankproj.android.mobilebank.R;
import ru.bankproj.android.mobilebank.base.activity.BaseBackContainerActivity;
import ru.bankproj.android.mobilebank.dataclasses.templates.Template;

/**
 * Created by Alexey Vereshchaga on 16.01.18.
 */

public class TransferTemplateActivity extends BaseBackContainerActivity{

    public static final String TEMPLATE = "TransferTemplateActivity.TEMPLATE";

    private Template template;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getExtras();
        setTitle(template.getTypeNameByType());
        if(savedInstanceState == null){
            openFragment();
        }
    }

    private void getExtras(){
        if(getIntent().getExtras() != null){
            template = getIntent().getParcelableExtra(TEMPLATE);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(TEMPLATE, template);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        template = savedInstanceState.getParcelable(TEMPLATE);
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container, getScreenCreator().newInstance(TransferTemplateFragment.class, createBundle())).commitAllowingStateLoss();
    }

    private Bundle createBundle(){
        Bundle bundle = new Bundle();
        bundle.putParcelable(TransferTemplateFragment.TEMPLATE, template);
        return bundle;
    }
}
