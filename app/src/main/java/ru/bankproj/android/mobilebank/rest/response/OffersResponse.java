package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.offers.Offer;

public class OffersResponse extends BaseResponse{

    @SerializedName("offers")
    private ArrayList<Offer> offers;

    public ArrayList<Offer> getOffers() {
        return offers;
    }

    public void setOffers(ArrayList<Offer> offers) {
        this.offers = offers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.offers);
    }

    public OffersResponse() {
    }

    protected OffersResponse(Parcel in) {
        super(in);
        this.offers = in.createTypedArrayList(Offer.CREATOR);
    }

    public static final Creator<OffersResponse> CREATOR = new Creator<OffersResponse>() {
        @Override
        public OffersResponse createFromParcel(Parcel source) {
            return new OffersResponse(source);
        }

        @Override
        public OffersResponse[] newArray(int size) {
            return new OffersResponse[size];
        }
    };
}
