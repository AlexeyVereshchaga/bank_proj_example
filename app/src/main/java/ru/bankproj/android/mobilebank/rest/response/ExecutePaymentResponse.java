package ru.bankproj.android.mobilebank.rest.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bankproj.android.mobilebank.base.response.BaseResponse;
import ru.bankproj.android.mobilebank.dataclasses.operationhistory.OperationStatus;
import ru.bankproj.android.mobilebank.dataclasses.product.ConfirmRequestDescriptor;
import ru.bankproj.android.mobilebank.dataclasses.providers.OutputData;

/**
 * Created by arsen on 23.12.17.
 */

public class ExecutePaymentResponse extends BaseResponse {

    @SerializedName("transRef")
    @Expose
    private String transRef;
    @SerializedName("authid")
    @Expose
    private String authid;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("fee")
    @Expose
    private Double fee;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("confirmReq")
    @Expose
    private ConfirmRequestDescriptor confirmReq;
    @SerializedName("operationStatus")
    @Expose
    private OperationStatus operationStatus;
    @SerializedName("recentOpId")
    @Expose
    private String recentOpId;
    @SerializedName("details")
    @Expose
    private ArrayList<OutputData> details;

    public String getTransRef() {
        return transRef;
    }

    public void setTransRef(String transRef) {
        this.transRef = transRef;
    }

    public String getAuthid() {

        return authid;
    }

    public void setAuthid(String authid) {
        this.authid = authid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ConfirmRequestDescriptor getConfirmReq() {
        return confirmReq;
    }

    public void setConfirmReq(ConfirmRequestDescriptor confirmReq) {
        this.confirmReq = confirmReq;
    }

    public OperationStatus getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(OperationStatus operationStatus) {
        this.operationStatus = operationStatus;
    }

    public String getRecentOpId() {
        return recentOpId;
    }

    public void setRecentOpId(String recentOpId) {
        this.recentOpId = recentOpId;
    }

    public ArrayList<OutputData> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<OutputData> details) {
        this.details = details;
    }

    public ExecutePaymentResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.transRef);
        dest.writeString(this.authid);
        dest.writeValue(this.status);
        dest.writeValue(this.fee);
        dest.writeString(this.currency);
        dest.writeParcelable(this.confirmReq, flags);
        dest.writeParcelable(this.operationStatus, flags);
        dest.writeString(this.recentOpId);
        dest.writeTypedList(this.details);
    }

    protected ExecutePaymentResponse(Parcel in) {
        super(in);
        this.transRef = in.readString();
        this.authid = in.readString();
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.fee = (Double) in.readValue(Double.class.getClassLoader());
        this.currency = in.readString();
        this.confirmReq = in.readParcelable(ConfirmRequestDescriptor.class.getClassLoader());
        this.operationStatus = in.readParcelable(OperationStatus.class.getClassLoader());
        this.recentOpId = in.readString();
        this.details = in.createTypedArrayList(OutputData.CREATOR);
    }

    public static final Creator<ExecutePaymentResponse> CREATOR = new Creator<ExecutePaymentResponse>() {
        @Override
        public ExecutePaymentResponse createFromParcel(Parcel source) {
            return new ExecutePaymentResponse(source);
        }

        @Override
        public ExecutePaymentResponse[] newArray(int size) {
            return new ExecutePaymentResponse[size];
        }
    };
}
